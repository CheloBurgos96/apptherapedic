﻿using System;
using System.Windows.Forms;
using System.Text.Json;
using System.Drawing;

namespace NetSuiteApps
{
    public partial class LogInForm : Form
    {
        public class Login
        {
            public string employee_id { get; set; }

            public string password { get; set; }
        }

        QueryNS GetQueryFromNS = new QueryNS();
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "LogInForm";
        DataBase DB = new DataBase(false);

        public LogInForm()
        {
            InitializeComponent();
            this.CenterToScreen();
            //this.Hide();
            WaitPanel WaitData = new WaitPanel();
            WaitData.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //cLog.Add("", NameScreen, "Apertura de BD", false);
            
            cLog.Add("", NameScreen, "Búsqueda de ID User en BD", false);
            string TakeUserID = DB.CounterDB("id_user", "user_reg");
            if (TakeUserID.Equals("") || TakeUserID.Equals("0"))
            {
                UserName.Text = "Usuario..";
            }
            else
            {
                UserName.Text = TakeUserID;
            }
            cLog.Add("", NameScreen, "Búsqueda de WorkStation en BD", false);
            string DataWorkS = DB.CounterDB("id_station", "workstation_logs");
            if (DataWorkS.Equals("") || DataWorkS.Equals("0"))
            {
                cLog.Add("", NameScreen, "Búsqueda de WorkStation no encontrada en BD", false);
                this.Hide();
                WorkStation TargetForm = new WorkStation();
                TargetForm.ShowDialog();
                DataWorkS = DB.CounterDB("id_station", "workstation_logs");
            }
            else
            {
                cLog.Add("", NameScreen, "Búsqueda de WorkStation encontrada en BD", false);
            }
            cLog.Add("", NameScreen, "Asignación de texto en pantalla inicio de LogIn", false);
            Workstation.Text = DataWorkS;
            Workstation.Enabled = false;

        }


        private void button1_Click(object sender, EventArgs e)
        {

            if (UserName.Text != "" && Passw.Text != "")
            {
                cLog.Add("", NameScreen, "Inicio de sesión", false);

                LoginBTN.Enabled = false;
                var user = new Login()
                {
                    employee_id = UserName.Text,
                    password = Passw.Text
                };
                string jsonString = JsonSerializer.Serialize(user);
                var Login = GetQueryFromNS.Login(jsonString, UserName.Text, Passw.Text, Workstation.Text, NameScreen);
                if (!Login.Item1)
                {
                    if (!Login.Item2)
                    {
                        if (!Login.Item3)
                        {
                            this.Hide();
                            MainFrm AccF = new MainFrm();
                            AccF.SetEmployeeInfo(Login.Item4);
                            AccF.Show();
                        }
                        else
                        {
                            cLog.Add("199", NameScreen, "Error en los permisos de usuario", true);
                            MessageBox.Show(new Form() { TopMost = true }, "Error en los permisos de usuario");
                            LoginBTN.Enabled = true;
                        }
                        
                    }
                    else
                    {
                        cLog.Add("199", NameScreen, "Error de usuario y/o contraseña", true);
                        MessageBox.Show(new Form() { TopMost = true }, "Error, contraseña y/o usuario incorrectos");
                        LoginBTN.Enabled = true;
                    }
                }
                else
                {
                    cLog.Add("199", NameScreen, "Error de credenciales", true);
                    MessageBox.Show(new Form() { TopMost = true }, "Error de credenciales");
                    LoginBTN.Enabled = true;
                }
            }
            else
            {
                cLog.Add("206", NameScreen, "Usario no ingresó datos par iniciar sesión", true);
                MessageBox.Show(new Form() { TopMost = true }, "Ingrese usuario y/o contraseña");
                LoginBTN.Enabled = true;
            }
        }

        private void UserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void Passw_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void UserName_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en User Name", false);
            UserName.Clear();
            panel1.BackColor = Color.Yellow;
            UserName.ForeColor = Color.Yellow;

            Passw.ForeColor = Color.WhiteSmoke;
            panel2.BackColor = Color.WhiteSmoke;

            Workstation.ForeColor = Color.WhiteSmoke;
            panel3.BackColor = Color.WhiteSmoke;
        }

        private void Passw_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en User Name", false);
            Passw.Clear();
            panel2.BackColor = Color.Yellow;
            Passw.ForeColor = Color.Yellow;

            UserName.ForeColor = Color.WhiteSmoke;
            panel1.BackColor = Color.WhiteSmoke;


            Workstation.ForeColor = Color.WhiteSmoke;
            panel3.BackColor = Color.WhiteSmoke;
        }

        private void Workstation_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en User Name", false);
            Workstation.ForeColor = Color.Yellow;
            panel3.BackColor = Color.Yellow;

            Passw.ForeColor = Color.WhiteSmoke;
            panel2.BackColor = Color.WhiteSmoke;

            UserName.ForeColor = Color.WhiteSmoke;
            panel1.BackColor = Color.WhiteSmoke;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en salida de aplicación", false);
            Application.Exit();
        }

    }
}
