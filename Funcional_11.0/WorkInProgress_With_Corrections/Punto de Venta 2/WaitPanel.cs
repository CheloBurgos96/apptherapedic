﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class WaitPanel : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Wait Panel";
        delegate void OpenFrmWS();
        delegate void CloseFrmWait();
        QueryNS QueryData = new QueryNS();
        DataBase DB = new DataBase(true);



        public WaitPanel()
        {
            InitializeComponent();
            this.CenterToScreen();
            Thread n = new Thread(SQLiteBD);
            n.Start();
        }


        public void SQLiteBD()
        {
            cLog.Add("", NameScreen, "Inicializando BD para ver si existe", false);
            
            if (!DB.DBExist())
            {
                cLog.Add("", NameScreen, "BD no existe, y habilito la elección de estación", false);
                OpenFrmWS N = new OpenFrmWS(OpenWorkStation);
                this.Invoke(N);
            }
            else
            {
                cLog.Add("", NameScreen, "BD existe", false);
            }
            DateNS();
        }

        public void DateNS()
        {
            try
            {
                EmployeeModel Employ = new EmployeeModel();
                Employ.RoleId = "0";
                bool CatchData = false;
                string[] date = { };
                DateTime Date1 = DateTime.Now.Date;
                
                DateTime DateUpdate = DateTime.Now.AddDays(4);

                string[] result = Date1.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                string[] resultUpdate = DateUpdate.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                
                string TakeDate = DB.CounterDB("date_works", "days_works");
                if (TakeDate.Equals("") || TakeDate.Equals("0"))
                {
                    cLog.Add("", NameScreen, "Actualizar datos de etiquetas", false);
                    DB.InserDayWork(resultUpdate[0]);
                    //QueryNS();
                }
                else
                {
                    cLog.Add("", NameScreen, "Obtengo datos de la fecha de la BD", false);
                    date = TakeDate.Split('/');
                    CatchData = true;
                }


                if (CatchData)
                {
                    cLog.Add("", NameScreen, "Borrado de etiquetas con tiempo muy grande", false);
                    DateTime DataCompare1 = new DateTime(Convert.ToInt16(date[2]), Convert.ToInt16(date[1]), Convert.ToInt16(date[0]), 0, 0, 0);
                    int CompareDate = DateTime.Compare(Date1, DataCompare1);
                    if (CompareDate > 0)
                    {

                        DateTime FourDaysAgo = DataCompare1.AddDays(-4);

                        string[] result3 = FourDaysAgo.Date.ToShortDateString().Split(new char[] { ' ', '/' }, StringSplitOptions.RemoveEmptyEntries);

                        string DateComplete = result3[2] + result3[1] + result3[0];

                        DB.UpdateDateWork(resultUpdate[0]);
                        DB.DeleteAllLabelsWithDate(DateComplete);
                        //QueryNS();
                        //QueryData.LabelsToPrint(result2[0], result[0], false, Employ, "WaitPanel - Line 86");
                    }
                }
                string[] resultDate = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                CloseFrmWait n = new CloseFrmWait(CloseThisFrm);
                this.Invoke(n);
            }
            catch(Exception ex)
            {
                cLog.Add("Line 120", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
        }

        private void OpenWorkStation()
        {
            cLog.Add("", NameScreen, "Abriendo panel de estación de trabajo", false);
            WorkStation TargetForm = new WorkStation();
            TargetForm.ShowDialog();
        }

        private void CloseThisFrm()
        {
            this.Close();
        }

        private void QueryNS()
        {
            for (int i = 30; i >= 0; i--)
            {
                DateTime Date2 = DateTime.Now.AddDays(((-1) * i));
                string[] result2 = Date2.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //MessageBox.Show(result2[0]);
                QueryData.LabelsToPrint(result2[0], result2[0], false, null, "WaitPanel - Line 127");
            }
        }


        private void WaitPanel_Load(object sender, EventArgs e)
        {
            
        }


    }
}
