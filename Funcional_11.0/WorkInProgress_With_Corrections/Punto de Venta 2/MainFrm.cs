﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace NetSuiteApps
{
    public partial class MainFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Main Frm";
        public string User = "";
        public EmployeeModel Employee = new EmployeeModel();
        public WaitPanelSmall WP = new WaitPanelSmall();
        public TransferMPFrm TMP = new TransferMPFrm();
        public TransferPTFrm TPT = new TransferPTFrm();
        public TransferBin TBIN = new TransferBin();
        public ValidateJaula VJ = new ValidateJaula();
        public TranferJaula TJ = new TranferJaula();
        public Com_LogFrm ImpCo = new Com_LogFrm();
        public EmbolsadoFrm EsBForm = new EmbolsadoFrm();
        public TransferBinMP TbinMP = new TransferBinMP();
        public ReturnMPFrm RMP = new ReturnMPFrm();
        public PhysicallInventoryMP PIMP = new PhysicallInventoryMP();
        public PhysicallInventoryPT PIPT = new PhysicallInventoryPT();

        public int IDTransfer = 0;
        public bool[] visibleButton = new bool[14];

        public struct CursorPoint
        {
            public int X;
            public int Y;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        internal static extern bool SetProcessDPIAware();

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }


        public MainFrm()
        {
            InitializeComponent();
            this.CenterToScreen();
            string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            label7.Text = result[0];
        }

        public void SetEmployeeInfo(EmployeeModel pData) {
            cLog.Add("", NameScreen, "Captura de datos del Main", false);
            Employee = pData;
            lblEmpl.Text = Employee.Name;
            lblIDEmp.Text = Employee.RoleId;
            lblRol.Text = Employee.RoleName;
            visibleButton[0] = Employee.ButtonPrint;
            visibleButton[1] = Employee.ButtonEmbol;
            visibleButton[2] = Employee.ButtonOperation;
            visibleButton[3] = Employee.ButtonTMP;
            visibleButton[4] = Employee.ButtonTPT;
            visibleButton[5] = Employee.ButtonBin;
            visibleButton[6] = Employee.ButtonTJaula;
            visibleButton[7] = Employee.ButtonValidateJaula;
            visibleButton[8] = Employee.ButtonBinMP;
            visibleButton[9] = Employee.ButtonReturnMP;
            visibleButton[10] = Employee.ButtonPIMP;
            visibleButton[11] = Employee.ButtonPIPT;
            visibleButton[12] = Employee.ButtonLog;
            visibleButton[13] = Employee.ButtonError;
            cLog.Add("", NameScreen, "Configuración de posición de botones a realizar", false);
            ShowButton();
            cLog.Add("", NameScreen, "Configuración de posición de botones hecha", false);
        }

        private void ImprCod_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de impresión", false);
            this.Hide();
            SeriesPrintingFrm ImpCo = new SeriesPrintingFrm();
            ImpCo.Empl(Employee);
            ImpCo.Show();
        }

        private void EscEmb_Click_1(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de embolsado", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 7;
                BGWait.RunWorkerAsync();
            }
        }

        private void EscaCod_Click_1(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de escaneo de operaciones", false);
            this.Hide();
            OperationsRegFrm OperReg = new OperationsRegFrm();
            OperReg.Empl(Employee);
            OperReg.Show();
        }

        private void ShowButton()
        {
            SetProcessDPIAware();
            int position = FinalBase.Location.Y;
            int counter = position + 8;
            int[] positionY = new int[visibleButton.Length];
            for (int i = 0; i < visibleButton.Length; i++)
            {
                if (visibleButton[i] == true)
                {
                    positionY[i] = counter;
                    counter += 35;
                }
                else
                {
                    positionY[i] = counter;
                }
            }
            ImprCod.Visible = visibleButton[0];
            P1.Visible = visibleButton[0];
            ImprCod.Location = new Point(31, positionY[0]);
            P1.Location = new Point(24, positionY[0]);

            EscEmb.Visible = visibleButton[1];
            P2.Visible = visibleButton[1];
            EscEmb.Location = new Point(31, positionY[1]);
            P2.Location = new Point(24, positionY[1]);

            EscaCod.Visible = visibleButton[2];
            P3.Visible = visibleButton[2];
            EscaCod.Location = new Point(31, positionY[2]);
            P3.Location = new Point(24, positionY[2]);

            btnMP.Visible = visibleButton[3];
            P4.Visible = visibleButton[3];
            btnMP.Location = new Point(31, positionY[3]);
            P4.Location = new Point(24, positionY[3]);

            btnPT.Visible = visibleButton[4];
            P5.Visible = visibleButton[4];
            btnPT.Location = new Point(31, positionY[4]);
            P5.Location = new Point(24, positionY[4]);

            btnBin.Visible = visibleButton[5];
            P6.Visible = visibleButton[5];
            btnBin.Location = new Point(31, positionY[5]);
            P6.Location = new Point(24, positionY[5]);

            BTNJaula.Visible = visibleButton[6];
            P7.Visible = visibleButton[6];
            BTNJaula.Location = new Point(31, positionY[6]);
            P7.Location = new Point(24, positionY[6]);

            BTNValidateJaula.Visible = visibleButton[7];
            P8.Visible = visibleButton[7];
            BTNValidateJaula.Location = new Point(31, positionY[7]);
            P8.Location = new Point(24, positionY[7]);

            TBINMP.Visible = visibleButton[8];
            P11.Visible = visibleButton[8];
            TBINMP.Location = new Point(31, positionY[8]);
            P11.Location = new Point(24, positionY[8]);

            ReturnMP.Visible = visibleButton[9];
            P12.Visible = visibleButton[9];
            ReturnMP.Location = new Point(31, positionY[9]);
            P12.Location = new Point(24, positionY[9]);

            btnPIMP.Visible = visibleButton[10];
            P13.Visible = visibleButton[10];
            btnPIMP.Location = new Point(31, positionY[10]);
            P13.Location = new Point(24, positionY[10]);

            btnPIPT.Visible = visibleButton[11];
            P14.Visible = visibleButton[11];
            btnPIPT.Location = new Point(31, positionY[11]);
            P14.Location = new Point(24, positionY[11]);

            BTN_Log.Visible = visibleButton[12];
            P9.Visible = visibleButton[12];
            BTN_Log.Location = new Point(31, positionY[12]);
            P9.Location = new Point(24, positionY[12]);

            BTNCom_log.Visible = visibleButton[13];
            P10.Visible = visibleButton[13];
            BTNCom_log.Location = new Point(31, positionY[13]);
            P10.Location = new Point(24, positionY[13]);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            /*DataBase Query = new DataBase();
            if (Query.LabelsNotSend())
            {
                this.Close();
                MessageBoxSupport Alert = new MessageBoxSupport();
                Alert.Empl(Employee);
                Alert.Show();
            }
            else
            {*/
            cLog.Add("", NameScreen, "Click en boton para salir de la app", false);
            this.Close();
            Application.Exit();
            //}
            
        }

        private void btnMP_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Transferencia de Inventario", false);
            this.Hide();
            WP.Show();
            //TMP.Empl(Employee);
            //TMP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 1;
                BGWait.RunWorkerAsync();
            }
        }

        private void btnPT_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Transferencias de Producto Terminado", false);
            this.Hide();
            WP.Show();
            //TPT.Empl(Employee);
            //TPT.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 2;
                BGWait.RunWorkerAsync();
            }
        }       

        private void btnBin_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Transferencia de Bines", false);
            this.Hide();
            WP.Show();
            //TBIN.Empl(Employee);
            //TBIN.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 3;
                BGWait.RunWorkerAsync();
            }
        }
        
        private void BTNJaula_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Transferencia de Jaula", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 4;
                BGWait.RunWorkerAsync();
            }
        }

        private void BTNValidateJaula_Click_1(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Validación de Jaula", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 5;
                BGWait.RunWorkerAsync();
            }
        }
        
        private void BGWait_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (IDTransfer)
            {
                case 1:
                    TMP.Empl(Employee);
                    break;
                case 2:
                    TPT.Empl(Employee);
                    break;
                case 3:
                    TBIN.Empl(Employee);
                    break;
                case 4:
                    TJ.Empl(Employee);
                    break;
                case 5:
                    VJ.Empl(Employee);
                    break;
                case 6:
                    ImpCo.Empl(Employee);
                    break;
                case 7:
                    EsBForm.Empl(Employee);
                    break;
                case 8:
                    TbinMP.Empl(Employee);
                    break;
                case 9:
                    RMP.Empl(Employee);
                    break;
                case 10:
                    PIMP.Empl(Employee);
                    break;
                case 11:
                    PIPT.Empl(Employee);
                    break;
            }
        }

        private void BGWait_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WP.CloseWait();
            switch (IDTransfer)
            {
                case 1:
                    TMP.Show();
                    break;
                case 2:
                    TPT.Show();
                    break;
                case 3:
                    TBIN.Show();
                    break;
                case 4:
                    TJ.Show();
                    break;
                case 5:
                    VJ.Show();
                    break;
                case 6:
                    ImpCo.Show();
                    break;
                case 7:
                    EsBForm.ShowInTaskbar = true;
                    EsBForm.Show();
                    break;
                case 8:
                    TbinMP.Show();
                    break;
                case 9:
                    RMP.Show();
                    break;
                case 10:
                    PIMP.Show();
                    break;
                case 11:
                    PIPT.Show();
                    break;
            }
        }

        private void BTN_Log_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Reenvío de datos", false);
            this.Hide();

            RetrySendingFrm n = new RetrySendingFrm();
            n.Empl(Employee, false);
            n.Show();
        }

        private void BTNCom_log_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Errores NS y App", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 6;
                BGWait.RunWorkerAsync();
            }
        }        

        private void TBINMP_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Transferencia de bin MP", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 8;
                BGWait.RunWorkerAsync();
            }
        }

        private void ReturnMP_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Devolución MP", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 9;
                BGWait.RunWorkerAsync();
            }
        }

        private void btnPIMP_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Inventario físico MP", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 10;
                BGWait.RunWorkerAsync();
            }
        }

        private void btnPIPT_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en boton de Inventario físico PT", false);
            this.Hide();
            WP.Show();
            if (BGWait.IsBusy != true)
            {
                IDTransfer = 11;
                BGWait.RunWorkerAsync();
            }
        }
    }
}