﻿
namespace NetSuiteApps
{
    partial class PhysicallInventoryPT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhysicallInventoryPT));
            this.panel2 = new System.Windows.Forms.Panel();
            this.PWait = new System.Windows.Forms.PictureBox();
            this.Counter = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CbBinInventory = new System.Windows.Forms.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSerieScanned = new System.Windows.Forms.Label();
            this.SendData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.CBInventoryLog = new System.Windows.Forms.ComboBox();
            this.txtBDes = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CodeBar = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BGSend = new System.ComponentModel.BackgroundWorker();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel2.Controls.Add(this.PWait);
            this.panel2.Controls.Add(this.Counter);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.CbBinInventory);
            this.panel2.Controls.Add(this.lblType);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.lblSerieScanned);
            this.panel2.Controls.Add(this.SendData);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.CBInventoryLog);
            this.panel2.Controls.Add(this.txtBDes);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.CodeBar);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(274, 427);
            this.panel2.TabIndex = 19;
            // 
            // PWait
            // 
            this.PWait.Enabled = false;
            this.PWait.Image = global::NetSuiteApps.Properties.Resources.cargando;
            this.PWait.Location = new System.Drawing.Point(103, 324);
            this.PWait.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PWait.Name = "PWait";
            this.PWait.Size = new System.Drawing.Size(69, 80);
            this.PWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PWait.TabIndex = 35;
            this.PWait.TabStop = false;
            this.PWait.Visible = false;
            // 
            // Counter
            // 
            this.Counter.AutoSize = true;
            this.Counter.BackColor = System.Drawing.Color.Transparent;
            this.Counter.ForeColor = System.Drawing.Color.White;
            this.Counter.Location = new System.Drawing.Point(103, 244);
            this.Counter.Name = "Counter";
            this.Counter.Size = new System.Drawing.Size(17, 20);
            this.Counter.TabIndex = 34;
            this.Counter.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(16, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "Escaneadas:";
            // 
            // CbBinInventory
            // 
            this.CbBinInventory.DropDownHeight = 110;
            this.CbBinInventory.FormattingEnabled = true;
            this.CbBinInventory.IntegralHeight = false;
            this.CbBinInventory.Location = new System.Drawing.Point(14, 129);
            this.CbBinInventory.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CbBinInventory.Name = "CbBinInventory";
            this.CbBinInventory.Size = new System.Drawing.Size(245, 28);
            this.CbBinInventory.TabIndex = 32;
            this.CbBinInventory.SelectedIndexChanged += new System.EventHandler(this.CbBinInventory_SelectedIndexChanged);
            this.CbBinInventory.Click += new System.EventHandler(this.CbBinInventory_Click);
            this.CbBinInventory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CbBinInventory_KeyUp);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblType.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblType.Location = new System.Drawing.Point(14, 105);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(161, 20);
            this.lblType.TabIndex = 31;
            this.lblType.Text = "Depósito de inventario";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(15, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 20);
            this.label6.TabIndex = 30;
            this.label6.Text = "Etiqueta escaneada:";
            // 
            // lblSerieScanned
            // 
            this.lblSerieScanned.AutoSize = true;
            this.lblSerieScanned.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSerieScanned.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblSerieScanned.Location = new System.Drawing.Point(149, 219);
            this.lblSerieScanned.Name = "lblSerieScanned";
            this.lblSerieScanned.Size = new System.Drawing.Size(15, 20);
            this.lblSerieScanned.TabIndex = 29;
            this.lblSerieScanned.Text = "-";
            // 
            // SendData
            // 
            this.SendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.SendData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SendData.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SendData.Location = new System.Drawing.Point(91, 272);
            this.SendData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SendData.Name = "SendData";
            this.SendData.Size = new System.Drawing.Size(86, 40);
            this.SendData.TabIndex = 27;
            this.SendData.Text = "Enviar";
            this.SendData.UseVisualStyleBackColor = false;
            this.SendData.Click += new System.EventHandler(this.SendData_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(14, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 20);
            this.label5.TabIndex = 26;
            this.label5.Text = "Registro de inventario";
            // 
            // CBInventoryLog
            // 
            this.CBInventoryLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CBInventoryLog.FormattingEnabled = true;
            this.CBInventoryLog.Location = new System.Drawing.Point(14, 71);
            this.CBInventoryLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CBInventoryLog.Name = "CBInventoryLog";
            this.CBInventoryLog.Size = new System.Drawing.Size(245, 28);
            this.CBInventoryLog.TabIndex = 23;
            this.CBInventoryLog.SelectedIndexChanged += new System.EventHandler(this.CBInventoryLog_SelectedIndexChanged);
            // 
            // txtBDes
            // 
            this.txtBDes.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtBDes.Enabled = false;
            this.txtBDes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBDes.ForeColor = System.Drawing.SystemColors.Window;
            this.txtBDes.Location = new System.Drawing.Point(14, 319);
            this.txtBDes.Multiline = true;
            this.txtBDes.Name = "txtBDes";
            this.txtBDes.Size = new System.Drawing.Size(245, 92);
            this.txtBDes.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel4.Location = new System.Drawing.Point(15, 215);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(246, 1);
            this.panel4.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(15, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ingresar código:";
            // 
            // CodeBar
            // 
            this.CodeBar.BackColor = System.Drawing.Color.White;
            this.CodeBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeBar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CodeBar.ForeColor = System.Drawing.Color.Black;
            this.CodeBar.HideSelection = false;
            this.CodeBar.Location = new System.Drawing.Point(16, 187);
            this.CodeBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CodeBar.Name = "CodeBar";
            this.CodeBar.Size = new System.Drawing.Size(246, 20);
            this.CodeBar.TabIndex = 18;
            this.CodeBar.Text = "123";
            this.CodeBar.Click += new System.EventHandler(this.CodeBar_Click);
            this.CodeBar.TextChanged += new System.EventHandler(this.CodeBar_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(274, 47);
            this.panel3.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(78, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Inventario físico PT";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(34, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BGSend
            // 
            this.BGSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGSend_DoWork);
            this.BGSend.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGSend_RunWorkerCompleted);
            // 
            // PhysicallInventoryPT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 427);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PhysicallInventoryPT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PhysicallInventoryPT";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox PWait;
        private System.Windows.Forms.Label Counter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CbBinInventory;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSerieScanned;
        private System.Windows.Forms.Button SendData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CBInventoryLog;
        private System.Windows.Forms.TextBox txtBDes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CodeBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.PictureBox pictureBox2;
        private System.ComponentModel.BackgroundWorker BGSend;
    }
}