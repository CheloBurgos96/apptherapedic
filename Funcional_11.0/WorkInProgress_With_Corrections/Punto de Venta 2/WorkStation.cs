﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class WorkStation : Form
    {
        private int IndexCMB = 0;
        public WorkStation()
        {
            InitializeComponent();
            this.CenterToScreen();
            cmbWStation.Items.Add("Estación 1");
            cmbWStation.Items.Add("Estación 2");
            cmbWStation.Items.Add("Estación 3");
            //cmbWStation.Items.Add("Estación 4");
        }

        private void cmbWStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            IndexCMB = cmbWStation.SelectedIndex;
        }

        private void BTNSave_Click(object sender, EventArgs e)
        {
            String[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DataBase InsertWorkStation = new DataBase(false);
            var findIt = "INSERT INTO workstation_logs (id_station, date_reg) VALUES (" + (IndexCMB + 1).ToString() + ",'" + result[0] + "')";
            InsertWorkStation.UpDateDB(findIt);
            this.Close();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
