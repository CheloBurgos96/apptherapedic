﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public class ReceiveMPPT
    {
        [JsonProperty("isSuccess")]
        public bool Result { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public Datas[] Data { get; set; }
        [JsonProperty("apiError")]
        public Info ApiError { get; set; }

        public class Datas
        {
            [JsonProperty("inventoryRecordID")] 
            public int S { get; set; }
        }
        public class Info
        {
            [JsonProperty("time")]
            public string time { get; set; }
            [JsonProperty("errorMessage")]
            public Msg[] errorMessage { get; set; }
        }

        public class Msg
        {
            [JsonProperty("index")] 
            public int Index { get; set; }
            [JsonProperty("message")] 
            public string message { get; set; }
        }
    }
}
