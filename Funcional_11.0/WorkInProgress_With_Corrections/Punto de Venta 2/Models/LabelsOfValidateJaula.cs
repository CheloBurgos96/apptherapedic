﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class LabelsOfValidateJaula
    {
        
            [JsonProperty("validateJaula")]
            public StringOfValidateJaula[] ValidateJ { get; set; }

            public class StringOfValidateJaula
            {
                [JsonProperty("idEmployee")]
                public string IdEmploye { get; set; }
                [JsonProperty("idJaula")]
                public string IdJaula { get; set; }
                [JsonProperty("serials")]
                public List<string> Serials { get; set; }
            }
    }
}
