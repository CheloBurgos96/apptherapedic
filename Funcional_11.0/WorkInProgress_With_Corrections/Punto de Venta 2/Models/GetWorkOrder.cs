﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class GetWorkOrder
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public DataWO[] Data { get; set; }

        [JsonProperty("apiError")]
        public Errors ErrosQuery { get; set; }

        public class DataWO{

            [JsonProperty("intId")]
            public int WorkOrderID { get; set; }

            [JsonProperty("values")]
            public string Values { get; set; }

        }
        
        public class Errors {
            [JsonProperty("time")]
            public string Time { get; set; }

            [JsonProperty("errorMessage")]
            public ErrorMS[] errorMessage { get; set; }
        }

        public class ErrorMS
        {
            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}
