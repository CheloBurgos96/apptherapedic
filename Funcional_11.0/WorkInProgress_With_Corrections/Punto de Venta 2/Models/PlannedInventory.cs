﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class PlannedInventory
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }
        [JsonProperty("data")]
        public Data DataJaula { get; set; }
        public class Data
        {
            [JsonProperty("name")]
            public string NameJaula { get; set; }
            [JsonProperty("id")]
            public int IDJaula { get; set; }

            [JsonProperty("binNumber")]
            public string BinNumber { get; set; }

            [JsonProperty("binName")]
            public string BinName { get; set; }

            [JsonProperty("items")]
            public Items[] ItemsJaula { get; set; }
            [JsonProperty("apiError")]
            public Errors ApiError { get; set; }
        }

        public class Errors
        {
            [JsonProperty("time")]
            public string Time { get; set; }
            [JsonProperty("errorMessage")]
            public string Status { get; set; }
        }

            public class Items
        {
            [JsonProperty("skuName")]
            public string SKUName { get; set; }
            [JsonProperty("skuID")]
            public string SKUID { get; set; }
            [JsonProperty("qty")]
            public string Quantity { get; set; }
            [JsonProperty("firstScannedQty")]
            public string firstScannedQty { get; set; }
            [JsonProperty("secondScannedQty")]
            public string secondScannedQty { get; set; }
        }
        

        

        

        
    }
}
