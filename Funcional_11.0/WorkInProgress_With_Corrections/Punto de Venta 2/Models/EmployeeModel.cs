﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class EmployeeModel
    {
        
        public EmployeeModel()
        {


        }
        
        //public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public int NumberWorkStation { get; set; }


        public bool ButtonPrint { get; set; }
        public bool ButtonEmbol { get; set; }
        public bool ButtonOperation { get; set; }
        public bool ButtonTMP { get; set; }
        public bool ButtonTPT { get; set; }
        public bool ButtonBin { get; set; }
        public bool ButtonBinMP { get; set; }
        public bool ButtonLog { get; set; }
        public bool ButtonError { get; set; }
        public bool ButtonTJaula { get; set; }
        public bool ButtonValidateJaula { get; set; }
        public bool ButtonReturnMP { get; set; }
        public bool ButtonPIMP { get; set; }
        public bool ButtonPIPT { get; set; }

        public bool TH_MP_PP { get; set; }
        public bool TH_MP_PP_Bases { get; set; }
        public bool TH_MP_PP_Bcos { get; set; }
        public bool TH_MP_MP_Bcos { get; set; }
        public bool TH_MP_MP_Bases { get; set; }
        public bool TH_MP_MQ { get; set; }
        public bool Bcos_MP_PP { get; set; }
        public bool Bcos_MP_MQ { get; set; }
        public bool Bcos_MP_MP_TH { get; set; }
        public bool Bcos_PP_PT { get; set; }
        public bool Bases_MP_PP { get; set; }


        public bool TH_PP_PT { get; set; }
        public bool Bases_PP_Trans { get; set; }
        public bool Trans_PT_TH { get; set; }
        public bool TH_PT_TALLER { get; set; }
        public bool TH_TALLER_PT { get; set; }
        public bool JAULA_PP { get; set; }
        public bool INSPECCION_TALLER { get; set; }
        public bool INSPECCION_PT { get; set; }





    }
   
}
