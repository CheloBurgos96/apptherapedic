﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class LabelsNotSendBin
    {
        [JsonProperty("bin")]
        public Datas[] Bin { get; set; }
        public class Datas
        {
            [JsonProperty("binOri")]
            public int binOri { get; set; }
            [JsonProperty("binDest")]
            public int binDest { get; set; }
            [JsonProperty("series")]
            public List<string> Serials { get; set; }
        }
        

        /*[JsonProperty("bin")]
        public TranfersPT[] BIN { get; set; }

        public class TranfersPT
        {
            [JsonProperty("binOri")]
            public Combination1[] Type1 { get; set; }
            [JsonProperty("binDest")]
            public Combination2[] Type2 { get; set; }
        }

        public class Combination1
        {
            [JsonProperty("binOri")]
            public int binOri { get; set; }
            [JsonProperty("binDest")]
            public int binDest { get; set; }

            [JsonProperty("series")]
            public List<string> Serials { get; set; }
        }

        public class Combination2
        {
            [JsonProperty("binOri")]
            public int binOri { get; set; }
            [JsonProperty("binDest")]
            public int binDest { get; set; }

            [JsonProperty("series")]
            public List<string> Serials { get; set; }
        }*/
    }
}
