﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class ReceiveJaulaResponse
    {
        [JsonProperty("isSuccess")]
        public bool Result { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public Datas[] Data { get; set; }

        [JsonProperty("apiError")]
        public Info ApiError { get; set; }

        public class Datas
        {
            [JsonProperty("index")]
            public int Index { get; set; }
            [JsonProperty("product")]
            public string Product { get; set; }
            [JsonProperty("inventoryRecordID")]
            public int InventoryRecordID { get; set; }
        }
        public class Info
        {
            [JsonProperty("time")]
            public string Time { get; set; }
            [JsonProperty("errorMessage")]
            public Msg[] ErrorMessage { get; set; }
        }

        public class Msg
        {
            [JsonProperty("index")]
            public int Index { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}
