﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class LabelsNotSends
    {
        [JsonProperty("pt")]
        public TranfersPT[] PT { get; set; }

        public class TranfersPT
        {
            [JsonProperty("areRawMaterials")]
            public Combination1[] AreRawMaterials { get; set; }
            /*[JsonProperty("change")]
            public Combination2[] Change { get; set; }
            [JsonProperty("areFinishedGoods")]
            public Combination1[] AreFinishedGoods { get; set; }*/
        }

        public class Combination1
        {
            [JsonProperty("combination")]
            public int Combination { get; set; }
            [JsonProperty("serials")]
            public List<string> Serials { get; set; }
        }

        /*public class Combination2
        {
            [JsonProperty("combination")]
            public int Combination { get; set; }
            [JsonProperty("serials")]
            public List<string> Serials { get; set; }
        }

        public class Combination3
        {
            [JsonProperty("combination")]
            public int Combination { get; set; }
            [JsonProperty("serials")]
            public List<string> Serials { get; set; }
        }*/


    }
}
