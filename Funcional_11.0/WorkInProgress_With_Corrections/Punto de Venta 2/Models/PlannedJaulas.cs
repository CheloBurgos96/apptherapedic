﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class PlannedJaulas
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }
        [JsonProperty("data")]
        public PlanJaulas[] Jaulas { get; set; }
        [JsonProperty("apiError")]
        public Errors ApiError { get; set; }

        public class Errors
        {
            [JsonProperty("time")]
            public string Time { get; set; }
            [JsonProperty("errorMessage")]
            public string Status { get; set; }
        }

        public class PlanJaulas
        {
            [JsonProperty("display")]
            public string BinName { get; set; }

            [JsonProperty("locationForValidationScan")]
            public string UbicationJaula { get; set; }

            [JsonProperty("binForValidationScan")]
            public string BinForValid { get; set; }

            [JsonProperty("id")]
            public string BinID { get; set; }
        }
    }
}
