﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class ReprintLabelPT
    {
        [JsonProperty("isSuccess")]
        public bool Result { get; set; }

        [JsonProperty("data")]
        public Datas Data { get; set; }

        [JsonProperty("apiError")]
        public Info ApiError { get; set; }

        public class Datas
        {
            [JsonProperty("serial")]
            public string Serial { get; set; }
            [JsonProperty("reimpresion")]
            public bool ReprintLabel { get; set; }
            [JsonProperty("description")]
            public string Descripcion { get; set; }
            [JsonProperty("sku")]
            public string SKU { get; set; }
            [JsonProperty("tipo_etiqueta")]
            public string TipoProducto { get; set; }
            [JsonProperty("customer_1")]
            public string Customer1 { get; set; }
            [JsonProperty("customer_2")]
            public string Customer2 { get; set; }
            [JsonProperty("customer_3")]
            public string Customer3 { get; set; }
            [JsonProperty("medida_producto")]
            public string MedidaProducto { get; set; }
            [JsonProperty("reparacion")]
            public bool Reparacion { get; set; }
            [JsonProperty("orden_trabajo")]
            public string OrdenVenta { get; set; }
            [JsonProperty("doble_bolsa")]
            public bool DoubleBag { get; set; }
        }
        public class Info
        {
            [JsonProperty("time")]
            public string Time { get; set; }
            [JsonProperty("errorMessage")]
            public Msg[] ErrorMessage { get; set; }
        }

        public class Msg
        {
            [JsonProperty("index")]
            public int Index { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}
