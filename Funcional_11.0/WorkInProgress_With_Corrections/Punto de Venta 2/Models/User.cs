﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class User
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
        [JsonProperty("message")]
        public Info[] Message { get; set; }

        public class Info
        {
            [JsonProperty("rolId")]
            public int RolId { get; set; }
            [JsonProperty("rol")]
            public string Rol { get; set; }
            [JsonProperty("user")]
            public string User { get; set; }

            //public ButtonMenu[] BtnVisible { get; set; }

        }

        /*public class ButtonMenu
        {
            public bool ButtonPrint { get; set; }
            public bool ButtonEmbol { get; set; }
            public bool ButtonOperation { get; set; }
            public bool ButtonTMP { get; set; }
            public bool ButtonTPT { get; set; }
            public bool ButtonBin { get; set; }
            public bool ButtonLog { get; set; }
            public bool ButtonError { get; set; }
            public PermissionMP[] TranferMP { get; set; }
            public PermissionPT[] TransferPT { get; set; }
        }

        public class PermissionMP
        {
            public bool TH_MP_PP { get; set; }
            public bool TH_MP_PP_Bases { get; set; }
            public bool TH_MP_PP_Bcos { get; set; }
            public bool TH_MP_MP_Bcos { get; set; }
            public bool TH_MP_MP_Bases { get; set; }
            public bool TH_MP_MQ { get; set; }
            public bool Bcos_MP_PP { get; set; }
            public bool Bcos_MP_MQ { get; set; }
            public bool Bcos_MP_MP_TH { get; set; }
            public bool Bcos_PP_PT { get; set; }
            public bool Bases_MP_PP { get; set; }
        }

        public class PermissionPT
        {
            public bool TH_PP_PT { get; set; }
            public bool Bases_PP_Trans { get; set; }
            public bool Trans_PT_TH { get; set; }
        }*/
    }
}
