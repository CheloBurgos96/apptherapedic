﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class UserPermissions
    {
        [JsonProperty("access")]
        public bool Result { get; set; }
        [JsonProperty("permissions")]
        public ButtonMenu BtnVisible { get; set; }



        public class ButtonMenu
        {
            [JsonProperty("imprimir_etiquetas")]
            public bool ButtonPrint { get; set; }

            [JsonProperty("registro_errores")]
            public bool ButtonError { get; set; }

            [JsonProperty("registro_operaciones")]
            public bool ButtonOperation { get; set; }

            [JsonProperty("errores_envio")]
            public bool ButtonLog { get; set; }

            [JsonProperty("embolsado")]
            public bool ButtonEmbol { get; set; }

            [JsonProperty("transferencia_jaula")]
            public bool ButtonTranferJaula { get; set; }

            [JsonProperty("validacion_jaula")]
            public bool ButtonValidateJaule { get; set; }

            [JsonProperty("MP_PP")]
            public PermissionMP TranferMP { get; set; }

            [JsonProperty("PT_PT")]
            public PermissionPT TransferPT { get; set; }

            [JsonProperty("BINES")]
            public PermissionBIN TransferBIN { get; set; }

            [JsonProperty("transferencia_bines")]
            public bool TransferBINMP { get; set; }

            [JsonProperty("devoluciones")]
            public bool ReturnMP { get; set; }

            [JsonProperty("inventario_fisico_mp")]
            public bool PhysicalInvMP { get; set; }

            [JsonProperty("inventario_fisico_pt")]
            public bool PhysicalInvPT { get; set; }
        }

        public class PermissionMP
        {
            [JsonProperty("access")]
            public bool ButtonTMP { get; set; }
            [JsonProperty("moves")]
            public MovesMP moves { get; set; }
        }

        public class MovesMP
        {
            [JsonProperty("move_1")]
            public bool TH_MP_PP { get; set; }
            [JsonProperty("move_2")]
            public bool TH_MP_PP_Bases { get; set; }
            [JsonProperty("move_3")]
            public bool TH_MP_PP_Bcos { get; set; }
            [JsonProperty("move_4")]
            public bool TH_MP_MP_Bcos { get; set; }
            [JsonProperty("move_5")]
            public bool TH_MP_MP_Bases { get; set; }
            [JsonProperty("move_6")]
            public bool TH_MP_MQ { get; set; }
            [JsonProperty("move_7")]
            public bool Bcos_MP_PP { get; set; }
            [JsonProperty("move_8")]
            public bool Bcos_MP_MQ { get; set; }
            [JsonProperty("move_9")]
            public bool Bcos_MP_MP_TH { get; set; }
            [JsonProperty("move_10")]
            public bool Bcos_PP_PT { get; set; }
            [JsonProperty("move_11")]
            public bool Bases_MP_PP { get; set; }
        }

        public class PermissionPT
        {

            [JsonProperty("access")]
            public bool ButtonTPT { get; set; }
            [JsonProperty("moves")]
            public MovePT moves { get; set; }
        }

        public class MovePT
        {
            [JsonProperty("move_1")]
            public bool TH_PP_PT { get; set; }
            [JsonProperty("move_2")]
            public bool Bases_PP_Trans { get; set; }
            [JsonProperty("move_3")]
            public bool Trans_PT_TH { get; set; }
            [JsonProperty("move_4")]
            public bool TH_PT_TALLER { get; set; }
            [JsonProperty("move_5")]
            public bool TH_TALLER_PT { get; set; }
            [JsonProperty("move_6")]
            public bool JAULA_PP { get; set; }
            [JsonProperty("move_7")]
            public bool INSPECCION_TALLER { get; set; }
            [JsonProperty("move_8")]
            public bool INSPECCION_PT { get; set; }
        }

        public class PermissionBIN
        {
            [JsonProperty("access")]
            public bool ButtonBin { get; set; }
            [JsonProperty("moves")]
            public MoveBin moves { get; set; }
        }

        public class MoveBin
        {
            [JsonProperty("move_1")]
            public bool Trans_PT_TH { get; set; }
        }

    }
}
