﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class WorkInProgressModel
    {
        public string Serie { get; set; }
        public string Descripcion { get; set; }
        public string SKU { get; set; }
        public bool doubleBag { get; set; }

        public bool Terminado { get; set; }
        public int Contador { get; set; }

        public int CounterPrint { get; set; }
        public bool Reprinted { get; set; }


        public string StepID1 { get; set; }
        public string StepSerial1 { get; set; }
        public string StepName1 { get; set; }

        public string StepID2 { get; set; }
        public string StepSerial2 { get; set; }
        public string StepName2 { get; set; }

        public string StepID3 { get; set; }
        public string StepSerial3 { get; set; }
        public string StepName3 { get; set; }

        public string StepID4 { get; set; }
        public string StepSerial4 { get; set; }
        public string StepName4 { get; set; }

        public string StepID5 { get; set; }
        public string StepSerial5 { get; set; }
        public string StepName5 { get; set; }

        public string StepID6 { get; set; }
        public string StepSerial6 { get; set; }
        public string StepName6 { get; set; }

        public string StepID7 { get; set; }
        public string StepSerial7 { get; set; }
        public string StepName7 { get; set; }

        public string StepID8 { get; set; }
        public string StepSerial8 { get; set; }
        public string StepName8 { get; set; }

        public string StepID9 { get; set; }
        public string StepSerial9 { get; set; }
        public string StepName9 { get; set; }

        public string StepID10 { get; set; }
        public string StepSerial10 { get; set; }
        public string StepName10 { get; set; }

        public string StepID11 { get; set; }
        public string StepSerial11 { get; set; }
        public string StepName11 { get; set; }

        public string StepID12 { get; set; }
        public string StepSerial12 { get; set; }
        public string StepName12 { get; set; }

        public string TipoProducto { get; set; }
        public string MedidaProducto { get; set; }
        public string OrdenVenta { get; set; }
        public string OrdenVentaSmall { get; set; }
        public string Reparacion { get; set; }
        public string Customer1 { get; set; }
        public string Customer2 { get; set; }
        public string Customer3 { get; set; }
        public bool NotBuild { get; set; }

        public Int64 DateSerie { get; set; }
    }
}
