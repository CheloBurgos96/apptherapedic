﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class LabelsOfTransferJaula
    {
        [JsonProperty("transferJaula")]
        public StringOfTransferJaula[] TransferJ { get; set; }

        public class StringOfTransferJaula
        {
            [JsonProperty("idEmployee")]
            public int IdEmploye { get; set; }
            [JsonProperty("idJaula")]
            public int IdJaula { get; set; }
            [JsonProperty("serials")]
            public List<string> Serials { get; set; }
        }
    }
}
