﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class GetBinsService
    {
        public GetBinsService(){}

        [JsonProperty("isSuccess")]
        public bool isSuccess { get; set; }
        [JsonProperty("data")]
        public DataArray[] data { get; set; }
        //public string data { get; set; }
        [JsonProperty("apiError")]
        public ApiError apiError { get; set; }

        public class DataArray
        {
            [JsonProperty("name")]
            public string name { get; set; }
            [JsonProperty("id")]
            public int id { get; set; }
        }

        public class ApiError
        {
            [JsonProperty("time")]
            public string time { get; set; }
            [JsonProperty("errorMessage")]
            public string msg { get; set; }
        }

    }
}
