﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class DataMP
    {
		[JsonProperty("isSuccess")]
		public bool IsSuccess { get; set; }

		[JsonProperty("message")]
		public string Message { get; set; }

		[JsonProperty("data")]
		public Value[] Data { get; set; }

		[JsonProperty("apiError")]
		public Errors ErrosQuery { get; set; }

		public class Errors
        {
			[JsonProperty("time")]
			public string Data { get; set; }

			[JsonProperty("errorMessage")]
			public string ErrosQuery { get; set; }
		}

		public class Value
		{
			[JsonProperty("binId")]
			public string BinId { get; set; }

			[JsonProperty("binName")]
			public string BinName { get; set; }

			[JsonProperty("location")]
			public string Location { get; set; }

			[JsonProperty("numeroSerie")]
			public string NumeroSerie { get; set; }

			[JsonProperty("description")]
			public string Description { get; set; }

			[JsonProperty("dateCreate")]
			public string DateCreate { get; set; }

			[JsonProperty("sku")]
			public string SKU { get; set; }

			[JsonProperty("employee")]
			public string Employee { get; set; }

			[JsonProperty("quantity")]
			public string Quantity { get; set; }

			[JsonProperty("currentquantity")]
			public string Currentquantity { get; set; }

			[JsonProperty("etiquetaCero")]
			public bool LabelZero { get; set; }
		}
	}
}
