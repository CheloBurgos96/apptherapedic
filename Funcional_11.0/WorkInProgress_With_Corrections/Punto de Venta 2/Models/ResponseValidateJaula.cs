﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class ResponseValidateJaula
    {
        [JsonProperty("isSuccess")]
        public bool Result { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public Datas[] Data { get; set; }

        [JsonProperty("apiError")]
        public Info ApiError { get; set; }

        public class Datas
        {
            [JsonProperty("index")]
            public int Index { get; set; }
            [JsonProperty("isUpdated")]
            public bool IsUpdated { get; set; }
        }
        public class Info
        {
            [JsonProperty("time")]
            public string Time { get; set; }
            [JsonProperty("errorMessage")]
            public Msg[] ErrorMessage { get; set; }
        }

        public class Msg
        {
            [JsonProperty("index")]
            public int Index { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}
