﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public class UbicationsPT
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("count")]
        public int Quantity { get; set; }

        [JsonProperty("data")]
        public Data[] Series { get; set; }

        [JsonProperty("apiError")]
        public Errors ErrorsQuery { get; set; }

        public class Errors
        {
            [JsonProperty("time")]
            public string Data { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrosQuery { get; set; }
        }

        public class Data
        {
            [JsonProperty("serial")]
            public string serial { get; set; }
            [JsonProperty("item")]
            public string item { get; set; }
            [JsonProperty("itemId")]
            public string itemID { get; set; }
            [JsonProperty("quantityAvailable")]
            public string quantity { get; set; }
            [JsonProperty("binNumber")]
            public string binID { get; set; }
            [JsonProperty("binName")]
            public string binName { get; set; }
            [JsonProperty("location")]
            public string Location { get; set; }
        }
    }
}
