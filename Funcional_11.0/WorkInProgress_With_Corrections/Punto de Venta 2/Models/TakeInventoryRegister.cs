﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class TakeInventoryRegister
    {
        [JsonProperty("isSuccessful")]
        public bool IsSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public Datas[] Data { get; set; }

        [JsonProperty("errors")]
        public Errors[] ApiError { get; set; }

        public class Errors
        {
            [JsonProperty("time")]
            public string Data { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrosQuery { get; set; }
        }

        public class Datas
        {
            [JsonProperty("text")]
            public string Text { get; set; }
            [JsonProperty("inventoryId")]
            public string IDLocatation { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }
    }
}
