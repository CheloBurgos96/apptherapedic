﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public class ReceiveBin
    {
        [JsonProperty("isSuccess")]
        public bool Result { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        [JsonProperty("apiError")]
        public Datas2 ApiError { get; set; }

        public class Datas2
        {
            [JsonProperty("time")]
            public string binDest { get; set; }
            [JsonProperty("errorMessage")]
            public Errors[] Serials { get; set; }
        }

        public class Errors
        {
            [JsonProperty("message")]
            public string MessageError { get; set; }
        }


    }
}
