﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class ResponsePhyInventory
    {
        [JsonProperty("isSuccessful")]
        public bool IsSuccessful { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public string[] Data { get; set; }

        [JsonProperty("errors")]
        public string[] Errors { get; set; }

        /*public class Datas
        {
            [JsonProperty("bin")]
            public int Bin { get; set; }

            [JsonProperty("code")]
            public int Code { get; set; }
        }

        public class Errors
        {
            [JsonProperty("bin")]
            public int Bin { get; set; }

            [JsonProperty("code")]
            public int Code { get; set; }
        }*/

    }


}

