﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class LabelsNotSendPT
    {
        [JsonProperty("areFinishedGoods")]
        public Combination1[] AreRawMaterials { get; set; }

        public class Combination1
        {
            [JsonProperty("combination")]
            public int Combination { get; set; }
            [JsonProperty("serials")]
            public List<string> Serials { get; set; }
        }
    }
}
