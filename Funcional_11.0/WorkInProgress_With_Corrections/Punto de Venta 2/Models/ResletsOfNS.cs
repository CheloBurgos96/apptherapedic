﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class ResletsOfNS
    {
        /*/// <summary>
        /// Ambiente de pruebas
        /// </summary>
        /// Pantalla de Login
        //Acceso a la aplicación
        public string SLogin = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2036&deploy=3";//Llamado 1 vez
        //Acceso a los permisos de pantallas
        public string SPermissionLogin = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2105&deploy=1";//Llamado 1 vez

        /// Impresiones de etiquetas
        //Petición de etiquetas en planeación y para imprimir
        public string SQueryPrintLabels = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2042&deploy=1";//Llamado 3 veces
        //Envío de los seriales seleccionados a imprimir
        public string SSendPrintedLabels = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2054&deploy=1";//Llamado 1 vez

        /// Envio de embolsado
        //Envio de etiqueta ya terminada e impresa en embolsado
        public string SPrintedEmbol = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2052&deploy=1";//Llamado 1 vez
        //Petición para reimprimir etiqueta en embolsado
        public string SReprintEmbol = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2122&deploy=1&serial=";//Llamado 1 vez

        /// Operaciones de usuarios
        //Envio de seriales de operación
        public string SOperations = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2045&deploy=1";//Llamado 1 vez

        public string ErrorsOfNS = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2100&deploy=1";//Llamado 1 vez

        /// Transferencia de Materia primas
        //Envío de seriales escaneados para transferencia de MP
        //public string STMPPT = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2101&deploy=1";//Llamado 5 veces

        public string STMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2135&deploy=1";//Llamado 3 veces

        public string STPT = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2136&deploy=1";//Llamado 2 veces

        /// Transferencia de Bines
        public string SBINS = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2137&deploy=1";//Llamado 2 vez
        /// Transferencia de Jaulas
        public string STJ = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2120&deploy=1";//Llamado 2 vez

        /// Validación de Jaulas
        public string SVJ = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2121&deploy=1";//Llamado 2 vez

        public string BinsOfLocation = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2099&deploy=1&location=";//Llamado 1 vez

        public string PlanTJaulas = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2118&deploy=1&status=1";//Llamado 1 vez

        public string PlanVJaulas = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2118&deploy=1&status=2";//Llamado 1 vez

        public string SerialsPT = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2134&deploy=1";//Llamado 4 veces

        public string SerialsMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2133&deploy=1";//Llamado 1 vez

        public string DetailsJaulas = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2119&deploy=1&id=";//Llamado 2 veces

        public string PlanPTJaulas = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2118&deploy=1&status=3";//Llamado 1 vez

        public string SerialJPT = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2149&deploy=1&id=";

        public string SerialsMPLive = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2165&deploy=1";

        public string SerialsPTLive = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2151&deploy=";

        public string SBINMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2152&deploy=1";

        //public string QuerySerialWIP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2160&deploy=1";

        public string SendTransMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2158&deploy=1";

        public string SendMPtoWIP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2161&deploy=1";

        public string TakeWorkOrde = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2162&deploy=1";

        public string TakeInventoryMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2182&deploy=1";

        public string TakeInventoryPT = "";

        public string SendInventoryMP = "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2182&deploy=1";

        public string SendInventoryPT = "";

        public string[] idUbicationMP = { "16", "16", "16", "16", "16", "16", "18", "18", "18", "21", "17" };
        public string[] idQueryMP = { "16", "18", "21", "17" };
        public int[] idQueryMPB = { 18 };
        public string idMPToValidate = "16";


        public string[] idUbicationPT = { "19", "20", "125", "22", "127", "122", "128" };
        public string[] allIDLocationPT = { "19", "20", "125", "22", "127", "122", "128", "128" };

        public string LocationMP = "[\"16\", \"18\", \"21\", \"17\"]";
        public string LocationPT = "[\"19\", \"20\", \"125\", \"22\", \"127\", \"122\", \"128\" ]";

        public string LocationPTUnique = "[\"22\"]";
        public string LBIN = "22";
        //public string LocationBIN = "[\"22\"]";
        //public string LocationTJ = "[\"22\"]";//*/


        /*/// <summary>
        /// Ambiente Operativo
        /// </summary>
        /// Pantalla de Login
        //Acceso a la aplicación
        public string SLogin = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1213&deploy=3";//Llamado 1 vez
                                                                                                                                //Acceso a los permisos de pantallas
        public string SPermissionLogin = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1192&deploy=1";//Llamado 1 vez

        /// Impresiones de etiquetas
        //Petición de etiquetas en planeación y para imprimir
        public string SQueryPrintLabels = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1193&deploy=1";//Llamado 3 veces
                                                                                                                                                //Envío de los seriales seleccionados a imprimir
        public string SSendPrintedLabels = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1210&deploy=1";//Llamado 1 vez

        /// Envio de embolsado
        //Envio de etiqueta ya terminada e impresa en embolsado
        public string SPrintedEmbol = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1211&deploy=1";//Llamado 1 vez
                                                                                                                                       //Petición para reimprimir etiqueta en embolsado
        //------
        public string SReprintEmbol = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1195&deploy=1&serial=";//Llamado 1 vez

        /// Operaciones de usuarios
        //Envio de seriales de operación
        public string SOperations = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1209&deploy=1";//Llamado 1 vez

        public string ErrorsOfNS = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1212&deploy=1";//Llamado 1 vez

        /// Transferencia de Materia primas
        //Envío de seriales escaneados para transferencia de MP

        public string STMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1267&deploy=1";//Llamado 3 veces

        public string STPT = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1268&deploy=1";//Llamado 2 veces

        /// Transferencia de Bines
        public string SBINS = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1269&deploy=1";//Llamado 2 vez

        /// Transferencia de Jaulas
        public string STJ = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1260&deploy=1";//Llamado 2 vez

        /// Validación de Jaulas
        public string SVJ = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1259&deploy=1";//Llamado 2 vez

        //------
        public string BinsOfLocation = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1191&deploy=1&location=";//Llamado 1 vez

        //------
        public string PlanTJaulas = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=1";//Llamado 1 vez

        //------
        public string PlanVJaulas = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=2";//Llamado 1 vez

        public string SerialsPT = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1266&deploy=1";//Llamado 4 veces

        public string SerialsMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1264&deploy=1";

        //------
        public string DetailsJaulas = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1256&deploy=1&id=";//Llamado 2 veces

        public string PlanPTJaulas = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=3";//Llamado 1 vez

        public string SerialJPT = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1366&deploy=1&id=";

        public string SerialsMPLive = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1378&deploy=1";

        public string SerialsPTLive = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1382&deploy=";

        public string SBINMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1377&deploy=1";

        public string SendTransMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1387&deploy=1";

        public string SendMPtoWIP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1391&deploy=1";

        public string TakeWorkOrde = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1390&deploy=1";

        public string TakeInventoryMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1505&deploy=1";

        public string TakeInventoryPT = "";

        public string SendInventoryMP = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1505&deploy=1";

        public string SendInventoryPT = "";


        public string[] idUbicationMP = { "37", "37", "37", "37", "37", "37", "36", "36", "36", "39", "35" };
        public string[] idQueryMP = { "37", "36", "39", "35" };
        public int[] idQueryMPB = { 37 };
        public string idMPToValidate = "37";

        public string LocationMP = "[\"37\", \"36\", \"39\", \"35\"]";
        public string LocationPT = "[\"40\", \"35\", \"59\", \"60\", \"61\", \"64\"]";

        public string[] idUbicationPT = { "40", "35", "59", "43", "60", "61", "64" };
        public string[] allIDLocationPT = { "40", "35", "59", "43", "60", "61", "64", "64" };

        public string LocationPTUnique = "[\"43\"]";
        public string LBIN = "43";
        //public string LocationBIN = "[\"43\"]";
        //public string LocationTJ = "[\"43\"]";//*/

        /// <summary>
        /// Ambiente SandBox 2
        /// </summary>
        /// Pantalla de Login
        //Acceso a la aplicación
        public string SLogin = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1213&deploy=3";//Llamado 1 vez
                                                                                                                                //Acceso a los permisos de pantallas
        public string SPermissionLogin = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1192&deploy=1";//Llamado 1 vez

        /// Impresiones de etiquetas
        //Petición de etiquetas en planeación y para imprimir
        public string SQueryPrintLabels = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1193&deploy=1";//Llamado 3 veces
                                                                                                                                                //Envío de los seriales seleccionados a imprimir
        public string SSendPrintedLabels = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1210&deploy=1";//Llamado 1 vez

        /// Envio de embolsado
        //Envio de etiqueta ya terminada e impresa en embolsado
        public string SPrintedEmbol = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1211&deploy=1";//Llamado 1 vez
                                                                                                                                       //Petición para reimprimir etiqueta en embolsado
        //------
        public string SReprintEmbol = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1195&deploy=1&serial=";//Llamado 1 vez

        /// Operaciones de usuarios
        //Envio de seriales de operación
        public string SOperations = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1209&deploy=1";//Llamado 1 vez

        public string ErrorsOfNS = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1212&deploy=1";//Llamado 1 vez

        /// Transferencia de Materia primas
        //Envío de seriales escaneados para transferencia de MP

        public string STMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1267&deploy=1";//Llamado 3 veces

        public string STPT = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1268&deploy=1";//Llamado 2 veces

        /// Transferencia de Bines
        public string SBINS = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1269&deploy=1";//

        /// Transferencia de Jaulas
        public string STJ = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1260&deploy=1";//Llamado 2 vez

        /// Validación de Jaulas
        public string SVJ = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1259&deploy=1";//Llamado 2 vez

        //------
        public string BinsOfLocation = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1191&deploy=1&location=";//Llamado 1 vez

        //------
        public string PlanTJaulas = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=1";//Llamado 1 vez

        //------
        public string PlanVJaulas = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=2";//Llamado 1 vez

        public string SerialsPT = "https://6387011.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1266&deploy=1";//Llamado 4 veces

        public string SerialsMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1264&deploy=1";//Llamado 1 vez

        //------
        public string DetailsJaulas = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1256&deploy=1&id=";//Llamado 2 veces

        public string PlanPTJaulas = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1257&deploy=1&status=3";//Llamado 1 vez

        public string SerialJPT = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1366&deploy=1&id=";

        public string SerialsMPLive = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1378&deploy=1";

        public string SerialsPTLive = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1382&deploy=";

        public string SBINMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1377&deploy=1";

        public string SendTransMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1387&deploy=1";

        public string SendMPtoWIP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1391&deploy=1";

        public string TakeWorkOrde = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1390&deploy=1";

        public string TakeInventoryMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1398&deploy=1";

        public string TakeInventoryPT = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1399&deploy=1";

        public string SendInventoryMP = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1398&deploy=1";

        public string SendInventoryPT = "https://6387011-sb2.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1399&deploy=1";


        public string[] idUbicationMP = { "37", "37", "37", "37", "37", "37", "36", "36", "36", "39", "35" };
        public string[] idQueryMP = { "37", "36", "39", "35" };
        public int[] idQueryMPB = { 37 };
        public string idMPToValidate = "37";

        public string LocationMP = "[\"37\", \"36\", \"39\", \"35\"]";
        public string LocationPT = "[\"40\", \"35\", \"59\", \"60\", \"61\", \"64\"]";

        public string[] idUbicationPT = { "40", "35", "59", "43", "60", "61", "64" };
        public string[] allIDLocationPT = { "40", "35", "59", "43", "60", "61", "64", "64" };

        public string LocationPTUnique = "[\"43\"]";
        public string LBIN = "43";
        //public string LocationBIN = "[\"43\"]";
        //public string LocationTJ = "[\"43\"]";//*/
    }
}
