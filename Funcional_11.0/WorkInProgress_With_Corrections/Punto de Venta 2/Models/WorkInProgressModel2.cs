﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class WorkInProgressModel2
    {
        [JsonProperty("Info")]
        public Colchon[] Datos { get; set; }

        public class Colchon
        {
            [JsonProperty("result")]
            public bool Result { get; set; }
            [JsonProperty("code")]
            public int Code { get; set; }
            [JsonProperty("totalQuantity")]
            public int TotalQuantity { get; set; }

            [JsonProperty("message")]
            //[JsonIgnore]
            public Message msg { get; set; }
        }

        public class Message
        {
            [JsonProperty("uniqueSerie")]
            public string UniqueSerie { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("sku")]
            public string SKU { get; set; }
            [JsonProperty("doubleBag")]
            public bool DoubleBag { get; set; }

            [JsonProperty("print")]
            public bool Print { get; set; }

            [JsonProperty("reprinted")]
            public bool Reprinted { get; set; }

            [JsonProperty("createdate")]
            public int Createdate { get; set; }

            [JsonProperty("workOrder")]
            public string WorkOrder { get; set; }

            [JsonProperty("workStatus")]
            public string WorkStatus { get; set; }

            [JsonProperty("lastNumber")]
            public string WorkOrderSmall { get; set; }

            [JsonProperty("typeLabel")]
            public string TypeLabel { get; set; }

            [JsonProperty("productMeasure")]
            public string ProductMeasure { get; set; }

            [JsonProperty("createdFrom")]
            public string CreatedFrom { get; set; }

            [JsonProperty("repair")]
            public string Repair { get; set; }

            [JsonProperty("customer1")]
            public string Customer1 { get; set; }

            [JsonProperty("customer2")]
            public string Customer2 { get; set; }

            [JsonProperty("customer3")]
            public string Customer3 { get; set; }

            public bool NotBuild { get; set; }

            [JsonProperty("operations")]
            public Operations[] Oper { get; set; }
        }

        public class Operations
        {
            [JsonProperty("process")]
            public int Process { get; set; }
            [JsonProperty("serial")]
            public string StepSerial1 { get; set; }
            [JsonProperty("nameOperation")]
            public string StepName1 { get; set; }
        }
    }
}
