﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    public class SerialsJPT
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("data")]
        public Data Serial { get; set; }

        [JsonProperty("apiError")]
        public Errors ErrosQuery { get; set; }

        public class Errors
        {
            [JsonProperty("time")]
            public string Data { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrosQuery { get; set; }
        }

        public class Data
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("jaulaId")]
            public string JaulaId { get; set; }
            [JsonProperty("binNumber")]
            public string BinNumber { get; set; }
            [JsonProperty("binName")]
            public string BinName { get; set; }
            [JsonProperty("serials")]
            public Details[] Serials { get; set; }
        }

        public class Details
        {
            [JsonProperty("detailId")]
            public string DetailId { get; set; }
            [JsonProperty("secondScannedQty")]
            public string SecondScannedQty { get; set; }
            [JsonProperty("serials")]
            public List<string> SerialsJ { get; set; }
        }
    }
}
