﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    class Com_logs_ns
    {
        [JsonProperty("message")]
        public string message { get; set; }
        [JsonProperty("code")]
        public int code { get; set; }
        [JsonProperty("status")]
        public bool status { get; set; }
        [JsonProperty("data")]
        public DataNS[] data { get; set; }

        public class DataNS
        {
            [JsonProperty("recordType")]
            public string recordType { get; set; }
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("values")]
            public ValuesEmployee Values { get; set; }
           
        }

        public class ValuesEmployee
        {
            [JsonProperty("custrecord_trp_id_employee")]
            public ValuesIDEm[] custrecord_trp_id_employee { get; set; }
            [JsonProperty("custrecord_trp_error")]
            public string custrecord_trp_error { get; set; }
            [JsonProperty("custrecord_trp_numero_serie")]
            public string custrecord_trp_numero_serie { get; set; }
            [JsonProperty("custrecord_trp_request")]
            public string custrecord_trp_request { get; set; }

            [JsonProperty("custrecord_trp_proceso")]
            public string custrecord_trp_proceso { get; set; }
            [JsonProperty("custrecord_trp_tipo_movimiento")]
            public string custrecord_trp_tipo_movimiento { get; set; }
        }

        public class ValuesIDEm
        {
            [JsonProperty("value")]
            public string ValueName { get; set; }
            [JsonProperty("text")]
            public string Text { get; set; }
        }
    }
}
