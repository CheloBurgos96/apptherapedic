﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Text.Json;
using System.Threading;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class PhysicallInventoryMP : Form
    {
        public class Items
        {
            public string mode { get; set; }

            public string inventory { get; set; }

            public Datas[] codes { get; set; }
        }

        public class Datas
        {
            public string bin { get; set; }

            public string code { get; set; }
        }

        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Physic Inventory MP";
        List<string> SerialsError = new List<string>();
        public List<string> ListLabels = new List<string>();
        public EmployeeModel Employ;
        public List<string> NameBins = new List<string>();
        public List<int> idBins = new List<int>();
        public List<string> NameInventoryLog = new List<string>();
        public List<string> idInventoryLog = new List<string>();
        public List<string> idLocationInventory = new List<string>();
        List<string> Message = new List<string>();
        List<Datas> Serials = new List<Datas>();

        //ResletsOfNS Locations = new ResletsOfNS();
        QueryNS GetQueryFromNS = new QueryNS();

        public bool NotDebounce = false;

        string Serial = "";

        public int ValueIndex = 0;
        public int VIInventory = 0;
        public int CounterLabels = 0;
        public double QuantityOfZero = 0;
        public bool ItsSame = false;

        delegate void PanelColor();
        delegate void Labels();
        delegate void InsertData();

        //SerialsMP FoundS = new SerialsMP();
        //ResletsOfNS Location = new ResletsOfNS();

        public void Empl(EmployeeModel n)
        {
            try
            {
                Employ = n;

                cLog.Add("", NameScreen, "Asignación de permisos a las ubicaciones", false);
                CBInventoryLog.Items.Clear();
                CbBinInventory.Items.Clear();
                idBins.Clear();
                NameBins.Clear();

                var IR = GetQueryFromNS.ListInventory(Employ, true, NameScreen);
                if (IR.Item1)
                {
                    TakeInventoryRegister InventoryRegisters = IR.Item2;
                    for (int i = 0; i < InventoryRegisters.Data.Length; i++)
                    {
                        string[] valores = InventoryRegisters.Data[i].Text.Split(new string[] { " : " }, StringSplitOptions.RemoveEmptyEntries);
                        int Data = valores.Length;
                        string ListN = InventoryRegisters.Data[i].Value + " " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(valores[Data-1]);
                        NameInventoryLog.Add(ListN);
                        CBInventoryLog.Items.Add(ListN);
                        idInventoryLog.Add(InventoryRegisters.Data[i].Value);
                        idLocationInventory.Add(InventoryRegisters.Data[i].IDLocatation);
                    }
                    CBInventoryLog.Text = NameInventoryLog[0];
                    TakeBins(idLocationInventory[0]);
                }
                else
                {
                    cLog.Add("", NameScreen, "No hay datos registros de inventario", false);
                    SendData.Enabled = false;
                    CbBinInventory.Enabled = false;
                    CBInventoryLog.Enabled = false;
                    CodeBar.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                SendData.Enabled = false;
                CbBinInventory.Enabled = false;
                CBInventoryLog.Enabled = false;
                CodeBar.Enabled = false;
                cLog.Add("Line 87", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }
        }

        public PhysicallInventoryMP()
        {
            InitializeComponent();
        }

        private void TakeBins(string IDBIN)
        {
            CbBinInventory.Text = "";
            CbBinInventory.Items.Clear();
            idBins.Clear();
            NameBins.Clear();
            GetBinsService BinsTrans = GetQueryFromNS.TakeBin(IDBIN, Employ, "PhysicInventoryMP, Line 706").Item2;
            int Leng = BinsTrans.data.Length;
            if (Leng > 0)
            {
                for (int i = 0; i < Leng; i++)
                {
                    CbBinInventory.Items.Add(BinsTrans.data[i].name);
                    idBins.Add(BinsTrans.data[i].id);
                    NameBins.Add(BinsTrans.data[i].name);
                }
                CbBinInventory.Text = NameBins[0];
            }
            else
            {
                CbBinInventory.Text = "Bin vacio";
            }
        }

        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                InsertData n = new InsertData(ControlPanel);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                cLog.Add("Line 130", NameScreen, ex.Message, true);
            }
        }

        private void ControlPanel()
        {
            panel2.BackColor = Color.Black;
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            ActiveWait(false);
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ListLabels.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún {0} no son enviados {0}¿Desea enviarlos?", Environment.NewLine);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                /*if (DB.ThereAreLabelsNotSend())
                {
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if (result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {*/
                cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                this.Close();
                MainFrm AccF = new MainFrm();
                AccF.SetEmployeeInfo(Employ);
                AccF.Show();
                //}
            }
        }

        private void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 10)
                {
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    if (ItsSame)
                    {
                       string ShowDes = "";
                       txtBDes.Text = ShowDes;
                        if (ListLabels.Count > 0)
                        {
                            bool findIt = false;
                            foreach (var dto in ListLabels)
                            {
                                if (dto.Equals(Serial))
                                {
                                    findIt = true;
                                    break;
                                }
                            }
                            if (!findIt)
                            {
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                panel2.BackColor = Color.Green;
                                ListLabels.Add(Serial);
                                Serials.Add(new Datas { bin = idBins[ValueIndex].ToString(), code = Serial });
                                lblSerieScanned.Text = Serial;
                                CodeBar.Clear();
                            }
                            else
                            {
                                cLog.Add("", NameScreen, "Etiqueta ya ingresada", false);
                                panel2.BackColor = Color.Red;
                                CodeBar.Clear();
                                MessageBox.Show(new Form() { TopMost = true }, "Ya ingresó esa etiqueta.");
                                panel2.BackColor = Color.Black;
                            }
                        }
                        else
                        {
                            ThreadStart delegado = new ThreadStart(ChangeColor);
                            var t = new Thread(delegado);
                            t.Start();
                            panel2.BackColor = Color.Green;
                            ListLabels.Add(Serial);
                            Serials.Add(new Datas { bin = idBins[ValueIndex].ToString(), code = Serial });
                            lblSerieScanned.Text = Serial;
                            CodeBar.Clear();
                        }
                        CounterLabels = ListLabels.Count;
                        Counter.Text = CounterLabels.ToString();
                        CodeBar.Focus();
                        ItsSame = true;
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Bin no encontrado", false);
                        panel2.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true }, "Bin no encontrado.");
                        panel2.BackColor = Color.Black;
                    }
                }
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 269", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void CbBinInventory_Click(object sender, EventArgs e)
        {
            NotDebounce = false;
        }

        private void CbBinInventory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValueIndex = CbBinInventory.SelectedIndex;
            ItsSame = true;
        }

        private void CbBinInventory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int CountElements = 0;
                ItsSame = false;
                foreach (string i in NameBins)
                {
                    CountElements++;
                    if (i.Equals(CbBinInventory.Text))
                    {
                        ItsSame = true;
                        cLog.Add("", NameScreen, "Usuario cambio el bin destino", false);
                        CbBinInventory.SelectedIndex = CountElements - 1;
                        ValueIndex = CountElements - 1;
                        CodeBar.Text = "";
                        CodeBar.Focus();
                        //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },idBins[ValueIndex].ToString());
                        break;
                    }
                }
                if (!ItsSame)
                {
                    CbBinInventory.Text = "";
                    CbBinInventory.Focus();
                }
            }
        }

        private void ColorBlack()
        {
            panel2.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListLabels.Count;
            Counter.Text = CounterLabels.ToString();
            CodeBar.Text = "";
        }
        /*private void WQ()
        {
            try
            {
                string Body = Serial + ", \"devolution\": false";
                var Serials = GetQueryFromNS.QueryUbications(Body, Employ, 9, NameScreen);
                FoundS = Serials.Item3;//DB.FindDescriptionMP(CodeBar.Text);
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 152", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }*/

        private void ActiveWait(bool Active)
        {
            SendData.Enabled = Active;
            CbBinInventory.Enabled = Active;
            CBInventoryLog.Enabled = Active;
            CodeBar.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private void BGSend_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (ListLabels.Count > 0)
                {
                    cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                    var serialsToSend = new Items()
                    {
                        mode = "register",
                        inventory = idInventoryLog[VIInventory],
                        codes = Serials.ToArray()
                    };
                    string jsonString = JsonSerializer.Serialize(serialsToSend);
                    PanelColor BlackColor = new PanelColor(ColorBlack);
                    this.Invoke(BlackColor);
                    var Query = GetQueryFromNS.SendDataTransfer("", jsonString, 0, 0, Employ, 11, "Physic Inventory MP, Line 248");
                    if (Query.Item1)
                    {
                        cLog.Add("", NameScreen, "Información enviada a NS para el primer tipo de envio", false);
                    }
                    Message.Clear();
                    Message = Query.Item2;
                    Serials.Clear();
                    foreach (var serialP in Query.Item3)
                    {
                        if (!ListLabels.Contains(serialP))
                        {
                            SerialsError.Add(serialP);
                        }
                    }
                    ListLabels.Clear();
                    Labels PanelTxtC = new Labels(LPanel);
                    this.Invoke(PanelTxtC);
                }
                else
                {
                    cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                    MessageBox.Show(new Form() { TopMost = true }, "No hay operaciones a enviar.");
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                cLog.Add("Line 475", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }
        }

        private void BGSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            if (SerialsError.Count > 0)
            {
                for (int i = 0; i < SerialsError.Count; i++)
                {
                    if (SerialsError[i].Length > 0 || !SerialsError[i].Equals(null))
                    {
                        if (i < 1)
                        {
                            MessageBox.Show("Seriales no procesados:");
                        }
                        MessageBox.Show(new Form() { TopMost = true }, SerialsError[i]);
                    }
                }
                SerialsError.Clear();
            }
            
            ActiveWait(true);
            CodeBar.Focus();
        }

        private void CBInventoryLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            VIInventory = CBInventoryLog.SelectedIndex;
            TakeBins(idLocationInventory[VIInventory]);
        }
    }
}
