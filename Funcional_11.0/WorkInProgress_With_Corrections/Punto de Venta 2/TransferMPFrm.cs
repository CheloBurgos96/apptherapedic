﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public partial class TransferMPFrm : Form
    {
        delegate void PanelColor();
        delegate void Labels();
        delegate void FocusTxt();
        delegate void INDCB();
        delegate void InsertData();

        EmployeeModel Employ;
        SystemLogs cLog = new SystemLogs();
        ResletsOfNS Locations = new ResletsOfNS();
        DataBase DB = new DataBase(false);
        QueryNS GetQueryFromNS = new QueryNS();
        SerialsMP FoundS = new SerialsMP();

        string[] transfers = { "TH MP - PP", "TH MP - PP BASES", "TH MP - PP BCOS", "TH MP - MP BCOS", "TH MP - MP BASES", "TH MP - MQ", "BCOS MP - PP", "BCOS MP - MQ", "BCOS MP - MP TH", "BCOS PP - PT", "BASES MP - PP" };
        int[] IDOfTransfer = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        bool[] authorizationTransfer = new bool[11];
        bool[] transfer = { false, false, false, false, false, false, false, false, false, false, false };

        List<string> ListTotalLabels = new List<string>();
        List<string> ListLabels1 = new List<string>();
        List<string> ListLabels2 = new List<string>();
        List<string> ListLabels3 = new List<string>();
        List<string> ListLabels4 = new List<string>();
        List<string> ListLabels5 = new List<string>();
        List<string> ListLabels6 = new List<string>();
        List<string> ListLabels7 = new List<string>();
        List<string> ListLabels8 = new List<string>();
        List<string> ListLabels9 = new List<string>();
        List<string> ListLabels10 = new List<string>();
        List<string> ListLabels11 = new List<string>();
        List<string> IDOfTransferL = new List<string>();
        List<int> ID_OrderWork = new List<int>();
        List<string> OrderWorList = new List<string>();
        List<string> Message = new List<string>();

        private string NameScreen = "Transfer MP";
        string Serial = "";
        string SeriesNotSend = "\"serials\":[";

        bool NotDebounce = false;
        bool WithWorkOrder = false;
        bool ItsSame = false;

        double QuantityOfZero = 0;
        int CounterLabels = 0;
        int IndexCB = 0;
        int IndexCB2 = 0;

        public void Empl(EmployeeModel n)
        {
            Employ = n;
            
            cLog.Add("", NameScreen, "Asignación de permisos a las ubicaciones", false);
            authorizationTransfer[0] = Employ.TH_MP_PP;
            authorizationTransfer[1] = Employ.TH_MP_PP_Bases;
            authorizationTransfer[2] = Employ.TH_MP_PP_Bcos;
            authorizationTransfer[3] = Employ.TH_MP_MP_Bcos;
            authorizationTransfer[4] = Employ.TH_MP_MP_Bases;
            authorizationTransfer[5] = Employ.TH_MP_MQ;
            authorizationTransfer[6] = Employ.Bcos_MP_PP;
            authorizationTransfer[7] = Employ.Bcos_MP_MQ;
            authorizationTransfer[8] = Employ.Bcos_MP_MP_TH;
            authorizationTransfer[9] = Employ.Bcos_PP_PT;
            authorizationTransfer[10] = Employ.Bases_MP_PP;
            CBTransferMP.Items.Clear();
            for (int i = 0; i < authorizationTransfer.Length; i++)
            {
                if (authorizationTransfer[i] == true)
                {
                    CBTransferMP.Items.Add(transfers[i]);
                    IDOfTransferL.Add(IDOfTransfer[i].ToString());
                }
            }
            for (int i = 0; i < authorizationTransfer.Length; i++)
            {
                if (authorizationTransfer[i] == true)
                {
                    cLog.Add("", NameScreen, "Muestra de valor del Bin Origen inicial", false);
                    CBTransferMP.Text = transfers[i];
                    break;
                }
            }
            panel2.BackColor = Color.Black;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ListLabels1.Count > 0 || ListLabels2.Count > 0 || ListLabels3.Count > 0 || ListLabels4.Count > 0 || ListLabels5.Count > 0 || ListLabels6.Count > 0 || ListLabels7.Count > 0 || ListLabels8.Count > 0 || ListLabels9.Count > 0 || ListLabels10.Count > 0 || ListLabels11.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún {0} no son enviados {0}¿Desea enviarlos?", Environment.NewLine);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    //UpdateAll = false;
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                //UpdateAll = false;
                if (DB.ThereAreLabelsNotSend())
                {
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if (result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
            }
        }

        public void SetValues()
        {
            ListTotalLabels.Clear();
            ListLabels1.Clear();
            ListLabels2.Clear();
            ListLabels3.Clear();
            ListLabels4.Clear();
            ListLabels5.Clear();
            ListLabels6.Clear();
            ListLabels7.Clear();
            ListLabels8.Clear();
            ListLabels9.Clear();
            ListLabels10.Clear();
            ListLabels11.Clear();
            for (int i = 0; i < transfer.Length; i++)
            {
                transfer[i] = false;
            }
            NotDebounce = false;
            //CodeBar.Focus();
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            CBTransferMP.Enabled = false;
            WorkOrderL.Enabled = false;
            CodeBar.Enabled = false;
            SendData.Enabled = false;
            pictureBox2.Enabled = false;
            PWait.Visible = true;
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        public TransferMPFrm()
        {
            InitializeComponent();
        }

        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                InsertData n = new InsertData(ControlPanel);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                cLog.Add("Line 200", NameScreen, ex.Message, true);
            }
        }

        private void ControlPanel()
        {
            panel2.BackColor = Color.Black;
        }

        public void WQ()
        {
            try
            {
                string Body = Serial + ", \"devolution\": false";
                var Serials = GetQueryFromNS.QueryUbications(Body, Employ, 9, NameScreen);
                FoundS = Serials.Item3;
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 223", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void ActiveWait(bool Active)
        {
            CBTransferMP.Enabled = Active;
            WorkOrderL.Enabled = Active;
            CodeBar.Enabled = Active;
            SendData.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 10)
                {
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    bool QuantityZero = false, isSuccess = false;
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);

                    isSuccess = FoundS.IsSuccess;

                    if (isSuccess)
                    {
                        cLog.Add("", NameScreen, "El serial está disponible y se encuentra en la BD", false);
                        bool SerialFound = false, LBLZero = false;
                        string ShowDes = "";
                        if (!WithWorkOrder)
                        {
                            for (int i = 0; i < FoundS.Quantity; i++)
                            {
                                //MessageBox.Show(new Form() { TopMost = true }, "Location: " + Locations.idUbicationMP[CBTransferMP.SelectedIndex] + " LS: " + FoundS.Data[i].Location);
                                //if (idBinsArray[CbBinOrig.SelectedIndex].ToString().Equals(FoundS.Data[i].BinId))//Por si no funciona regresar a esta validación
                                if (Locations.idUbicationMP[CBTransferMP.SelectedIndex].Equals(FoundS.Data[i].Location))
                                {
                                    ShowDes = FoundS.Data[i].Description;
                                    LBLZero = FoundS.Data[i].LabelZero;
                                    SerialFound = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < FoundS.Quantity; i++)
                            {
                                //MessageBox.Show("LV: " + Locations.idMPToValidate + " LS: " + FoundS.Data[i].Location);
                                //if (ID_OrderWork[CbBinOrig.SelectedIndex].ToString().Equals(1))//Agregar con lo que se tenga de Ashanti
                                //if (FoundS.Data[i].BinId.Equals("8"))
                                if (FoundS.Data[i].Location.Equals(Locations.idMPToValidate))//Para productivo
                                //if(FoundS.Data[i].BinName.Equals("GENETAL-MPTH"))//Para sandbox
                                {
                                    ShowDes = FoundS.Data[i].Description;
                                    LBLZero = FoundS.Data[i].LabelZero;
                                    SerialFound = true;
                                    break;
                                }
                            }
                        }
                        

                        if (SerialFound)
                        {
                            cLog.Add("", NameScreen, "El serial está en la misma ubicación seleccionada", false);
                            txtBDes.Text = ShowDes;
                            NotDebounce = false;

                            if (LBLZero)
                            {
                                cLog.Add("", NameScreen, "El serial contiene cantidad cero", false);
                                QuantityZero = true;
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                panel2.BackColor = Color.Green;
                                using (QuantityMP ShowQ = new QuantityMP())
                                {
                                    var result = ShowQ.ShowDialog();
                                    if (result == DialogResult.OK)
                                    {
                                        if (!WithWorkOrder)
                                        {
                                            QuantityOfZero = ShowQ.CurrentQuantity;
                                            SeriesNotSend = "\"combination\": " + IDOfTransferL[CBTransferMP.SelectedIndex] + ", \"cero\": true, \"qty\": " + QuantityOfZero.ToString() + ", \"serials\":[\"" + Serial + "\"";
                                            cLog.Add("", NameScreen, "Envio de serie con cantidad cero", false);
                                            var Query = GetQueryFromNS.SendDataTransfer("", SeriesNotSend, 0, 0, Employ, 6, "Transfer MP, Line 471");
                                            if (Query.Item1)
                                            {
                                                cLog.Add("", NameScreen, "Envio de serie con cantidad cero, realizado", false);
                                                MessageBox.Show(new Form() { TopMost = true }, "Envío de etiqueta cero, hecho.");
                                            }
                                            else
                                            {
                                                List<string> Message2 = Query.Item2;
                                                for (int i = 0; i < Message2.Count; i++)
                                                {
                                                    MessageBox.Show(new Form() { TopMost = true }, Message2[i]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            QuantityOfZero = ShowQ.CurrentQuantity; 
                                            SeriesNotSend = ", \"orderWork\":" + ID_OrderWork[WorkOrderL.SelectedIndex] + ", \"cero\": true, \"qty\": " + QuantityOfZero.ToString() + ", \"serials\":[\"" + Serial + "\"";
                                            cLog.Add("", NameScreen, "Envio de serie con cantidad cero", false);
                                            var Query = GetQueryFromNS.SendDataTransfer("2", SeriesNotSend, 0, 0, Employ, 10, "Transfer MP, Line 471");
                                            if (Query.Item1)
                                            {
                                                cLog.Add("", NameScreen, "Envio de serie con cantidad cero, realizado", false);
                                                MessageBox.Show(new Form() { TopMost = true }, "Envío de etiqueta cero, hecho.");
                                            }
                                            else
                                            {
                                                List<string> Message2 = Query.Item2;
                                                for (int i = 0; i < Message2.Count; i++)
                                                {
                                                    MessageBox.Show(new Form() { TopMost = true }, Message2[i]);
                                                }
                                            }
                                        }
                                        CodeBar.Clear();
                                        CodeBar.Focus();
                                    }
                                }
                            }

                            if (!QuantityZero)
                            {
                                if (ListTotalLabels.Count > 0)
                                {
                                    bool findIt = false;
                                    foreach (var dto in ListTotalLabels)
                                    {
                                        if (dto.Equals(Serial))
                                        {
                                            findIt = true;
                                            break;
                                        }
                                    }


                                    if (!findIt)
                                    {
                                        ThreadStart delegado = new ThreadStart(ChangeColor);
                                        var t = new Thread(delegado);
                                        t.Start();
                                        panel2.BackColor = Color.Green;
                                        string LBL = Serial;
                                        if (CBTransferMP.SelectedItem.Equals(transfers[0]) && !transfer[0])
                                        {
                                            ListLabels1.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[1]) && !transfer[1])
                                        {
                                            ListLabels2.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[2]) && !transfer[2])
                                        {
                                            ListLabels3.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[3]) && !transfer[3])
                                        {
                                            ListLabels4.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[4]) && !transfer[4])
                                        {
                                            ListLabels5.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[5]) && !transfer[5])
                                        {
                                            ListLabels6.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[6]) && !transfer[6])
                                        {
                                            ListLabels7.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[7]) && !transfer[7])
                                        {
                                            ListLabels8.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[8]) && !transfer[8])
                                        {
                                            ListLabels9.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[9]) && !transfer[9])
                                        {
                                            ListLabels10.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }
                                        else if (CBTransferMP.SelectedItem.Equals(transfers[10]) && !transfer[10])
                                        {
                                            ListLabels11.Add(LBL);
                                            ListTotalLabels.Add(LBL);
                                            lblSerieScanned.Text = LBL;
                                        }

                                        CodeBar.Clear();
                                    }
                                    else
                                    {
                                        cLog.Add("", NameScreen, "Etiqueta ya ingresada", false);
                                        panel2.BackColor = Color.Red;
                                        CodeBar.Clear();
                                        MessageBox.Show(new Form() { TopMost = true },"Ya ingresó esa etiqueta.");
                                        panel2.BackColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    ThreadStart delegado = new ThreadStart(ChangeColor);
                                    var t = new Thread(delegado);
                                    t.Start();
                                    panel2.BackColor = Color.Green;
                                    string LBL = Serial;
                                    if (CBTransferMP.SelectedItem.Equals(transfers[0]))
                                    {
                                        ListLabels1.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[1]))
                                    {
                                        ListLabels2.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[2]))
                                    {
                                        ListLabels3.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[3]))
                                    {
                                        ListLabels4.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[4]))
                                    {
                                        ListLabels5.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[5]))
                                    {
                                        ListLabels6.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[6]))
                                    {
                                        ListLabels7.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[7]))
                                    {
                                        ListLabels8.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[8]))
                                    {
                                        ListLabels9.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[9]))
                                    {
                                        ListLabels10.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    else if (CBTransferMP.SelectedItem.Equals(transfers[10]))
                                    {
                                        ListLabels11.Add(LBL);
                                        ListTotalLabels.Add(LBL);
                                    }
                                    lblSerieScanned.Text = LBL;
                                    CodeBar.Clear();
                                }
                                CounterLabels = ListTotalLabels.Count;
                                Counter.Text = CounterLabels.ToString();
                            }
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Serial no pertenece a esta ubicación", false);
                            panel2.BackColor = Color.Red;
                            CodeBar.Clear();
                            MessageBox.Show(new Form() { TopMost = true },"Este serial no pertenece a esta ubicación.");
                            panel2.BackColor = Color.Black;
                        }
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Etiqueta no encontrada", false);
                        panel2.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true }, FoundS.ErrosQuery.ErrosQuery);
                        panel2.BackColor = Color.Black;
                    }
                    CodeBar.Focus();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                panel2.BackColor = Color.Red;
                cLog.Add("Line 499", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void CBTransferMP_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ListLabels1.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 0)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[0];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[0] + " primero");
                    }
                }
                else if (ListLabels2.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 1)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[1];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[1] + " primero");
                    }
                }
                else if (ListLabels3.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 2)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[2];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[2] + " primero");
                    }
                }
                else if (ListLabels4.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 3)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[3];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[3] + " primero");
                    }
                }
                else if (ListLabels5.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 4)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[4];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[4] + " primero");
                    }
                }
                else if (ListLabels6.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 5)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[5];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[5] + " primero");
                    }
                }
                else if (ListLabels7.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 6)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[6];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[6] + " primero");
                    }
                }
                else if (ListLabels8.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 7)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[7];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[7] + " primero");
                    }
                }
                else if (ListLabels9.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 8)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[8];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[8] + " primero");
                    }
                }
                else if (ListLabels10.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 9)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[9];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[9] + " primero");
                    }
                }
                else if (ListLabels11.Count > 0)
                {
                    for (int i = 0; i < transfer.Length; i++)
                    {
                        if (i != 10)
                        {
                            transfer[i] = true;
                        }
                        else
                        {
                            transfer[i] = false;
                        }
                    }
                    CBTransferMP.Text = transfers[10];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + transfers[10] + " primero");
                    }
                }
                
                if (!CBTransferMP.SelectedItem.Equals(transfers[0]))
                {
                    WorkOrderL.Enabled = false;
                    WithWorkOrder = false;
                }
                else
                {
                    WithWorkOrder = true;
                    WorkOrderL.Enabled = true;
                    var WO = GetQueryFromNS.TakeOrderWork(Employ, "Transfer MP, Line 739");
                    if (WO.Item1)
                    {
                        GetWorkOrder WOR = WO.Item2;
                        lblType.Text = "Orden de trabajo";
                        WorkOrderL.Items.Clear();
                        OrderWorList.Clear();
                        ID_OrderWork.Clear();
                        int Leng = WOR.Data.Length;
                        if (Leng > 0)
                        {
                            for (int i = 0; i < Leng; i++)
                            {
                                WorkOrderL.Items.Add(WOR.Data[i].Values);
                                OrderWorList.Add(WOR.Data[i].Values);
                                ID_OrderWork.Add(WOR.Data[i].WorkOrderID);
                            }
                            WorkOrderL.SelectedIndex = 0;
                        }
                        else
                        {
                            WorkOrderL.Text = "Orden de trabajo vacio";
                        }

                    }
                }

                if (!NotDebounce)
                {
                    cLog.Add("", NameScreen, "Cambio de ubicacion", false);
                }



            }
            catch (Exception ex)
            {
                cLog.Add("Line 703" + "", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
            
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void TransferMPFrm_Load(object sender, EventArgs e)
        {
        }

        private void ColorBlack()
        {
            panel2.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListTotalLabels.Count;
            Counter.Text = CounterLabels.ToString();
        }

        private void FocusT()
        {
            CodeBar.Focus();
        }

        private void CurrentIndex()
        {
            IndexCB2 = ID_OrderWork[WorkOrderL.SelectedIndex];
        }

        private void Tick()
        {
            TUpdateL.Interval = 60000;
            TUpdateL.Enabled = true;
            TUpdateL.Start();
        }

        private void BGWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            /*try
            {
                if (UpdateAll)
                {
                    TUpdateL.Stop();
                    cLog.Add("", NameScreen, "--Actualizar BD--", false);
                    GetQueryFromNS.UpdateMP(Employ, NameScreen);
                    cLog.Add("", NameScreen, "--BD Actualizada--", false);
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 771", NameScreen, ex.Message, true);
            }*/
        }

        private void BGWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            /*if (UpdateAll)
            {
                cLog.Add("", NameScreen, "Inicio Timer", false);
                Tick();
            }
            else
            {
                cLog.Add("", NameScreen, "--Trabajo cancelado--", false);
                TUpdateL.Stop();
            }*/
        }

        private void TUpdateL_Tick(object sender, EventArgs e)
        {
            /*cLog.Add("", NameScreen, "Comienzo de actualización", false);
            if (BGWork.IsBusy != true)
            {
                BGWork.RunWorkerAsync();
            }*/
        }

        private void BGSend_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                string DataList1 = "";
                SeriesNotSend = "\"serials\":[";
                if (ListLabels1.Count > 0 || ListLabels2.Count > 0 || ListLabels3.Count > 0 || ListLabels4.Count > 0 || ListLabels5.Count > 0 || ListLabels6.Count > 0 || ListLabels7.Count > 0 || ListLabels8.Count > 0 || ListLabels9.Count > 0 || ListLabels10.Count > 0 || ListLabels11.Count > 0)
                {
                    PanelColor BlackColor = new PanelColor(ColorBlack);
                    this.Invoke(BlackColor);
                    //CurrentIndex();
                    //
                    if (authorizationTransfer[0] && ListLabels1.Count > 0)
                    {
                        INDCB Number = new INDCB(CurrentIndex);
                        this.Invoke(Number);
                        foreach (var dto in ListLabels1)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial = SeriesNotSend.LastIndexOf(",");
                        int longitud = posInicial - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud);

                        string OrderComplete = ", \"orderWork\":" + IndexCB2 + ", " + SeriesNotSend;
                        if (ItsSame)
                        {
                            var Query = GetQueryFromNS.SendDataTransfer("1", OrderComplete, IndexCB2, 0, Employ, 10, "Transfer MP, Line 267");
                            
                            if (Query.Item1)
                            {
                                cLog.Add("", NameScreen, "Información enviada a NS para el primer tipo de envio", false);
                            }
                            Message.Clear();
                            Message = Query.Item2;
                        }
                        /*else
                        {
                            MessageBox.Show(new Form() { TopMost = true }, "Orden de trabajo no se encuentra");
                        }*/
                        
                    }

                    if (authorizationTransfer[1] && ListLabels2.Count > 0)
                    {
                        foreach (var dto in ListLabels2)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial2 = SeriesNotSend.LastIndexOf(",");
                        int longitud2 = posInicial2 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud2);
                        var Query = GetQueryFromNS.SendDataTransfer("2", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 180");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el segundo tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }

                    if (authorizationTransfer[2] && ListLabels3.Count > 0)
                    {
                        foreach (var dto in ListLabels3)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("3", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 196");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el tercer tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[3] && ListLabels4.Count > 0)
                    {
                        foreach (var dto in ListLabels4)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("4", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 211");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el cuarto tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[4] && ListLabels5.Count > 0)
                    {
                        foreach (var dto in ListLabels5)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("5", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 226");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el quinto tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[5] && ListLabels6.Count > 0)
                    {
                        foreach (var dto in ListLabels6)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("6", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 241");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el sexto tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[6] && ListLabels7.Count > 0)
                    {
                        foreach (var dto in ListLabels7)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("7", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 256");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el séptimo tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[7] && ListLabels8.Count > 0)
                    {
                        foreach (var dto in ListLabels8)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("8", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 271");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el octavo tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[8] && ListLabels9.Count > 0)
                    {
                        foreach (var dto in ListLabels9)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("9", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 286");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el noveno tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[9] && ListLabels10.Count > 0)
                    {
                        foreach (var dto in ListLabels10)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("10", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el décimo tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    if (authorizationTransfer[10] && ListLabels11.Count > 0)
                    {
                        foreach (var dto in ListLabels11)
                        {
                            DataList1 += "\"" + dto + "\",";
                        }
                        SeriesNotSend = SeriesNotSend + DataList1;
                        int posInicial3 = SeriesNotSend.LastIndexOf(",");
                        int longitud3 = posInicial3 - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud3);
                        var Query = GetQueryFromNS.SendDataTransfer("11", SeriesNotSend, IndexCB, 0, Employ, 1, "Transfer MP, Line 316");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el onceavo tipo de envio", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        /*for (int i = 0; i < Message.Count; i++)
                        {
                            MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                        }*/
                    }
                    SetValues();
                    Labels PanelTxtC = new Labels(LPanel);
                    this.Invoke(PanelTxtC);
                }
                else
                {
                    cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                    MessageBox.Show(new Form() { TopMost = true },"No hay operaciones a enviar.");
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                cLog.Add("Line 1051", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
            
        }

        private void BGSend_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            CBTransferMP.Enabled = true;
            WorkOrderL.Enabled = true;
            CodeBar.Enabled = true;
            SendData.Enabled = true;
            pictureBox2.Enabled = true;
            PWait.Visible = false;
            CodeBar.Focus();
        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }

        private void CBTransferMP_Click(object sender, EventArgs e)
        {
            NotDebounce = false;
        }

        private void CbBinOrig_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //if (ListLabels.Count == 0)
                //{
                //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },ListLabels);
                int CountElements = 0;
                ItsSame = false;
                foreach (string i in OrderWorList)
                {
                    CountElements++;
                    if (i.Equals(WorkOrderL.Text))
                    {
                        ItsSame = true;
                        cLog.Add("", NameScreen, "Usuario cambio el bin destino", false);
                        WorkOrderL.SelectedIndex = CountElements - 1;
                        CodeBar.Text = "";
                        CodeBar.Focus();
                        //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },idBins[ValueIndex].ToString());
                        break;
                    }
                }
                if (!ItsSame)
                {
                    WorkOrderL.Text = "";
                    WorkOrderL.Focus();
                }
                //}
            }
        }

        private void CbBinOrig_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItsSame = true;
        }
    }
}
