﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class WaitPanelSmall : Form
    {
        public WaitPanelSmall()
        {
            InitializeComponent();
        }

        private void WaitPanelSmall_Load(object sender, EventArgs e)
        {

        }

        public void CloseWait()
        {
            this.Close();
        }
    }
}
