﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public partial class TransferPTFrm : Form
    {
        delegate void PanelColor();
        delegate void Labels();
        delegate void FocusTxt();
        delegate void InsertData();

        DataBase DB = new DataBase(false);
        ResletsOfNS Locations = new ResletsOfNS();
        QueryNS GetQueryFromNS = new QueryNS();
        UbicationsPT FoundS = new UbicationsPT();
        EmployeeModel Employ;
        PlannedJaulasPT PublicJaulas;
        SerialsJPT SJPT = new SerialsJPT();
        SystemLogs cLog = new SystemLogs();

        List<string> ListNames = new List<string>();
        List<string> ListUbicationJaulas = new List<string>();
        List<string> ListIDJaulas = new List<string>();
        List<string> ListTotalLabels = new List<string>();
        List<string> ListLabels1 = new List<string>();
        List<string> ListLabels1b = new List<string>();
        List<string> ListLabels2 = new List<string>();
        List<string> ListLabels3 = new List<string>();
        List<string> ListLabels4 = new List<string>();
        List<string> ListLabels5 = new List<string>();
        List<string> ListLabels6 = new List<string>();
        List<string> ListLabels7 = new List<string>();
        List<string> ListLabels8 = new List<string>();
        List<string> NameBins = new List<string>();
        List<int> idBinsArray = new List<int>();
        List<string> Message = new List<string>();
        List<string> IDLocation = new List<string>();

        string[] transfers = { "TH PP - PT", "BASES PP - TRANS", "TRANS - PT TH", "TH PT - TALLER", "TH TALLER - PT", "JAULA - PP", "INSPECCION - TALLER", "INSPECCION - PT" };
        bool[] authorizationTransfer = new bool[8];
        bool[] transfer = new bool[8];

        private string NameScreen = "Transfer PT";
        string Serial = "";
        string Series_TH_PP_PT = "\"serials\":[";
        string Series_BASES_PP_TRANS = "\"serials\":[";
        string Series_TRANS_PT_TH = "\"serials\":[";
        string Series_TH_PT_TALLER = "\"serials\":[";
        string Series_TH_TALLER_PP = "\"serials\":[";
        string Series_JAULA_PP = "\"serials\":[";
        string Series_INSPECCION_TALLER = "\"serials\":[";
        string Series_INSPECCION_PT = "\"serials\":[";
        bool NotDebounce = false;
        bool NotDebounce2 = false;
        bool UpdateAll = true;
        bool FindJaulaPT = false;
        bool PPtoPT = false;
        int CounterLabels = 0;
        int IndexJaula = 0;

        public void Empl(EmployeeModel n)
        {
            try
            {
                Employ = n;

                var Q = GetQueryFromNS.TakeJaulasPT(Employ, NameScreen);

                bool ThereAreData = Q.Item1;

                CBJaula.Items.Clear();

                if (ThereAreData)
                {
                    PlannedJaulasPT Jaulas = PublicJaulas = Q.Item2;
                    int Leng = Jaulas.Jaulas.Length;
                    for (int i = 0; i < Leng; i++)
                    {
                        ListIDJaulas.Add(Jaulas.Jaulas[i].BinID);
                        CBJaula.Items.Add(Jaulas.Jaulas[i].BinName);
                        ListNames.Add(Jaulas.Jaulas[i].BinName);
                        if (ListUbicationJaulas.Count > 0)
                        {
                            bool FindIt = false;
                            foreach (var dto in new List<string>(ListUbicationJaulas))
                            {
                                if (dto.Equals(Jaulas.Jaulas[i].UbicationJaula))
                                {
                                    FindIt = true;
                                    break;
                                }
                            }
                            if (!FindIt)
                            {
                                ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                            }
                        }
                        else
                        {
                            ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                        }
                    }
                    CBJaula.Text = ListNames[IndexJaula];
                }
                cLog.Add("", NameScreen, "Asignación de permisos a las ubicaciones", false);
                authorizationTransfer[0] = Employ.TH_PP_PT;
                authorizationTransfer[1] = Employ.Bases_PP_Trans;
                authorizationTransfer[2] = Employ.Trans_PT_TH;
                authorizationTransfer[3] = Employ.TH_PT_TALLER;
                authorizationTransfer[4] = Employ.TH_TALLER_PT;
                authorizationTransfer[5] = Employ.JAULA_PP;
                authorizationTransfer[6] = Employ.INSPECCION_TALLER;
                authorizationTransfer[7] = Employ.INSPECCION_PT;
                for (int i = 0; i < authorizationTransfer.Length; i++)
                {
                    if (authorizationTransfer[i] == true)
                    {
                        CBTransferPT.Items.Add(transfers[i]);
                        IDLocation.Add(Locations.allIDLocationPT[i]);
                    }
                }
                for (int i = 0; i < authorizationTransfer.Length; i++)
                {
                    if (authorizationTransfer[i] == true)
                    {
                        CBTransferPT.Text = transfers[i];
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                CBTransferPT.Enabled = false;
                CodeBar.Enabled = false;
                SendData.Enabled = false;
                pictureBox2.Enabled = false;
                CBJaula.Enabled = false;
                CBJaula.Visible = false;
                label4.Visible = false;
                cLog.Add("Line 153", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }

        }

        private void ValueFalse(int j)
        {
            for (int i = 0; i < 8; i++)
            {
                if (j == i)
                {
                    transfer[i] = false;
                }
                else
                {
                    transfer[i] = true;
                }
            }
        }

        public TransferPTFrm()
        {
            InitializeComponent();
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ListLabels1.Count > 0)
                {
                    ValueFalse(0);
                    CBTransferPT.Text = transfers[0];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[0] + " primero");
                    }
                }
                if (ListLabels2.Count > 0)
                {
                    ValueFalse(1);
                    CBTransferPT.Text = transfers[1];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[1] + " primero");
                    }
                }
                if (ListLabels3.Count > 0)
                {
                    ValueFalse(2);
                    CBTransferPT.Text = transfers[2];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[2] + " primero");
                    }
                }
                if (ListLabels4.Count > 0)
                {
                    ValueFalse(3);
                    CBTransferPT.Text = transfers[3];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[3] + " primero");
                    }
                }
                if (ListLabels5.Count > 0)
                {
                    ValueFalse(4);
                    CBTransferPT.Text = transfers[4];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[4] + " primero");
                    }
                }
                if (ListLabels6.Count > 0)
                {
                    ValueFalse(5);
                    CBTransferPT.Text = transfers[5];

                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[5] + " primero");
                    }
                }
                if (ListLabels7.Count > 0)
                {
                    ValueFalse(6);
                    CBTransferPT.Text = transfers[6];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[6] + " primero");
                    }
                }
                if (ListLabels8.Count > 0)
                {
                    ValueFalse(7);
                    CBTransferPT.Text = transfers[7];
                    if (!NotDebounce)
                    {
                        NotDebounce = true;
                        MessageBox.Show(new Form() { TopMost = true }, "Envie los datos de " + transfers[7] + " primero");
                    }
                }

                for (int w = 0; w < transfers.Length; w++)
                {
                    if (CBTransferPT.SelectedItem.Equals(transfers[w]))
                    {
                        ResletsOfNS Locations = new ResletsOfNS();
                        QueryNS GetQueryFromNS = new QueryNS();
                        GetBinsService BinsTrans = GetQueryFromNS.TakeBin(Locations.allIDLocationPT[w], Employ, "Transfer PT, Line 706").Item2;
                        idBinsArray.Clear();
                        NameBins.Clear();
                        int Leng = BinsTrans.data.Length;
                        if (Leng > 0)
                        {
                            int i;
                            for (i = 0; i < Leng; i++)
                            {
                                idBinsArray.Add(BinsTrans.data[i].id);
                                NameBins.Add(BinsTrans.data[i].name);
                            }
                        }
                    }
                }

                if (CBTransferPT.SelectedItem.ToString().Equals(transfers[5]))
                {
                    CBJaula.Enabled = true;
                    CBJaula.Visible = true;
                    FindJaulaPT = true;
                    label4.Visible = true;

                    label1.Location = new Point(12, 124);
                    CodeBar.Location = new Point(12, 142);

                    panel4.Location = new Point(12, 163);
                    label6.Location = new Point(12, 166);
                    lblSerieScanned.Location = new Point(130, 166);
                    label2.Location = new Point(12, 185);
                    Counter.Location = new Point(90, 185);
                    SendData.Location = new Point(83, 204);
                    txtBDes.Location = new Point(12, 239);
                    PWait.Location = new Point(83, 245);
                }
                else
                {
                    CBJaula.Enabled = false;
                    CBJaula.Visible = false;
                    FindJaulaPT = false;
                    label4.Visible = false;

                    label1.Location = new Point(12, 82);
                    CodeBar.Location = new Point(12, 100);

                    panel4.Location = new Point(12, 121);
                    label6.Location = new Point(12, 124);
                    lblSerieScanned.Location = new Point(130, 124);
                    label2.Location = new Point(12, 143);
                    Counter.Location = new Point(90, 143);
                    SendData.Location = new Point(83, 163);
                    txtBDes.Location = new Point(12, 198);
                    PWait.Location = new Point(83, 204);
                }

                if (CBTransferPT.SelectedItem.ToString().Equals(transfers[0]))
                {
                    PPtoPT = true;
                }
                else
                {
                    PPtoPT = false;
                }

                if (!NotDebounce)
                {
                    cLog.Add("", NameScreen, "Cambio de ubicacion", false);
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 332", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }
        }

        private void ChangeColor()
        {
            Thread.Sleep(2000);
            InsertData n = new InsertData(ControlPanel);
            this.Invoke(n);
        }
        
        private void ControlPanel()
        {
            panel2.BackColor = Color.Black;
        }

        private void WQ()
        {
            try
            {
                if (!FindJaulaPT)
                {
                    var Serials = GetQueryFromNS.QueryUbications(Serial, Employ, 10, NameScreen);
                    FoundS = Serials.Item1;
                }
                else
                {
                    var Serials = GetQueryFromNS.QueryUbications(PublicJaulas.Jaulas[IndexJaula].BinID, Employ, 8, NameScreen);
                    SJPT = Serials.Item5;
                }
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 367", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void ActiveWait(bool Active)
        {
            CBTransferPT.Enabled = Active;
            CodeBar.Enabled = Active;
            SendData.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 11)
                {
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    string ShowDes = "";
                    bool IsSuccess = false;
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);
                    if (!FindJaulaPT)
                    {
                        IsSuccess = FoundS.IsSuccess;
                    }
                    else
                    {
                        IsSuccess = SJPT.IsSuccess;
                    }


                    if (IsSuccess || ShowDes.Length > 0)
                    {
                        cLog.Add("", NameScreen, "El serial está disponible y se encuentra en la BD", false);
                        bool SerialFound = false;
                        if (!FindJaulaPT)
                        {
                            if (!PPtoPT)
                            {
                                foreach (var name in idBinsArray)
                                {
                                    for (int i = 0; i < FoundS.Quantity; i++)
                                    {
                                        if (name.ToString().Equals(FoundS.Series[i].binID))
                                        {

                                            ShowDes = FoundS.Series[i].item;
                                            SerialFound = true;
                                            break;
                                        }
                                    }
                                    if (SerialFound)
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < FoundS.Quantity; i++)
                                {
                                    if (FoundS.Series[i].binID.Equals("452"))
                                    {
                                        ShowDes = FoundS.Series[i].item;
                                        SerialFound = true;
                                        break;
                                    }
                                }
                            }                            
                        }
                        else
                        {
                            for (int j = 0; j < SJPT.Serial.Serials.Length; j++)
                            {
                                foreach (string serial in SJPT.Serial.Serials[j].SerialsJ)
                                {
                                    if (Serial.Equals(serial))
                                    {
                                        SerialFound = true;
                                        break;
                                    }
                                }
                                if (SerialFound)
                                {
                                    break;
                                }
                            }
                        }

                        if (SerialFound)
                        {
                            cLog.Add("", NameScreen, "El serial está en la misma ubicación que el serial", false);
                            txtBDes.Text = ShowDes;
                            NotDebounce = false;
                            NotDebounce2 = false;

                            if (ListTotalLabels.Count > 0)
                            {
                                bool findIt = false;
                                foreach (var dto in ListTotalLabels)
                                {
                                    if (dto.Equals(Serial))
                                    {
                                        findIt = true;
                                        break;
                                    }
                                }

                                if (!findIt)
                                {
                                    ThreadStart delegado = new ThreadStart(ChangeColor);
                                    var t = new Thread(delegado);
                                    t.Start();
                                    panel2.BackColor = Color.Green;
                                    if (CBTransferPT.SelectedItem.Equals(transfers[0]) && !transfer[0])
                                    {
                                        ListLabels1.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[1]) && !transfer[1])
                                    {
                                        ListLabels2.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[2]) && !transfer[2])
                                    {
                                        ListLabels3.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[3]) && !transfer[3])
                                    {
                                        ListLabels4.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[4]) && !transfer[4])
                                    {
                                        ListLabels5.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[5]) && !transfer[5])
                                    {
                                        ListLabels6.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[6]) && !transfer[6])
                                    {
                                        ListLabels7.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    else if (CBTransferPT.SelectedItem.Equals(transfers[7]) && !transfer[7])
                                    {
                                        ListLabels8.Add(Serial);
                                        ListTotalLabels.Add(Serial);
                                    }
                                    lblSerieScanned.Text = Serial;
                                    CodeBar.Clear();
                                }
                                else
                                {
                                    cLog.Add("", NameScreen, "Etiqueta ya ingresada", false);
                                    panel2.BackColor = Color.Red;
                                    CodeBar.Clear();
                                    MessageBox.Show(new Form() { TopMost = true }, "Ya ingresó esa etiqueta.");
                                    panel2.BackColor = Color.Black;
                                }
                            }
                            else
                            {
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                panel2.BackColor = Color.Green;
                                if (CBTransferPT.SelectedItem.Equals(transfers[0]) && !transfer[0])
                                {
                                    ListLabels1.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[1]) && !transfer[1])
                                {
                                    ListLabels2.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[2]) && !transfer[2])
                                {
                                    ListLabels3.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[3]) && !transfer[3])
                                {
                                    ListLabels4.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[4]) && !transfer[4])
                                {
                                    ListLabels5.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[5]) && !transfer[5])
                                {
                                    ListLabels6.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[6]) && !transfer[6])
                                {
                                    ListLabels7.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                else if (CBTransferPT.SelectedItem.Equals(transfers[7]) && !transfer[7])
                                {
                                    ListLabels8.Add(Serial);
                                    ListTotalLabels.Add(Serial);
                                }
                                lblSerieScanned.Text = Serial;
                                CodeBar.Clear();
                            }
                            CounterLabels = ListTotalLabels.Count;
                            Counter.Text = CounterLabels.ToString();
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Serial no pertenece a esta ubicación", false);
                            panel2.BackColor = Color.Red;
                            CodeBar.Clear();
                            MessageBox.Show(new Form() { TopMost = true }, "Este serial no pertenece a esta ubicación.");
                            panel2.BackColor = Color.Black;
                        }
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Etiqueta no encontrada", false);
                        panel2.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true }, "No se encuentra esa etiqueta.");
                        panel2.BackColor = Color.Black;
                    }
                    CodeBar.Focus();
                }
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 269", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }

        }        

        private void SendData_Click(object sender, EventArgs e)
        {
            CBTransferPT.Enabled = false;
            CodeBar.Enabled = false;
            SendData.Enabled = false;
            pictureBox2.Enabled = false;
            PWait.Visible = true;
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        public void SetValues()
        {
            ListTotalLabels.Clear();
            ListLabels1.Clear();
            ListLabels1b.Clear();
            ListLabels2.Clear();
            ListLabels3.Clear();
            ListLabels4.Clear();
            ListLabels5.Clear();
            ListLabels6.Clear();
            ListLabels7.Clear();
            ListLabels8.Clear();
            for (int i = 0; i < 8; i++)
            {
                transfer[i] = false;
            }
            NotDebounce = false;
            NotDebounce2 = false;
            CodeBar.Text = "";
        }


        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ListLabels1.Count > 0 || ListLabels1b.Count > 0 || ListLabels2.Count > 0 || ListLabels3.Count > 0 || ListLabels4.Count > 0 || ListLabels5.Count > 0 || ListLabels6.Count > 0 || ListLabels7.Count > 0 || ListLabels8.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún {0} no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                //Alert.Empl(Employ, strMensaje, false);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    UpdateAll = false;
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                UpdateAll = false;
                //DataBase Query = new DataBase();
                if (DB.ThereAreLabelsNotSend())
                {
                    //this.Close();
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea enviarlos?", Environment.NewLine);
                    //Alert.Empl(Employ, strMensaje, false);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvpios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if (result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }

            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TransferPTFrm_Load(object sender, EventArgs e)
        {
            
        }


        private void ColorBlack()
        {
            panel2.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListTotalLabels.Count;
            Counter.Text = CounterLabels.ToString();
            //CodeBar.Text = "";
        }

        private void Tick()
        {
            TUpdateL.Interval = 60000;
            TUpdateL.Enabled = true;
            TUpdateL.Start();
        }


        private void BGWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (UpdateAll)
                {
                    TUpdateL.Stop();
                    cLog.Add("", NameScreen, "--Actualizar BD--", false);
                    QueryNS GetQueryFromNS = new QueryNS();
                    GetQueryFromNS.UpdatePT(Employ, NameScreen, true);
                    cLog.Add("", NameScreen, "--BD Actualizada--", false);
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 757", NameScreen, ex.Message, true);
            }
        }

        private void BGWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (UpdateAll)
            {
                cLog.Add("", NameScreen, "Inicio Timer", false);
                Tick();
            }
            else
            {
                cLog.Add("", NameScreen, "--Trabajo cancelado--", false);
                TUpdateL.Stop();
            }
            
        }

        private void TUpdateL_Tick(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Comienzo de actualización", false);
            if (BGWork.IsBusy != true)
            {
                BGWork.RunWorkerAsync();
            }
        }


        private void BGSend_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                string DataList1 = "";
                string DataList2 = "";
                string DataList3 = "";
                string DataComplete = "{ \"areFinishedGoods\":[ ";
                Series_TH_PP_PT = "\"serials\":[";
                Series_BASES_PP_TRANS = "\"serials\":[";
                Series_TRANS_PT_TH = "\"serials\":[";
                Series_TH_PT_TALLER = "\"serials\":[";
                Series_TH_TALLER_PP = "\"serials\":[";
                Series_JAULA_PP = "\"serials\":[";
                Series_INSPECCION_TALLER = "\"serials\":[";
                Series_INSPECCION_PT = "\"serials\":[";

                if (ListLabels1.Count > 0 || ListLabels1b.Count > 0 || ListLabels2.Count > 0 || ListLabels3.Count > 0 || ListLabels4.Count > 0 || ListLabels5.Count > 0 || ListLabels6.Count > 0 || ListLabels7.Count > 0 || ListLabels8.Count > 0)
                {
                    PanelColor BlackColor = new PanelColor(ColorBlack);
                    this.Invoke(BlackColor);
                    if (authorizationTransfer[0])
                    {
                        if (ListLabels1.Count > 0)
                        {
                            foreach (var dto in ListLabels1)
                            {
                                DataList1 += "\"" + dto + "\",";
                            }
                            Series_TH_PP_PT += DataList1;
                            int posInicial = Series_TH_PP_PT.LastIndexOf(",");
                            int longitud = posInicial - Series_TH_PP_PT.IndexOf("\"s");
                            Series_TH_PP_PT = Series_TH_PP_PT.Substring(0, longitud);
                            DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 1 ," + Series_TH_PP_PT + "]}";
                            var Query = GetQueryFromNS.SendDataTransfer("1", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 747");
                            if (Query.Item1)
                            {
                                cLog.Add("", NameScreen, "Envío completo PT 1", false);
                            }
                            Message.Clear();
                            Message = Query.Item2;
                        }
                    }

                    if (authorizationTransfer[1] && ListLabels2.Count > 0)
                    {
                        foreach (var dto in ListLabels2)
                        {
                            DataList2 += "\"" + dto + "\",";
                        }
                        Series_BASES_PP_TRANS += DataList2;
                        int posInicial2 = Series_BASES_PP_TRANS.LastIndexOf(",");
                        int longitud2 = posInicial2 - Series_BASES_PP_TRANS.IndexOf("\"s");
                        Series_BASES_PP_TRANS = Series_BASES_PP_TRANS.Substring(0, longitud2);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 2, " + Series_BASES_PP_TRANS + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("2", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 277");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 2", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[2] && ListLabels3.Count > 0)
                    {
                        foreach (var dto in ListLabels3)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_TRANS_PT_TH = Series_TRANS_PT_TH + DataList3;
                        int posInicial3 = Series_TRANS_PT_TH.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_TRANS_PT_TH.IndexOf("\"s");
                        Series_TRANS_PT_TH = Series_TRANS_PT_TH.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 3 , " + Series_TRANS_PT_TH + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("3", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 3", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[3] && ListLabels4.Count > 0)
                    {
                        foreach (var dto in ListLabels4)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_TH_PT_TALLER = Series_TH_PT_TALLER + DataList3;
                        int posInicial3 = Series_TH_PT_TALLER.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_TH_PT_TALLER.IndexOf("\"s");
                        Series_TH_PT_TALLER = Series_TH_PT_TALLER.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 4 ," + Series_TH_PT_TALLER + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("4", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 4", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[4] && ListLabels5.Count > 0)
                    {
                        foreach (var dto in ListLabels5)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_TH_TALLER_PP = Series_TH_TALLER_PP + DataList3;
                        int posInicial3 = Series_TH_TALLER_PP.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_TH_TALLER_PP.IndexOf("\"s");
                        Series_TH_TALLER_PP = Series_TH_TALLER_PP.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 5 , " + Series_TH_TALLER_PP + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("5", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 5", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[5] && ListLabels6.Count > 0)
                    {
                        foreach (var dto in ListLabels6)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_JAULA_PP = Series_JAULA_PP + DataList3;
                        int posInicial3 = Series_JAULA_PP.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_JAULA_PP.IndexOf("\"s");
                        Series_JAULA_PP = Series_JAULA_PP.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 6, \"idJaula\": " + ListIDJaulas[IndexJaula] + " , " + Series_JAULA_PP + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("6", DataComplete, 0, 0, Employ, 7, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 6", false);
                            foreach (string serie in ListLabels6) {
                                DB.EreaseJPT(serie);
                            }
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[6] && ListLabels7.Count > 0)
                    {
                        foreach (var dto in ListLabels7)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_INSPECCION_TALLER = Series_INSPECCION_TALLER + DataList3;
                        int posInicial3 = Series_INSPECCION_TALLER.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_INSPECCION_TALLER.IndexOf("\"s");
                        Series_INSPECCION_TALLER = Series_INSPECCION_TALLER.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 7 , " + Series_INSPECCION_TALLER + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("7", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 7", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }

                    if (authorizationTransfer[7] && ListLabels8.Count > 0)
                    {
                        foreach (var dto in ListLabels8)
                        {
                            DataList3 += "\"" + dto + "\",";
                        }
                        Series_INSPECCION_PT = Series_INSPECCION_PT + DataList3;
                        int posInicial3 = Series_INSPECCION_PT.LastIndexOf(",");
                        int longitud3 = posInicial3 - Series_INSPECCION_PT.IndexOf("\"s");
                        Series_INSPECCION_PT = Series_INSPECCION_PT.Substring(0, longitud3);
                        DataComplete += "{ \"idEmployee\":" + Employ.RoleId + " , \"combination\": 8 , " + Series_INSPECCION_PT + "]}";
                        var Query = GetQueryFromNS.SendDataTransfer("8", DataComplete, 0, 0, Employ, 2, "Transfer PT, Line 301");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Envío completo PT 8", false);
                        }
                        Message.Clear();
                        Message = Query.Item2;
                    }
                    SetValues();
                    Labels PanelTxtC = new Labels(LPanel);
                    this.Invoke(PanelTxtC);
                }
                else
                {
                    cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                    MessageBox.Show(new Form() { TopMost = true },"No hay operaciones a enviar.");
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 961", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
        }

        private void BGSend_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            CBTransferPT.Enabled = true;
            CodeBar.Enabled = true;
            SendData.Enabled = true;
            pictureBox2.Enabled = true;
            PWait.Visible = false;
            CodeBar.Focus();
        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }

        private void CBJaula_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListLabels1.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels2.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels3.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels4.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels5.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels6.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels7.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            if (ListLabels8.Count > 0)
            {
                CBJaula.SelectedIndex = IndexJaula;
                if (!NotDebounce2)
                {
                    NotDebounce2 = true;
                    MessageBox.Show(new Form() { TopMost = true },"Envie los datos de " + CBJaula.Text + " primero");
                }
            }
            IndexJaula = CBJaula.SelectedIndex;
        }

        private void CBTransferPT_Click(object sender, EventArgs e)
        {
            NotDebounce = false;
        }

        private void CBJaula_Click(object sender, EventArgs e)
        {
            NotDebounce2 = false;
        }
    }
}