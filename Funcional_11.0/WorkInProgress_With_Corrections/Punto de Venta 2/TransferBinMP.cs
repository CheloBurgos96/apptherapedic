﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class TransferBinMP : Form
    {
        delegate void PanelColor();
        delegate void Labels();
        delegate void FocusTxt();
        delegate void INDC();
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Transfer BIN ";
        public EmployeeModel Employ;
        delegate void InsertData();
        public List<string> ListLabels = new List<string>();

        //public List<int> idBins = new List<int>();
        public List<int> idBinsDest = new List<int>();

        public List<string> authorizationTransfer = new List<string>();

        public List<string> NameBinsInserts = new List<string>();
        public List<string> IDBinsInserts = new List<string>();

        List<string> BINDest = new List<string>();

        List<string> Message = new List<string>();

        public string SeriesNotSend = "\"series\":[";
        public int CounterLabels = 0;
        //
        public bool ItsSame = false;
        public bool Once = false;
        public bool WriteCb = false;
        public int INDCB = 0;
        public bool UpdateAll = true;
        public int ValueIndex = 0;
        public int Index = 0;
        public double QuantityOfZero = 0;
        SerialsMP FoundS = new SerialsMP();
        string Serial = "";
        ResletsOfNS Location = new ResletsOfNS();
        QueryNS GetQueryFromNS = new QueryNS();


        DataBase DB = new DataBase(false);

        public void Empl(EmployeeModel n)
        {
            try
            {
                Employ = n;
                
                cLog.Add("", NameScreen, "Toma de datos de bines de las ubicaciones", false);
                cbBinDest.Items.Clear();
                for (int j = 0; j < Location.idQueryMPB.Length; j++)
                {
                    var Q = GetQueryFromNS.TakeBin(Location.idQueryMPB[j].ToString(), Employ, "Transfer Bin, Line 23");
                    if (Q.Item1)
                    {
                        GetBinsService BinsTrans = Q.Item2;
                        int Leng = BinsTrans.data.Length;
                        if (Leng > 0)
                        {
                            for (int i = 0; i < Leng; i++)
                            {
                                cbBinDest.Items.Add(BinsTrans.data[i].name);
                                idBinsDest.Add(BinsTrans.data[i].id);
                                authorizationTransfer.Add(BinsTrans.data[i].name);
                                BINDest.Add(BinsTrans.data[i].name);
                            }
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "No hay datos registros de bines", false);
                            MessageBox.Show(new Form() { TopMost = true },"No hay registros de bins.");
                        }
                    }
                }

                if (cbBinDest.Items.Count < 2)
                {
                    cLog.Add("", NameScreen, "No hay suficientes depósitos para realizar la transferencia", false);
                    cbBinDest.Enabled = false;
                    CodeBar.Enabled = false;
                    SendData.Enabled = false;
                    MessageBox.Show(new Form() { TopMost = true },"No hay registros de bins.");
                }
                else
                {
                    cbBinDest.SelectedIndex = 0;
                    cbBinDest.Focus();
                }
            }
            catch (Exception ex)
            {
                cbBinDest.Enabled = false;
                CodeBar.Enabled = false;
                SendData.Enabled = false;
                cLog.Add("Line 114", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }

        }

        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                InsertData n = new InsertData(ControlPanel);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                cLog.Add("Line 130", NameScreen, ex.Message, true);
            }
        }

        private void ControlPanel()
        {
            panel1.BackColor = Color.Black;
        }

        private void WQ()
        {
            try
            {
                string Body = Serial + ", \"devolution\": false"; 
                var Serials = GetQueryFromNS.QueryUbications(Body, Employ, 9, NameScreen);
                FoundS = Serials.Item3;//DB.FindDescriptionMP(CodeBar.Text);
            }
            catch(Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 152", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void ActiveWait(bool Active)
        {
            SendData.Enabled = Active;
            cbBinDest.Enabled = Active;
            CodeBar.Enabled = Active;
            Exit.Enabled = Active;
            PWait.Visible = !Active;
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            //QueryNS GetQueryFromNS = new QueryNS();
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 10)
                {
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    bool QuantityZero = false;
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    string BinN = "", IDBin = "";
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);
                    if (ItsSame)
                    {
                        if (FoundS.IsSuccess)
                        {
                            string ShowDes = "";
                            bool FoundIt = false, LBLZero = false;
                            int ValidateBinFound = 0;
                            bool IcanSend = false;

                            for (int i = 0; i < FoundS.Quantity; i++)
                            {
                                //MessageBox.Show("Bin select: " + cbBinDest.SelectedItem.ToString() +  " Bin del servicio: " + FoundS.Data[i].BinName);
                                //if (cbBinDest.SelectedItem.Equals(FoundS.Data[i].BinName))
                                if(idBinsDest[cbBinDest.SelectedIndex].ToString().Equals(FoundS.Data[i].BinId))
                                {
                                    ValidateBinFound = i;
                                    FoundIt = true;
                                    //IcanSend = true;
                                    break;
                                }
                                else
                                {
                                    //Este es para productivo
                                    if (FoundS.Data[i].Location.Equals(Location.idMPToValidate))
                                    {
                                        IcanSend = true;
                                        ShowDes = FoundS.Data[i].Description;
                                        LBLZero = FoundS.Data[i].LabelZero;
                                        BinN = FoundS.Data[i].BinName;
                                        IDBin = FoundS.Data[i].BinId;
                                    }//*/
                                }
                            }
                            

                            if (IcanSend)
                            {
                                cLog.Add("", NameScreen, "El serial está disponible", false);
                                if (!FoundIt)
                                {
                                    if (LBLZero)
                                    {
                                        cLog.Add("", NameScreen, "El serial contiene cantidad cero", false);
                                        QuantityZero = true;
                                        ThreadStart delegado = new ThreadStart(ChangeColor);
                                        var t = new Thread(delegado);
                                        t.Start();
                                        panel1.BackColor = Color.Green;
                                        using (QuantityMP ShowQ = new QuantityMP())
                                        {
                                            var result = ShowQ.ShowDialog();
                                            if (result == DialogResult.OK)
                                            {
                                                QuantityOfZero = ShowQ.CurrentQuantity;
                                                SeriesNotSend = "\"cero\": true, \"qty\": " + QuantityOfZero.ToString() + ", \"serials\":[\"" + Serial + "\"";
                                                string Body = "{\"idEmployee\":" + Employ.RoleId + " , \"binDest\":\"" + idBinsDest[ValueIndex] + "\"," + SeriesNotSend + "]}";
                                                //MessageBox.Show(new Form() { TopMost = true }, Body);
                                                cLog.Add("", NameScreen, "Envio de serie con cantidad cero", false);
                                                var Query = GetQueryFromNS.SendDataTransfer("1", Body, 0, 0, Employ, 8, "Transfer MP, Line 254");
                                                if (Query.Item1)
                                                {
                                                    cLog.Add("", NameScreen, "Envio de serie con cantidad cero, realizado", false);
                                                    MessageBox.Show(new Form() { TopMost = true }, "Envío de etiqueta cero, hecho.");
                                                }
                                                else
                                                {
                                                    List<string> Message2 = Query.Item2;
                                                    for (int i = 0; i < Message2.Count; i++)
                                                    {
                                                        MessageBox.Show(new Form() { TopMost = true }, Message2[i]);
                                                    }
                                                }

                                                CodeBar.Clear();
                                                CodeBar.Focus();
                                            }
                                        }
                                    }
                                    if (!QuantityZero)
                                    {
                                        cLog.Add("", NameScreen, "El serial está en el mismo bin seleccionado", false);
                                        txtBDes.Text = ShowDes;
                                        if (ListLabels.Count > 0)
                                        {
                                            bool findIt = false;
                                            foreach (var dto in ListLabels)
                                            {
                                                if (dto.Equals(Serial))
                                                {
                                                    findIt = true;
                                                    break;
                                                }
                                            }
                                            if (!findIt)
                                            {
                                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                                var t = new Thread(delegado);
                                                t.Start();
                                                panel1.BackColor = Color.Green;
                                                ListLabels.Add(Serial);
                                                NameBinsInserts.Add(BinN);
                                                IDBinsInserts.Add(IDBin);
                                                lblSerieScanned.Text = Serial;
                                                CodeBar.Clear();
                                            }
                                            else
                                            {
                                                panel1.BackColor = Color.Red;
                                                CodeBar.Clear();
                                                MessageBox.Show(new Form() { TopMost = true }, "Ya ingresó esa etiqueta.");
                                                panel1.BackColor = Color.Black;
                                            }
                                        }
                                        else
                                        {
                                            ThreadStart delegado = new ThreadStart(ChangeColor);
                                            var t = new Thread(delegado);
                                            t.Start();
                                            panel1.BackColor = Color.Green;
                                            ListLabels.Add(Serial);
                                            NameBinsInserts.Add(BinN);
                                            IDBinsInserts.Add(IDBin);
                                            lblSerieScanned.Text = Serial;
                                            CodeBar.Clear();
                                        }
                                        CounterLabels = ListLabels.Count;
                                        Counter.Text = CounterLabels.ToString();
                                    }
                                }
                                else
                                {
                                    cLog.Add("", NameScreen, "Serial pertenece a este bin", false);
                                    panel1.BackColor = Color.Red;
                                    CodeBar.Clear();
                                    MessageBox.Show(new Form() { TopMost = true }, "Serial pertenece a este bin.");
                                    panel1.BackColor = Color.Black;
                                }
                            }
                            else
                            {
                                if (!FoundIt)
                                {
                                    cLog.Add("", NameScreen, "Serial no está en esta ubicación", false);
                                    panel1.BackColor = Color.Red;
                                    CodeBar.Clear();
                                    MessageBox.Show(new Form() { TopMost = true }, "Serial no está en esta ubicación.");
                                    panel1.BackColor = Color.Black;
                                }
                                else
                                {
                                    cLog.Add("", NameScreen, "Serial pertenece a este bin", false);
                                    panel1.BackColor = Color.Red;
                                    CodeBar.Clear();
                                    MessageBox.Show(new Form() { TopMost = true }, "Serial pertenece a este bin.");
                                    panel1.BackColor = Color.Black;
                                }
                                   
                            }
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Etiqueta no encontrada", false);
                            panel1.BackColor = Color.Red;
                            CodeBar.Clear();
                            MessageBox.Show(new Form() { TopMost = true },FoundS.ErrosQuery.ErrosQuery);
                            panel1.BackColor = Color.Black;
                        }
                        CodeBar.Focus();
                        ItsSame = true;
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Bin no encontrado", false);
                        panel1.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true },"Bin no encontrado.");
                        panel1.BackColor = Color.Black;
                    }
                }
            }
            catch (Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 269", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
            
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            SendData.Enabled = false;
            cbBinDest.Enabled = false;
            CodeBar.Enabled = false;
            Exit.Enabled = false;
            PWait.Visible = true;
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        public TransferBinMP()
        {
            InitializeComponent();
        }

        

        private void Exit_Click(object sender, EventArgs e)
        {
            if (ListLabels.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún {0} no son enviados {0}¿Desea enviarlos?", Environment.NewLine);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    UpdateAll = false;
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                UpdateAll = false;
                if (DB.ThereAreLabelsNotSend())
                {
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvpios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if (result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
            }
        }

        private void cbBinDest_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValueIndex = cbBinDest.SelectedIndex;
            ItsSame = true;
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void cbBinDest_Click(object sender, EventArgs e)
        {
            Once = false;
        }

        private void TransferBinMP_Load(object sender, EventArgs e)
        {
            if (!(idBinsDest.Count > 0))
            {
                cLog.Add("", NameScreen, "Error al obtener datos de bines, ubicaciones y seriales", false);
                MessageBox.Show(new Form() { TopMost = true },"Error al cargar los datos.");
            }
        }
        private void ColorBlack()
        {
            panel1.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListLabels.Count;
            Counter.Text = CounterLabels.ToString();
            CodeBar.Text = "";
        }

        private void Tick()
        {
            TUpdateL.Interval = 60000;
            TUpdateL.Enabled = true;
            TUpdateL.Start();
        }

        private void BGSend_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (ListLabels.Count > 0)
                {
                    bool MessageUser = false; int counter = 0;
                    cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                    string DataList = "";
                    SeriesNotSend = "\"serials\":[";
                    List<string> ListSupport = new List<string>();
                    List<string> ListSupport2 = new List<string>();
                    for (int i = 0; i < ListLabels.Count; i++)
                    {
                        if (!idBinsDest[ValueIndex].Equals(IDBinsInserts[i]))
                        {
                            DataList += "\"" + ListLabels[i] + "\",";
                            counter++;
                        }
                        else
                        {
                            ListSupport.Add(ListLabels[i]);
                            ListSupport2.Add(IDBinsInserts[i]);
                            MessageUser = true;
                        }
                    }
                    if (MessageUser)
                    {
                        string strMensaje = string.Format("Hay seriales que pertenecen a este{0}depósito, no se enviarán esos datos.", Environment.NewLine);
                        //MessageBox.Show(new Form() { TopMost = true },strMensaje);
                        foreach (string data in ListSupport)
                        {
                            MessageBox.Show(new Form() { TopMost = true },data);
                        }
                    }
                    if (counter > 0)
                    {
                        SeriesNotSend = SeriesNotSend + DataList;
                        int posInicial = SeriesNotSend.LastIndexOf(",");
                        int longitud = posInicial - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud);
                        PanelColor BlackColor = new PanelColor(ColorBlack);
                        this.Invoke(BlackColor);
                        QueryNS GetQueryFromNS = new QueryNS();
                        string Body = "{\"idEmployee\":" + Employ.RoleId + " , \"binDest\":\"" + idBinsDest[ValueIndex] + "\"," + SeriesNotSend + "]}";
                        //MessageBox.Show(Body);
                        var Query = GetQueryFromNS.SendDataTransfer("2", Body, 0, 0, Employ, 8, "Transfer Bin MP, Line 439");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el primer tipo de envio", false);

                            if (ListSupport.Count > 0)
                            {
                                ListLabels.Clear();
                                IDBinsInserts.Clear();
                                ListLabels = ListSupport;
                                IDBinsInserts = ListSupport2;
                            }
                        }
                        Message.Clear();
                        Message = Query.Item2;

                        if (!MessageUser)
                        {
                            ListLabels.Clear();
                            IDBinsInserts.Clear();
                        }
                        Labels PanelTxtC = new Labels(LPanel);
                        this.Invoke(PanelTxtC);
                    }
                    else
                    {
                        MessageBox.Show(new Form() { TopMost = true },"No se pueden enviar los seriales.");
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                    MessageBox.Show(new Form() { TopMost = true },"No hay operaciones a enviar.");
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                cLog.Add("Line 475", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
        }

        private void BGSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            SendData.Enabled = true;
            cbBinDest.Enabled = true;
            CodeBar.Enabled = true;
            Exit.Enabled = true;
            PWait.Visible = false;
            CodeBar.Focus();
        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }


        private void cbBinDest_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (ListLabels.Count == 0)
                {
                    int CountElements = 0;
                    bool BinNF = false;
                    foreach (string i in BINDest)
                    {
                        CountElements++;
                        if (i.Equals(cbBinDest.Text))
                        {
                            BinNF = true;
                            ItsSame = true;
                            cLog.Add("", NameScreen, "Usuario cambio el bin destino", false);
                            cbBinDest.SelectedIndex = CountElements - 1;
                            ValueIndex = CountElements - 1;
                            CountElements = 0;
                            CodeBar.Text = "";
                            CodeBar.Focus();
                            break;
                        }
                    }
                    if (!BinNF)
                    {
                        ItsSame = false;
                        cbBinDest.Text = "";
                        cbBinDest.Focus();
                    }
                }
            }
        }
    }
}
