﻿
namespace NetSuiteApps
{
    partial class EmbolsadoFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmbolsadoFrm));
            this.CodeBar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Indicador = new System.Windows.Forms.PictureBox();
            this.LB = new System.Windows.Forms.Label();
            this.CantidadCol = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBDes = new System.Windows.Forms.TextBox();
            this.lblSerieScanned = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PWait = new System.Windows.Forms.PictureBox();
            this.BGWorkEmbol = new System.ComponentModel.BackgroundWorker();
            this.BGWait = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.Indicador)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).BeginInit();
            this.SuspendLayout();
            // 
            // CodeBar
            // 
            this.CodeBar.BackColor = System.Drawing.SystemColors.ControlText;
            this.CodeBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeBar.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CodeBar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.CodeBar.Location = new System.Drawing.Point(36, 150);
            this.CodeBar.Name = "CodeBar";
            this.CodeBar.Size = new System.Drawing.Size(480, 86);
            this.CodeBar.TabIndex = 4;
            this.CodeBar.Text = "123";
            this.CodeBar.Click += new System.EventHandler(this.CodeBar_Click);
            this.CodeBar.TextChanged += new System.EventHandler(this.CodeBar_TextChanged);
            this.CodeBar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CodeBar_KeyPress);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(25, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(542, 85);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ingresar código:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Indicador
            // 
            this.Indicador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Indicador.Image = ((System.Drawing.Image)(resources.GetObject("Indicador.Image")));
            this.Indicador.Location = new System.Drawing.Point(0, 0);
            this.Indicador.Name = "Indicador";
            this.Indicador.Size = new System.Drawing.Size(730, 588);
            this.Indicador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Indicador.TabIndex = 7;
            this.Indicador.TabStop = false;
            this.Indicador.Click += new System.EventHandler(this.Indicador_Click);
            // 
            // LB
            // 
            this.LB.AutoSize = true;
            this.LB.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.LB.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LB.Location = new System.Drawing.Point(25, 346);
            this.LB.Name = "LB";
            this.LB.Size = new System.Drawing.Size(318, 86);
            this.LB.TabIndex = 8;
            this.LB.Text = "Contador:";
            this.LB.Click += new System.EventHandler(this.LB_Click);
            // 
            // CantidadCol
            // 
            this.CantidadCol.AutoSize = true;
            this.CantidadCol.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CantidadCol.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.CantidadCol.Location = new System.Drawing.Point(392, 346);
            this.CantidadCol.Name = "CantidadCol";
            this.CantidadCol.Size = new System.Drawing.Size(72, 86);
            this.CantidadCol.TabIndex = 9;
            this.CantidadCol.Text = "0";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel1.Controls.Add(this.PWait);
            this.panel1.Controls.Add(this.txtBDes);
            this.panel1.Controls.Add(this.lblSerieScanned);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CodeBar);
            this.panel1.Controls.Add(this.LB);
            this.panel1.Controls.Add(this.CantidadCol);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(570, 650);
            this.panel1.TabIndex = 10;
            // 
            // txtBDes
            // 
            this.txtBDes.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtBDes.Enabled = false;
            this.txtBDes.Font = new System.Drawing.Font("Segoe UI", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBDes.ForeColor = System.Drawing.SystemColors.Window;
            this.txtBDes.Location = new System.Drawing.Point(9, 457);
            this.txtBDes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBDes.Multiline = true;
            this.txtBDes.Name = "txtBDes";
            this.txtBDes.Size = new System.Drawing.Size(555, 203);
            this.txtBDes.TabIndex = 11;
            // 
            // lblSerieScanned
            // 
            this.lblSerieScanned.AutoSize = true;
            this.lblSerieScanned.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSerieScanned.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblSerieScanned.Location = new System.Drawing.Point(36, 260);
            this.lblSerieScanned.Name = "lblSerieScanned";
            this.lblSerieScanned.Size = new System.Drawing.Size(63, 86);
            this.lblSerieScanned.TabIndex = 10;
            this.lblSerieScanned.Text = "-";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel4.Location = new System.Drawing.Point(36, 239);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(500, 8);
            this.panel4.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(570, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(730, 62);
            this.panel2.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(261, 65);
            this.label3.TabIndex = 18;
            this.label3.Text = "Embolsado";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(663, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Indicador);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(570, 62);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(730, 588);
            this.panel3.TabIndex = 12;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // PWait
            // 
            this.PWait.Image = global::NetSuiteApps.Properties.Resources.cargando;
            this.PWait.Location = new System.Drawing.Point(105, 201);
            this.PWait.Name = "PWait";
            this.PWait.Size = new System.Drawing.Size(367, 377);
            this.PWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PWait.TabIndex = 12;
            this.PWait.TabStop = false;
            this.PWait.Visible = false;
            // 
            // BGWorkEmbol
            // 
            this.BGWorkEmbol.WorkerReportsProgress = true;
            this.BGWorkEmbol.WorkerSupportsCancellation = true;
            this.BGWorkEmbol.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGWorkEmbol_DoWork);
            this.BGWorkEmbol.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BGWorkEmbol_ProgressChanged);
            this.BGWorkEmbol.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGWorkEmbol_RunWorkerCompleted);
            // 
            // BGWait
            // 
            this.BGWait.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGWait_DoWork);
            this.BGWait.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGWait_RunWorkerCompleted);
            // 
            // EmbolsadoFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.MaximizeBox = false;
            this.Name = "EmbolsadoFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Embolsado";
            this.Load += new System.EventHandler(this.EscBForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Indicador)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox CodeBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Indicador;
        private System.Windows.Forms.Label LB;
        private System.Windows.Forms.Label CantidadCol;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblSerieScanned;
        internal System.Windows.Forms.PictureBox pictureBox2;
        private System.ComponentModel.BackgroundWorker BGWorkEmbol;
        private System.Windows.Forms.TextBox txtBDes;
        private System.ComponentModel.BackgroundWorker BGWait;
        private System.Windows.Forms.PictureBox PWait;
    }
}