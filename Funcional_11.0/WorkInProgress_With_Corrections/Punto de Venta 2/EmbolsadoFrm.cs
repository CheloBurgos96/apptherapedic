﻿using iText.Layout.Element;
using System;
using System.Drawing;
using System.Windows.Forms;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using System.Threading;

namespace NetSuiteApps
{
    public partial class EmbolsadoFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Embolsado";
        string DescriptionMat = "", CD = "";
        int Counter = 0;
        public EmployeeModel Employ;
        delegate void ShowGreen ();
        delegate void ShowYellow();
        delegate void ShowRed();
        delegate void ShowGray();
        DataBase DB = new DataBase(false);

        public void Empl(EmployeeModel n)
        {
            Employ = n;
            //AskForNewData();
        }

        /*private void AskForNewData()
        {
            DateTime Date1 = DateTime.Now.Date;
            DateTime Date2 = DateTime.Now.AddDays(-1);
            QueryNS QueryData = new QueryNS();
            string[] result = Date1.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            string[] result2 = Date2.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            try
            {
                cLog.Add("", NameScreen, "Inicio de petición de etiquetas nuevas para BD", false);
                QueryData.LabelsToPrint(result2[0], result[0], false, null, "Embolsado - Line 32");
                cLog.Add("", NameScreen, "Final de petición de etiquetas nuevas para BD", false);
            }
            catch (Exception ex)
            {
                cLog.Add("", NameScreen, ex.ToString(), false);
                MessageBox.Show("Error al solicitar datos");
            }
        }*/

        public EmbolsadoFrm()
        {
            InitializeComponent();
            this.CenterToScreen();
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = new System.Drawing.Point(0, 0);
            /*Descomentar si se quiere cambiar motor de base de datos del programa
             y cambiarlo por el motor que desee, SQLite o SQL Server*/
            
            //SQLServerDataBase Contador = new SQLServerDataBase();
            cLog.Add("", NameScreen, "Toma de datos de contador de colchones", false);
            CantidadCol.Text = DB.CounterDB("count_mattress", "counter_mattress_logs");
            Counter = Convert.ToInt32(CantidadCol.Text);
        }

        private void EscBForm_Load(object sender, EventArgs e)
        {

        }


        private void CodeBar_TextChanged(object sender, EventArgs e)
        {
            if(BGWorkEmbol.IsBusy != true && CodeBar.Text.Length > 10)
            {
                PWait.Visible = true;
                cLog.Add("", NameScreen, "Ingresó el valor del serial a encontrar", false);
                BGWorkEmbol.RunWorkerAsync();
            }
            CodeBar.Focus();
        }

        //Agregar a metodo enviar datos en services netsuite
        

        public void ImprimirEtiquetas(WorkInProgressModel datos)
        {
            cLog.Add("", NameScreen, "Impresión de etiquetas en proceso", false);
            //MessageBox.Show(new Form() { TopMost = true },"Imprimiendo Serie: " + Ser);
            /*Descomentar si se quiere cambiar motor de base de datos del programa
             y cambiarlo por el motor que desee, SQLite o SQL Server*/
            //DataBase Consultas = new DataBase();
            //SQLServerDataBase Consultas = new SQLServerDataBase();
            //WorkInProgressModel datos = DB.TablesDatas(Ser);
            cLog.Add("", NameScreen, "Generación de archivo para impresión.", false);
            string nameF = "EMBOL_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
            cLog.Add("", NameScreen, "Generación de tabla de información de la etiqueta", false);
            CreateTables(nameF, datos);
            cLog.Add("", NameScreen, "Impresión de etiquetas", false);
            //MessageBox.Show(new Form() { TopMost = true },"Etiqueta impresa");
            PrintLabels Printlbl = new PrintLabels();
            while (true)
            {
                if (Printlbl.Print(nameF, Employ.RoleId, "Bagged"))
                {
                    cLog.Add("", NameScreen, "Etiqueta impresa", false);
                    DialogResult dialog;
                    dialog = MessageBox.Show("¿Se imprimió la etiqueta?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialog == DialogResult.Yes)
                    {
                        //proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        cLog.Add("", NameScreen, "Proceso de impresión finalizado", false);
                        Printlbl.KillAdobe("AcroRd32");
                        cLog.Add("", NameScreen, "Proceso de cerrar Adobe finalizado", false);
                        break;
                    }
                    //MessageBox.Show(new Form() { TopMost = true },"Etiqueta impresa");
                }
                else
                {
                    cLog.Add("", NameScreen, "Error al tratar de imprimir", false);
                    MessageBox.Show("Error al imprimir");
                    break;
                }
            }
            ShowRed FCodeBar = new ShowRed(FocuCode);
            this.Invoke(FCodeBar);
        }

        private void CreateTables(string nameF, WorkInProgressModel datos)
        {
            CodeBar Serie = new CodeBar(nameF,false);
            int SBL = 5;
            float ang = 4.71239f;
            float CodeBarSerieW = 285.3848f;
            float CodeBarSerieH = 84.9359f; //56.45f = 2.07cm // 74.53f = 
            float CellBarSerieH = 90f; //92.3699f;
            float CellSKUH = 54.97f; //175f = 6.332cm //201.80f = 
            float CellDesW = 120.357798f;
            float CellInfo = 80f;
            float CellX = 109f;

            if (datos.Serie != null && datos.SKU != null && datos.Serie != "" && datos.SKU != "")
            {
                Table table = Serie.CreateOneTableInsert2(4);
                iText.Layout.Element.Image SerieSM = Serie.CodeBarStick(datos.Serie, SBL, 6, 0, CodeBarSerieW, CodeBarSerieH);
                SerieSM.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.RIGHT);
                Cell cell1 = new Cell(2, 2).Add(SerieSM);
                cell1.SetHeight(CellBarSerieH);
                cell1.SetRotationAngle(4.71239f);
                cell1.SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                table.AddCell(cell1);
                //Para cambiar el largo es necesario quitar los 490 que tiene esta parte.
                Paragraph Description = Serie.CreateParagraph(datos.Descripcion, ang, 35f, 490f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Description.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.RIGHT);
                Description.SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                Cell cell2 = new Cell(4, 1).Add(Description);
                cell2.SetWidth(CellDesW);
                cell2.SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);
                table.AddCell(cell2);

                iText.Layout.Element.Image SKU = Serie.CodeBarStick2(datos.SKU, 0, 200f, 40f);
                Paragraph SKUT = Serie.CreateParagraph(datos.SKU, 0, 10f, 200f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                SKUT.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                Cell cell3 = new Cell().Add(SKUT).Add(SKU);
                cell3.SetHeight(CellSKUH);
                cell3.SetRotationAngle(4.71239f);
                table.AddCell(cell3);

                Paragraph ProductType = Serie.CreateParagraph(datos.TipoProducto, ang, 40f, 90f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell4 = new Cell().Add(ProductType);
                cell4.SetHeight(CellBarSerieH).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                table.AddCell(cell4);

                Paragraph Customer1 = Serie.CreateParagraph(datos.Customer1, ang, 18f, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell5 = new Cell().Add(Customer1);
                cell5.SetHeight(CellInfo);
                table.AddCell(cell5);

                Paragraph Customer2 = Serie.CreateParagraph(datos.Customer2, ang, 18f, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell6 = new Cell().Add(Customer2);
                table.AddCell(cell6);

                Paragraph ProductDimen = Serie.CreateParagraph(datos.MedidaProducto, ang, 40f, 200f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell7 = new Cell(2, 1).Add(ProductDimen).SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER); ;
                table.AddCell(cell7);
                //Agregar luego a aqui abajo ----------> datos.Reparacion
                Paragraph Repair = Serie.CreateParagraph(datos.Reparacion, ang, 18f, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell8 = new Cell().Add(Repair).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER); ;
                cell8.SetHeight(CellInfo);
                table.AddCell(cell8);

                Paragraph ProductSell = Serie.CreateParagraph(datos.OrdenVenta, ang, 10f, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell9 = new Cell().Add(ProductSell).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER); ;
                table.AddCell(cell9);

                Paragraph Customer3 = Serie.CreateParagraph(datos.Customer3, ang, 20f, 115f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                Cell cell10 = new Cell(1, 4).Add(Customer3);
                cell10.SetHeight(CellX).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER); ;
                table.AddCell(cell10);

                Serie.CloseDocument(true);
            }
            else
            {
                cLog.Add("", NameScreen, "Datos de serie vacios", false);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(Employ.RoleId, Date[0], Date[1], "Data Ticket Empty", "Bagged");
                MessageBox.Show("Valores vacios para crear etiqueta. Consulte con un técnico si el error persiste.");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Indicador_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Regreso al menú principal de la aplicación", false);
            this.Close();
            MainFrm AccF = new MainFrm();
            AccF.SetEmployeeInfo(Employ);
            AccF.Show();
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en el cuadro de texto para seriales", false);
            CodeBar.Clear();
        }

        private void LB_Click(object sender, EventArgs e)
        {

        }

        private void Green()
        {
            Bitmap verde = new Bitmap(Properties.Resources.Circulo_verde);
            Indicador.Image = verde;
        }

        private void Yellow()
        {
            Bitmap amarillo = new Bitmap(Properties.Resources.Circulo_Amarillo);
            Indicador.Image = amarillo;
        }

        private void Red()
        {
            Bitmap rojo = new Bitmap(Properties.Resources.Circulo_Rojo);
            Indicador.Image = rojo;
        }

        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                ShowGray n = new ShowGray(Grey);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(new Form() { TopMost = true },ex.ToString());
                cLog.Add("Line 246", NameScreen, ex.Message, true);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(Employ.RoleId, Date[0], Date[1], ex.ToString(), "Bagged");
                //MessageBox.Show(new Form() { TopMost = true },"Un error en la pantalla a ocurrido");
            }
        }

        private void Grey()
        {
            Bitmap gris = new Bitmap(Properties.Resources.Circulo_Gris);
            Indicador.Image = gris;
            //CodeBar.Text = "";
            //CodeBar.Focus();
        }

        private void TakeString()
        {
            //DataBase Consultas = new DataBase();
            txtBDes.Text = DescriptionMat;
        }

        private void ActualString()
        {
            lblSerieScanned.Text = CD;
        }

        private void Erase()
        {
            CodeBar.Text = "";
            CodeBar.Enabled = false;
        }

        private void CountMat()
        {
            Counter++;
            CantidadCol.Text = Counter.ToString();
        }

        private void FocuCode()
        {
            //CodeBar.Text = "";
            CodeBar.Enabled = true;
            CodeBar.Focus();
        }

        private void VisibleWait()
        {
            
        }

        private void NotVisibleWait()
        {

        }

        private void BGWorkEmbol_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                cLog.Add("", NameScreen, "Iniciar consultas de información de impresión en BD", false);
                if (CodeBar.Text != "" && CodeBar.Text.Length > 10)
                {

                    QueryNS Reprint = new QueryNS();
                    CD = CodeBar.Text;

                    ShowGreen LabelB2 = new ShowGreen(Erase);
                    this.Invoke(LabelB2);

                    var findIt = "SELECT double_bag, finished, counter FROM mattress_logs WHERE unique_serie = '" + CD + "'";
                    cLog.Add("", NameScreen, "Consulta de datos en la BD", false);
                    var InforSerial = Reprint.ReprintPT(CD, Employ, "Embol print - Line 297");
                    if (InforSerial.Result)
                    {
                        WorkInProgressModel data = new WorkInProgressModel();

                        data.Serie = CD;
                        data.Descripcion = InforSerial.Data.Descripcion;
                        data.SKU = InforSerial.Data.SKU;
                        data.TipoProducto = InforSerial.Data.TipoProducto;
                        data.Customer1 = InforSerial.Data.Customer1;
                        data.Customer2 = InforSerial.Data.Customer2;
                        data.Customer3 = InforSerial.Data.Customer3;
                        data.MedidaProducto = InforSerial.Data.MedidaProducto;
                        data.doubleBag = InforSerial.Data.DoubleBag;
                        if (InforSerial.Data.Reparacion)
                        {
                            data.Reparacion = "999";
                        }
                        else
                        {
                            data.Reparacion = "";
                        }
                        data.OrdenVenta = InforSerial.Data.OrdenVenta;

                        if (InforSerial.Data.ReprintLabel)
                        {
                            cLog.Add("", NameScreen, "Recepción de respues de NS aceptada, procediendo a reimprimir", false);
                            ImprimirEtiquetas(data);
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Serial encontrado dentro de la BD", false);
                            ShowGreen LabelA = new ShowGreen(ActualString);
                            this.Invoke(LabelA);
                            //lblSerieScanned.Text = CD;
                            cLog.Add("", NameScreen, "Proceso de captura de información de la seria obtenida de la BD", false);
                            WorkInProgressModel datos = DB.TablesDatas(CD);
                            DescriptionMat = data.Descripcion;
                            ShowGreen LabelDes = new ShowGreen(TakeString);
                            this.Invoke(LabelDes);

                            if (data.doubleBag == true && datos.Terminado == false && datos.Contador == 0)
                            {
                                cLog.Add("", NameScreen, "Etiqueta contiene bolsa y es escaneado por primera vez", false);
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                ShowYellow ImageYellow = new ShowYellow(Yellow);
                                this.Invoke(ImageYellow);
                                cLog.Add("", NameScreen, "Envío de la serie a NS para indicar impresión ", false);
                                var Print = Reprint.SendDataNSEmbol(CD, Employ, "Embol print - Line 280");
                                if (Print.Item1)
                                {
                                    cLog.Add("", NameScreen, "Recepción de respues de NS aceptada, procediendo a imprimir", false);
                                    DB.UpDateDB("UPDATE mattress_logs SET Finished = 1, counter = 2 WHERE unique_serie = '" + CD + "'");
                                    ShowGreen LabelC = new ShowGreen(CountMat);
                                    this.Invoke(LabelC);
                                    //Counter++;
                                    DB.UpDateDB("UPDATE counter_mattress_logs SET count_mattress = " + Counter.ToString());
                                    cLog.Add("", NameScreen, "Proceso de impresión de la etiqueta", false);
                                    ImprimirEtiquetas(data);
                                }
                                else
                                {
                                    var t2 = new Thread(delegado);
                                    t2.Start();
                                    ShowRed ImageRed = new ShowRed(Red);
                                    this.Invoke(ImageRed);
                                    cLog.Add("", NameScreen, "Envío de información de serie para reimprimir", false);
                                    MessageBox.Show(Print.Item2.Message);
                                }
                            }
                            if (datos.doubleBag == true && datos.Terminado == true && datos.Contador == 2)
                            {
                                cLog.Add("", NameScreen, "Etiqueta tiene doble bolsa y no es escaneada por primera vez", false);
                                ThreadStart delegado2 = new ThreadStart(ChangeColor);
                                var t1 = new Thread(delegado2);
                                t1.Start();
                                ShowRed ImageRed = new ShowRed(Red);
                                this.Invoke(ImageRed);
                            }
                            if (data.doubleBag == false && datos.Terminado == false && datos.Contador == 0)
                            {
                                cLog.Add("", NameScreen, "Etiqueta no contiene bolsa y es escaneado por primera vez", false);
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                ShowGreen ImageGreen = new ShowGreen(Green);
                                this.Invoke(ImageGreen);
                                cLog.Add("", NameScreen, "Envío de la serie a NS para indicar impresión ", false);
                                var Print = Reprint.SendDataNSEmbol(CD, Employ, "Embol print - Line 324");
                                if (Print.Item1)
                                {
                                    cLog.Add("", NameScreen, "Recepción de respues de NS aceptada, procediendo a imprimir", false);
                                    DB.UpDateDB("UPDATE mattress_logs SET finished = 1, counter = 1 WHERE unique_serie = '" + CD + "'");
                                    ShowGreen LabelC = new ShowGreen(CountMat);
                                    this.Invoke(LabelC);
                                    //Counter++;
                                    DB.UpDateDB("UPDATE counter_mattress_logs SET count_mattress = " + Counter.ToString());
                                    ImprimirEtiquetas(data);
                                }
                                else
                                {
                                    var t2 = new Thread(delegado);
                                    t2.Start();
                                    ShowRed ImageRed = new ShowRed(Red);
                                    this.Invoke(ImageRed);
                                    MessageBox.Show(Print.Item2.Message);
                                }
                            }
                            if (datos.doubleBag == false && datos.Terminado == true && datos.Contador == 1)
                            {
                                cLog.Add("", NameScreen, "Etiqueta no contiene bolsa y no es escaneado por primera vez", false);
                                //cLog.Add("", NameScreen, "Recepción de respuesta NS aceptada, no se reimprime", false);
                                ThreadStart delegado2 = new ThreadStart(ChangeColor);
                                var t2 = new Thread(delegado2);
                                t2.Start();
                                ShowRed ImageRed = new ShowRed(Red);
                                this.Invoke(ImageRed);
                            }
                        }
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Serial no encontrada en la BD", false);
                        ThreadStart delegado = new ThreadStart(ChangeColor);
                        var t = new Thread(delegado);
                        t.Start();
                        ShowRed ImageRed = new ShowRed(Red);
                        this.Invoke(ImageRed);
                        MessageBox.Show("No se encuentra esta etiqueta.");
                    }
                    ShowRed FCodeBar = new ShowRed(FocuCode);
                    this.Invoke(FCodeBar);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(new Form() { TopMost = true },ex.ToString());
                cLog.Add("Line 453", NameScreen, ex.Message, true);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(Employ.RoleId, Date[0], Date[1], ex.ToString(), "Bagged");
                MessageBox.Show("Un error ha ocurrido.");
                //MessageBox.Show(new Form() { TopMost = true },"Valores vacios para crear etiqueta. Consulte con un técnico si el error persiste.");
            }
        }
            

        private void BGWorkEmbol_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void BGWorkEmbol_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            PWait.Visible = false;
            FocuCode();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }

        private void BGWait_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //AskForNewData();
        }

        private void BGWait_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            PWait.Visible = false;
        }
    }
}
