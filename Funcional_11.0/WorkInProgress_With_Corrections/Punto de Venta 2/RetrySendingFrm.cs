﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public partial class RetrySendingFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Reenvio de datos";
        public EmployeeModel Employ;
        public string SeriesNotSendMP = "";
        //public string SeriesNotSendBINT1 = "";
        //public string SeriesNotSendBINT2 = "";
        //public string Series_TH_PP_PT = "";
        //public string Series_BASES_PP_TRANS = "";
        //public string Series_TRANS_PT_TH = "";
        public bool AllData = false;

        DataBase DB = new DataBase(false);

        public void Empl(EmployeeModel n, bool PrintData)
        {
            Employ = n;
            TrySend.Enabled = false;
            if (PrintData)
            {
                cLog.Add("", NameScreen, "Actualizar DGV para mostrar los seriales no enviados", false);
                /*var query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE send = '0';";
                /*Descomentar si se quiere cambiar motor de base de datos del programa
                 y cambiarlo por el motor que desee, SQLite o SQL Server*/
                //DataBase InsertInDGV = new DataBase();
                //SQLServerDataBase InsertInDGV = new SQLServerDataBase();
                DataTable dt = DB.DataTableBD(8, "", "", "", "", "");
                if (dt.Rows.Count > 0)
                {
                    this.DGDB.DataSource = dt;
                    this.DGDB.RowHeadersVisible = false;
                    this.DGDB.AllowUserToAddRows = false;
                    DGDB.Columns[0].HeaderText = "Serie";
                    DGDB.Columns[1].HeaderText = "Descripcion";
                    DGDB.Columns[2].HeaderText = "Usuario";
                    DGDB.Columns[3].HeaderText = "Tipo de Movimiento";
                    DGDB.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    
                }
                TrySend.Enabled = true;
                AllData = true;
            }
        }

        

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para salir al menú principal de la aplicación", false);
            this.Close();
            MainFrm AccF = new MainFrm();
            AccF.SetEmployeeInfo(Employ);
            AccF.Show();
        }

        private void FindError_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para encontrar los seriales no enviados", false);
            AllData = false;
            String[] result = WorkDate.Value.Date.ToShortDateString().Split(' ');
            /*var query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE date = \"" + result[0] + "\" AND send = '0';";
            /*Descomentar si se quiere cambiar motor de base de datos del programa
             y cambiarlo por el motor que desee, SQLite o SQL Server*/
            //DataBase InsertInDGV = new DataBase();
            //SQLServerDataBase InsertInDGV = new SQLServerDataBase();
            DataTable dt = DB.DataTableBD(9, result[0], "", "", "", "");
            if (dt.Rows.Count > 0)
            {
                this.DGDB.DataSource = dt;
                this.DGDB.RowHeadersVisible = false;
                this.DGDB.AllowUserToAddRows = false;
                DGDB.Columns[0].HeaderText = "Serie";
                DGDB.Columns[1].HeaderText = "Descripcion";
                DGDB.Columns[2].HeaderText = "Usuario";
                DGDB.Columns[3].HeaderText = "Tipo de Movimiento";
                DGDB.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                TrySend.Enabled = true;
            }
            else
            {
                cLog.Add("", NameScreen, "No se encontraron registro de errores de la app o NS", false);
                MessageBox.Show(new Form() { TopMost = true },"No se encontraron registros");
            }

        }

        public RetrySendingFrm()
        {
            InitializeComponent();
        }

        
        private void TrySend_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para enviar los seriales no enviados", false);
            bool ThereArePT = false, ThereAreBIN = false, ThereAreMP = false, ThereAreTJ = false, ThereAreVJ = false;
            string[] result = WorkDate.Value.Date.ToShortDateString().Split(' ');
            
            if (DB.FindLabelsNotSend("MP", AllData, result[0]))
            {
                var Info = DB.LabelsNotSend("MP");
                int TotalOfSends = Info.Item2.Count;
                int Counter = 0;
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envios de etiquetas MP", false);
                foreach (var i in Info.Item1)
                {
                    if (SendData.ResendData(i, Employ, 1, "Retry Send MP - Line 110"))
                    {
                        DB.UpdateMP(AllData, Info.Item2[Counter], result[0]);
                        DB.UpdateStringMsg(AllData, "MP", Info.Item2[Counter], result[0]);
                    }
                    Counter++;
                    if (Counter > TotalOfSends)
                    {
                        Counter = TotalOfSends;
                    }
                }
                cLog.Add("", NameScreen, "Envios finalizados de etiquetas MP", false);
            }

            if (DB.FindLabelsNotSend("PT", AllData, result[0]))
            {
                var Info = DB.LabelsNotSend("PT");
                int TotalOfSends = Info.Item2.Count;
                int Counter = 0;
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envios de etiquetas PT", false);
                foreach (var i in Info.Item1)
                {
                    if (SendData.ResendData(i, Employ, 2, "Retry Send PT - Line 110"))
                    {
                        DB.UpdatePT(AllData, Info.Item2[Counter], result[0]);
                        DB.UpdateStringMsg(AllData, "PT", Info.Item2[Counter], result[0]);
                    }
                    Counter++;
                    if (Counter > TotalOfSends)
                    {
                        Counter = TotalOfSends;
                    }
                }
                cLog.Add("", NameScreen, "Envios finalizados de etiquetas PT", false);
            }

            if (DB.FindLabelsNotSend("BIN", AllData, result[0]))
            {
                var Info = DB.LabelsNotSend("BIN");
                int TotalOfSends = Info.Item2.Count;
                int Counter = 0;
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envios de etiquetas BIN", false);
                foreach (var i in Info.Item1)
                {
                    if (SendData.ResendData(i, Employ, 3, "Retry Send BIN - Line 110"))
                    {
                        DB.UpdateBIN(AllData, Info.Item2[Counter], result[0]);
                        DB.UpdateStringMsg(AllData, "BIN", Info.Item2[Counter], result[0]);
                    }
                    Counter++;
                    if (Counter > TotalOfSends)
                    {
                        Counter = TotalOfSends;
                    }
                }
                cLog.Add("", NameScreen, "Envios finalizados de etiquetas BIN", false);
            }

            if (DB.FindLabelsNotSend("TJ", AllData, result[0]))
            {
                var Info = DB.LabelsNotSend("TJ");
                int TotalOfSends = Info.Item2.Count;
                int Counter = 0;
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envios de etiquetas TJ", false);
                foreach (var i in Info.Item1)
                {
                    if (SendData.ResendData(i, Employ, 4, "Retry Send TJ - Line 110"))
                    {
                        DB.UpdateTJ(AllData, Info.Item2[Counter], result[0]);
                        DB.UpdateStringMsg(AllData, "TJ", Info.Item2[Counter], result[0]);
                    }
                    Counter++;
                    if (Counter > TotalOfSends)
                    {
                        Counter = TotalOfSends;
                    }
                }
                cLog.Add("", NameScreen, "Envios finalizados de etiquetas TJ", false);
            }

            if (DB.FindLabelsNotSend("VJ", AllData, result[0]))
            {
                var Info = DB.LabelsNotSend("VJ");
                int TotalOfSends = Info.Item2.Count;
                int Counter = 0;
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envios de etiquetas VJ", false);
                foreach (var i in Info.Item1)
                {
                    if (SendData.ResendData(i, Employ, 5, "Retry Send VJ - Line 110"))
                    {
                        DB.UpdateVJ(AllData, Info.Item2[Counter], result[0]);
                        DB.UpdateStringMsg(AllData, "VJ", Info.Item2[Counter], result[0]);
                    }
                    Counter++;
                    if (Counter > TotalOfSends)
                    {
                        Counter = TotalOfSends;
                    }
                }
                cLog.Add("", NameScreen, "Envios finalizados de etiquetas VJ", false);
            }


            if (!ThereArePT && !ThereAreBIN && !ThereAreMP && !ThereAreTJ && !ThereAreVJ && AllData)
            {
                MessageBox.Show(new Form() { TopMost = true },"Ya no hay series pendientes a enviar.");
            }else if (!ThereArePT && !ThereAreBIN && !ThereAreMP && !ThereAreTJ && !ThereAreVJ && !AllData)
            {
                MessageBox.Show(new Form() { TopMost = true },"Ya no hay series pendientes a enviar en esta fecha.");
            }
        }
    }
}
