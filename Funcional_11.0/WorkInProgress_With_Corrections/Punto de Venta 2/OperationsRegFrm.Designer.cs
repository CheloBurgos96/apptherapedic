﻿
namespace NetSuiteApps
{
    partial class OperationsRegFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperationsRegFrm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.LBLabels = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CleanScreenBTN = new System.Windows.Forms.Button();
            this.SendReportBTN = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.WorkDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.CodeBar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stsCounterlbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel1.Controls.Add(this.LBLabels);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.CleanScreenBTN);
            this.panel1.Controls.Add(this.SendReportBTN);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.WorkDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CodeBar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 38);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 586);
            this.panel1.TabIndex = 13;
            // 
            // LBLabels
            // 
            this.LBLabels.FormattingEnabled = true;
            this.LBLabels.ItemHeight = 15;
            this.LBLabels.Location = new System.Drawing.Point(23, 273);
            this.LBLabels.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LBLabels.Name = "LBLabels";
            this.LBLabels.Size = new System.Drawing.Size(161, 139);
            this.LBLabels.TabIndex = 22;
            this.LBLabels.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LBLabels_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(41, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "Etiquetas escaneadas";
            // 
            // CleanScreenBTN
            // 
            this.CleanScreenBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.CleanScreenBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CleanScreenBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CleanScreenBTN.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.CleanScreenBTN.Location = new System.Drawing.Point(41, 218);
            this.CleanScreenBTN.Name = "CleanScreenBTN";
            this.CleanScreenBTN.Size = new System.Drawing.Size(135, 26);
            this.CleanScreenBTN.TabIndex = 16;
            this.CleanScreenBTN.Text = "Limpiar";
            this.CleanScreenBTN.UseVisualStyleBackColor = false;
            this.CleanScreenBTN.Click += new System.EventHandler(this.CleanScreenBTN_Click);
            // 
            // SendReportBTN
            // 
            this.SendReportBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.SendReportBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SendReportBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SendReportBTN.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SendReportBTN.Location = new System.Drawing.Point(41, 176);
            this.SendReportBTN.Name = "SendReportBTN";
            this.SendReportBTN.Size = new System.Drawing.Size(135, 26);
            this.SendReportBTN.TabIndex = 15;
            this.SendReportBTN.Text = "Enviar Reporte";
            this.SendReportBTN.UseVisualStyleBackColor = false;
            this.SendReportBTN.Click += new System.EventHandler(this.SendReportBTN_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label8.Location = new System.Drawing.Point(36, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Fecha de Etiquetas";
            // 
            // WorkDate
            // 
            this.WorkDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.WorkDate.Location = new System.Drawing.Point(45, 24);
            this.WorkDate.Name = "WorkDate";
            this.WorkDate.Size = new System.Drawing.Size(108, 23);
            this.WorkDate.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(64, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "ID Usuario:";
            // 
            // CodeBar
            // 
            this.CodeBar.Location = new System.Drawing.Point(23, 82);
            this.CodeBar.Name = "CodeBar";
            this.CodeBar.Size = new System.Drawing.Size(162, 23);
            this.CodeBar.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(50, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingresar código:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(23, 139);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(162, 23);
            this.textBox1.TabIndex = 12;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1300, 38);
            this.panel2.TabIndex = 14;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(251, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 22;
            this.label7.Text = "-/-/-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(203, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 21;
            this.label6.Text = "Fecha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(35, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 15);
            this.label3.TabIndex = 20;
            this.label3.Text = "Registro Operaciones";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1262, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.stsStatus,
            this.toolStripStatusLabel2,
            this.stsCounterlbl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 624);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1300, 26);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(112, 21);
            this.toolStripStatusLabel1.Text = "Estatus de reporte:";
            // 
            // stsStatus
            // 
            this.stsStatus.AutoSize = false;
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(100, 21);
            this.stsStatus.Text = "No enviado";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(1, 4, 0, 2);
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(144, 20);
            this.toolStripStatusLabel2.Text = "Operaciones escaneadas:";
            // 
            // stsCounterlbl
            // 
            this.stsCounterlbl.Margin = new System.Windows.Forms.Padding(1, 4, 0, 2);
            this.stsCounterlbl.Name = "stsCounterlbl";
            this.stsCounterlbl.Size = new System.Drawing.Size(13, 20);
            this.stsCounterlbl.Text = "0";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel3.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(203, 38);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1097, 586);
            this.panel3.TabIndex = 22;
            // 
            // OperationsRegFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OperationsRegFrm";
            this.Text = "Regitro de operaciones";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button SendReportBTN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker WorkDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CodeBar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button CleanScreenBTN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel stsStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel stsCounterlbl;
        private System.Windows.Forms.ListBox LBLabels;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
    }
}