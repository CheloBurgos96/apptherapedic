﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;

namespace NetSuiteApps
{
    class SQLServerDataBase
    {
        /*var query = "INSERT INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, id_operation1, serial_operation1, name_operation1," +
                                                            "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                                            "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                                            "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, repair_order)" +
                                                            "VALUES (@Unique_Serie,@Description,@SKU,@Double_Bag,@Finished,@Counter,@ID_Operation1,@Serial_Operation1,@Name_Operation1," +
                                                            "@ID_Operation2,@Serial_Operation2,@Name_Operation2,@ID_Operation3,@Serial_Operation3,@Name_Operation3,@ID_Operation4," +
                                                            "@Serial_Operation4,@Name_Operation4,@ID_Operation5,@Serial_Operation5,@Name_Operation5,@ID_Operation6,@Serial_Operation6," +
                                                            "@Name_Operation6,@ID_Operation7,@Serial_Operation7,@Name_Operation7,@ID_Operation8,@Serial_Operation8,@Name_Operation8," +
                                                            "@Type_Product,@Size_Product,@Sell_Order,@Repair_Order)";*/
        /*var query = "INSERT INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, id_operation1, serial_operation1, name_operation1," +
                                    "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                    "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                    "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, repair_order)" +
                                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";*/


        /*var query = "INSERT INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, id_operation1, serial_operation1, name_operation1," +
                                                    "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                                    "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                                    "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, repair_order)" +
                                                    "VALUES (@Unique_Serie,@Description,@SKU,@Double_Bag,@Finished,@Counter,@ID_Operation1,@Serial_Operation1,@Name_Operation1," +
                                                    "@ID_Operation2,@Serial_Operation2,@Name_Operation2,@ID_Operation3,@Serial_Operation3,@Name_Operation3,@ID_Operation4," +
                                                    "@Serial_Operation4,@Name_Operation4,@ID_Operation5,@Serial_Operation5,@Name_Operation5,@ID_Operation6,@Serial_Operation6," +
                                                    "@Name_Operation6,@ID_Operation7,@Serial_Operation7,@Name_Operation7,@ID_Operation8,@Serial_Operation8,@Name_Operation8," +
                                                    "@Type_Product,@Size_Product,@Sell_Order,@Repair_Order)";*/
        public static string StringCon = "Data Source=DESKTOP-JFVKB5B\\THERAPEDIC; Initial Catalog= TherapedicApp; Integrated Security=True ";
        public SqlConnection ConectionDB;
        public SqlCommand CommandDB;
        public SqlDataReader DataReaderDB;
        public SqlDataAdapter DataAdapter;
        public DataTable DT;
        private bool DBExisted = false;

        public void ConnectDB()
        {
            try
            {
                ConectionDB = new SqlConnection("SERVER=DESKTOP-JFVKB5B\\THERAPEDIC;DATABASE=TherapedicApp;Integrated security=true");
                //MessageBox.Show(new Form() { TopMost = true },"Conexion hecha");
            }catch
            {
                MessageBox.Show(new Form() { TopMost = true },"Base de datos no existe");
            }
            
        }

        public SQLServerDataBase()
        {
            int D = 0;
            ConnectDB();
            ConectionDB.Open();
            using (CommandDB = new SqlCommand("SELECT ID_station FROM Workstation_logs", ConectionDB))
            {
                using (DataReaderDB = CommandDB.ExecuteReader())
                {
                    while (DataReaderDB.Read())
                    {
                        D = Convert.ToInt16(DataReaderDB["ID_station"]);
                    }
                }
            }
            ConectionDB.Close();
            if (D > 0)
            {
                DBExisted = true;
            }
            else
            {
                DBExisted = false;
            }
        }

        public bool DBExist()
        {
            return DBExisted;
        }


        public string CounterDB(string Query, string NameColumn)
        {
            string DataINDB = "";
            ConectionDB.Open();
            using (CommandDB = new SqlCommand(Query, ConectionDB))
            {
                using (DataReaderDB = CommandDB.ExecuteReader())
                {
                    while (DataReaderDB.Read())
                    {
                        DataINDB = DataReaderDB[NameColumn].ToString();
                    }
                }
            }
            ConectionDB.Close();
            //return Number;
            return DataINDB;
        }

        public bool ExistInDB(string Query)
        {
            bool Exist = false;
            ConectionDB.Open();
            using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
            {
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read() == true)
                    {
                        Exist = true;
                    }
                }
            }
            ConectionDB.Close();
            return Exist;
        }

        public void UpDateDB(string Query)
        {
            ConectionDB.Open();
            using (CommandDB = new SqlCommand(Query, ConectionDB))
            {
                CommandDB.ExecuteNonQuery();
            }
            ConectionDB.Close();
        }

        public void DelateAllInTable(string Query)
        {
            ConectionDB.Open();
            using (CommandDB = new SqlCommand(Query, ConectionDB))
            {
                CommandDB.ExecuteNonQuery();
            }
            ConectionDB.Close();
        }

        public void InsertAllInDB(string Query, int SizeData, List<int> ListOfData, WorkInProgressModel2 empObj)
        {
            ConectionDB.Open();
            using (CommandDB = new SqlCommand())
            {
                CommandDB.Connection = ConectionDB;
                CommandDB.CommandType = CommandType.Text;
                CommandDB.CommandText = Query;
                CommandDB.Parameters.Add("@Unique_Serie", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@Description", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@SKU", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@Double_Bag", SqlDbType.Bit);
                CommandDB.Parameters.Add("@Finished", SqlDbType.Bit);
                CommandDB.Parameters.Add("@Counter", SqlDbType.Int);
                CommandDB.Parameters.Add("@Counterprint", SqlDbType.Int);
                CommandDB.Parameters.Add("@Printed", SqlDbType.Bit);
                for (int i = 1; i < 9; i++)
                {
                    CommandDB.Parameters.Add("@ID_Operation" + i.ToString(), SqlDbType.Int);
                    CommandDB.Parameters.Add("@Serial_Operation" + i.ToString(), SqlDbType.VarChar, 150);
                    CommandDB.Parameters.Add("@Name_Operation" + i.ToString(), SqlDbType.VarChar, 150);
                }
                CommandDB.Parameters.Add("@Type_Product", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@Size_Product", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@Sell_Order", SqlDbType.VarChar, 150);
                CommandDB.Parameters.Add("@Repair_Order", SqlDbType.VarChar, 150);
                /*try
                {*/
                    for (int f = 0; f < SizeData; f++)
                    {
                        CommandDB.Parameters["@Unique_Serie"].Value = empObj.Datos[f].msg.UniqueSerie;
                        CommandDB.Parameters["@Description"].Value = empObj.Datos[f].msg.Description;
                        CommandDB.Parameters["@SKU"].Value = empObj.Datos[f].msg.SKU;
                        CommandDB.Parameters["@Double_Bag"].Value = empObj.Datos[f].msg.DoubleBag;
                        CommandDB.Parameters["@Finished"].Value = false;
                        CommandDB.Parameters["@Counter"].Value = 0;
                        CommandDB.Parameters["@Counterprint"].Value = 0;
                        CommandDB.Parameters["@Printed"].Value = false;

                    for (int p = 0; p < 8; p++)
                        {
                            //MessageBox.Show(new Form() { TopMost = true },"Contador: " + p.ToString());
                            if (p < ListOfData[f])
                            {
                                CommandDB.Parameters["@ID_Operation" + (p + 1).ToString()].Value = empObj.Datos[f].msg.Oper[p].Process;
                                CommandDB.Parameters["@Serial_Operation" + (p + 1).ToString()].Value = empObj.Datos[f].msg.Oper[p].StepSerial1;
                                CommandDB.Parameters["@Name_Operation" + (p + 1).ToString()]. Value =  empObj.Datos[f].msg.Oper[p].StepName1;
                            }
                            else
                            {
                                CommandDB.Parameters["@ID_Operation" + (p + 1).ToString()].Value = 0;
                                CommandDB.Parameters["@Serial_Operation" + (p + 1).ToString()].Value = "";
                                CommandDB.Parameters["@Name_Operation" + (p + 1).ToString()].Value = "";
                            }
                        }
                        CommandDB.Parameters["@Type_Product"].Value = "";
                        CommandDB.Parameters["@Size_Product"].Value = "";
                        CommandDB.Parameters["@Sell_Order"].Value = "";
                        CommandDB.Parameters["@Repair_Order"].Value = "";

                        CommandDB.ExecuteNonQuery();
                    }
                /*}
                catch{
                    MessageBox.Show(new Form() { TopMost = true },"Error");
                }*/
                

            }
            ConectionDB.Close();
        }

        public DataTable DataTableBD(string Query)
        {
            ConectionDB.Open();
            using (CommandDB = new SqlCommand(Query, ConectionDB))
            {
                //cmd.CommandType = CommandType.Text;
                using (DataAdapter = new SqlDataAdapter(CommandDB))
                {

                    using (DT = new DataTable())
                    {

                        DataAdapter.Fill(DT);
                    }
                }
            }
            ConectionDB.Close();
            return DT;
        }

        public WorkInProgressModel TablesDatas(string Query)
        {
            WorkInProgressModel Data = new WorkInProgressModel();
            ConectionDB.Open();
            using (CommandDB = new SqlCommand(Query, ConectionDB))
            {
                using (DataReaderDB = CommandDB.ExecuteReader())
                {
                    while (DataReaderDB.Read())
                    {
                        Data.Serie = DataReaderDB["Unique_Serie"].ToString();
                        Data.Descripcion = DataReaderDB["Description"].ToString();
                        Data.SKU = DataReaderDB["SKU"].ToString();

                        Data.doubleBag = Convert.ToBoolean(DataReaderDB["Double_Bag"]);
                        Data.Terminado = Convert.ToBoolean(DataReaderDB["Finished"]);
                        Data.Contador = Convert.ToInt16(DataReaderDB["Counter"]);
                        Data.CounterPrint = Convert.ToInt16(DataReaderDB["series_count"]);
                        Data.Reprinted = Convert.ToBoolean(DataReaderDB["series_reprinted"]);

                        Data.StepID1 = DataReaderDB["ID_Operation1"].ToString();
                        Data.StepSerial1 = DataReaderDB["Serial_Operation1"].ToString();
                        Data.StepName1 = DataReaderDB["Name_Operation1"].ToString();

                        Data.StepID2 = DataReaderDB["ID_Operation2"].ToString();
                        Data.StepSerial2 = DataReaderDB["Serial_Operation2"].ToString();
                        Data.StepName2 = DataReaderDB["Name_Operation2"].ToString();

                        Data.StepID3 = DataReaderDB["ID_Operation3"].ToString();
                        Data.StepSerial3 = DataReaderDB["Serial_Operation3"].ToString();
                        Data.StepName3 = DataReaderDB["Name_Operation3"].ToString();

                        Data.StepID4 = DataReaderDB["ID_Operation4"].ToString();
                        Data.StepSerial4 = DataReaderDB["Serial_Operation4"].ToString();
                        Data.StepName4 = DataReaderDB["Name_Operation4"].ToString();

                        Data.StepID5 = DataReaderDB["ID_Operation5"].ToString();
                        Data.StepSerial5 = DataReaderDB["Serial_Operation5"].ToString();
                        Data.StepName5 = DataReaderDB["Name_Operation5"].ToString();

                        Data.StepID6 = DataReaderDB["ID_Operation6"].ToString();
                        Data.StepSerial6 = DataReaderDB["Serial_Operation6"].ToString();
                        Data.StepName6 = DataReaderDB["Name_Operation6"].ToString();

                        Data.StepID7 = DataReaderDB["ID_Operation7"].ToString();
                        Data.StepSerial7 = DataReaderDB["Serial_Operation7"].ToString();
                        Data.StepName7 = DataReaderDB["Name_Operation7"].ToString();

                        Data.StepID8 = DataReaderDB["ID_Operation8"].ToString();
                        Data.StepSerial8 = DataReaderDB["Serial_Operation8"].ToString();
                        Data.StepName8 = DataReaderDB["Name_Operation8"].ToString();

                        Data.TipoProducto = DataReaderDB["Type_Product"].ToString();
                        Data.MedidaProducto = DataReaderDB["Size_Product"].ToString();
                        Data.OrdenVenta = DataReaderDB["Sell_Order"].ToString();
                        Data.Reparacion = DataReaderDB["Repair_Order"].ToString();
                    }
                }
            }
            ConectionDB.Close();
            return Data;
        }
    }
}
