﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NetSuiteApps
{
    public class SystemLogs
    {
        private string Path = @"C:\Temp";

        public SystemLogs()
        {
        }

        public void Add(string Line, string Screen, string sLog, bool Error)
        {
            try
            {
                CreateDirectory();
                string nombre = GetNameFile();
                string chaine = "";
                if (Error)
                {
                    chaine += DateTime.Now + " - Screen: " + Screen + " Line: " + Line + " Error: " + sLog + Environment.NewLine;
                }
                else
                {
                    chaine += DateTime.Now + " - Screen: " + Screen + " Action: " + sLog + Environment.NewLine;
                }


                StreamWriter sw = new StreamWriter(Path + "/" + nombre, true);
                sw.Write(chaine);
                sw.Close();
            }
            catch
            {

            }
            
        }

        #region HELPER

        private string GetNameFile()
        {
            string nombre = "log_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt";
            return nombre;
        }

        private void CreateDirectory()
        {
            try
            {
                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        #endregion

    }
}
