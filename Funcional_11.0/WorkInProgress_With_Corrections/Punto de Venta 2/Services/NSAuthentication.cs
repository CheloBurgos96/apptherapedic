﻿using RestSharp;
using System;
using System.Windows.Forms;

namespace NetSuiteApps
{
    class NSAuthentication
    {
        DataBase DB = new DataBase(false);
        public NSAuthentication(){}

        public String Header(string URL, string type)
        {
            try
            {
                string header = "OAuth  ";
                /*const string consumerKey = "0325b783c0da4bd5e9806f73a048ab5f027c42ba561ab1effaea2324675a07d9";
                const string consumerSecret = "802cfc1b9fc93331116dc32a6e5e1b619945ee33bb781a1261239fc7bf037159";
                const string tokenValue = "a3e8e35912bbfb8becc5ef9517aea2b353c0b9b92b0043aa2d01e40bb9b12211";
                const string tokenSecret = "48a8bf2327a7166c1e2c2fc71bbdf0778620229a8e5749bdecf73a4227f31a8a";
                const string realm = "6387011_SB1";//*/
                /*const string consumerKey = "b90b06cfe19a1e59ddd0e409b1ddc5cb36ef17751efde50edc206af7b58e8004";
                const string consumerSecret = "eca966a2cc5624f0e268ff1122a6d22420f0aa24920895ae3e02ad5d9c73efb5";
                const string tokenValue = "f70e6d0a81c77138851e20e04721df49a374cf47079b93b654f35389db3f1c49";
                const string tokenSecret = "6906bcf90e7ae11e9ecf5422c99acd75c1a13c32cf314d3954d1cc1d7cb6e2ef";
                const string realm = "6387011";//*/
                const string consumerKey = "5abcece1ae9959e3ca9a02165fb86fca352d91de9073e2238f35767f8b20406c";
                const string consumerSecret = "77057452e125be912b826d10405e40c1bdf56ad16ccd5519570f68113d0f8cce";
                const string tokenValue = "f22f247f921e94a93405e9334b2992b5e8b9df6b0ec66ff37e3741234d445184";
                const string tokenSecret = "de2a9e214f8f772ca0c35147edca4a8fab3c0e0f42f7a8fd0d22fd69d7b04aaa";
                const string realm = "6387011_SB2";//*/
                string url = URL;


                Uri uri = new Uri(url);
                OAuthBase req = new OAuthBase();
                string normalized_url;
                string normalized_params;
                string nonce = req.GenerateNonce();
                string time = req.GenerateTimeStamp();
                //string signature = req.GenerateSignature(uri, consumerKey, consumerSecret, tokenValue, tokenSecret, type, time, nonce, out normalized_url, out normalized_params);
                string signature = req.GenerateSignature256(uri, consumerKey, consumerSecret, tokenValue, tokenSecret, type, time, nonce, out normalized_url, out normalized_params);

                if (signature.Contains("+"))
                {
                    signature = signature.Replace("+", "%2B");
                }

                header += "realm=\"" + realm + "\",";
                header += "oauth_signature=\"" + signature + "\",";
                header += "oauth_version=\"1.0\",";
                header += "oauth_nonce=\"" + nonce + "\",";
                header += "oauth_signature_method=\"HMAC-SHA256\",";
                header += "oauth_consumer_key=\"" + consumerKey + "\",";
                header += "oauth_token=\"" + tokenValue + "\",";
                header += "oauth_timestamp=\"" + time + "\"";

                Console.WriteLine("Header: " + header);
                return header;

            }
            catch (Exception q)
            {
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //
                DB.SaveError("", Date[0], Date[1], q.ToString(), "NSAuthentication");
                MessageBox.Show(new Form() { TopMost = true },"No se pueden obtener los datos. Consulte con un técnico si el error persiste.");
                return null;
            }
        }

        public string RequestGet(string URL)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            RestClient client = new RestClient(URL);
            client.Timeout = -1;
            string header = Header(URL, "GET");
            //MessageBox.Show(new Form() { TopMost = true },header);
            var request = new RestRequest(URL, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", header);

            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content.ToString();
        }

        public string Request(string URL, string JSONRequest)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            RestClient client = new RestClient(URL);
            client.Timeout = -1;
            string header = Header(URL, "POST");
            //MessageBox.Show(new Form() { TopMost = true },header);
            var request = new RestRequest(URL, Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", header);

            request.AddParameter("application/json", JSONRequest, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content.ToString();
        }
    }
}