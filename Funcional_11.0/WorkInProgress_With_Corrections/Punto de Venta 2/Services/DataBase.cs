﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NetSuiteApps
{
    class DataBase
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "DataBase";
        private const string Version = "4.3";

        private const string TableMattress = "CREATE TABLE IF NOT EXISTS  mattress_logs (unique_serie varchar(150) UNIQUE, description varchar(150), sku varchar(150)," +
                    "double_bag BIT, finished BIT, counter INTEGER, series_count BIT, series_reprinted BIT, id_operation1 INTEGER," +
                    "serial_operation1 varchar(150), name_operation1 varchar(150), id_operation2 INTEGER, serial_operation2 varchar(150), name_operation2 varchar(150)," +
                    "id_operation3 INTEGER, serial_operation3 varchar(150), name_operation3 varchar(150), id_operation4 INTEGER," +
                    "serial_operation4 varchar(150), name_operation4 varchar(150), id_operation5 INTEGER, serial_operation5 varchar(150)," +
                    "name_operation5 varchar(150), id_operation6 INTEGER, serial_operation6 varchar(150), name_operation6 varchar(150)," +
                    "id_operation7 INTEGER, serial_operation7 varchar(150), name_operation7 varchar(150), id_operation8 INTEGER," +
                    "serial_operation8 varchar(150), name_operation8 varchar(150), type_product varchar(150), size_product varchar(150)," +
                    "sell_order varchar(150), sell_order_small varchar(150), repair_order varchar(150), customer1 varchar(500), customer2 varchar(500), customer3 varchar(500), notBuild BIT, date_serie INTEGER);";

        private const string TableMP = "CREATE TABLE IF NOT EXISTS rawmaterial_logs (unique_serie varchar(150), description varchar(150), dateCration varchar(150), sku varchar(150), employee varchar(150)," +
                          "currentquantity INTEGER, quantity INTEGER, labelZero BIT, scaned BIT, bin varchar(150), binName varchar(200), location varchar(150));";

        private const string TablePT = "CREATE TABLE IF NOT EXISTS pt_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableBin = "CREATE TABLE IF NOT EXISTS bins_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableTJ = "CREATE TABLE IF NOT EXISTS tj_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableVJ = "CREATE TABLE IF NOT EXISTS vj_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableCounterMattress = "CREATE TABLE IF NOT EXISTS counter_mattress_logs (count_mattress INTEGER, date_mattress TEXT);";

        private const string TableWorkstation = "CREATE TABLE IF NOT EXISTS workstation_logs (id_station INTEGER, date_reg TEXT);";        

        private const string TableUser = "CREATE TABLE IF NOT EXISTS user_reg (id_user varchar(600));";

        private const string TableDate = "CREATE TABLE IF NOT EXISTS days_works(date_works TEXT);";

        private const string TableDate2 = "CREATE TABLE IF NOT EXISTS day_update(date_works TEXT);";

        private const string TableVersion = "CREATE TABLE IF NOT EXISTS version_log (current_ver TEXT) ";

        private const string TableErrorsNS = "CREATE TABLE IF NOT EXISTS com_errorsNS_logs(recordType varchar(200), id_error INTEGER, name_user varchar(200), custrecord_trp_error varchar(1000)," +
                               "custrecord_trp_numero_serie varchar(1000), custrecord_trp_request varchar(1000), custrecord_trp_ubicacion varchar(1000)," +
                               "custrecord_trp_proceso varchar(1000), custrecord_trp_tipo_movimiento varchar(1000))";

        private const string TableJaula = "CREATE TABLE IF NOT EXISTS jaulasTJ_logs (idCount INTEGER, name_jaula varchar(200), id_jaula INTEGER, binNumber INTEGER,  binName varchar(200), skuName INTEGER, skuID INTEGER, quantity INTEGER, firtScannedQty INTEGER, secondScannedQty INTEGER)";

        private const string TableJaula2 = "CREATE TABLE IF NOT EXISTS jaulasVJ_logs (idCount INTEGER, name_jaula varchar(200), id_jaula INTEGER, binNumber INTEGER,  binName varchar(200), skuName INTEGER, skuID INTEGER, quantity INTEGER, firtScannedQty INTEGER, secondScannedQty INTEGER)";

        private const string TableCom = "CREATE TABLE IF NOT EXISTS com_logs (id_user INTEGER, date_reg varchar(50), hour_reg varchar(50), error_reg text, place_reg varchar(50));";
        
        private const string TableSend = "CREATE TABLE IF NOT EXISTS send_logs(unique_serie varchar(150), description varchar(150), id_user varchar(200), id_transfer INTEGER, bin_ori INTEGER, bin_dest INTEGER, typesend TEXT, date varchar(50), hour_reg varchar(50), send BIT, identity varchar(200));";

        private const string TableSendS = "CREATE TABLE IF NOT EXISTS send_logsVJ(unique_serie varchar(150), date_insert varchar(150))";

        private const string TableJPT = "CREATE TABLE IF NOT EXISTS JPT_logs(unique_serie varchar(150), nameJaula varchar(150), idJaula varchar(150), binNumber varchar(150), binName varchar(150), detailID varchar(150), secondSan varchar(150));";

        private const string TableStringToSend = "CREATE TABLE IF NOT EXISTS resend_logs (string_notsend varchar(1000), send BIT, typeSend varchar(50), identity varchar(200), date varchar(50), hour_reg varchar(50))";//*

        private const string Path = @"C:\Temp";
        private const string DataBaseSQL = @"C:\Temp\TherapedicApp.sqlite";
        public bool DBExisted = false;
        private SQLiteConnection ConectionDB;
        private SQLiteCommand CommandDB;
        private SQLiteDataReader DataReaderDB;
        private SQLiteDataAdapter DataAdapter;
        private DataTable DT;//*/

        /*private const string TableMattress = "CREATE TABLE dbo.mattress_logs (unique_serie varchar(150), description varchar(150), sku varchar(150)," +
                    "double_bag BIT, finished BIT, counter INTEGER, series_count BIT, series_reprinted BIT, id_operation1 INTEGER," +
                    "serial_operation1 varchar(150), name_operation1 varchar(150), id_operation2 INTEGER, serial_operation2 varchar(150), name_operation2 varchar(150)," +
                    "id_operation3 INTEGER, serial_operation3 varchar(150), name_operation3 varchar(150), id_operation4 INTEGER," +
                    "serial_operation4 varchar(150), name_operation4 varchar(150), id_operation5 INTEGER, serial_operation5 varchar(150)," +
                    "name_operation5 varchar(150), id_operation6 INTEGER, serial_operation6 varchar(150), name_operation6 varchar(150)," +
                    "id_operation7 INTEGER, serial_operation7 varchar(150), name_operation7 varchar(150), id_operation8 INTEGER," +
                    "serial_operation8 varchar(150), name_operation8 varchar(150), type_product varchar(150), size_product varchar(150)," +
                    "sell_order varchar(150), sell_order_small varchar(150), repair_order varchar(150), customer1 varchar(500), customer2 varchar(500), customer3 varchar(500), notBuild BIT, date_serie INTEGER);";

        private const string TableMP = "CREATE TABLE dbo.rawmaterial_logs (unique_serie varchar(150), description varchar(150), dateCration varchar(150), sku varchar(150), employee varchar(150)," +
                          "currentquantity INTEGER, quantity INTEGER, labelZero BIT, scaned BIT, bin varchar(150), binName varchar(200), location varchar(150));";

        private const string TablePT = "CREATE TABLE dbo.pt_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableBin = "CREATE TABLE dbo.bins_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableTJ = "CREATE TABLE dbo.tj_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableVJ = "CREATE TABLE dbo.vj_logs(unique_serie varchar(150), item varchar(150), itemID varchar(150), quantity varchar(150), binId varchar(150), binName varchar(150), idLocation varchar(150));";

        private const string TableCounterMattress = "CREATE TABLE dbo.counter_mattress_logs (count_mattress INTEGER, date_mattress TEXT);";

        private const string TableWorkstation = "CREATE TABLE dbo.workstation_logs (id_station INTEGER, date_reg TEXT);";

        private const string TableUser = "CREATE TABLE dbo.user_reg (id_user varchar(600));";

        private const string TableDate = "CREATE TABLE dbo.days_works(date_works TEXT);";

        private const string TableVersion = "CREATE TABLE dbo.version_log (current_ver TEXT) ";

        private const string TableErrorsNS = "CREATE TABLE dbo.com_errorsNS_logs(recordType varchar(200), id_error INTEGER, name_user varchar(200), custrecord_trp_error varchar(200)," +
                                             "custrecord_trp_numero_serie INTEGER, custrecord_trp_request varchar(200), custrecord_trp_ubicacion varchar(200)," +
                                             "custrecord_trp_proceso varchar(200), custrecord_trp_tipo_movimiento varchar(200))";

        private const string TableJaula = "CREATE TABLE dbo.jaulasTJ_logs (idCount INTEGER, name_jaula varchar(200), id_jaula INTEGER, binNumber INTEGER,  binName varchar(200), skuName varchar(200), skuID INTEGER, quantity varchar(200), firtScannedQty varchar(200), secondScannedQty varchar(200))";

        private const string TableJaula2 = "CREATE TABLE dbo.jaulasVJ_logs (idCount INTEGER, name_jaula varchar(200), id_jaula INTEGER, binNumber INTEGER,  binName varchar(200), skuName varchar(200), skuID INTEGER, quantity varchar(200), firtScannedQty varchar(200), secondScannedQty varchar(200))";

        private const string TableCom = "CREATE TABLE dbo.com_logs (id_user INTEGER, date_reg varchar(50), hour_reg varchar(50), error_reg text, place_reg varchar(50));";

        private const string TableSend = "CREATE TABLE dbo.send_logs(unique_serie varchar(150), description varchar(150), id_user varchar(200), id_transfer INTEGER, bin_ori INTEGER, bin_dest INTEGER, typesend TEXT, date varchar(50), hour_reg varchar(50), send BIT, iden varchar(200));";

        private const string TableStringToSend = "CREATE TABLE dbo.resend_logs (string_notsend TEXT, send BIT, typeSend varchar(50), iden varchar(200), date varchar(50), hour_reg varchar(50))";

        public static string StringConNormaly = "SERVER=LAPTOP-LL8421I1\\THERAPEDICAPP; Integrated Security=True ";
        public static string StringCon = "SERVER=LAPTOP-LL8421I1\\THERAPEDICAPP;DATABASE=THAPP; Integrated Security=True ";
        
        //public static string StringConNormaly = "Data Source = (IP Address)\SQLEXPRESS,1433;Network Library = DBMSSOCN; Initial Catalog = dbase; User ID = sa; Password=password";
        //public static string StringCon = "SERVER=LAPTOP-LL8421I1\\THERAPEDICAPP;DATABASE=THAPP; Integrated Security=True ";

        public SqlConnection ConectionDB;
        public SqlCommand CommandDB;
        public SqlDataReader DataReaderDB;
        public SqlDataAdapter DataAdapter;
        public DataTable DT;
        public bool DBExisted = false;//*/


        public void ConnectDB()
        {
            ConectionDB = new SQLiteConnection(string.Format("Data Source={0};Version=3;", DataBaseSQL));
        }

        public DataBase(bool DontVerify)
        {
            try
            {
                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }

                if (!File.Exists(System.IO.Path.GetFullPath(DataBaseSQL)))
                {
                    SQLiteConnection.CreateFile(DataBaseSQL);
                    DBExisted = false;
                }
                else
                {

                    DBExisted = true;
                    ConnectDB();
                    if (DontVerify)
                    {
                        //MessageBox.Show(new Form() { TopMost = true },"EEEEE");
                        if (ExistInDB("SELECT * FROM sqlite_master WHERE TYPE = 'table' AND name = 'version_log';"))
                        {
                            string ver = CounterDB("current_ver", "version_log");
                            if (ver != "")
                            {
                                if (Convert.ToDouble(ver) < 4.2)
                                {
                                    UpDateDB(TableDate2);
                                    string Updat = "INSERT INTO day_update (date_works) VALUES (0)";
                                    UpDateDB(Updat);
                                }
                                if (Convert.ToDouble(Version) > Convert.ToDouble(ver))
                                {
                                    DeleteTablesIfExists();
                                    CreateTableIfNotExists();
                                    string VersDB = "UPDATE version_log SET current_ver = \"" + Version + "\"";
                                    UpDateDB(VersDB);
                                }
                                string[] resultDate = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                                //MessageBox.Show(new Form() { TopMost = true },resultDate[0]);
                                string DateInDB = CounterDB("date_mattress", "counter_mattress_logs");

                                if (!DateInDB.Equals(resultDate[0]))
                                {
                                    UpDateDB("UPDATE counter_mattress_logs SET count_mattress = 0, date_mattress = '" + resultDate[0] + "';");
                                }

                                DateTime FourDaysAgo = DateTime.Now.AddDays(-7);
                                string[] result3 = FourDaysAgo.Date.ToShortDateString().Split(new char[] { ' ', '/' }, StringSplitOptions.RemoveEmptyEntries);
                                string DateComplete = result3[2] + result3[1] + result3[0];
                                //MessageBox.Show(new Form() { TopMost = true },DateComplete);
                                EreaseDataSend(DateComplete);
                            }
                            else
                            {
                                DeleteTablesIfExists();
                                CreateTableIfNotExists();
                                string VersDB = "INSERT INTO version_log (current_ver) VALUES (\"" + Version + "\")";
                                UpDateDB(VersDB);
                            }
                        }
                        else
                        {
                            DeleteTablesIfExists();
                            CreateTableIfNotExists();
                        }
                    }
                }
                if (!DBExisted)
                {
                    ConnectDB();
                    UpDateDB(TableMattress);
                    UpDateDB(TableCounterMattress);
                    UpDateDB(TableWorkstation);
                    UpDateDB(TableCom);
                    UpDateDB(TableUser);
                    UpDateDB(TableDate);
                    UpDateDB(TableVersion);
                    UpDateDB(TableMP);
                    UpDateDB(TableSend);
                    UpDateDB(TableErrorsNS);
                    UpDateDB(TablePT);
                    UpDateDB(TableJaula);
                    UpDateDB(TableStringToSend);
                    UpDateDB(TableSendS);
                    UpDateDB(TableJPT);
                    UpDateDB(TableDate2);

                    string Updat = "INSERT INTO day_update (date_works) VALUES (0)";
                    UpDateDB(Updat);
                    string User = "INSERT INTO user_reg (id_user) VALUES (0)";
                    UpDateDB(User);
                    string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    string BedCounter = "INSERT INTO counter_mattress_logs (count_mattress, date_mattress) VALUES (0, '" + result[0] + "')";
                    UpDateDB(BedCounter);
                    string VerDB = "INSERT INTO version_log (current_ver) VALUES (0)";
                    UpDateDB(VerDB);
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 213", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }


        /// <summary>
        /// Función que permite saber si la tabla de datos se encontró o no,
        /// esto ayuda a saber que la aplicación ha sido lanzada por primera vez
        /// o ha sido ya utilizada en el dispositivo instalado.
        /// Este es leido por el formulario llamado "WaitPanel".
        /// </summary>
        /// <returns></returns>
        public bool DBExist()
        {
            return DBExisted;
        }

        public void SaveError(string IdUser, string DateError, string HourError, string MessageError, string Place)
        {
            try
            {
                //MessageBox.Show(new Form() { TopMost = true },MessageError);
                MessageError = MessageError.Replace("'", "");
                string BedCounter = "INSERT INTO com_logs (id_user, date_reg, hour_reg, error_reg, place_reg) VALUES (" + IdUser + ",'" + DateError + "','" + HourError + "','" + MessageError + "','" + Place + "')";
                //MessageBox.Show(new Form() { TopMost = true },MessageError);
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(BedCounter, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 249", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void CreateTableIfNotExists()
        {
            try
            {
                string[] Tables = { TableMattress, TableMP, TableSend, TablePT, TableErrorsNS, TableJaula, TableJaula2, TableStringToSend, TableCom, TableSend, TableStringToSend, TableBin, TableTJ, TableVJ, TableSendS, TableJPT };
                for (int i = 0; i < Tables.Length; i++)
                {
                    ConectionDB.OpenAsync();
                    using (CommandDB = new SQLiteCommand(Tables[i], ConectionDB))
                    {
                        CommandDB.ExecuteNonQuery();
                    }
                    ConectionDB.Close();
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 271", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }

        public void DeleteTablesIfExists()
        {
            try
            {
                string Query = "DROP TABLE IF EXISTS ";
                string[] Tables = { "mattress_logs", "rawmaterial_logs", "send_logs", "pt_logs", "com_errorsNS_logs", "jaulas_logs", "jaulasTJ_logs", "jaulasVJ_logs", "resend_logs", "com_logs", "send_logs", "resend_logs", "bins_logs", "tj_logs", "vj_logs", "JPT_logs" };
                for (int i = 0; i < Tables.Length; i++)
                {
                    ConectionDB.OpenAsync();
                    using (CommandDB = new SQLiteCommand(Query + Tables[i], ConectionDB))
                    {
                        CommandDB.ExecuteNonQuery();
                    }
                    ConectionDB.Close();
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 295", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }


        public string CounterDB(string column, string table)
        {
            string Query = "SELECT "+ column + " FROM " + table;
            string DataINDB = "";  
            try
            {
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB[column].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 322", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }

        public bool ExistInDB(string Query)
        {
            bool Exist = false;
            try
            {
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch(Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 348", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }

        public void UpDateDB(string Query)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch(Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 367", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }


        public void UpdateDateWork(string Date)
        {
            try
            {
                string Query = "UPDATE days_works SET date_works = '" + Date + "'";// AND series_count > 0"; ";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 388", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }

        public void DelateAllInTable(string table)
        {
            try
            {
                string delete = "DELETE FROM " + table;
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(delete, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 408", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void InsertAllInDB2(WorkInProgressModel2 empObj)
        {
            try
            {
                //MessageBox.Show(new Form() { TopMost = true },empObj.Datos.Length.ToString());
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Datos.Length; i++)
                    {
                        string InserDataMatress = "REPLACE INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, series_count, series_reprinted, id_operation1, serial_operation1, name_operation1," +
                                        "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                        "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                        "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, sell_order_small, repair_order, customer1, customer2, customer3, notBuild, date_serie)" +
                                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@uniqueSerie", empObj.Datos[i].msg.UniqueSerie));
                        CommandDB.Parameters.Add(new SQLiteParameter("@description", empObj.Datos[i].msg.Description));
                        CommandDB.Parameters.Add(new SQLiteParameter("@sku", empObj.Datos[i].msg.SKU));
                        CommandDB.Parameters.Add(new SQLiteParameter("@doubleBag", empObj.Datos[i].msg.DoubleBag));
                        
                        CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "0"));
                        CommandDB.Parameters.Add(new SQLiteParameter("@contador", "0"));
                        CommandDB.Parameters.Add(new SQLiteParameter("@counterprint", empObj.Datos[i].msg.Print));
                        CommandDB.Parameters.Add(new SQLiteParameter("@printed", empObj.Datos[i].msg.Reprinted));
                        int DataLabels = empObj.Datos[i].msg.Oper.Length;
                        for (int p = 0; p < DataLabels; p++)
                        {

                            CommandDB.Parameters.Add(new SQLiteParameter("@IDOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].Process.ToString()));
                            CommandDB.Parameters.Add(new SQLiteParameter("@SerialOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].StepSerial1));
                            CommandDB.Parameters.Add(new SQLiteParameter("@NombreOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].StepName1));
                        }
                        for (int p = DataLabels + 1; p < 9; p++)
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@IDOperacion" + (p + 1).ToString(), ""));
                            CommandDB.Parameters.Add(new SQLiteParameter("@SerialOperacion" + (p + 1).ToString(), ""));
                            CommandDB.Parameters.Add(new SQLiteParameter("@NombreOperacion" + (p + 1).ToString(), ""));
                        }
                        CommandDB.Parameters.Add(new SQLiteParameter("@TipoProducto", empObj.Datos[i].msg.TypeLabel));
                        CommandDB.Parameters.Add(new SQLiteParameter("@MedidaProducto", empObj.Datos[i].msg.ProductMeasure));
                        CommandDB.Parameters.Add(new SQLiteParameter("@OrdenVenta", empObj.Datos[i].msg.WorkOrder));
                        CommandDB.Parameters.Add(new SQLiteParameter("@OrdenVentaSmall", empObj.Datos[i].msg.WorkOrderSmall));
                        CommandDB.Parameters.Add(new SQLiteParameter("@Reparacion", empObj.Datos[i].msg.Repair));
                        CommandDB.Parameters.Add(new SQLiteParameter("@Customer1", empObj.Datos[i].msg.Customer1));
                        CommandDB.Parameters.Add(new SQLiteParameter("@Customer2", empObj.Datos[i].msg.Customer2));
                        CommandDB.Parameters.Add(new SQLiteParameter("@Customer3", empObj.Datos[i].msg.Customer3));
                        CommandDB.Parameters.Add(new SQLiteParameter("@notBuild", empObj.Datos[i].msg.NotBuild.ToString()));
                        CommandDB.Parameters.Add(new SQLiteParameter("@Date_serie", empObj.Datos[i].msg.Createdate));

                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 482", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            
        }

        public void EreaseErrorsNS()
        {
            try
            {
                string Query = "DELETE FROM com_errorsNS_logs;";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 892", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void InsertErrors(int p, Com_logs_ns empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.data.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO com_errorsNS_logs (recordType, id_error, name_user, custrecord_trp_error, custrecord_trp_numero_serie, custrecord_trp_request," +
                            "custrecord_trp_ubicacion, custrecord_trp_proceso, custrecord_trp_tipo_movimiento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        //string InserDataMatress = "INSERT INTO com_errorsNS_logs (recordType, id_error, name_user, custrecord_trp_error, custrecord_trp_numero_serie, custrecord_trp_request," +
                        //            "custrecord_trp_ubicacion, custrecord_trp_proceso, custrecord_trp_tipo_movimiento) SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?" +
                        //            "WHERE NOT EXISTS(SELECT 1 FROM com_errorsNS_logs WHERE id_error = '" + empObj.data[i].id + "');";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@recordType", empObj.data[i].recordType));
                        CommandDB.Parameters.Add(new SQLiteParameter("@id_error", empObj.data[i].id));
                        if (empObj.data[i].Values.custrecord_trp_id_employee.Length > 0)
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@name_user", empObj.data[i].Values.custrecord_trp_id_employee[0].Text));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@name_user", ""));
                        }
                        
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_error", empObj.data[i].Values.custrecord_trp_error));
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_numero_serie", empObj.data[i].Values.custrecord_trp_numero_serie));
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_request", empObj.data[i].Values.custrecord_trp_request));
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_ubicacion", ""));
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_proceso", empObj.data[i].Values.custrecord_trp_proceso));
                        CommandDB.Parameters.Add(new SQLiteParameter("@custrecord_trp_tipo_movimiento", empObj.data[i].Values.custrecord_trp_tipo_movimiento));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 605", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }


        /// <summary>
        /// Función que permite el envio de la información contenida en la BD,
        /// el que permite el intercambio de información es "TypeQuery", ya que con ese
        /// se puede especificar el query que necesitamos para la BD.
        /// Los demás valores pueden usarse o no, dependiendo del caso y de la pantalla que
        /// lo llame, en ocasiones estos datos vienen vacios porque no se necesitan.
        /// Este es leido por los formularios "SeriesPrintingFrm", "Com_LogFrm" y "RetrySendingFrm".
        /// </summary>
        /// <param name="TypeQuery"></param>
        /// <param name="Date1"></param>
        /// <param name="Date2"></param>
        /// <param name="OrderWrok"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataTable DataTableBD(int TypeQuery, string Date1, string Date2, string OrderWrok, string UserID, string SKU)
        {
            string Query = "";
            switch (TypeQuery)
            {
                case 0:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                    break;
                case 1:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                    break;
                case 2:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                    break;
                case 3:
                    Query = "SELECT * FROM com_logs WHERE date_reg = '" + Date1 + "';";
                    break;
                case 4:
                    Query = "SELECT * FROM com_logs WHERE id_user = " + UserID + ";";
                    break;
                case 5:
                    Query = "SELECT * FROM com_logs WHERE id_user = " + UserID + " AND date_reg = '" + Date1 + "';";
                    break;
                case 6:
                    Query = "SELECT * FROM com_logs;";
                    break;
                case 7:
                    Query = "SELECT custrecord_trp_error, name_user, custrecord_trp_proceso, custrecord_trp_tipo_movimiento FROM com_errorsNS_logs";
                    break;
                case 8:
                    Query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE send = '0';";
                    break;
                case 9:
                    Query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE date = \"" + Date1 + "\" AND send = '0';";
                    break;
                case 10:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sku = '" + SKU + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                    break;
                case 11:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sku = '" + SKU + "'AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                    break;
                case 12:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                    break;
                case 13:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sku = '" + SKU + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                    break;
                case 14:
                    Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND sku = '" + SKU + "'AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                    break;
            }
            try
            {
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    //cmd.CommandType = CommandType.Text;
                    using (DataAdapter = new SQLiteDataAdapter(CommandDB))
                    {

                        using (DT = new DataTable())
                        {

                            DataAdapter.Fill(DT);
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 696", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DT;
        }

        public WorkInProgressModel TablesDatas(string serie)
        {            
            WorkInProgressModel Data = new WorkInProgressModel();
            try
            {
                string sqlQuery = "SELECT * FROM mattress_logs WHERE unique_serie = '" + serie + "'";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(sqlQuery, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            Data.Serie = DataReaderDB["unique_serie"].ToString();
                            Data.Descripcion = DataReaderDB["description"].ToString();
                            Data.SKU = DataReaderDB["sku"].ToString();

                            Data.doubleBag = Convert.ToBoolean(DataReaderDB["double_bag"]);
                            Data.Terminado = Convert.ToBoolean(DataReaderDB["finished"]);
                            Data.Contador = Convert.ToInt16(DataReaderDB["counter"]);
                            Data.CounterPrint = Convert.ToInt16(DataReaderDB["series_count"]);
                            Data.Reprinted = Convert.ToBoolean(DataReaderDB["series_reprinted"]);

                            Data.StepID1 = DataReaderDB["id_operation1"].ToString();
                            Data.StepSerial1 = DataReaderDB["serial_operation1"].ToString();
                            Data.StepName1 = DataReaderDB["name_operation1"].ToString();

                            Data.StepID2 = DataReaderDB["id_operation2"].ToString();
                            Data.StepSerial2 = DataReaderDB["serial_operation2"].ToString();
                            Data.StepName2 = DataReaderDB["name_operation2"].ToString();

                            Data.StepID3 = DataReaderDB["id_operation3"].ToString();
                            Data.StepSerial3 = DataReaderDB["serial_operation3"].ToString();
                            Data.StepName3 = DataReaderDB["name_operation3"].ToString();

                            Data.StepID4 = DataReaderDB["id_operation4"].ToString();
                            Data.StepSerial4 = DataReaderDB["serial_operation4"].ToString();
                            Data.StepName4 = DataReaderDB["name_operation4"].ToString();

                            Data.StepID5 = DataReaderDB["id_operation5"].ToString();
                            Data.StepSerial5 = DataReaderDB["serial_operation5"].ToString();
                            Data.StepName5 = DataReaderDB["name_operation5"].ToString();

                            Data.StepID6 = DataReaderDB["id_operation6"].ToString();
                            Data.StepSerial6 = DataReaderDB["serial_operation6"].ToString();
                            Data.StepName6 = DataReaderDB["name_operation6"].ToString();

                            Data.StepID7 = DataReaderDB["id_operation7"].ToString();
                            Data.StepSerial7 = DataReaderDB["serial_operation7"].ToString();
                            Data.StepName7 = DataReaderDB["name_operation7"].ToString();

                            Data.StepID8 = DataReaderDB["id_operation8"].ToString();
                            Data.StepSerial8 = DataReaderDB["serial_operation8"].ToString();
                            Data.StepName8 = DataReaderDB["name_operation8"].ToString();

                            Data.TipoProducto = DataReaderDB["type_product"].ToString();
                            Data.MedidaProducto = DataReaderDB["size_product"].ToString();
                            Data.OrdenVenta = DataReaderDB["sell_order"].ToString();
                            Data.OrdenVentaSmall = DataReaderDB["sell_order_small"].ToString();
                            Data.Reparacion = DataReaderDB["repair_order"].ToString();

                            Data.Customer1 = DataReaderDB["customer1"].ToString();
                            Data.Customer2 = DataReaderDB["customer2"].ToString();
                            Data.Customer3 = DataReaderDB["customer3"].ToString();

                            Data.NotBuild = Convert.ToBoolean(DataReaderDB["notBuild"]);

                            Data.DateSerie = Convert.ToInt64(DataReaderDB["date_serie"]);
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 777", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return Data;
        }


        public void InserDayWork(string Date)
        {
            try
            {
                string Query = "INSERT INTO days_works (date_works) VALUES ('" + Date + "')";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 799", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }

        public void DeleteAllLabelsWithDate(string DateComplete)
        {
            try
            {
                string Query = "DELETE FROM mattress_logs WHERE date_serie < '" + DateComplete + "'";// AND series_count > 0";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 819", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }




        //######Todo lo relacionado con Transferencias MP######
        public void SaveMP(DataMP empObj)
        {
                //int Count = 0;
            try
            {
                DelateAllInTable("rawmaterial_logs");
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Data.Length; i++)
                    {
                        //Count = i;
                        int CQ = 0, Q = 0;
                        bool parse1 = int.TryParse(empObj.Data[i].Currentquantity, out CQ);
                        bool parse2 = int.TryParse(empObj.Data[i].Quantity, out Q);
                        if (!parse1)
                        {
                            CQ = 0;
                        }
                        if (!parse2)
                        {
                            Q = 0;
                        }
                        if (empObj.Data[i].Description.Contains("\""))
                        {
                            //MessageBox.Show(new Form() { TopMost = true },empObj.Data[i].Description);
                            empObj.Data[i].Description = empObj.Data[i].Description.Replace("\" ", " ");
                            //MessageBox.Show(new Form() { TopMost = true },empObj.Data[i].Description);
                        }
                        if (empObj.Data[i].Description.Contains("'"))
                        {
                            //MessageBox.Show(new Form() { TopMost = true },empObj.Data[i].Description);
                            empObj.Data[i].Description = empObj.Data[i].Description.Replace("'", "");
                            //MessageBox.Show(new Form() { TopMost = true },empObj.Data[i].Description);
                        }
                        //string InserDataMatress = "INSERT INTO rawmaterial_logs (unique_serie, description, dateCration, sku, employee, currentquantity, quantity, labelZero, scaned, bin, binName, location) SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
                        //    "WHERE NOT EXISTS(SELECT 1 FROM rawmaterial_logs WHERE unique_serie = '" + empObj.Data[i].NumeroSerie + "' AND bin = '" + empObj.Data[i].BinId + "' AND location = '" + empObj.Data[i].Location + "')";
                        //string InserDataMatress = "REPLACE INTO rawmaterial_logs (unique_serie, description, dateCration, sku, employee, currentquantity, quantity, labelZero, scaned, bin, binName, location)" +
                        //                      "VALUES (@uniqueSerie, @description, @dateCration, @sku, @employee, @currentquantity, @quantity, @labelZero, @scaned, @bin, @binName, @location)";
                        string InserDataMatress = "INSERT INTO rawmaterial_logs (unique_serie, description, dateCration, sku, employee, currentquantity, quantity, labelZero, scaned, bin, binName, location)" +
                                               "VALUES (@uniqueSerie, @description, @dateCration, @sku, @employee, @currentquantity, @quantity, @labelZero, @scaned, @bin, @binName, @location)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@uniqueSerie", empObj.Data[i].NumeroSerie));
                        CommandDB.Parameters.Add(new SQLiteParameter("@description", empObj.Data[i].Description));
                        CommandDB.Parameters.Add(new SQLiteParameter("@dateCration", empObj.Data[i].DateCreate));
                        CommandDB.Parameters.Add(new SQLiteParameter("@sku", empObj.Data[i].SKU));
                        CommandDB.Parameters.Add(new SQLiteParameter("@employee", empObj.Data[i].Employee));

                        CommandDB.Parameters.Add(new SQLiteParameter("@currentquantity", CQ));
                        CommandDB.Parameters.Add(new SQLiteParameter("@quantity", Q));
                        CommandDB.Parameters.Add(new SQLiteParameter("@labelZero", empObj.Data[i].LabelZero.ToString()));
                        CommandDB.Parameters.Add(new SQLiteParameter("@scaned", "0"));
                        CommandDB.Parameters.Add(new SQLiteParameter("@bin", empObj.Data[i].BinId));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Data[i].BinName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@location", empObj.Data[i].Location));

                        CommandDB.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                ConectionDB.Close();

            }
            catch (Exception e)
            {
                //MessageBox.Show(new Form() { TopMost = true },Count.ToString());
                //MessageBox.Show(new Form() { TopMost = true },empObj.Data[Count].NumeroSerie);
                ConectionDB.Close();
                cLog.Add("Line 873", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseMP(string Serial)
        {
            try
            {
                string Query = "DELETE FROM rawmaterial_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 892", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }
        public string FindDescriptionMP(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT description FROM rawmaterial_logs WHERE unique_serie = '" + Serie + "';";

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["description"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 920", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public bool FindQuantityZero(string Serie)
        {
            bool DataINDB = false;
            try
            {
                string Query = "SELECT labelZero FROM rawmaterial_logs WHERE unique_serie = '" + Serie + "';";

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            //if ())
                            //{
                            DataINDB = Convert.ToBoolean(DataReaderDB["labelZero"]);
                            //}
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 951", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public bool FindMPSerialWithBin(string Serie, string Bin)
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM rawmaterial_logs WHERE unique_serie = \"" + Serie + "\" AND bin = \"" + Bin + "\";";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 978", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return Exist;
        }
        public bool FindInfoMP()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM rawmaterial_logs;";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1006", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return Exist;
        }


        //######Todo lo relacionado con Transferencias PT######
        public void SavePT(UbicationsPT empObj)
        {
            try
            {
                /*int p = empObj.Quantity / 1000;
                //MessageBox.Show(new Form() { TopMost = true },p.ToString());
                Int64 i = 0;
                Int64 counter = 0, counter2 = 0, quantity = 1000;
                if (p < (1))
                {
                    quantity = empObj.Quantity;
                }
                //MessageBox.Show(new Form() { TopMost = true },"Qty Init: " + quantity.ToString());
                for (int j = 0; j < (p+1);  j++)
                {
                    
                }*/
                
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    //for (i = counter; i < quantity; i++)
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        //string InserDataMatress = "INSERT INTO pt_logs(unique_serie, item, itemID, quantity, binId, binName, idLocation) SELECT ?, ?, ?, ?, ?, ?, ?" + 
                        //    "WHERE NOT EXISTS(SELECT 1 FROM pt_logs WHERE unique_serie = '" + empObj.Series[i].serial + "' AND item = '" + empObj.Series[i].item + 
                        //    "' AND itemID = '" + empObj.Series[i].itemID + "' AND quantity = '"+ empObj.Series[i].quantity + "' AND binId = '" + empObj.Series[i].binID + 
                        //    "' AND binName = '" + empObj.Series[i].binName + "' AND idLocation = '" + empObj.Series[i].Location + "')";
                        //string InserDataMatress = "REPLACE INTO pt_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                        //            "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        string InserDataMatress = "INSERT INTO pt_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";

                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", empObj.Series[i].serial));
                        CommandDB.Parameters.Add(new SQLiteParameter("@item", empObj.Series[i].item));
                        CommandDB.Parameters.Add(new SQLiteParameter("@itemID", empObj.Series[i].itemID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.Series[i].quantity));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binId", empObj.Series[i].binID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Series[i].binName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@idLocation", empObj.Series[i].Location));
                        CommandDB.ExecuteNonQuery();
                        //counter2++;
                    }

                    //MessageBox.Show(new Form() { TopMost = true },count.ToString());
                    /*transaction.Commit();
                    counter = counter2;
                    Int64 sus = empObj.Quantity - counter;
                    //MessageBox.Show(new Form() { TopMost = true },sus.ToString());
                    if (sus < 1000)
                    {
                        quantity = empObj.Quantity;
                    }
                    else
                    {
                        quantity += 1000;
                    }*/
                    //MessageBox.Show(new Form() { TopMost = true },"Qty After: " + quantity.ToString());
                    transaction.Commit();
                }
                ConectionDB.Close();

            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1045", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void SaveJPT(SerialsJPT empObj)
        {
            try
            {
                DelateAllInTable("JPT_logs");
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Serial.Serials.Length; i++)
                    {
                        //string InserDataMatress = "INSERT INTO pt_logs(unique_serie, item, itemID, quantity, binId, binName, idLocation) SELECT ?, ?, ?, ?, ?, ?, ?" + 
                        //    "WHERE NOT EXISTS(SELECT 1 FROM pt_logs WHERE unique_serie = '" + empObj.Series[i].serial + "' AND item = '" + empObj.Series[i].item + 
                        //    "' AND itemID = '" + empObj.Series[i].itemID + "' AND quantity = '"+ empObj.Series[i].quantity + "' AND binId = '" + empObj.Series[i].binID + 
                        //    "' AND binName = '" + empObj.Series[i].binName + "' AND idLocation = '" + empObj.Series[i].Location + "')";
                        //unique_serie, idJaula, binNumber, binName, detailID, secondSan;
                        for (int j = 0; j < empObj.Serial.Serials[i].SerialsJ.Count; j++)
                        {
                            //string InserDataMatress = "REPLACE INTO JPT_logs (unique_serie, nameJaula, idJaula, binNumber, binName, detailID, secondSan)" +
                            //        "VALUES (@unique_serie, @nameJaula, @idJaula, @binNumber, @binName, @detailID, @secondSan)";
                            string InserDataMatress = "INSERT INTO JPT_logs (unique_serie, nameJaula, idJaula, binNumber, binName, detailID, secondSan)" +
                                    "VALUES (@unique_serie, @nameJaula, @idJaula, @binNumber, @binName, @detailID, @secondSan)";

                            CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                            CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", empObj.Serial.Serials[i].SerialsJ[j]));
                            CommandDB.Parameters.Add(new SQLiteParameter("@nameJaula", empObj.Serial.Name));
                            CommandDB.Parameters.Add(new SQLiteParameter("@idJaula", empObj.Serial.JaulaId));
                            CommandDB.Parameters.Add(new SQLiteParameter("@binNumber", empObj.Serial.BinNumber));
                            CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Serial.BinName));
                            CommandDB.Parameters.Add(new SQLiteParameter("@detailID", empObj.Serial.Serials[i].DetailId));
                            CommandDB.Parameters.Add(new SQLiteParameter("@secondSan", empObj.Serial.Serials[i].SecondScannedQty));
                            CommandDB.ExecuteNonQuery();
                        }
                        
                    }

                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1045", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void EreasePT(string Serial)
        {
            try
            {
                string Query = "DELETE FROM pt_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1065", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void EreaseJPT(string Serial)
        {
            try
            {
                /*string Query = "DELETE FROM JPT_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();*/
                string Query2 = "DELETE FROM send_logsVJ WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query2, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1065", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public string FindDescriptionPT(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT item FROM pt_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["item"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1091", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }

        public string FindDescriptionJPT(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT nameJaula FROM JPT_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["nameJaula"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1091", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }


        public List<string> FindIDBins(string Ubication)
        {
            List<string> ListID = new List<string>();
            try
            {
                string Query = "SELECT DISTINCT binId FROM pt_logs WHERE idLocation = '" + Ubication + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            ListID.Add(DataReaderDB["binId"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1118", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return ListID;
        }
        public bool FindPTSerialWithBin(string Serie, string Bin)
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM pt_logs WHERE unique_serie = \"" + Serie + "\" AND binId = \"" + Bin + "\";";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1146", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return Exist;
        }

        public bool FindSerialWithSKUAndBINJPT(string serie, string bin, string jaulaID)
        {
            //MessageBox.Show(new Form() { TopMost = true },"Serie: " + serie + " bin: " + bin + " jaulaID: " + jaulaID);
            bool Exist = false;
            
            try
            {
                string query = "SELECT * FROM JPT_logs WHERE unique_serie = '" + serie + "'AND binNumber = '" + bin + "' AND idJaula = " + jaulaID + ";";
                //string query = "SELECT itemID, binI FROM pt_logs WHERE unique_serie = '" + serie + "';";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1172", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        public bool FindInfoPT()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM pt_logs;";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1174", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        
        //######Todo lo relacionado con Transferencias BIN######
        public void SaveBIN(UbicationsPT empObj)
        {
            try
            {
                DelateAllInTable("bins_logs");
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        //string InserDataMatress = "INSERT INTO bins_logs(unique_serie, item, itemID, quantity, binId, binName, idLocation) SELECT ?, ?, ?, ?, ?, ?, ?" +
                        //    "WHERE NOT EXISTS(SELECT 1 FROM bins_logs WHERE unique_serie = '" + empObj.Series[i].serial + "' AND item = '" + empObj.Series[i].item +
                        //    "' AND itemID = '" + empObj.Series[i].itemID + "' AND quantity = '" + empObj.Series[i].quantity + "' AND binId = '" + empObj.Series[i].binID +
                        //    "' AND binName = '" + empObj.Series[i].binName + "' AND idLocation = '" + empObj.Series[i].Location + "')";
                        //string InserDataMatress = "REPLACE INTO bins_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                        //            "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        string InserDataMatress = "INSERT INTO bins_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";

                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", empObj.Series[i].serial));
                        CommandDB.Parameters.Add(new SQLiteParameter("@item", empObj.Series[i].item));
                        CommandDB.Parameters.Add(new SQLiteParameter("@itemID", empObj.Series[i].itemID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.Series[i].quantity));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binId", empObj.Series[i].binID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Series[i].binName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@idLocation", empObj.Series[i].Location));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1213", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseBIN(string Serial)
        {
            try
            {
                string Query = "DELETE FROM bins_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1232", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }
        public string FindDescriptionBIN(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT item FROM bins_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["item"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1259", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public List<string> FindBINIDBins(string Ubication)
        {
            List<string> ListID = new List<string>();
            try
            {
                string Query = "SELECT DISTINCT binId FROM pt_logs WHERE idLocation = '" + Ubication + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            ListID.Add(DataReaderDB["binId"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1286", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return ListID;
        }
        public (bool, bool) FindBINSerialWithBin(string Serie, string Bin)
        {
            //MessageBox.Show(new Form() { TopMost = true },"Serie: " + Serie + " , Bin: " + Bin);
            bool Exist = false;
            bool SameBin = false;
            try
            {
                string BinA = "";
                int B = Convert.ToInt32(Bin);
                string Query = "SELECT binId FROM bins_logs WHERE unique_serie = \"" + Serie + "\";";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Exist = true;
                            BinA = rdr["binId"].ToString();

                        }
                    }
                }
                ConectionDB.Close();

                int Bi = Convert.ToInt32(BinA);
                if (Bi == B)
                {
                    //MessageBox.Show(new Form() { TopMost = true },B.ToString());
                    //MessageBox.Show(new Form() { TopMost = true },Bi.ToString());
                    SameBin = true;
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1327", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return (Exist, SameBin);
        }
        public bool FindInfoBIN()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM bins_logs;";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1355", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }




        //######Todo lo relacionado con Transferencias JAULA######
        public void SaveTJ(UbicationsPT empObj)
        {
            try
            {
                DelateAllInTable("tj_logs");
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        //string InserDataMatress = "INSERT INTO tj_logs(unique_serie, item, itemID, quantity, binId, binName, idLocation) SELECT ?, ?, ?, ?, ?, ?, ?" +
                        //    "WHERE NOT EXISTS(SELECT 1 FROM tj_logs WHERE unique_serie = '" + empObj.Series[i].serial + "' AND item = '" + empObj.Series[i].item +
                        //    "' AND itemID = '" + empObj.Series[i].itemID + "' AND quantity = '" + empObj.Series[i].quantity + "' AND binId = '" + empObj.Series[i].binID +
                        //    "' AND binName = '" + empObj.Series[i].binName + "' AND idLocation = '" + empObj.Series[i].Location + "')";
                        //string InserDataMatress = "REPLACE INTO tj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                        //            "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        string InserDataMatress = "INSERT INTO tj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", empObj.Series[i].serial));
                        CommandDB.Parameters.Add(new SQLiteParameter("@item", empObj.Series[i].item));
                        CommandDB.Parameters.Add(new SQLiteParameter("@itemID", empObj.Series[i].itemID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.Series[i].quantity));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binId", empObj.Series[i].binID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Series[i].binName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@idLocation", empObj.Series[i].Location));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1394", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseTJ(string Serial)
        {
            try
            {
                string Query = "DELETE FROM tj_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1413", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }
        public int UpdateDBJaulaTransfer(string Serie, string SKUID, string JaulaID)
        {
            int RefreshData = 0;
            try
            {
                /*string SKUID = "";
                string query = "SELECT itemID FROM tj_logs WHERE unique_serie = '" + Serie + "'";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            SKUID = DataReaderDB["itemID"].ToString();
                        }
                    }
                }
                ConectionDB.Close();

                //MessageBox.Show(new Form() { TopMost = true },query);
                */
                //if (SKUID.Length > 0)
                //{
                    bool Exist = false;
                    string Query = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                    ConectionDB.OpenAsync();
                    using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read() == true)
                            {
                                Exist = true;
                            }
                        }
                    }
                    ConectionDB.Close();
                    int Quantity = 0, QuantityToScan = 0, idCount = 0;
                    if (Exist)
                    {
                        string TakeQuantity = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND quantity != firtScannedQty ;";
                        //string TakeQuantity = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                        RefreshData = 6;
                        ConectionDB.OpenAsync();
                        using (CommandDB = new SQLiteCommand(TakeQuantity, ConectionDB))
                        {
                            using (DataReaderDB = CommandDB.ExecuteReader())
                            {
                                if (DataReaderDB.Read())
                                {
                                    if (!DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 1;
                                        idCount = Convert.ToInt32(DataReaderDB["idCount"]);
                                        QuantityToScan = Convert.ToInt32(DataReaderDB["quantity"]);
                                        Quantity = Convert.ToInt32(DataReaderDB["firtScannedQty"]);
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 2;
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 3;
                                    }
                                    else if (!DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 4;
                                    }
                                    //break;
                                }
                            }
                        }
                        ConectionDB.Close();
                    }
                    if (RefreshData == 1)
                    {
                        Quantity++;
                        if (QuantityToScan >= Quantity)
                        {
                            ConectionDB.Close();
                            string UpdateJaula = "UPDATE jaulasTJ_logs SET firtScannedQty = " + Quantity + " WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND idCount = '" + idCount + "';";
                            ConectionDB.OpenAsync();
                            using (CommandDB = new SQLiteCommand(UpdateJaula, ConectionDB))
                            {
                                CommandDB.ExecuteNonQuery();
                            }
                            ConectionDB.Close();
                        }
                        else
                        {
                            RefreshData = 5;
                        }

                    }
                    if (RefreshData == 6)
                    {
                        RefreshData = 5;
                    }
                //}
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1523", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return RefreshData;
        }
        public bool FindSerialsWithSKUTJ(string serie, string sku)
        {
            bool Exist = false;
            try
            {
                string query = "SELECT * FROM tj_logs WHERE unique_serie = '" + serie + "' AND itemID = '" + sku + "';";
                //MessageBox.Show(new Form() { TopMost = true },query);
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1552", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
            return Exist;
        }
        public List<Int64> TakeIDSKUFromJaulas(string IDJaula)
        {
            List<Int64> DataINDB = new List<Int64>();
            try
            {
                string Query = "SELECT skuID FROM jaulasTJ_logs WHERE id_jaula = '" + IDJaula + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB.Add(Convert.ToInt64(DataReaderDB["skuID"]));
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1580", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return DataINDB;
        }
        public void SaveJaula(PlannedInventory empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.DataJaula.ItemsJaula.Length; i++)
                    {
                        string InserDataMatress = "REPLACE INTO jaulasTJ_logs (idCount, name_jaula, id_jaula, binNumber, binName, skuName, skuID, quantity, firtScannedQty, secondScannedQty)" +
                                      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@idCount", (i + 1).ToString()));
                        CommandDB.Parameters.Add(new SQLiteParameter("@name_jaula", empObj.DataJaula.NameJaula));
                        CommandDB.Parameters.Add(new SQLiteParameter("@id_jaula", empObj.DataJaula.IDJaula));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binNumber", empObj.DataJaula.BinNumber));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.DataJaula.BinName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@skuName", empObj.DataJaula.ItemsJaula[i].SKUName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@skuID", empObj.DataJaula.ItemsJaula[i].SKUID));
                        if (empObj.DataJaula.ItemsJaula[i].Quantity == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@quantity", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.DataJaula.ItemsJaula[i].Quantity));
                        }
                        if (empObj.DataJaula.ItemsJaula[i].firstScannedQty == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@firtScannedQty", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@firtScannedQty", empObj.DataJaula.ItemsJaula[i].firstScannedQty));
                        }
                        if (empObj.DataJaula.ItemsJaula[i].secondScannedQty == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@secondScannedQty", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@secondScannedQty", empObj.DataJaula.ItemsJaula[i].secondScannedQty));
                        }
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1638", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public bool FindInfoTJ()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM tj_logs;";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1664", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        //######Todo lo relacionado con la Validación JAULA######
        public void SaveVJ(UbicationsPT empObj)
        {
            try
            {
                DelateAllInTable("vj_logs");
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        //string InserDataMatress = "INSERT INTO vj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation) SELECT ?, ?, ?, ?, ?, ?, ?" +
                        //    "WHERE NOT EXISTS(SELECT 1 FROM vj_logs WHERE unique_serie = '" + empObj.Series[i].serial + "' AND item = '" + empObj.Series[i].item +
                        //    "' AND itemID = '" + empObj.Series[i].itemID + "' AND quantity = '" + empObj.Series[i].quantity + "' AND binId = '" + empObj.Series[i].binID +
                        //    "' AND binName = '" + empObj.Series[i].binName + "' AND idLocation = '" + empObj.Series[i].Location + "')";
                        //string InserDataMatress = "REPLACE INTO vj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                        //            "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        string InserDataMatress = "INSERT INTO vj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", empObj.Series[i].serial));
                        CommandDB.Parameters.Add(new SQLiteParameter("@item", empObj.Series[i].item));
                        CommandDB.Parameters.Add(new SQLiteParameter("@itemID", empObj.Series[i].itemID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.Series[i].quantity));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binId", empObj.Series[i].binID));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.Series[i].binName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@idLocation", empObj.Series[i].Location));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1700", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseVJ(string Serial)
        {
            try
            {
                string Query = "DELETE FROM vj_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1719", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }
        public int UpdateDBJaulaValidation(string SKUID, string JaulaID)
        {
            //MessageBox.Show(new Form() { TopMost = true },"Serie: " + Serie + " bin: " + bin + "JaulaID: " + JaulaID);
            int RefreshData = 0;
            try
            {
                /*string SKUID = "";
                string query = "SELECT itemID FROM vj_logs WHERE unique_serie = '" + Serie + "' AND binId = '" + bin + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            SKUID = DataReaderDB["itemID"].ToString();
                        }
                    }
                }
                ConectionDB.Close();*/
                if (SKUID.Length > 0)
                {
                    bool Exist = false;
                    string Query = "SELECT idCount, quantity, firtScannedQty FROM jaulasVJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                    ConectionDB.OpenAsync();
                    using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read() == true)
                            {
                                Exist = true;
                            }
                        }
                    }
                    ConectionDB.Close();
                    int Quantity = 0, QuantityToScan = 0, idCount = 0;
                    if (Exist)
                    {
                        string TakeQuantity = "SELECT idCount, quantity, secondScannedQty FROM jaulasVJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND quantity != secondScannedQty;";
                        RefreshData = 6;
                        ConectionDB.OpenAsync();
                        using (CommandDB = new SQLiteCommand(TakeQuantity, ConectionDB))
                        {
                            using (DataReaderDB = CommandDB.ExecuteReader())
                            {
                                while (DataReaderDB.Read())
                                {
                                    if (!DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 1;
                                        idCount = Convert.ToInt32(DataReaderDB["idCount"]);
                                        QuantityToScan = Convert.ToInt32(DataReaderDB["quantity"]);
                                        Quantity = Convert.ToInt32(DataReaderDB["secondScannedQty"]);
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 2;
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 3;
                                    }
                                    else if (!DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 4;
                                    }

                                }
                            }
                        }
                        ConectionDB.Close();
                    }
                    if (RefreshData == 1)
                    {
                        Quantity++;
                        if (QuantityToScan >= Quantity)
                        {
                            ConectionDB.Close();
                            string UpdateJaula = "UPDATE jaulasVJ_logs SET secondScannedQty = " + Quantity + " WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND idCount = '" + idCount + "';";
                            ConectionDB.OpenAsync();
                            using (CommandDB = new SQLiteCommand(UpdateJaula, ConectionDB))
                            {
                                CommandDB.ExecuteNonQuery();
                            }
                            ConectionDB.Close();
                        }
                        else
                        {
                            RefreshData = 5;
                        }
                    }
                    if (RefreshData == 6)
                    {
                        RefreshData = 5;
                    }
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1825", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return RefreshData;
        }
        public bool FindSerialWithSKUAndBINVJ(string serie)
        {
            //MessageBox.Show(new Form() { TopMost = true },"Serie: " + serie + " bin: " + bin + "SKU: " + sku);
            bool Exist = false;
            try
            {
                bool FoundIt = true;
                string queryF = "SELECT * FROM send_logsVJ WHERE unique_serie = '" + serie + "';";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(queryF, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();

                /*if (FoundIt)
                {
                    string query = "SELECT * FROM vj_logs WHERE unique_serie = '" + serie + "' AND itemID = '" + sku + "' AND binId = '" + bin + "';";
                    //string query = "SELECT itemID, binI FROM pt_logs WHERE unique_serie = '" + serie + "';";
                    ConectionDB.OpenAsync();
                    using (SQLiteCommand cmd = new SQLiteCommand(query, ConectionDB))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read() == true)
                            {
                                Exist = true;
                            }
                        }
                    }
                    ConectionDB.Close();
                }*/
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1855", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }
        public List<Int64> TakeIDSKUFromJaulas2(string IDJaula)
        {
            List<Int64> DataINDB = new List<Int64>();
            try
            {
                string Query = "SELECT skuID FROM jaulasVJ_logs WHERE id_jaula = '" + IDJaula + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB.Add(Convert.ToInt64(DataReaderDB["skuID"]));
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1883", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return DataINDB;
        }
        public void SaveJaula2(PlannedInventory empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.DataJaula.ItemsJaula.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO jaulasVJ_logs (idCount, name_jaula, id_jaula, binNumber, binName, skuName, skuID, quantity, firtScannedQty, secondScannedQty)" +
                                      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@idCount", (i + 1).ToString()));
                        CommandDB.Parameters.Add(new SQLiteParameter("@name_jaula", empObj.DataJaula.NameJaula));
                        CommandDB.Parameters.Add(new SQLiteParameter("@id_jaula", empObj.DataJaula.IDJaula));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binNumber", empObj.DataJaula.BinNumber));
                        CommandDB.Parameters.Add(new SQLiteParameter("@binName", empObj.DataJaula.BinName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@skuName", empObj.DataJaula.ItemsJaula[i].SKUName));
                        CommandDB.Parameters.Add(new SQLiteParameter("@skuID", empObj.DataJaula.ItemsJaula[i].SKUID));
                        if (empObj.DataJaula.ItemsJaula[i].Quantity == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@quantity", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@quantity", empObj.DataJaula.ItemsJaula[i].Quantity));
                        }
                        if (empObj.DataJaula.ItemsJaula[i].firstScannedQty == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@firtScannedQty", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@firtScannedQty", empObj.DataJaula.ItemsJaula[i].firstScannedQty));
                        }
                        if (empObj.DataJaula.ItemsJaula[i].secondScannedQty == "")
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@secondScannedQty", null));
                        }
                        else
                        {
                            CommandDB.Parameters.Add(new SQLiteParameter("@secondScannedQty", empObj.DataJaula.ItemsJaula[i].secondScannedQty));
                        }
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1940", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public bool FindInfoVJ()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM vj_logs;";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(Query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1967", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        public void InsertLabelSend(List<string> Data)
        {
            try
            {
                string[] result3 = DateTime.Now.Date.ToShortDateString().Split(new char[] { ' ', '/' }, StringSplitOptions.RemoveEmptyEntries);

                string DateComplete = result3[2] + result3[1] + result3[0];
                ConectionDB.OpenAsync();
                using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                {
                    foreach (string i in Data)
                    {
                        string InserDataMatress = "INSERT INTO send_logsVJ(unique_serie, date_insert) SELECT ?, ?" +
                            "WHERE NOT EXISTS(SELECT 1 FROM send_logsVJ WHERE unique_serie = '" + i + "' AND date_insert = '" + DateComplete + "')";
                        CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", i));
                        CommandDB.Parameters.Add(new SQLiteParameter("@date_insert", DateComplete));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1638", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void EreaseDataSend(string Date)
        {
            try
            {
                string Query = "DELETE FROM send_logsVJ WHERE date_insert < '" + Date + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 1719", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }







        public void InsertLabelsNotSends(string Serial, string Description, string User, string IDTransfer, string BinOrin, string BinDest, string TypeSend, string Date, string Hour, string identity)
        {
            try
            {
                string InserDataMatress = "INSERT INTO send_logs (unique_serie, description, id_user, id_transfer, bin_ori, bin_dest, typesend, date, hour_reg, send, identity) SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
                                                    "WHERE NOT EXISTS(SELECT 1 FROM send_logs WHERE unique_serie = '" + Serial + "' AND id_transfer = '" + IDTransfer + "');";

                ConectionDB.OpenAsync();
                using (var transaction = ConectionDB.BeginTransaction())
                {
                    using (CommandDB = ConectionDB.CreateCommand())
                    {
                        CommandDB.CommandText = InserDataMatress;
                        CommandDB.Parameters.Add(new SQLiteParameter("@unique_serie", Serial));
                        CommandDB.Parameters.Add(new SQLiteParameter("@description", Description));
                        CommandDB.Parameters.Add(new SQLiteParameter("@id_user", User));
                        CommandDB.Parameters.Add(new SQLiteParameter("@id_transfer", IDTransfer));
                        CommandDB.Parameters.Add(new SQLiteParameter("@bin_ori", BinOrin));
                        CommandDB.Parameters.Add(new SQLiteParameter("@bin_dest", BinDest));
                        CommandDB.Parameters.Add(new SQLiteParameter("@typesend", TypeSend));
                        CommandDB.Parameters.Add(new SQLiteParameter("@date", Date));
                        CommandDB.Parameters.Add(new SQLiteParameter("@hour_reg", Hour));
                        CommandDB.Parameters.Add(new SQLiteParameter("@send", "0"));
                        CommandDB.Parameters.Add(new SQLiteParameter("@identity", identity));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2013", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void SaveLabelsNotSend(string MSG, string TypeSend, string identity, string date, string hour)
        {
            try
            {
                string InserDataMatress = "INSERT INTO resend_logs (string_notsend, send, typeSend, identity, date, hour_reg) VALUES (?, ?, ?, ?, ?, ?)";

                ConectionDB.OpenAsync();
                using (var transaction = ConectionDB.BeginTransaction())
                {
                    using (CommandDB = ConectionDB.CreateCommand())
                    {
                        //CommandDB.Connection = ConectionDB;

                        CommandDB.CommandText = InserDataMatress;
                        CommandDB.Parameters.Add(new SQLiteParameter("@string_notsend", MSG));
                        CommandDB.Parameters.Add(new SQLiteParameter("@send", "0"));
                        CommandDB.Parameters.Add(new SQLiteParameter("@typeSend", TypeSend));
                        CommandDB.Parameters.Add(new SQLiteParameter("@identity", identity));
                        CommandDB.Parameters.Add(new SQLiteParameter("@date", date));
                        CommandDB.Parameters.Add(new SQLiteParameter("@hour_reg", hour));
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2048", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            
        }
        /// <summary>
        ///Función que permite encontrar las series no enviadas a NS
        /// con ello se sabe que hay algunas o varias series no enviadas
        /// y se toma de la tabla "resend_logs"
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="AllData"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        public bool FindLabelsNotSend(string Type, bool AllData, string Date)
        {
            bool Exist = false;
            try
            {
                string query;
                if (AllData)
                {
                    query = "SELECT * FROM resend_logs WHERE typeSend = '" + Type + "' AND send = 0;";
                }
                else
                {
                    query = "SELECT * FROM resend_logs WHERE typeSend = '" + Type + "' AND send = 0 AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2094", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        /// <summary>
        /// Función que permite a los formularios "ValidateJaula", "TransferMPFrm",
        /// "TransferBin", "TransferJaula" y "TransferPTFrm" confirmar si existen valores
        /// que no han sido enviados a NS, por lo que aquí valido si existe información no
        /// enviada.
        /// </summary>
        /// <returns></returns>
        public bool ThereAreLabelsNotSend()
        {
            bool Exist = false;
            try
            {
                var query = "SELECT * FROM resend_logs WHERE send = '0';";
                ConectionDB.OpenAsync();
                using (SQLiteCommand cmd = new SQLiteCommand(query, ConectionDB))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2130", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return Exist;
        }

        public (List<string>, List<string>) LabelsNotSend(string TypeSend)
        {
            List<string> Serial = new List<string>();
            List<string> Identity = new List<string>();
            try
            {
                string Query = "SELECT string_notsend, identity FROM resend_logs WHERE send = 0 AND typeSend = '" + TypeSend + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            Serial.Add(DataReaderDB["string_notsend"].ToString());
                            Identity.Add(DataReaderDB["identity"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2161", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

            return (Serial, Identity);
        }

        public void UpdatePT(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdatePT;
                if (AllData)
                {
                    StUpdatePT = "UPDATE send_logs SET send = 1 WHERE typesend = 'PT' AND identity = '" + Identity + "'";
                }
                else
                {
                    StUpdatePT = "UPDATE send_logs SET send = 1 WHERE typesend = 'PT' AND identity = '" + Identity + "'" + " AND date = '" + Date + "'";
                }


                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdatePT, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2193", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void UpdateMP(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateMP;
                if (AllData)
                {
                    StUpdateMP = "UPDATE send_logs SET send = 1 WHERE typesend = 'MP' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateMP = "UPDATE send_logs SET send = 1 WHERE typesend = 'MP' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }


                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdateMP, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2224", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void UpdateBIN(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateBIN;
                if (AllData)
                {
                    StUpdateBIN = "UPDATE send_logs SET send = 1 WHERE typesend = 'BIN' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateBIN = "UPDATE send_logs SET send = 1 WHERE typesend = 'BIN' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdateBIN, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2254", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void UpdateTJ(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateTJ;
                if (AllData)
                {
                    StUpdateTJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'TJ' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateTJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'TJ' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdateTJ, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2284", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void UpdateVJ(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateVJ;
                if (AllData)
                {
                    StUpdateVJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'VJ' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateVJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'VJ' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdateVJ, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2314", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void UpdateStringMsg(bool AllData, string TypeSend, string Identity, string Date)
        {
            try
            {
                string StUpdate;
                if (AllData)
                {
                    StUpdate = "UPDATE resend_logs SET send = 1 WHERE typeSend = '" + TypeSend + "' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdate = "UPDATE resend_logs SET send = 1 WHERE typeSend = '" + TypeSend + "' AND identity = '" + Identity + "' AND date = '" + Date + "';";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SQLiteCommand(StUpdate, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2344", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }//*/

        //Colocar en la linea 442 cuando sea necesario
        /*if (empObj.Datos[i].msg.WorkStatus.Equals("fullyBuilt") && empObj.Datos[i].msg.DoubleBag)
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "1"));
                                CommandDB.Parameters.Add(new SQLiteParameter("@contador", "2"));
                            }
                            else if (empObj.Datos[i].msg.WorkStatus.Equals("fullyBuilt") && !empObj.Datos[i].msg.DoubleBag)
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "1"));
                                CommandDB.Parameters.Add(new SQLiteParameter("@contador", "1"));
                            }
                            else
                            {

                            }*/

        //Colocar en la linea 485 cuando sea neceario, es para cuando los lotes no puedan ser ingresados rápidamente
        /*ConectionDB.OpenAsync();
                int NextData = 0;
                int Len = empObj.Datos.Length;
                MessageBox.Show(new Form() { TopMost = true },"Cantidad de datos: "+Len.ToString());
                int Retry = 1;
                if (Len > 1000)
                {
                    Retry = Len / 1000;
                    NextData = 1000;
                }
                else
                {
                    NextData = Len;
                }
                MessageBox.Show(new Form() { TopMost = true },"Cantidad de repeticiones: " + Retry.ToString());
                int l = 0;
                for (int e = 0; e < Retry; e++)
                {
                    using (SQLiteTransaction transaction = ConectionDB.BeginTransaction())
                    {

                        for (int i = l; i < NextData; i++)
                        {
                            string InserDataMatress = "REPLACE INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, series_count, series_reprinted, id_operation1, serial_operation1, name_operation1," +
                                            "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                            "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                            "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, sell_order_small, repair_order, customer1, customer2, customer3, notBuild, date_serie)" +
                                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                            CommandDB = new SQLiteCommand(InserDataMatress, ConectionDB, transaction);
                            CommandDB.Parameters.Add(new SQLiteParameter("@uniqueSerie", empObj.Datos[i].msg.UniqueSerie));
                            CommandDB.Parameters.Add(new SQLiteParameter("@description", empObj.Datos[i].msg.Description));
                            CommandDB.Parameters.Add(new SQLiteParameter("@sku", empObj.Datos[i].msg.SKU));
                            CommandDB.Parameters.Add(new SQLiteParameter("@doubleBag", empObj.Datos[i].msg.DoubleBag));
                            if (empObj.Datos[i].msg.WorkStatus.Equals("fullyBuilt") && empObj.Datos[i].msg.DoubleBag)
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "1"));
                                CommandDB.Parameters.Add(new SQLiteParameter("@contador", "2"));
                            }
                            else if (empObj.Datos[i].msg.WorkStatus.Equals("fullyBuilt") && !empObj.Datos[i].msg.DoubleBag)
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "1"));
                                CommandDB.Parameters.Add(new SQLiteParameter("@contador", "1"));
                            }
                            else
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@terminado", "0"));
                                CommandDB.Parameters.Add(new SQLiteParameter("@contador", "0"));
                            }
                            CommandDB.Parameters.Add(new SQLiteParameter("@counterprint", empObj.Datos[i].msg.Print));
                            CommandDB.Parameters.Add(new SQLiteParameter("@printed", empObj.Datos[i].msg.Reprinted));
                            int DataLabels = empObj.Datos[i].msg.Oper.Length;
                            for (int p = 0; p < DataLabels; p++)
                            {

                                CommandDB.Parameters.Add(new SQLiteParameter("@IDOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].Process.ToString()));
                                CommandDB.Parameters.Add(new SQLiteParameter("@SerialOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].StepSerial1));
                                CommandDB.Parameters.Add(new SQLiteParameter("@NombreOperacion" + (p + 1).ToString(), empObj.Datos[i].msg.Oper[p].StepName1));
                            }
                            for (int p = DataLabels + 1; p < 9; p++)
                            {
                                CommandDB.Parameters.Add(new SQLiteParameter("@IDOperacion" + (p + 1).ToString(), ""));
                                CommandDB.Parameters.Add(new SQLiteParameter("@SerialOperacion" + (p + 1).ToString(), ""));
                                CommandDB.Parameters.Add(new SQLiteParameter("@NombreOperacion" + (p + 1).ToString(), ""));
                            }
                            CommandDB.Parameters.Add(new SQLiteParameter("@TipoProducto", empObj.Datos[i].msg.TypeLabel));
                            CommandDB.Parameters.Add(new SQLiteParameter("@MedidaProducto", empObj.Datos[i].msg.ProductMeasure));
                            CommandDB.Parameters.Add(new SQLiteParameter("@OrdenVenta", empObj.Datos[i].msg.WorkOrder));
                            CommandDB.Parameters.Add(new SQLiteParameter("@OrdenVentaSmall", empObj.Datos[i].msg.WorkOrderSmall));
                            CommandDB.Parameters.Add(new SQLiteParameter("@Reparacion", empObj.Datos[i].msg.Repair));
                            CommandDB.Parameters.Add(new SQLiteParameter("@Customer1", empObj.Datos[i].msg.Customer1));
                            CommandDB.Parameters.Add(new SQLiteParameter("@Customer2", empObj.Datos[i].msg.Customer2));
                            CommandDB.Parameters.Add(new SQLiteParameter("@Customer3", empObj.Datos[i].msg.Customer3));
                            CommandDB.Parameters.Add(new SQLiteParameter("@notBuild", empObj.Datos[i].msg.NotBuild.ToString()));
                            CommandDB.Parameters.Add(new SQLiteParameter("@Date_serie", empObj.Datos[i].msg.Createdate));
                            l++;
                            CommandDB.ExecuteNonQuery();
                        }
                        MessageBox.Show(new Form() { TopMost = true },"Contador: " + l.ToString());
                        transaction.Commit();
                        NextData = Len - 1000;
                        if (NextData == 0)
                        {
                            MessageBox.Show(new Form() { TopMost = true },"Ya no hay");
                        }
                    }
                }
                ConectionDB.Close();//*/

        /*public void ConnectDB()
        {
            //ConectionDB = new SqlConnection("Server=localhost;Integrated security=SSPI;database=master");
            ConectionDB = new SqlConnection(StringConNormaly);
        }

        public void ConnectDB2()
        {
            //ConectionDB = new SqlConnection("Server=localhost;Integrated security=SSPI;database=master");
            ConectionDB = new SqlConnection(StringCon);
        }

        public DataBase()
        {
            try
            {

                ConnectDB();

                if (ExistInDB("SELECT * FROM dbo.sysdatabases WHERE name ='THAPP'"))
                {
                    ConnectDB2();
                    DBExisted = true;
                    if (ExistInDB("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'version_log';"))
                    {
                        string ver = CounterDB("current_ver", "version_log");
                        if (ver != "")
                        {
                            if (Convert.ToDouble(Version) > Convert.ToDouble(ver))
                            {
                                DeleteTablesIfExists();
                                CreateTableIfNotExistsSQLSErver();
                                //CreateTableIfNotExists();
                                string VersDB = "UPDATE version_log SET current_ver = '" + Version + "'";
                                UpDateDB(VersDB);
                            }
                            string[] resultDate = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                            string DateInDB = CounterDB("date_mattress", "counter_mattress_logs");

                            if (!DateInDB.Equals(resultDate[0]))
                            {
                                UpDateDB("UPDATE counter_mattress_logs SET count_mattress = 0, date_mattress = '" + resultDate[0] + "';");
                            }
                        }
                        else
                        {
                            DeleteTablesIfExists();
                            CreateTableIfNotExistsSQLSErver();

                            string VersDB = "INSERT INTO version_log (current_ver) VALUES ('" + Version + "')";
                            UpDateDB(VersDB);
                        }
                    }
                    else
                    {
                        DeleteTablesIfExists();
                        CreateTableIfNotExistsSQLSErver();
                    }
                }
                else
                {
                    DBExisted = false;
                    string DB = "CREATE DATABASE THAPP";
                    UpDateDB(DB);
                    ConnectDB2();
                    CreateTableIfNotExistsSQLSErver();
                    string UserID = "INSERT INTO user_reg (id_user) VALUES  ('0'); ";
                    UpDateDB(UserID);
                    string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    string BedCounter = "INSERT INTO counter_mattress_logs (count_mattress, date_mattress) VALUES (0, '" + result[0] + "')";
                    UpDateDB(BedCounter);
                    string VersDB = "INSERT INTO version_log (current_ver) VALUES ('" + Version + "')";
                    UpDateDB(VersDB);
                }

                //}

            }
            catch (Exception e)
            {
                DBExisted = false;
                ConectionDB.Close();
                cLog.Add("Line 213", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }


        /// <summary>
        /// Función que permite saber si la tabla de datos se encontró o no,
        /// esto ayuda a saber que la aplicación ha sido lanzada por primera vez
        /// o ha sido ya utilizada en el dispositivo instalado.
        /// Este es leido por el formulario llamado "WaitPanel".
        /// </summary>
        /// <returns></returns>
        public bool DBExist()
        {
            return DBExisted;
        }

        public void SaveError(string IdUser, string DateError, string HourError, string MessageError, string Place)
        {
            try
            {
                //MessageBox.Show(new Form() { TopMost = true },MessageError);
                MessageError = MessageError.Replace("'", "");
                string BedCounter = "INSERT INTO com_logs (id_user, date_reg, hour_reg, error_reg, place_reg) VALUES (" + IdUser + ",'" + DateError + "','" + HourError + "','" + MessageError + "','" + Place + "')";
                //MessageBox.Show(new Form() { TopMost = true },MessageError);
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(BedCounter, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2504", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void CreateTableIfNotExistsSQLSErver()
        {
            try
            {
                string[] Tables2 = { "mattress_logs", "rawmaterial_logs", "pt_logs", "bins_logs", "tj_logs", "vj_logs", "counter_mattress_logs", "workstation_logs", "user_reg", "days_works", "version_log", "com_errorsNS_logs", "jaulasTJ_logs", "jaulasVJ_logs", "com_logs", "send_logs", "resend_logs" };
                string[] Tables = { TableMattress, TableMP, TablePT, TableBin, TableTJ, TableVJ, TableCounterMattress, TableWorkstation, TableUser, TableDate, TableVersion, TableErrorsNS, TableJaula, TableJaula2, TableCom, TableSend, TableStringToSend };
                string Q1 = "if not exists(select * from sysobjects where name = '";
                string Q2 = "' and xtype = 'U')";
                for (int i = 0; i < Tables.Length; i++)
                {
                    //MessageBox.Show(new Form() { TopMost = true },Tables[i]);
                    ConectionDB.OpenAsync();
                    using (CommandDB = new SqlCommand(Q1 + Tables2[i] + Q2 + Tables[i], ConectionDB))
                    {
                        CommandDB.ExecuteNonQuery();
                    }
                    ConectionDB.Close();
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2519", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }

        public void DeleteTablesIfExists()
        {
            try
            {
                string[] Tables = { "mattress_logs", "rawmaterial_logs", "pt_logs", "bins_logs", "tj_logs", "vj_logs", "com_errorsNS_logs", "jaulasTJ_logs", "jaulasVJ_logs" };
                string Query = "DROP TABLE IF EXISTS dbo.";
                for (int i = 0; i < Tables.Length; i++)
                {
                    ConectionDB.OpenAsync();
                    using (CommandDB = new SqlCommand(Query + Tables[i], ConectionDB))
                    {
                        CommandDB.ExecuteNonQuery();
                    }
                    ConectionDB.Close();
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2555", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            
        }


        public string CounterDB(string column, string table)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT " + column + " FROM " + table;
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB[column].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2585", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }

        public bool ExistInDB(string Query)
        {
            bool Exist = false;
            try
            {
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2585", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }

        public void UpDateDB(string Query)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2585", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }


        public void UpdateDateWork(string Date)
        {
            try
            {
                string Query = "UPDATE days_works SET date_works = '" + Date + "'";// AND series_count > 0"; ";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2653", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void DelateAllInTable(string table)
        {
            try
            {
                string delete = "DELETE FROM " + table;
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(delete, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2673", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void InsertAllInDB2(WorkInProgressModel2 empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    int SizeData = empObj.Datos.Length;
                    for (int f = 0; f < SizeData; f++)
                    {
                        string InserDataMatress = "INSERT INTO mattress_logs (unique_serie, description, sku, double_bag, finished, counter, series_count, series_reprinted, id_operation1, serial_operation1, name_operation1," +
                                        "id_operation2, serial_operation2, name_operation2, id_operation3, serial_operation3, name_operation3, id_operation4, serial_operation4, name_operation4," +
                                        "id_operation5, serial_operation5, name_operation5, id_operation6, serial_operation6, name_operation6, id_Operation7, serial_operation7, name_operation7," +
                                        "id_operation8, serial_operation8, name_operation8, type_product, size_product, sell_order, sell_order_small, repair_order, customer1, customer2, customer3, notBuild, date_serie)" +
                                        "VALUES (@Unique_Serie, @Description, @SKU, @Double_Bag, @Finished, @Counter, @Counterprint, @Printed, @ID_Operation1, @Serial_Operation1 ,@Name_Operation1," +
                                                    "@ID_Operation2, @Serial_Operation2, @Name_Operation2, @ID_Operation3, @Serial_Operation3, @Name_Operation3, @ID_Operation4," +
                                                    "@Serial_Operation4, @Name_Operation4, @ID_Operation5, @Serial_Operation5, @Name_Operation5, @ID_Operation6, @Serial_Operation6," +
                                                    "@Name_Operation6, @ID_Operation7, @Serial_Operation7, @Name_Operation7, @ID_Operation8, @Serial_Operation8, @Name_Operation8," +
                                                    "@Type_Product, @Size_Product, @Sell_Order, @Sell_OrderSmall, @Repair_Order, @Customer1, @Customer2, @Customer3, @notBuild, @Date_serie)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@Unique_Serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Description", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@SKU", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Double_Bag", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@Finished", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@Counter", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Counterprint", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@Printed", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@ID_Operation1", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation1", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation1", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation2", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation2", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation2", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation3", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation3", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation3", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation4", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation4", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation4", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation5", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation5", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation5", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation6", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation6", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation6", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation7", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation7", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation7", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@ID_Operation8", SqlDbType.Int);
                        CommandDB.Parameters.Add("@Serial_Operation8", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Name_Operation8", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Type_Product", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Size_Product", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Sell_Order", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Sell_OrderSmall", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Repair_Order", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Customer1", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Customer2", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@Customer3", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@notBuild", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@Date_serie", SqlDbType.Int);
                    
                        CommandDB.Parameters["@Unique_Serie"].Value = empObj.Datos[f].msg.UniqueSerie;
                        CommandDB.Parameters["@Description"].Value = empObj.Datos[f].msg.Description;
                        CommandDB.Parameters["@SKU"].Value = empObj.Datos[f].msg.SKU;
                        CommandDB.Parameters["@Double_Bag"].Value = empObj.Datos[f].msg.DoubleBag;
                        CommandDB.Parameters["@Finished"].Value = false;
                        CommandDB.Parameters["@Counter"].Value = 0;
                        CommandDB.Parameters["@Counterprint"].Value = empObj.Datos[f].msg.Print;
                        CommandDB.Parameters["@Printed"].Value = empObj.Datos[f].msg.Reprinted;

                        int DataLabels = empObj.Datos[f].msg.Oper.Length;
                        if (DataLabels > 0)
                        {
                            int DataLabelsRest = 8 - DataLabels;
                            for (int p = 0; p < 8; p++)
                            {
                                if (p < DataLabels)
                                {
                                    CommandDB.Parameters["@ID_Operation" + (p + 1).ToString()].Value = empObj.Datos[f].msg.Oper[p].Process;
                                    CommandDB.Parameters["@Serial_Operation" + (p + 1).ToString()].Value = empObj.Datos[f].msg.Oper[p].StepSerial1;
                                    CommandDB.Parameters["@Name_Operation" + (p + 1).ToString()].Value = empObj.Datos[f].msg.Oper[p].StepName1;
                                }
                                else
                                {
                                    CommandDB.Parameters["@ID_Operation" + (p + 1).ToString()].Value = 0;
                                    CommandDB.Parameters["@Serial_Operation" + (p + 1).ToString()].Value = "";
                                    CommandDB.Parameters["@Name_Operation" + (p + 1).ToString()].Value = "";
                                }
                            }
                        }
                        else
                        {
                            for (int p = 0; p < 8; p++)
                            {
                                CommandDB.Parameters["@ID_Operation" + (p + 1).ToString()].Value = 0;
                                CommandDB.Parameters["@Serial_Operation" + (p + 1).ToString()].Value = "";
                                CommandDB.Parameters["@Name_Operation" + (p + 1).ToString()].Value = "";
                            }
                        }
                        

                        CommandDB.Parameters["@Type_Product"].Value = empObj.Datos[f].msg.TypeLabel;
                        CommandDB.Parameters["@Size_Product"].Value = empObj.Datos[f].msg.ProductMeasure;
                        CommandDB.Parameters["@Sell_Order"].Value = empObj.Datos[f].msg.WorkOrder;
                        if (!empObj.Datos[f].msg.WorkOrderSmall.Equals("") || !empObj.Datos[f].msg.WorkOrderSmall.Equals(null))
                        {
                            CommandDB.Parameters["@Sell_OrderSmall"].Value = empObj.Datos[f].msg.WorkOrderSmall;
                        }
                        else
                        {
                            CommandDB.Parameters["@Sell_OrderSmall"].Value = "";
                        }
                        
                        CommandDB.Parameters["@Repair_Order"].Value = empObj.Datos[f].msg.Repair;
                        CommandDB.Parameters["@Customer1"].Value = empObj.Datos[f].msg.Customer1;
                        CommandDB.Parameters["@Customer2"].Value = empObj.Datos[f].msg.Customer2;
                        CommandDB.Parameters["@Customer3"].Value = empObj.Datos[f].msg.Customer3;
                        CommandDB.Parameters["@notBuild"].Value = empObj.Datos[f].msg.NotBuild;
                        CommandDB.Parameters["@Date_serie"].Value = empObj.Datos[f].msg.Createdate;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }

                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2809", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void InsertErrors(int j, Com_logs_ns empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.data.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO com_errorsNS_logs (recordType, id_error, name_user, custrecord_trp_error, custrecord_trp_numero_serie, custrecord_trp_request," +
                                    "custrecord_trp_ubicacion, custrecord_trp_proceso, custrecord_trp_tipo_movimiento) VALUES (@recordType, @id_error, @name_user, @custrecord_trp_error, @custrecord_trp_numero_serie, @custrecord_trp_request, @custrecord_trp_ubicacion, @custrecord_trp_proceso, @custrecord_trp_tipo_movimiento)";
                                    //"WHERE NOT EXISTS(SELECT 1 FROM com_errorsNS_logs WHERE id_error = '" + empObj.data[i].id + "');";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@recordType", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@id_error", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@name_user", SqlDbType.Int);
                        CommandDB.Parameters.Add("@custrecord_trp_error", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@custrecord_trp_numero_serie", SqlDbType.Int);
                        CommandDB.Parameters.Add("@custrecord_trp_request", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@custrecord_trp_ubicacion", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@custrecord_trp_proceso", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@custrecord_trp_tipo_movimiento", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@recordType"].Value = empObj.data[i].recordType;
                        CommandDB.Parameters["@id_error"].Value = empObj.data[i].id;
                        int CQ = 0, Q = 0;
                        bool parse1 = int.TryParse(empObj.data[i].values.name, out CQ);
                        bool parse2 = int.TryParse(empObj.data[i].values.custrecord_trp_numero_serie, out Q);
                        if (!parse1)
                        {
                            CQ = 0;
                        }
                        if (!parse2)
                        {
                            Q = 0;
                        }
                        CommandDB.Parameters["@name_user"].Value = CQ;
                        CommandDB.Parameters["@custrecord_trp_error"].Value = empObj.data[i].values.custrecord_trp_error;
                        CommandDB.Parameters["@custrecord_trp_numero_serie"].Value = Q;
                        CommandDB.Parameters["@custrecord_trp_request"].Value = empObj.data[i].values.custrecord_trp_request;
                        CommandDB.Parameters["@custrecord_trp_ubicacion"].Value = "";
                        CommandDB.Parameters["@custrecord_trp_proceso"].Value = empObj.data[i].values.custrecord_trp_proceso;
                        CommandDB.Parameters["@custrecord_trp_tipo_movimiento"].Value = empObj.data[i].values.custrecord_trp_tipo_movimiento;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2867", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }


        /// <summary>
        /// Función que permite el envio de la información contenida en la BD,
        /// el que permite el intercambio de información es "TypeQuery", ya que con ese
        /// se puede especificar el query que necesitamos para la BD.
        /// Los demás valores pueden usarse o no, dependiendo del caso y de la pantalla que
        /// lo llame, en ocasiones estos datos vienen vacios porque no se necesitan.
        /// Este es leido por los formularios "SeriesPrintingFrm", "Com_LogFrm" y "RetrySendingFrm".
        /// </summary>
        /// <param name="TypeQuery"></param>
        /// <param name="Date1"></param>
        /// <param name="Date2"></param>
        /// <param name="OrderWrok"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataTable DataTableBD(int TypeQuery, string Date1, string Date2, string OrderWrok, string UserID, string SKU)
        {
            try
            {
                string Query = "";
                switch (TypeQuery)
                {
                    case 0:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                        break;
                    case 1:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                        break;
                    case 2:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                        break;
                    case 3:
                        Query = "SELECT * FROM com_logs WHERE date_reg = '" + Date1 + "';";
                        break;
                    case 4:
                        Query = "SELECT * FROM com_logs WHERE id_user = " + UserID + ";";
                        break;
                    case 5:
                        Query = "SELECT * FROM com_logs WHERE id_user = " + UserID + " AND date_reg = '" + Date1 + "';";
                        break;
                    case 6:
                        Query = "SELECT * FROM com_logs;";
                        break;
                    case 7:
                        Query = "SELECT recordType, name_user, custrecord_trp_numero_serie, custrecord_trp_tipo_movimiento FROM com_errorsNS_logs";
                        break;
                    case 8:
                        Query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE send = '0';";
                        break;
                    case 9:
                        Query = "SELECT unique_serie, description, id_user, typesend FROM send_logs WHERE date = \"" + Date1 + "\" AND send = '0';";
                        break;
                    case 10:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sku = '" + SKU + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                        break;
                    case 11:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sku = '" + SKU + "'AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "';";
                        break;
                    case 12:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                        break;
                    case 13:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sku = '" + SKU + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                        break;
                    case 14:
                        Query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = 'false' AND sku = '" + SKU + "'AND sell_order = '" + OrderWrok + "' AND date_serie BETWEEN '" + Date1 + "' AND '" + Date2 + "' AND series_count < 1;";
                        break;
                }
                //MessageBox.Show(new Form() { TopMost = true },Query);
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    //cmd.CommandType = CommandType.Text;
                    using (DataAdapter = new SqlDataAdapter(CommandDB))
                    {

                        using (DT = new DataTable())
                        {

                            DataAdapter.Fill(DT);
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 2960", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DT;
        }

        public WorkInProgressModel TablesDatas(string serie)
        {
            WorkInProgressModel Data = new WorkInProgressModel();
            try
            {
                string sqlQuery = "SELECT * FROM mattress_logs WHERE unique_serie = '" + serie + "'";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(sqlQuery, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            Data.Serie = DataReaderDB["unique_serie"].ToString();
                            Data.Descripcion = DataReaderDB["description"].ToString();
                            Data.SKU = DataReaderDB["sku"].ToString();

                            Data.doubleBag = Convert.ToBoolean(DataReaderDB["double_bag"]);
                            Data.Terminado = Convert.ToBoolean(DataReaderDB["finished"]);
                            Data.Contador = Convert.ToInt16(DataReaderDB["counter"]);
                            Data.CounterPrint = Convert.ToInt16(DataReaderDB["series_count"]);
                            Data.Reprinted = Convert.ToBoolean(DataReaderDB["series_reprinted"]);

                            Data.StepID1 = DataReaderDB["id_operation1"].ToString();
                            Data.StepSerial1 = DataReaderDB["serial_operation1"].ToString();
                            Data.StepName1 = DataReaderDB["name_operation1"].ToString();

                            Data.StepID2 = DataReaderDB["id_operation2"].ToString();
                            Data.StepSerial2 = DataReaderDB["serial_operation2"].ToString();
                            Data.StepName2 = DataReaderDB["name_operation2"].ToString();

                            Data.StepID3 = DataReaderDB["id_operation3"].ToString();
                            Data.StepSerial3 = DataReaderDB["serial_operation3"].ToString();
                            Data.StepName3 = DataReaderDB["name_operation3"].ToString();

                            Data.StepID4 = DataReaderDB["id_operation4"].ToString();
                            Data.StepSerial4 = DataReaderDB["serial_operation4"].ToString();
                            Data.StepName4 = DataReaderDB["name_operation4"].ToString();

                            Data.StepID5 = DataReaderDB["id_operation5"].ToString();
                            Data.StepSerial5 = DataReaderDB["serial_operation5"].ToString();
                            Data.StepName5 = DataReaderDB["name_operation5"].ToString();

                            Data.StepID6 = DataReaderDB["id_operation6"].ToString();
                            Data.StepSerial6 = DataReaderDB["serial_operation6"].ToString();
                            Data.StepName6 = DataReaderDB["name_operation6"].ToString();

                            Data.StepID7 = DataReaderDB["id_operation7"].ToString();
                            Data.StepSerial7 = DataReaderDB["serial_operation7"].ToString();
                            Data.StepName7 = DataReaderDB["name_operation7"].ToString();

                            Data.StepID8 = DataReaderDB["id_operation8"].ToString();
                            Data.StepSerial8 = DataReaderDB["serial_operation8"].ToString();
                            Data.StepName8 = DataReaderDB["name_operation8"].ToString();

                            Data.TipoProducto = DataReaderDB["type_product"].ToString();
                            Data.MedidaProducto = DataReaderDB["size_product"].ToString();
                            Data.OrdenVenta = DataReaderDB["sell_order"].ToString();
                            Data.OrdenVentaSmall = DataReaderDB["sell_order_small"].ToString();
                            Data.Reparacion = DataReaderDB["repair_order"].ToString();

                            Data.Customer1 = DataReaderDB["customer1"].ToString();
                            Data.Customer2 = DataReaderDB["customer2"].ToString();
                            Data.Customer3 = DataReaderDB["customer3"].ToString();

                            Data.NotBuild = Convert.ToBoolean(DataReaderDB["notBuild"]);

                            Data.DateSerie = Convert.ToInt64(DataReaderDB["date_serie"]);
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3042", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Data;
        }


        public void InserDayWork(string Date)
        {
            try
            {
                string Query = "INSERT INTO days_works (date_works) VALUES ('" + Date + "')";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3064", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }

        }

        public void DeleteAllLabelsWithDate(string DateComplete)
        {
            try
            {
                string Query = "DELETE FROM mattress_logs WHERE date_serie < '" + DateComplete + "'";// AND series_count > 0";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3085", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }




        //######Todo lo relacionado con Transferencias MP######
        public void SaveMP(int j, DataMP empObj, string bin)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Data.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO rawmaterial_logs (unique_serie, description, dateCration, sku, employee, currentquantity, quantity, labelZero, scaned, bin, binName, location)" +
                                              "VALUES (@uniqueSerie, @description, @dateCration, @sku, @employee, @currentquantity, @quantity, @labelZero, @scaned, @bin, @binName, @location)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@uniqueSerie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@description", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@dateCration", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@sku", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@employee", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@currentquantity", SqlDbType.Int);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.Int);
                        CommandDB.Parameters.Add("@labelZero", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@scaned", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@bin", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@location", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@uniqueSerie"].Value = empObj.Data[i].NumeroSerie;
                        CommandDB.Parameters["@description"].Value = empObj.Data[i].Description;
                        CommandDB.Parameters["@dateCration"].Value = empObj.Data[i].DateCreate;
                        CommandDB.Parameters["@sku"].Value = empObj.Data[i].SKU;
                        CommandDB.Parameters["@employee"].Value = empObj.Data[i].Employee;
                        int CQ = 0, Q = 0;
                        bool parse1 = int.TryParse(empObj.Data[i].Currentquantity, out CQ);
                        bool parse2 = int.TryParse(empObj.Data[i].Quantity, out Q);
                        if (!parse1)
                        {
                            CQ = 0;
                        }
                        if (!parse2)
                        {
                            Q = 0;
                        }
                        CommandDB.Parameters["@currentquantity"].Value = CQ;
                        CommandDB.Parameters["@quantity"].Value = Q;
                        CommandDB.Parameters["@labelZero"].Value = empObj.Data[i].LabelZero;
                        CommandDB.Parameters["@scaned"].Value = false;
                        CommandDB.Parameters["@bin"].Value = empObj.Data[i].BinId;
                        CommandDB.Parameters["@binName"].Value = empObj.Data[i].BinName;
                        CommandDB.Parameters["@location"].Value = empObj.Data[i].Location;
                        CommandDB.ExecuteNonQuery();

                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3148", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseMP(string Serial)
        {
            try
            {
                string Query = "DELETE FROM rawmaterial_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3167", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public string FindDescriptionMP(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT description FROM rawmaterial_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["description"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3193", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public bool FindQuantityZero(string Serie)
        {
            bool DataINDB = false;
            try
            {
                string Query = "SELECT labelZero FROM rawmaterial_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = Convert.ToBoolean(DataReaderDB["labelZero"]);
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3220", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public bool FindMPSerialWithBin(string Serie, string Bin)
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM rawmaterial_logs WHERE unique_serie = '" + Serie + "' AND bin = '" + Bin + "';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (rdr["bin"].ToString().Equals(Bin))
                            {
                                Exist = true;
                            }
                        }
                        
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3247", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }
        public bool FindInfoMP()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM rawmaterial_logs;";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3274", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }


        //######Todo lo relacionado con Transferencias PT######
        public void SavePT(int j, UbicationsPT empObj, string idUbi)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO pt_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@unique_serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@item", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@itemID", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binId", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@idLocation", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@unique_serie"].Value = empObj.Series[i].serial;
                        CommandDB.Parameters["@item"].Value = empObj.Series[i].item;
                        CommandDB.Parameters["@itemID"].Value = empObj.Series[i].itemID;
                        CommandDB.Parameters["@quantity"].Value = empObj.Series[i].quantity;
                        CommandDB.Parameters["@binId"].Value = empObj.Series[i].binID;
                        CommandDB.Parameters["@binName"].Value = empObj.Series[i].binName;
                        CommandDB.Parameters["@idLocation"].Value = empObj.Series[i].Location;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3318", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreasePT(string Serial)
        {
            try
            {
                string Query = "DELETE FROM pt_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3337", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public string FindDescriptionPT(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT item FROM pt_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["item"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3363", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public List<string> FindIDBins(string Ubication)
        {
            List<string> ListID = new List<string>();
            try
            {
                string Query = "SELECT DISTINCT binId FROM pt_logs WHERE idLocation = '" + Ubication + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            ListID.Add(DataReaderDB["binId"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3390", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return ListID;
        }
        public bool FindPTSerialWithBin(string Serie, string Bin)
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM pt_logs WHERE unique_serie = '" + Serie + "' AND binId = '" + Bin + "';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (rdr["binId"].ToString().Equals(Bin))
                            {
                                Exist = true;
                            }
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3417", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }
        public bool FindInfoPT()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM pt_logs;";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3444", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }




        //######Todo lo relacionado con Transferencias BIN######
        public void SaveBIN(int j, UbicationsPT empObj, string idUbi)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO bins_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@unique_serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@item", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@itemID", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binId", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@idLocation", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@unique_serie"].Value = empObj.Series[i].serial;
                        CommandDB.Parameters["@item"].Value = empObj.Series[i].item;
                        CommandDB.Parameters["@itemID"].Value = empObj.Series[i].itemID;
                        CommandDB.Parameters["@quantity"].Value = empObj.Series[i].quantity;
                        CommandDB.Parameters["@binId"].Value = empObj.Series[i].binID;
                        CommandDB.Parameters["@binName"].Value = empObj.Series[i].binName;
                        CommandDB.Parameters["@idLocation"].Value = idUbi;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3490", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseBIN(string Serial)
        {
            try
            {
                string Query = "DELETE FROM bins_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3509", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public string FindDescriptionBIN(string Serie)
        {
            string DataINDB = "";
            try
            {
                string Query = "SELECT item FROM bins_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB = DataReaderDB["item"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3535", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public List<string> FindBINIDBins(string Ubication)
        {
            List<string> ListID = new List<string>();
            try
            {
                string Query = "SELECT DISTINCT binId FROM pt_logs WHERE idLocation = '" + Ubication + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            ListID.Add(DataReaderDB["binId"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3562", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return ListID;
        }
        public (bool, bool) FindBINSerialWithBin(string Serie, string Bin)
        {
            bool Exist = false;
            bool SameBin = false;
            try
            {
                string BinA = "";
                
                string Query = "SELECT binId FROM bins_logs WHERE unique_serie = '" + Serie + "';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            //if (rdr["binId"].ToString().Equals(Bin))
                            //{
                                Exist = true;
                                BinA = rdr["binId"].ToString();
                            //}
                            

                        }
                    }
                }
                ConectionDB.Close();
                int B = 0, Bi = 0;
                bool parse1 = int.TryParse(Bin, out B);
                bool parse2 = int.TryParse(BinA, out Bi);
                if (!parse1)
                {
                    B = 0;
                }
                if (!parse2)
                {
                    Bi = 0;
                }

                if (Bi == B && Bi != 0 && B != 0)
                {
                    SameBin = true;
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3600", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return (Exist, SameBin);
        }
        public bool FindInfoBIN()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM bins_logs;";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3627", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }




        //######Todo lo relacionado con Transferencias JAULA######
        public void SaveTJ(int j, UbicationsPT empObj, string idUbi)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO tj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@unique_serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@item", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@itemID", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binId", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@idLocation", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@unique_serie"].Value = empObj.Series[i].serial;
                        CommandDB.Parameters["@item"].Value = empObj.Series[i].item;
                        CommandDB.Parameters["@itemID"].Value = empObj.Series[i].itemID;
                        CommandDB.Parameters["@quantity"].Value = empObj.Series[i].quantity;
                        CommandDB.Parameters["@binId"].Value = empObj.Series[i].binID;
                        CommandDB.Parameters["@binName"].Value = empObj.Series[i].binName;
                        CommandDB.Parameters["@idLocation"].Value = idUbi;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3673", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseTJ(string Serial)
        {
            try
            {
                string Query = "DELETE FROM tj_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3692", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public int UpdateDBJaulaTransfer(string Serie, string JaulaID)
        {
            int RefreshData = 0;
            try
            {
                string SKUID = "";
                string query = "SELECT itemID FROM tj_logs WHERE unique_serie = '" + Serie + "'";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            SKUID = DataReaderDB["itemID"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
                if (SKUID.Length > 0)
                {
                    bool Exist = false;
                    string Query = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                    ConectionDB.OpenAsync();
                    using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                    {
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read() == true)
                            {
                                Exist = true;
                            }
                        }
                    }
                    ConectionDB.Close();
                    int Quantity = 0, QuantityToScan = 0, idCount = 0;
                    if (Exist)
                    {
                        string TakeQuantity = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND quantity != firtScannedQty ;";
                        //string TakeQuantity = "SELECT idCount, quantity, firtScannedQty FROM jaulasTJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                        RefreshData = 6;
                        ConectionDB.OpenAsync();
                        using (CommandDB = new SqlCommand(TakeQuantity, ConectionDB))
                        {
                            using (DataReaderDB = CommandDB.ExecuteReader())
                            {
                                if (DataReaderDB.Read())
                                {
                                    if (!DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 1;
                                        idCount = Convert.ToInt32(DataReaderDB["idCount"]);
                                        QuantityToScan = Convert.ToInt32(DataReaderDB["quantity"]);
                                        Quantity = Convert.ToInt32(DataReaderDB["firtScannedQty"]);
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 2;
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 3;
                                    }
                                    else if (!DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["firtScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 4;
                                    }
                                }
                            }
                        }
                        ConectionDB.Close();
                    }
                    if (RefreshData == 1)
                    {
                        Quantity++;
                        if (QuantityToScan >= Quantity)
                        {
                            ConectionDB.Close();
                            string UpdateJaula = "UPDATE jaulasTJ_logs SET firtScannedQty = " + Quantity + " WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND idCount = '" + idCount + "';";
                            ConectionDB.OpenAsync();
                            using (CommandDB = new SqlCommand(UpdateJaula, ConectionDB))
                            {
                                CommandDB.ExecuteNonQuery();
                            }
                            ConectionDB.Close();
                        }
                        else
                        {
                            RefreshData = 5;
                        }

                    }
                    if (RefreshData == 6)
                    {
                        RefreshData = 5;
                    }
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3797", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return RefreshData;
        }
        public bool FindSerialsWithSKUTJ(string serie, string sku)
        {
            bool Exist = false;
            try
            {
                string query = "SELECT * FROM tj_logs WHERE unique_serie = '" + serie + "' AND itemID = '" + sku + "';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (rdr["itemID"].ToString().Equals(sku))
                            {
                                Exist = true;
                            }
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3824", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }
        public List<Int64> TakeIDSKUFromJaulas(string IDJaula)
        {
            List<Int64> DataINDB = new List<Int64>();
            try
            {
                string Query = "SELECT skuID FROM jaulasTJ_logs WHERE id_jaula = '" + IDJaula + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB.Add(Convert.ToInt64(DataReaderDB["skuID"]));
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3851", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public void SaveJaula(int j, PlannedInventory empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.DataJaula.ItemsJaula.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO jaulasTJ_logs (idCount, name_jaula, id_jaula, binNumber, binName, skuName, skuID, quantity, firtScannedQty, secondScannedQty)" +
                                      "VALUES (@idCount, @name_jaula, @id_jaula, @binNumber, @binName, @skuName, @skuID, @quantity, @firtScannedQty, @secondScannedQty)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@idCount", SqlDbType.Int);
                        CommandDB.Parameters.Add("@name_jaula", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@id_jaula", SqlDbType.Int);
                        CommandDB.Parameters.Add("@binNumber", SqlDbType.Int);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@skuName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@skuID", SqlDbType.Int);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@firtScannedQty", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@secondScannedQty", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@idCount"].Value = (i + 1);
                        CommandDB.Parameters["@name_jaula"].Value = empObj.DataJaula.NameJaula;
                        CommandDB.Parameters["@id_jaula"].Value = empObj.DataJaula.IDJaula;
                        int binN = 0, SKUI = 0;
                        bool parse1 = int.TryParse(empObj.DataJaula.BinNumber, out binN);
                        bool parse2 = int.TryParse(empObj.DataJaula.ItemsJaula[i].SKUID, out SKUI);
                        if (!parse1)
                        {
                            binN = 0;
                        }
                        if (!parse2)
                        {
                            SKUI = 0;
                        }
                        CommandDB.Parameters["@binNumber"].Value = binN;
                        CommandDB.Parameters["@binName"].Value = empObj.DataJaula.BinName;
                        CommandDB.Parameters["@skuName"].Value = empObj.DataJaula.ItemsJaula[i].SKUName;
                        CommandDB.Parameters["@skuID"].Value = SKUI;
                        if (empObj.DataJaula.ItemsJaula[i].Quantity == "")
                        {
                            CommandDB.Parameters["@quantity"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@quantity"].Value = empObj.DataJaula.ItemsJaula[i].Quantity;
                        }
                        if (empObj.DataJaula.ItemsJaula[i].firstScannedQty == "")
                        {
                            CommandDB.Parameters["@firtScannedQty"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@firtScannedQty"].Value = empObj.DataJaula.ItemsJaula[i].firstScannedQty;
                        }
                        if (empObj.DataJaula.ItemsJaula[i].secondScannedQty == "")
                        {
                            CommandDB.Parameters["@secondScannedQty"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@secondScannedQty"].Value = empObj.DataJaula.ItemsJaula[i].secondScannedQty;
                        }
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3930", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public bool FindInfoTJ()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM tj_logs;";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3956", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }

        //######Todo lo relacionado con la Validación JAULA######
        public void SaveVJ(int j, UbicationsPT empObj, string idUbi)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.Series.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO vj_logs (unique_serie, item, itemID, quantity, binId, binName, idLocation)" +
                                    "VALUES (@unique_serie, @item, @itemID, @quantity, @binId, @binName, @idLocation)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@unique_serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@item", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@itemID", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binId", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@idLocation", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@unique_serie"].Value = empObj.Series[i].serial;
                        CommandDB.Parameters["@item"].Value = empObj.Series[i].item;
                        CommandDB.Parameters["@itemID"].Value = empObj.Series[i].itemID;
                        CommandDB.Parameters["@quantity"].Value = empObj.Series[i].quantity;
                        CommandDB.Parameters["@binId"].Value = empObj.Series[i].binID;
                        CommandDB.Parameters["@binName"].Value = empObj.Series[i].binName;
                        CommandDB.Parameters["@idLocation"].Value = idUbi;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 3956", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public void EreaseVJ(string Serial)
        {
            try
            {
                string Query = "DELETE FROM vj_logs WHERE unique_serie = '" + Serial + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4018", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public int UpdateDBJaulaValidation(string Serie, string bin, string JaulaID)
        {
            int RefreshData = 0;
            try
            {
                string SKUID = "";
                string query = "SELECT itemID FROM vj_logs WHERE unique_serie = '" + Serie + "' AND binId = '" + bin + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            SKUID = DataReaderDB["itemID"].ToString();
                        }
                    }
                }
                ConectionDB.Close();
                if (SKUID.Length > 0)
                {
                    bool Exist = false;
                    string Query = "SELECT idCount, quantity, firtScannedQty FROM jaulasVJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "';";
                    ConectionDB.OpenAsync();
                    using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                    {
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read() == true)
                            {
                                Exist = true;
                            }
                        }
                    }
                    ConectionDB.Close();
                    int Quantity = 0, QuantityToScan = 0, idCount = 0;
                    if (Exist)
                    {
                        string TakeQuantity = "SELECT idCount, quantity, secondScannedQty FROM jaulasVJ_logs WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND quantity != secondScannedQty;";
                        RefreshData = 6;
                        ConectionDB.OpenAsync();
                        using (CommandDB = new SqlCommand(TakeQuantity, ConectionDB))
                        {
                            using (DataReaderDB = CommandDB.ExecuteReader())
                            {
                                while (DataReaderDB.Read())
                                {
                                    if (!DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 1;
                                        idCount = Convert.ToInt32(DataReaderDB["idCount"]);
                                        QuantityToScan = Convert.ToInt32(DataReaderDB["quantity"]);
                                        Quantity = Convert.ToInt32(DataReaderDB["secondScannedQty"]);
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 2;
                                    }
                                    else if (DataReaderDB["quantity"].ToString().Equals("") && !DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 3;
                                    }
                                    else if (!DataReaderDB["quantity"].ToString().Equals("") && DataReaderDB["secondScannedQty"].ToString().Equals(""))
                                    {
                                        RefreshData = 4;
                                    }

                                }
                            }
                        }
                        ConectionDB.Close();
                    }
                    if (RefreshData == 1)
                    {
                        Quantity++;
                        if (QuantityToScan >= Quantity)
                        {
                            ConectionDB.Close();
                            string UpdateJaula = "UPDATE jaulasVJ_logs SET secondScannedQty = " + Quantity + " WHERE skuID = '" + SKUID + "' AND id_jaula = '" + JaulaID + "' AND idCount = '" + idCount + "';";
                            ConectionDB.OpenAsync();
                            using (CommandDB = new SqlCommand(UpdateJaula, ConectionDB))
                            {
                                CommandDB.ExecuteNonQuery();
                            }
                            ConectionDB.Close();
                        }
                        else
                        {
                            RefreshData = 5;
                        }
                    }
                    if (RefreshData == 6)
                    {
                        RefreshData = 5;
                    }
                }
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4122", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return RefreshData;
        }
        public bool FindSerialWithSKUAndBINVJ(string serie, string sku, string bin)
        {
            bool Exist = false;
            try
            {
                string query = "SELECT * FROM vj_logs WHERE unique_serie = '" + serie + "' AND itemID = '" + sku + "' AND binId = '" + bin + "';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (rdr["itemID"].ToString().Equals(sku) && rdr["binId"].ToString().Equals(bin))
                            {
                                Exist = true;
                            }
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4149", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }
        public List<Int64> TakeIDSKUFromJaulas2(string IDJaula)
        {
            List<Int64> DataINDB = new List<Int64>();
            try
            {
                string Query = "SELECT skuID FROM jaulasVJ_logs WHERE id_jaula = '" + IDJaula + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            DataINDB.Add(Convert.ToInt64(DataReaderDB["skuID"]));
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4176", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return DataINDB;
        }
        public void SaveJaula2(int j, PlannedInventory empObj)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < empObj.DataJaula.ItemsJaula.Length; i++)
                    {
                        string InserDataMatress = "INSERT INTO jaulasVJ_logs (idCount, name_jaula, id_jaula, binNumber, binName, skuName, skuID, quantity, firtScannedQty, secondScannedQty)" +
                                       "VALUES (@idCount, @name_jaula, @id_jaula, @binNumber, @binName, @skuName, @skuID, @quantity, @firtScannedQty, @secondScannedQty)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@idCount", SqlDbType.Int);
                        CommandDB.Parameters.Add("@name_jaula", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@id_jaula", SqlDbType.Int);
                        CommandDB.Parameters.Add("@binNumber", SqlDbType.Int);
                        CommandDB.Parameters.Add("@binName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@skuName", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@skuID", SqlDbType.Int);
                        CommandDB.Parameters.Add("@quantity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@firtScannedQty", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@secondScannedQty", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@idCount"].Value = (i + 1);
                        CommandDB.Parameters["@name_jaula"].Value = empObj.DataJaula.NameJaula;
                        CommandDB.Parameters["@id_jaula"].Value = empObj.DataJaula.IDJaula;
                        int binN = 0, SKUI = 0;
                        bool parse1 = int.TryParse(empObj.DataJaula.BinNumber, out binN);
                        bool parse2 = int.TryParse(empObj.DataJaula.ItemsJaula[i].SKUID, out SKUI);
                        if (!parse1)
                        {
                            binN = 0;
                        }
                        if (!parse2)
                        {
                            SKUI = 0;
                        }
                        CommandDB.Parameters["@binNumber"].Value = binN;
                        CommandDB.Parameters["@binName"].Value = empObj.DataJaula.BinName;
                        CommandDB.Parameters["@skuName"].Value = empObj.DataJaula.ItemsJaula[i].SKUName;
                        CommandDB.Parameters["@skuID"].Value = SKUI;
                        if (empObj.DataJaula.ItemsJaula[i].Quantity == "")
                        {
                            CommandDB.Parameters["@quantity"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@quantity"].Value = empObj.DataJaula.ItemsJaula[i].Quantity;
                        }
                        if (empObj.DataJaula.ItemsJaula[i].firstScannedQty == "")
                        {
                            CommandDB.Parameters["@firtScannedQty"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@firtScannedQty"].Value = empObj.DataJaula.ItemsJaula[i].firstScannedQty;
                        }
                        if (empObj.DataJaula.ItemsJaula[i].secondScannedQty == "")
                        {
                            CommandDB.Parameters["@secondScannedQty"].Value = null;
                        }
                        else
                        {
                            CommandDB.Parameters["@secondScannedQty"].Value = empObj.DataJaula.ItemsJaula[i].secondScannedQty;
                        }
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4255", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        public bool FindInfoVJ()
        {
            bool Exist = false;
            try
            {
                string Query = "SELECT * FROM vj_logs;";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(Query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4281", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }


        public void InsertLabelsNotSends(string Serial, string Description, string User, string IDTransfer, string BinOrin, string BinDest, string TypeSend, string Date, string Hour, string identity)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < 1; i++)
                    {
                        string InserDataMatress = "INSERT INTO send_logs (unique_serie, description, id_user, id_transfer, bin_ori, bin_dest, typesend, date, hour_reg, send, identity) VALUES (@unique_serie, @description," +
                            "@id_user, @id_transfer, @bin_ori, @bin_dest, @typesend, @date, @hour_reg, @send, @identity)";
                        //"WHERE NOT EXISTS(SELECT 1 FROM send_logs WHERE unique_serie = '" + Serial + "' AND id_transfer = '" + IDTransfer + "');";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@unique_serie", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@description", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@id_user", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@id_transfer", SqlDbType.Int);
                        CommandDB.Parameters.Add("@bin_ori", SqlDbType.Int);
                        CommandDB.Parameters.Add("@bin_dest", SqlDbType.Int);
                        CommandDB.Parameters.Add("@typesend", SqlDbType.Text);
                        CommandDB.Parameters.Add("@date", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@hour_reg", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@send", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@identity", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@unique_serie"].Value = Serial;
                        CommandDB.Parameters["@description"].Value = Description;
                        CommandDB.Parameters["@id_user"].Value = User;
                        CommandDB.Parameters["@id_transfer"].Value = IDTransfer;
                        CommandDB.Parameters["@bin_ori"].Value = BinOrin;
                        CommandDB.Parameters["@bin_dest"].Value = BinDest;
                        CommandDB.Parameters["@typesend"].Value = TypeSend;
                        CommandDB.Parameters["@date"].Value = Date;
                        CommandDB.Parameters["@hour_reg"].Value = Hour;
                        CommandDB.Parameters["@send"].Value = false;
                        CommandDB.Parameters["@identity"].Value = identity;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4338", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void SaveLabelsNotSend(string MSG, string TypeSend, string identity, string date, string hour)
        {
            try
            {
                ConectionDB.OpenAsync();
                using (SqlTransaction transaction = ConectionDB.BeginTransaction())
                {
                    for (int i = 0; i < 1; i++)
                    {
                        string InserDataMatress = "INSERT INTO resend_logs (string_notsend, send, typeSend, identity, date, hour_reg) VALUES (?, ?, ?, ?, ?, ?)";
                        CommandDB = new SqlCommand(InserDataMatress, ConectionDB, transaction);
                        CommandDB.Parameters.Add("@string_notsend", SqlDbType.Text);
                        CommandDB.Parameters.Add("@send", SqlDbType.Bit);
                        CommandDB.Parameters.Add("@typeSend", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@identity", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@date", SqlDbType.VarChar, 150);
                        CommandDB.Parameters.Add("@hour_reg", SqlDbType.VarChar, 150);

                        CommandDB.Parameters["@string_notsend"].Value = MSG;
                        CommandDB.Parameters["@send"].Value = false;
                        CommandDB.Parameters["@typeSend"].Value = TypeSend;
                        CommandDB.Parameters["@identity"].Value = identity;
                        CommandDB.Parameters["@date"].Value = date;
                        CommandDB.Parameters["@hour_reg"].Value = hour;
                        CommandDB.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4371", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }
        /// <summary>
        ///Función que permite encontrar las series no enviadas a NS
        /// con ello se sabe que hay algunas o varias series no enviadas
        /// y se toma de la tabla "resend_logs"
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="AllData"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        public bool FindLabelsNotSend(string Type, bool AllData, string Date)
        {
            bool Exist = false;
            try
            {
                string query;
                if (AllData)
                {
                    query = "SELECT * FROM resend_logs WHERE typeSend = '" + Type + "' AND send = 0;";
                }
                else
                {
                    query = "SELECT * FROM resend_logs WHERE typeSend = '" + Type + "' AND send = 0 AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4415", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }

        /// <summary>
        /// Función que permite a los formularios "ValidateJaula", "TransferMPFrm",
        /// "TransferBin", "TransferJaula" y "TransferPTFrm" confirmar si existen valores
        /// que no han sido enviados a NS, por lo que aquí valido si existe información no
        /// enviada.
        /// </summary>
        /// <returns></returns>
        public bool ThereAreLabelsNotSend()
        {
            bool Exist = false;
            try
            {
                var query = "SELECT * FROM resend_logs WHERE send = '0';";
                ConectionDB.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(query, ConectionDB))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read() == true)
                        {
                            Exist = true;
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4450", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return Exist;
        }

        public (List<string>, List<string>) LabelsNotSend(string TypeSend)
        {
            List<string> Serial = new List<string>();
            List<string> Identity = new List<string>();
            try
            {
                string Query = "SELECT string_notsend, identity FROM resend_logs WHERE send = 0 AND typeSend = '" + TypeSend + "';";
                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(Query, ConectionDB))
                {
                    using (DataReaderDB = CommandDB.ExecuteReader())
                    {
                        while (DataReaderDB.Read())
                        {
                            Serial.Add(DataReaderDB["string_notsend"].ToString());
                            Identity.Add(DataReaderDB["identity"].ToString());
                        }
                    }
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4480", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
            return (Serial, Identity);
        }

        public void UpdatePT(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdatePT;
                if (AllData)
                {
                    StUpdatePT = "UPDATE send_logs SET send = 1 WHERE typesend = 'PT' AND identity = '" + Identity + "'";
                }
                else
                {
                    StUpdatePT = "UPDATE send_logs SET send = 1 WHERE typesend = 'PT' AND identity = '" + Identity + "'" + " AND date = '" + Date + "'";
                }


                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdatePT, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4511", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void UpdateMP(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateMP;
                if (AllData)
                {
                    StUpdateMP = "UPDATE send_logs SET send = 1 WHERE typesend = 'MP' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateMP = "UPDATE send_logs SET send = 1 WHERE typesend = 'MP' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }


                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdateMP, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4541", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void UpdateBIN(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateBIN;
                if (AllData)
                {
                    StUpdateBIN = "UPDATE send_logs SET send = 1 WHERE typesend = 'BIN' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateBIN = "UPDATE send_logs SET send = 1 WHERE typesend = 'BIN' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdateBIN, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4570", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void UpdateTJ(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateTJ;
                if (AllData)
                {
                    StUpdateTJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'TJ' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateTJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'TJ' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdateTJ, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4599", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void UpdateVJ(bool AllData, string Identity, string Date)
        {
            try
            {
                string StUpdateVJ;
                if (AllData)
                {
                    StUpdateVJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'VJ' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdateVJ = "UPDATE send_logs SET send = 1 WHERE typesend = 'VJ' AND identity = '" + Identity + "' AND date = '" + Date + "'";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdateVJ, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4628", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }

        public void UpdateStringMsg(bool AllData, string TypeSend, string Identity, string Date)
        {
            try
            {
                string StUpdate;
                if (AllData)
                {
                    StUpdate = "UPDATE resend_logs SET send = 1 WHERE typeSend = '" + TypeSend + "' AND identity = '" + Identity + "';";
                }
                else
                {
                    StUpdate = "UPDATE resend_logs SET send = 1 WHERE typeSend = '" + TypeSend + "' AND identity = '" + Identity + "' AND date = '" + Date + "';";
                }

                ConectionDB.OpenAsync();
                using (CommandDB = new SqlCommand(StUpdate, ConectionDB))
                {
                    CommandDB.ExecuteNonQuery();
                }
                ConectionDB.Close();
            }
            catch (Exception e)
            {
                ConectionDB.Close();
                cLog.Add("Line 4657", NameScreen, e.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Error en Base de Datos");
            }
        }//*/
    }
}
