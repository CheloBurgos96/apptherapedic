﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetSuiteApps
{
    class FindInString
    {
        DataBase DB = new DataBase(false);
        public FindInString()
        {

        }

        public int Found(string Response,string ID, string Place)
        {
            int TypeError = 0;
            bool ErrorNlauth = Response.Contains("Could not determine customer compid");
            if (ErrorNlauth)
            {
                String[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                DB.SaveError(ID, Date[0], Date[1], "Error NLauth_Account", Place);
                TypeError = 1;
            }
            bool ErrorMailorSignature = Response.Contains("You have entered an invalid email address or password. Please try again.");
            if (ErrorMailorSignature)
            {
                String[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(ID, Date[0], Date[1], "Error NLauth_Mail or NLauth_Signature", Place);
                TypeError = 1;
            }
            bool ErrorRole = Response.Contains("You role does not give you permission to view this page.");
            if (ErrorRole)
            {
                String[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(ID, Date[0], Date[1], "Error NLauth_Role", Place);
                TypeError = 1;
            }
            bool ErrorApplication = Response.Contains("Invalid application id:");
            if (ErrorApplication)
            {
                String[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(ID, Date[0], Date[1], "Error NLauth_Application", Place);
                TypeError = 1;
            }
            return TypeError;
        }

    }
}
