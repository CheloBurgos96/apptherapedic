﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows;

namespace NetSuiteApps
{
    public class QueryNS
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "NS Query";
        DataBase DB = new DataBase(false);

        private void ErrorSerialization(EmployeeModel Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ.RoleId, Date[0], Date[1] + " " + Date[2]+Date[3], error, Place);
        }

        private void ErrorJsonReader(EmployeeModel Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ.RoleId, Date[0], Date[1] + " " + Date[2] + Date[3], error, Place);
        }

        private void ErrorWithOutInternet(EmployeeModel Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ.RoleId, Date[0], Date[1] + " " + Date[2] + Date[3], error, Place);
        }

        private void ErrorSerialization2(string Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ, Date[0], Date[1] + " " + Date[2] + Date[3], error, Place);
        }

        private void ErrorJsonReader2(string Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ, Date[0], Date[1] + " " + Date[2] + Date[3], error, Place);
        }

        private void ErrorWithOutInternet2(string Employ, string error, string Place)
        {
            string[] Date = DateTime.Now.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            DB.SaveError(Employ, Date[0], Date[1] + " " + Date[2] + Date[3], error, Place);
        }

        public (bool, WorkInProgressModel2) LabelsToPrint(string Date1, string Date2, bool FormPrint, EmployeeModel Employ, string Place)
        {
            bool DataCatch = false;
            WorkInProgressModel2 Data = new WorkInProgressModel2();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string jsonRequest = "{ \"fechaInicio\":\"" + Date1 + "\", \"fechaFin\":\"" + Date2 + "\"}";
                string URL = Reslet.SQueryPrintLabels;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2042&deploy=1";
                NSAuthentication Connection = new NSAuthentication();
                //MessageBox.Show(jsonRequest);
                cLog.Add("", NameScreen, "Envio de datos a NS", false);
                string response = Connection.Request(URL, jsonRequest);
                cLog.Add("", NameScreen, "Recepción de datos a NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, "0", "NS Query") > 0)
                {
                    MessageBox.Show("No se pueden obtener los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    bool containsSearchResult = response.Contains("No se encontraron etiquetas");
                    bool containsSearchResultError = response.Contains("Error al consultar etiquetas");
                    if (!containsSearchResult)
                    {
                        if (!containsSearchResultError)
                        {
                            string completeJSON = "{\"Info\":" + response + "}";
                            WorkInProgressModel2 empObj = JsonConvert.DeserializeObject<WorkInProgressModel2>(completeJSON);
                            if (empObj.Datos[0].Result)
                            {
                                Data = empObj;
                                DataCatch = true;
                                if (!FormPrint)
                                {
                                    cLog.Add("", NameScreen, "Calculo de datos obtenidos de NS", false);
                                    cLog.Add("", NameScreen, "Guardado de datos a BD", false);
                                    DB.InsertAllInDB2(empObj);
                                    cLog.Add("", NameScreen, "Termino de guardar en BD", false);
                                }
                            }
                        }
                        else
                        {
                            if (FormPrint)
                            {
                                MessageBox.Show("Error al consultar etiquetas");
                            }
                        }
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                if (FormPrint)
                {
                    ErrorSerialization(Employ, exx.ToString(), Place);
                }
                else
                {
                    ErrorSerialization2("0", exx.ToString(), Place);
                }
                cLog.Add("Line 113", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (JsonReaderException e)
            {
                if (FormPrint)
                {
                    ErrorJsonReader(Employ, e.ToString(), Place);
                }
                else
                {
                    ErrorJsonReader2("0", e.ToString(), Place);
                }
                cLog.Add("Line 126", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (NullReferenceException withOutInternet)
            {
                if (FormPrint)
                {
                    ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                }
                else
                {
                    ErrorWithOutInternet2("0", withOutInternet.ToString(), Place);
                }
                cLog.Add("Line 139", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (DataCatch, Data);
        }

        public (bool, PlannedJaulas) TakeJaulas(bool Validation, EmployeeModel Employ, string Place)
        {
            bool ThereAreData = false;
            PlannedJaulas Jaulas = new PlannedJaulas();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string URL = "";
                if (!Validation)
                {
                    URL = Reslet.PlanTJaulas;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2118&deploy=1&status=1";
                }
                else
                {
                    URL = Reslet.PlanVJaulas;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2118&deploy=1&status=2";
                }
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    PlannedJaulas empObj = JsonConvert.DeserializeObject<PlannedJaulas>(response);
                    if (empObj.IsSuccess == false)
                    {
                        bool Error = empObj.ApiError.Status.Contains("Ninguna planeación de jaula encontrada!");
                        if (!Error)
                        {
                            cLog.Add("", NameScreen, "Planeaciones no adquiridas", false);
                            MessageBox.Show("Error al traer planeaciones.");
                        }
                    }
                    else
                    {
                        if (empObj.Jaulas.Length > 0)
                        {
                            Jaulas = empObj;
                            cLog.Add("", NameScreen, "Planeaciones de jaula adquiridas", false);
                            ThereAreData = true;
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "No hay planeaciones", false);
                            MessageBox.Show("No hay planeaciones");
                        }

                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 203", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 209", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                cLog.Add("Line 214", NameScreen, withOutInternet.Message, true);
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (ThereAreData, Jaulas);
        }

        public (bool, PlannedJaulasPT) TakeJaulasPT(EmployeeModel Employ, string Place)
        {
            bool ThereAreData = false;
            PlannedJaulasPT Jaulas = new PlannedJaulasPT();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string URL = Reslet.PlanPTJaulas;
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    PlannedJaulasPT empObj = JsonConvert.DeserializeObject<PlannedJaulasPT>(response);
                    if (empObj.IsSuccess == false)
                    {
                        bool Error = empObj.ApiError.Status.Contains("Ninguna planeación de jaula encontrada!");
                        if (!Error)
                        {
                            cLog.Add("", NameScreen, "Planeaciones no adquiridas", false);
                        }
                    }
                    else
                    {
                        if (empObj.Jaulas.Length > 0)
                        {
                            Jaulas = empObj;
                            cLog.Add("", NameScreen, "Planeaciones de jaula adquiridas", false);
                            ThereAreData = true;
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "No hay planeaciones", false);
                            MessageBox.Show("No hay planeaciones");
                        }

                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 270", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 276", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                cLog.Add("Line 281", NameScreen, withOutInternet.Message, true);
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (ThereAreData, Jaulas);
        }

        public (bool, GetBinsService) TakeBin(string location, EmployeeModel Employ, string Place)
        {
            bool ThereAreData = false;
            GetBinsService BinsTrans = new GetBinsService();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                //MessageBox.Show("Ubicación: " + location + " , Lugar: " + Place);
                string URL = Reslet.BinsOfLocation + location;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2099&deploy=1&location=" + location;
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    GetBinsService empObj = JsonConvert.DeserializeObject<GetBinsService>(response);
                    if (empObj.isSuccess == false)
                    {
                        bool Error = empObj.apiError.msg.Contains("No bins found for this location!");
                        if (!Error)
                        {
                            cLog.Add("", NameScreen, "Bins no adquiridos", false);
                            MessageBox.Show("Error trayendo los bines");
                        }
                    }
                    else
                    {
                        BinsTrans = empObj;
                        cLog.Add("", NameScreen, "Bins adquiridos", false);
                        ThereAreData = true;
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 330", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 336", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 342", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (ThereAreData, BinsTrans);
        }

        public (bool,GetWorkOrder) TakeOrderWork(EmployeeModel Employ, string Place)
        {
            bool isSuccess = false;
            GetWorkOrder WO = new GetWorkOrder();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                cLog.Add("", NameScreen, " Lugar: " + Place, false);
                string URL = Reslet.TakeWorkOrde;
                
                NSAuthentication Connection = new NSAuthentication();
                string response = "";
                response = Connection.RequestGet(URL);
                //MessageBox.Show(response);
                //string CompleteResponse = "{\"series\":" + response + "}";
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se pueden recibir los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    WO = JsonConvert.DeserializeObject<GetWorkOrder>(response);
                    isSuccess = WO.IsSuccess;
                    if (!isSuccess)
                    {
                        MessageBox.Show("Error trayendo las ordenes de trabajo.");
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 502", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 508", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 514", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (isSuccess, WO);
        }

        public (bool, TakeInventoryRegister) ListInventory(EmployeeModel Employ, bool MP, string Place)
        {
            bool success = false;
            TakeInventoryRegister Inventory = new TakeInventoryRegister();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string URL = "";
                if (MP)
                {
                    URL = Reslet.TakeInventoryMP;
                }
                else
                {
                    URL = Reslet.TakeInventoryPT;
                }

                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    Inventory = JsonConvert.DeserializeObject<TakeInventoryRegister>(response);
                    if (Inventory.IsSuccess == false)
                    {
                        cLog.Add("", NameScreen, "Lista de inventario no adquirida", false);
                    }
                    else
                    {
                        
                        if (Inventory.Data.Length > 0)
                        {
                            cLog.Add("", NameScreen, "Lista de inventario adquirida", false);
                            success = true;
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Lista de inventario no adquirida", false);
                        }
                        
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 502", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 508", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 514", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (success, Inventory);
        }

        public  (bool, bool, bool, EmployeeModel) Login(string datas, string User, string Password, string WorkStation, string Place)
        {
            bool ErrorAuth = false;
            bool FirstLogin = false;
            bool SecondLogin = false;
            EmployeeModel Empl = new EmployeeModel();

            try
            {
                //var jsonRequest = "{ \"employee_id\":\"" + User + "\", \"password\":\"" + Password + "\"}";
                //var jsonRequest2 = "{ \"employee_id\":\"" + User + "\", \"password\":\"" + Password + "\"}";
                ResletsOfNS Reslet = new ResletsOfNS();
                var URL = Reslet.SLogin;
                var URL2 = Reslet.SPermissionLogin;
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envío de datos de inicio de sesión", false);
                string response = Connection.Request(URL, datas);
                //MessageBox.Show(response);
                cLog.Add("", NameScreen, "Recepción de datos de inicio de sesión", false);
                cLog.Add("", NameScreen, "Envío de datos de permisos de sesión", false);
                string response2 = Connection.Request(URL2, datas);
                cLog.Add("", NameScreen, "Recepción de datos de permisos de sesión", false);

                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, User, "Login") > 0 || TypeError.Found(response2, User, "Login") > 0)
                {
                    ErrorAuth = true;
                    cLog.Add("77", NameScreen, "Error de credenciales de autentificación inicio de sesión", true);
                    MessageBox.Show(new Form() { TopMost = true }, "Error en credenciales de autentificación");
                }
                else
                {
                    //EmployeeModel Empl = new EmployeeModel();
                    bool containsSearchResult = response.Contains("Error informacion del empleado incorrecta");
                    if (!containsSearchResult)
                    {
                        cLog.Add("", NameScreen, "Deserialización JSON de inicio de sesión", false);
                        User empObj = JsonConvert.DeserializeObject<User>(response);
                        if (empObj.Result == true)
                        {

                            cLog.Add("", NameScreen, "Asignación de usuario de inicio de sesión", false);
                            Empl.Name = empObj.Message[0].User;
                            Empl.RoleId = User;
                            Empl.RoleName = empObj.Message[0].Rol;
                            Empl.NumberWorkStation = Convert.ToInt16(WorkStation);
                            cLog.Add("", NameScreen, "Guardado de número de usuario em BD", false);
                            string UserID = "UPDATE user_reg SET id_user = '" + User + "'; ";
                            DB.UpDateDB(UserID);
                            bool containsSearchResult2 = response.Contains("Error informacion del empleado incorrecta");
                            if (!containsSearchResult2)
                            {
                                cLog.Add("", NameScreen, "Deserialización JSON de permisos de sesión", false);
                                UserPermissions empObj2 = JsonConvert.DeserializeObject<UserPermissions>(response2);
                                //MessageBox.Show(response2);
                                //MessageBox.Show(empObj2.BtnVisible.PhysicalInvMP.ToString());
                                if (empObj2.Result == true)
                                {

                                    cLog.Add("", NameScreen, "Asignación de usuario de permisos de sesión", false);
                                    Empl.ButtonPrint = empObj2.BtnVisible.ButtonPrint;
                                    Empl.ButtonEmbol = empObj2.BtnVisible.ButtonEmbol;
                                    Empl.ButtonOperation = empObj2.BtnVisible.ButtonOperation;
                                    Empl.ButtonTMP = empObj2.BtnVisible.TranferMP.ButtonTMP;
                                    Empl.ButtonTPT = empObj2.BtnVisible.TransferPT.ButtonTPT;
                                    Empl.ButtonBin = empObj2.BtnVisible.TransferBIN.ButtonBin;
                                    Empl.ButtonLog = empObj2.BtnVisible.ButtonLog;
                                    Empl.ButtonError = empObj2.BtnVisible.ButtonError;
                                    Empl.ButtonTJaula = empObj2.BtnVisible.ButtonTranferJaula;
                                    Empl.ButtonValidateJaula = empObj2.BtnVisible.ButtonValidateJaule;
                                    Empl.ButtonBinMP = empObj2.BtnVisible.TransferBINMP;
                                    Empl.ButtonReturnMP = empObj2.BtnVisible.ReturnMP;
                                    Empl.ButtonPIMP = empObj2.BtnVisible.PhysicalInvMP;
                                    //Empl.ButtonPIPT = empObj2.BtnVisible.PhysicalInvPT;
                                    //Empl.ButtonPIMP = true;
                                    Empl.ButtonPIPT = false;

                                    Empl.TH_MP_PP = empObj2.BtnVisible.TranferMP.moves.TH_MP_PP;
                                    Empl.TH_MP_PP_Bases = empObj2.BtnVisible.TranferMP.moves.TH_MP_PP_Bases;
                                    Empl.TH_MP_PP_Bcos = empObj2.BtnVisible.TranferMP.moves.TH_MP_PP_Bcos;
                                    Empl.TH_MP_MP_Bcos = empObj2.BtnVisible.TranferMP.moves.TH_MP_MP_Bcos;
                                    Empl.TH_MP_MP_Bases = empObj2.BtnVisible.TranferMP.moves.TH_MP_MP_Bases;
                                    Empl.TH_MP_MQ = empObj2.BtnVisible.TranferMP.moves.TH_MP_MQ;
                                    Empl.Bcos_MP_PP = empObj2.BtnVisible.TranferMP.moves.Bcos_MP_PP;
                                    Empl.Bcos_MP_MQ = empObj2.BtnVisible.TranferMP.moves.Bcos_MP_MQ;
                                    Empl.Bcos_MP_MP_TH = empObj2.BtnVisible.TranferMP.moves.Bcos_MP_MP_TH;
                                    Empl.Bcos_PP_PT = empObj2.BtnVisible.TranferMP.moves.Bcos_PP_PT;
                                    Empl.Bases_MP_PP = empObj2.BtnVisible.TranferMP.moves.Bases_MP_PP;

                                    Empl.TH_PP_PT = empObj2.BtnVisible.TransferPT.moves.TH_PP_PT;
                                    Empl.Bases_PP_Trans = empObj2.BtnVisible.TransferPT.moves.Bases_PP_Trans;
                                    Empl.Trans_PT_TH = empObj2.BtnVisible.TransferPT.moves.Trans_PT_TH;
                                    Empl.TH_PT_TALLER = empObj2.BtnVisible.TransferPT.moves.TH_PT_TALLER;
                                    Empl.TH_TALLER_PT = empObj2.BtnVisible.TransferPT.moves.TH_TALLER_PT;
                                    Empl.JAULA_PP = empObj2.BtnVisible.TransferPT.moves.JAULA_PP;
                                    Empl.INSPECCION_TALLER = empObj2.BtnVisible.TransferPT.moves.INSPECCION_TALLER;
                                    Empl.INSPECCION_PT = empObj2.BtnVisible.TransferPT.moves.INSPECCION_PT;
                                }
                            }
                        }
                        else
                        {
                            SecondLogin = true;
                        }
                    }
                    else
                    {
                        FirstLogin = true;
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization2(User, exx.ToString(), Place);
                cLog.Add("Line 502", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (JsonReaderException e)
            {
                ErrorJsonReader2(User, e.ToString(), Place);
                cLog.Add("Line 508", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet2(User, withOutInternet.ToString(), Place);
                cLog.Add("Line 514", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            catch (Exception ex)
            {
                cLog.Add("159", NameScreen, ex.Message, true);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //DataBase Error = new DataBase();
                DB.SaveError(User, Date[0], Date[1], ex.ToString(), "Login Permissions");
                MessageBox.Show(new Form() { TopMost = true }, "Error al cargar los datos de empleado");
            }

            return (ErrorAuth, FirstLogin, SecondLogin, Empl);
        }

        public (UbicationsPT, DataMP, SerialsMP, PlannedInventory, SerialsJPT) QueryUbications(string idUbi, EmployeeModel Employ, int TypeSearch, string Place)
        {
            UbicationsPT PT = new UbicationsPT();
            DataMP MP = new DataMP();
            PlannedInventory Jaula = new PlannedInventory();
            SerialsJPT JPT = new SerialsJPT();
            SerialsMP SMP = new SerialsMP();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                cLog.Add("", NameScreen, "Ubicación: " + idUbi + " , Tipo de busqueda: " + TypeSearch.ToString() + " , Lugar: " + Place, false);
                //MessageBox.Show("Ubicación: " + idUbi + " , Tipo de busqueda: " + TypeSearch.ToString() + " , Lugar: " + Place);
                string[] result = DateTime.Now.ToShortDateString().Split(' ');
                string JSONComplete = "";
                string URL = "";
                switch (TypeSearch)
                {
                    case 1:
                        JSONComplete = "{ \"locations\": " + idUbi + "}";
                        URL = Reslet.SerialsPT;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2113&deploy=1";
                        break;
                    case 2:
                        JSONComplete = "{ \"locations\": " + idUbi + "}";
                        URL = Reslet.SerialsMP;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2103&deploy=1";
                        break;
                    case 3:
                        JSONComplete = "";
                        URL = Reslet.DetailsJaulas + idUbi;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2119&deploy=1&id=" + idUbi;
                        break;
                    case 4:
                        JSONComplete = "{ \"locations\": " + idUbi + "}";
                        URL = Reslet.SerialsPT;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2113&deploy=1";
                        break;
                    case 5:
                        JSONComplete = "{ \"locations\": " + idUbi + "}";
                        URL = Reslet.SerialsPT;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2113&deploy=1";
                        break;
                    case 6:
                        JSONComplete = "{ \"locations\": " + idUbi + "}";
                        URL = Reslet.SerialsPT;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2113&deploy=1";
                        break;
                    case 7:
                        JSONComplete = "";
                        URL = Reslet.DetailsJaulas + idUbi;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2119&deploy=1&id=" + idUbi;
                        break;
                    case 8:
                        URL = Reslet.SerialJPT + idUbi;
                        break;
                    case 9:
                        JSONComplete = "{ \"serial\": " + idUbi + "}";
                        URL = Reslet.SerialsMPLive;
                        break;
                    case 10:
                        JSONComplete = "{ \"serial\": \"" + idUbi +"\"}";
                        URL = Reslet.SerialsPTLive + Employ.NumberWorkStation.ToString();
                        break;
                    case 11:
                        JSONComplete = "{ \"serial\": \"" + idUbi + "\"}";
                        //URL = Reslet.QuerySerialWIP;
                        break;
                    case 12:
                        JSONComplete = "";
                        URL = Reslet.DetailsJaulas + idUbi;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2119&deploy=1&id=" + idUbi;
                        break;
                }
                //MessageBox.Show(JSONComplete);
                NSAuthentication Connection = new NSAuthentication();
                string response = "";
                if (TypeSearch != 3 && TypeSearch != 7 && TypeSearch != 8 && TypeSearch != 12)
                {
                    cLog.Add("", NameScreen, "Envio de datos NS " + Place, false);
                    response = Connection.Request(URL, JSONComplete);
                    cLog.Add("", NameScreen, "Recepción de datos NS " + Place, false);
                }
                else
                {
                    cLog.Add("", NameScreen, "Envio de datos NS " + Place, false);
                    response = Connection.RequestGet(URL);
                    cLog.Add("", NameScreen, "Recepción de datos NS " + Place, false);
                }
                //MessageBox.Show(response);
                string CompleteResponse = "{\"series\":" + response + "}";
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se pueden recibir los datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    switch (TypeSearch)
                    {
                        case 1:
                            PT = JsonConvert.DeserializeObject<UbicationsPT>(response);
                            break;
                        case 2:
                            MP = JsonConvert.DeserializeObject<DataMP>(response);
                            break;
                        case 3:
                            Jaula = JsonConvert.DeserializeObject<PlannedInventory>(response);
                            if (Jaula.IsSuccess == true)
                            {
                                cLog.Add("", NameScreen, "Guardado en la DB " + Place, false);
                                DB.SaveJaula(Jaula);
                                cLog.Add("", NameScreen, "Término del guardado en la DB " + Place, false);
                            }
                            break;
                        case 4:
                            PT = JsonConvert.DeserializeObject<UbicationsPT>(response);
                            break;
                        case 5:
                            PT = JsonConvert.DeserializeObject<UbicationsPT>(response);
                            break;
                        case 6:
                            PT = JsonConvert.DeserializeObject<UbicationsPT>(response);
                            break;
                        case 7:
                            Jaula = JsonConvert.DeserializeObject<PlannedInventory>(response);
                            if (Jaula.IsSuccess == true)
                            {
                                cLog.Add("", NameScreen, "Guardado en la DB " + Place, false);
                                DB.SaveJaula2(Jaula);
                                cLog.Add("", NameScreen, "Término del guardado en la DB " + Place, false);
                            }
                            break;
                        case 8:
                            JPT = JsonConvert.DeserializeObject<SerialsJPT>(response);
                            break;
                        case 9:
                            SMP = JsonConvert.DeserializeObject<SerialsMP>(response);
                            break;
                        case 10:
                            PT = JsonConvert.DeserializeObject<UbicationsPT>(response);
                            break;
                        case 11:
                            SMP = JsonConvert.DeserializeObject<SerialsMP>(response);
                            break;
                        case 12:
                            Jaula = JsonConvert.DeserializeObject<PlannedInventory>(response);
                            break;
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 502", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 508", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 514", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (PT, MP, SMP, Jaula, JPT);
        }

        public (bool, List<string>, List<string>) SendDataTransfer(string cambination, string serie, int binOri, int binDest, EmployeeModel Employ, int TypeSend, string Place)
        {
            bool DoIt = false;
            string NotSend = "";
            List<string> Message = new List<string>();
            List<string> SerialP = new List<string>();
            LabelsNotSends notSends = new LabelsNotSends();
            LabelsNotSendPT notSendsPT = new LabelsNotSendPT();
            LabelsNotSendBin labelsBin = new LabelsNotSendBin();
            LabelsOfTransferJaula TJ = new LabelsOfTransferJaula();
            LabelsOfValidateJaula VJ = new LabelsOfValidateJaula();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string JSONComplete = "";
                string JSONBin = "";
                string URL = "";
                string[] result = DateTime.Now.ToShortDateString().Split(' ');
                switch (TypeSend)
                {
                    case 1:
                        //JSONComplete = "{ \"areRawMaterials\":[{\"idEmployee\":" + Employ.RoleId + " ,\"combination\": " + cambination + " ,\"bin\":" + binOri + " , " + serie + "]}]}";
                        JSONComplete = "{ \"areRawMaterials\":[{\"idEmployee\":" + Employ.RoleId + " ,\"combination\": " + cambination + ", " + serie + "]}]}";
                        URL = Reslet.STMP;
                        string ModelNotSend1 = "{\"pt\":[" + JSONComplete + "]}";
                        notSends = JsonConvert.DeserializeObject<LabelsNotSends>(ModelNotSend1);
                        break;
                    case 2:
                        JSONComplete = serie + "]}";
                        URL = Reslet.STPT;
                        notSendsPT = JsonConvert.DeserializeObject<LabelsNotSendPT>(JSONComplete);
                        break;
                    case 3:
                        string JSONImcomplete = "{\"idEmployee\":" + Employ.RoleId + " ,\"binDest\":\"" + binDest + "\",";
                        JSONComplete = JSONImcomplete + serie + "]}";
                        JSONBin = "{\"bin\":[" + JSONComplete + " ]}";
                        labelsBin = JsonConvert.DeserializeObject<LabelsNotSendBin>(JSONBin);
                        URL = Reslet.SBINS;
                        break;
                    case 4:
                        JSONComplete = "{ \"transferJaula\":[{\"idEmployee\":" + Employ.RoleId + ", \"idJaula\":" + binOri + "," + serie + "]}]}";
                        TJ = JsonConvert.DeserializeObject<LabelsOfTransferJaula>(JSONComplete);
                        URL = Reslet.STJ;
                        break;
                    case 5:
                        JSONComplete = "{ \"validateJaula\":[{\"idEmployee\":" + Employ.RoleId + ", \"idJaula\":" + binOri + "," + serie + "]}]}";
                        VJ = JsonConvert.DeserializeObject<LabelsOfValidateJaula>(JSONComplete);
                        URL = Reslet.SVJ;
                        break;
                    case 6:
                        JSONComplete = "{ \"areRawMaterials\":[{\"idEmployee\":" + Employ.RoleId + " ," + serie + "]}]}";
                        URL = Reslet.STMP;
                        string ModelNotSend3 = "{\"pt\":[" + JSONComplete + "]}";
                        notSends = JsonConvert.DeserializeObject<LabelsNotSends>(ModelNotSend3);
                        break;
                    case 7:
                        JSONComplete = serie + "]}";
                        URL = Reslet.STPT;
                        string ModelNotSend4 = "{\"pt\":[" + JSONComplete + "]}";
                        notSends = JsonConvert.DeserializeObject<LabelsNotSends>(ModelNotSend4);
                        break;
                    case 8:
                        JSONComplete = serie;//JSONImcomplete2 + serie + "]}";
                        URL = Reslet.SBINMP;
                        break;
                    case 9:
                        string JSONImcomplete3 = "{" ;
                        JSONComplete = JSONImcomplete3 + serie + "}";
                        //MessageBox.Show(JSONComplete);
                        URL = Reslet.SendTransMP;
                        break;
                    case 10:
                        string JSONImcomplete4 = "{\"idEmployee\":" + Employ.RoleId + serie;
                        JSONComplete = JSONImcomplete4 + "]}";
                        URL = Reslet.SendMPtoWIP;
                        break;
                    case 11:
                        JSONComplete = serie;
                        URL = Reslet.SendInventoryMP;
                        break;
                    case 12:
                        JSONComplete = serie;
                        URL = Reslet.SendInventoryPT;
                        break;
                }
                //MessageBox.Show(JSONComplete);
                NotSend = JSONComplete;
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS " + Place, false);
                string response = Connection.Request(URL, JSONComplete);
                cLog.Add("", NameScreen, "Recepción de datos NS " + Place, false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede enviar la transferencia.");
                }
                else
                {
                    switch (TypeSend)
                    {
                        case 1:
                            ReceiveMPPT MP = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            Message.Add(MP.Message);
                            if (MP.Result == true)
                            {
                                for (int i = 0; i < MP.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(MP.ApiError.errorMessage[i].message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                for (int i = 0; i < MP.ApiError.errorMessage.Length; i++)
                                {
                                     Message.Add(MP.ApiError.errorMessage[i].message);
                                }
                            }
                            break;
                        case 2:
                            ReceiveMPPT PT = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (PT.Result == true)
                            {
                                Message.Add(PT.Message);
                                for (int i = 0; i < PT.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(PT.ApiError.errorMessage[i].message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(PT.Message);
                                for (int i = 0; i < PT.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(PT.ApiError.errorMessage[i].message);
                                }
                            }
                            break;
                        case 3:
                            ReceiveBin BIN = JsonConvert.DeserializeObject<ReceiveBin>(response);
                            if (BIN.Result == true)
                            {
                                Message.Add(BIN.Message);
                                DoIt = true;
                            }
                            else
                            {
                                if(BIN.Message.Length > 0)
                                {
                                    Message.Add(BIN.Message);
                                }
                                for (int i = 0; i < BIN.ApiError.Serials.Length; i++)
                                {
                                    Message.Add(BIN.ApiError.Serials[i].MessageError);
                                }
                            }
                            break;
                        case 4:
                            ReceiveJaulaResponse Jaula = JsonConvert.DeserializeObject<ReceiveJaulaResponse>(response);
                            if (Jaula.Result == true)
                            {
                                Message.Add(Jaula.Message);
                                for (int i = 0; i < Jaula.ApiError.ErrorMessage.Length; i++)
                                {
                                    Message.Add(Jaula.ApiError.ErrorMessage[i].Message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(Jaula.Message);
                                for (int i = 0; i < Jaula.ApiError.ErrorMessage.Length; i++)
                                {
                                    Message.Add(Jaula.ApiError.ErrorMessage[i].Message);
                                }
                            }
                            break;
                        case 5:
                            ResponseValidateJaula VJaula = JsonConvert.DeserializeObject<ResponseValidateJaula>(response);
                            if (VJaula.Result == true)
                            {
                                Message.Add(VJaula.Message);
                                for (int i = 0; i < VJaula.ApiError.ErrorMessage.Length; i++)
                                {
                                    Message.Add(VJaula.ApiError.ErrorMessage[i].Message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(VJaula.Message);
                                for (int i = 0; i < VJaula.ApiError.ErrorMessage.Length; i++)
                                {
                                    Message.Add(VJaula.ApiError.ErrorMessage[i].Message);
                                }
                            }
                            break;
                        case 6:
                            ReceiveMPPT PT2 = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (PT2.Result == true)
                            {
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(PT2.ApiError.errorMessage[0].message);
                            }
                            break;
                        case 7:
                            ReceiveMPPT JPT = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (JPT.Result == true)
                            {
                                Message.Add(JPT.Message);
                                for (int i = 0; i < JPT.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(JPT.ApiError.errorMessage[i].message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(JPT.Message);
                                for (int i = 0; i < JPT.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(JPT.ApiError.errorMessage[i].message);
                                }
                            }
                            break;
                        case 8:
                            ReceiveBin BIN2 = JsonConvert.DeserializeObject<ReceiveBin>(response);
                            if (BIN2.Result == true)
                            {
                                if (cambination.Equals("2"))
                                {
                                    Message.Add(BIN2.Message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                if (BIN2.Message.Length > 0)
                                {
                                    Message.Add(BIN2.Message);
                                }
                                for (int i = 0; i < BIN2.ApiError.Serials.Length; i++)
                                {
                                    Message.Add(BIN2.ApiError.Serials[i].MessageError);
                                }
                            }
                            break;
                        case 9:
                            ReceiveMPPT MP2 = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (MP2.Result == true)
                            {
                                Message.Add(MP2.Message);
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(MP2.Message);
                                for (int i = 0; i < MP2.ApiError.errorMessage.Length; i++)
                                {
                                    Message.Add(MP2.ApiError.errorMessage[i].message);
                                }
                            }
                            break;
                        case 10:
                            GetResponseWO MP3 = JsonConvert.DeserializeObject<GetResponseWO>(response);
                            if (MP3.IsSuccess == true)
                            {
                                if (!cambination.Equals("2"))
                                {
                                    Message.Add(MP3.Message);
                                }
                                DoIt = true;
                            }
                            else
                            {
                                Message.Add(MP3.Message);
                                if (MP3.ErrosQuery.errorMessage.Length > 0)
                                {
                                    Message.Add(MP3.ErrosQuery.errorMessage[0].Message);
                                }
                            }
                            break;
                        case 11:
                            ResponsePhyInventory IPMP = JsonConvert.DeserializeObject<ResponsePhyInventory>(response);
                            Message.Add(IPMP.Message);
                            if (IPMP.IsSuccessful)
                            {
                                if (IPMP.Data != null)
                                {
                                    for (int i = 0; i < IPMP.Data.Length; i++)
                                    {
                                        if (IPMP.Data[i] != null)
                                        {
                                            SerialP.Add(IPMP.Data[i]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (IPMP.Errors != null)
                                {
                                    for (int i = 0; i < IPMP.Errors.Length; i++)
                                    {
                                    
                                        Message.Add(IPMP.Errors[i]);
                                    }
                                }
                            }
                            break;
                        case 12:
                            ResponsePhyInventory IPPT = JsonConvert.DeserializeObject<ResponsePhyInventory>(response);
                            Message.Add(IPPT.Message);
                            if (IPPT.IsSuccessful)
                            {
                                if (IPPT.Data != null)
                                {
                                    for (int i = 0; i < IPPT.Data.Length; i++)
                                    {
                                        if (IPPT.Data[i] != null)
                                        {
                                            SerialP.Add(IPPT.Data[i]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (IPPT.Errors != null)
                                {
                                    for (int i = 0; i < IPPT.Errors.Length; i++)
                                    {

                                        Message.Add(IPPT.Errors[i]);
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 714", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 720", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                string UniqueID = DateTime.Now.ToString("yyyyMMddHHmmss");
                switch (TypeSend)
                {
                    case 1:
                        foreach (var n in notSends.PT[0].AreRawMaterials[0].Serials)
                        {
                            DB.InsertLabelsNotSends(n, "", Employ.RoleId, cambination, binOri.ToString(), "0", "MP", Date[0], Date[1], UniqueID);
                        }
                        DB.SaveLabelsNotSend(NotSend, "MP", UniqueID, Date[0], Date[1]);
                        break;
                    case 2:
                        foreach (var n in notSendsPT.AreRawMaterials[0].Serials)
                        {
                            DB.InsertLabelsNotSends(n, "", Employ.RoleId, cambination, "0", "0", "PT", Date[0], Date[1], UniqueID);
                        }
                        DB.SaveLabelsNotSend(NotSend, "PT", UniqueID, Date[0], Date[1]);
                        break;
                    case 3:
                        foreach (var m in labelsBin.Bin[0].Serials)
                        {
                            DB.InsertLabelsNotSends(m, "", Employ.RoleId, "0", labelsBin.Bin[0].binOri.ToString(), labelsBin.Bin[0].binDest.ToString(), "BIN", Date[0], Date[1], UniqueID);
                        }
                        DB.SaveLabelsNotSend(NotSend, "BIN", UniqueID, Date[0], Date[1]);
                        break;
                    case 4:
                        foreach (var l in TJ.TransferJ[0].Serials)
                        {
                            DB.InsertLabelsNotSends(l, "", Employ.RoleId, "0", binOri.ToString(), "0", "TJ", Date[0], Date[1], UniqueID);
                        }
                        DB.SaveLabelsNotSend(NotSend, "TJ", UniqueID, Date[0], Date[1]);
                        break;
                    case 5:
                        foreach (var l2 in VJ.ValidateJ[0].Serials)
                        {
                            DB.InsertLabelsNotSends(l2, "", Employ.RoleId, "0", binOri.ToString(), "0", "VJ", Date[0], Date[1], UniqueID);
                        }
                        DB.SaveLabelsNotSend(NotSend, "VJ", UniqueID, Date[0], Date[1]);
                        break;
                }
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 769", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (DoIt, Message, SerialP);
        }

        public bool ResendData(string msg, EmployeeModel Employ, int TypeSend, string Place)
        {
            bool DoIt = false;
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string URL = "";
                string[] result = DateTime.Now.ToShortDateString().Split(' ');
                switch (TypeSend)
                {
                    case 1:
                        URL = Reslet.STMP;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2101&deploy=1";
                        break;
                    case 2:
                        URL = Reslet.STPT;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2101&deploy=1";
                        break;
                    case 3:
                        URL = Reslet.SBINS;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2107&deploy=1";
                        break;
                    case 4:
                        URL = Reslet.STJ;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2120&deploy=1";
                        break;
                    case 5:
                        URL = Reslet.SVJ;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2121&deploy=1";
                        break;
                }
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.Request(URL, msg);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede enviar el reporte. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    switch (TypeSend)
                    {
                        case 1:
                            ReceiveMPPT MP = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (MP.Result == true)
                            {
                                MessageBox.Show(MP.Message);
                                DoIt = true;
                            }
                            else
                            {
                                MessageBox.Show(MP.Message);
                            }
                            break;
                        case 2:
                            ReceiveMPPT PT = JsonConvert.DeserializeObject<ReceiveMPPT>(response);
                            if (PT.Result == true)
                            {
                                MessageBox.Show(PT.Message);
                                DoIt = true;
                            }
                            else
                            {
                                MessageBox.Show(PT.Message);
                            }
                            break;
                        case 3:
                            ReceiveBin BIN = JsonConvert.DeserializeObject<ReceiveBin>(response);
                            if (BIN.Result == true)
                            {
                                MessageBox.Show(BIN.Message);
                                DoIt = true;
                            }
                            else
                            {
                                MessageBox.Show(BIN.Message);
                            }
                            break;
                        case 4:
                            ReceiveJaulaResponse Jaula = JsonConvert.DeserializeObject<ReceiveJaulaResponse>(response);
                            if (Jaula.Result == true)
                            {
                                MessageBox.Show(Jaula.Message);
                                DoIt = true;
                            }
                            else
                            {
                                MessageBox.Show(Jaula.Message);
                            }
                            break;
                        case 5:
                            ResponseValidateJaula VJaula = JsonConvert.DeserializeObject<ResponseValidateJaula>(response);
                            if (VJaula.Result == true)
                            {
                                MessageBox.Show(VJaula.Message);
                                DoIt = true;
                            }
                            else
                            {
                                MessageBox.Show(VJaula.Message);
                            }
                            break;
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 887", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 893", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 899", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return DoIt;
        }

        public ReprintLabelPT ReprintPT(string Serial, EmployeeModel Employ, string Place)
        {
            ReprintLabelPT empObj = new ReprintLabelPT();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                //MessageBox.Show("Serial: " + Serial + " , Lugar: " + Place);
                string URL = Reslet.SReprintEmbol + Serial;
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, Place) > 0)
                {
                    MessageBox.Show("No se puede enviar el reporte. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    empObj = JsonConvert.DeserializeObject<ReprintLabelPT>(response);
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 945", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 951", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 957", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return empObj;
        }

        public bool RegisterOperations(string Serial, string ID, string Date, EmployeeModel Employ, string Place)
        {
            bool DoIt = false;
            try
            {
                //MessageBox.Show(Employ.RoleId.ToString());
                ResletsOfNS Reslet = new ResletsOfNS();
                string JSONImcomplete = "[{ \"date\":\"" + Date + "\", \"idEmployee\":\"" + ID + "\",";
                string JSONComplete = JSONImcomplete + Serial + "]}]";
                var URL = Reslet.SOperations;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2045&deploy=1";
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.Request(URL, JSONComplete);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, "Operation Reg") > 0)
                {
                    MessageBox.Show("No se puede enviar el reporte. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    var empObj = JsonConvert.DeserializeObject<UpDateOperation>(response);
                    if (empObj.Result == true)
                    {
                        MessageBox.Show(empObj.Message);
                        DoIt = true;
                    }
                    else
                    {
                        MessageBox.Show(empObj.Message);
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 996", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 1002", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 1008", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return DoIt;
        }

        public (bool, UpDateOperation) SendDataNSEmbol(string Series, EmployeeModel Employ, string Place)
        {
            bool Succes = false;
            UpDateOperation empObj = new UpDateOperation();
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                //Cambiar aquí para determinar la estación de trabajo y el id de empleado
                var jsonRequest = "[{ \"date\":\"" + result[0] + "\", \"idEmployee\":\"" + Employ.RoleId + "\",\"workStations\": \"" + Employ.NumberWorkStation.ToString() + "\"," + "\"series\":[\"" + Series + "\"]}]";
                var URL = Reslet.SPrintedEmbol;// "https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2052&deploy=1";
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.Request(URL, jsonRequest);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, "EmbolFrm") > 0)
                {
                    MessageBox.Show("No se puede enviar la elaboración de impresión. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    empObj = JsonConvert.DeserializeObject<UpDateOperation>(response);
                    if (empObj.Result == true)
                    {
                        Succes = true;
                    }/*
                    else
                    {
                        MessageBox.Show(empObj.Message);
                    }*/
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 1054", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 1060", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 1066", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
            return (Succes, empObj);
        }

        public void SendDataNSPrintLabels(string Series, string Date,  EmployeeModel Employ, string Place)
        {
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string JSONImcomplete = "[{ \"date\":\"" + Date + "\", \"idEmployee\":\"" + Employ.RoleId + "\",";
                string JSONComplete = JSONImcomplete + Series + "]}]";
                var URL = Reslet.SSendPrintedLabels;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2054&deploy=1";
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.Request(URL, JSONComplete);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                //MessageBox.Show(response);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, "Series Print - Send") > 0)
                {
                    MessageBox.Show("No se pueden enviar las etiquetas. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    var empObj = JsonConvert.DeserializeObject<UpDateOperation>(response);
                    if (empObj.Result == true)
                    {
                        MessageBox.Show(empObj.Message);
                    }
                    else
                    {
                        MessageBox.Show(empObj.Message);
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), Place);
                cLog.Add("Line 1106", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), Place);
                cLog.Add("Line 1112", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), Place);
                cLog.Add("Line 1118", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
        }

        public void TakeErrorsNS(EmployeeModel Employ)
        {
            try
            {
                ResletsOfNS Reslet = new ResletsOfNS();
                string URL = Reslet.ErrorsOfNS;//"https://6387011-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2100&deploy=1";
                NSAuthentication Connection = new NSAuthentication();
                cLog.Add("", NameScreen, "Envio de datos NS", false);
                string response = Connection.RequestGet(URL);
                cLog.Add("", NameScreen, "Recepción de datos NS", false);
                FindInString TypeError = new FindInString();
                if (TypeError.Found(response, Employ.RoleId, "Consult error") > 0)
                {
                    MessageBox.Show("No se puede obtener datos. Consulte con un técnico si el error persiste.");
                }
                else
                {
                    var empObj = JsonConvert.DeserializeObject<Com_logs_ns>(response);
                    if (empObj.status == false)
                    {
                        MessageBox.Show("Error de datos");
                    }
                    else
                    {
                        //DataBase inserValues = new DataBase();
                        DB.EreaseErrorsNS();
                        cLog.Add("", NameScreen, "Guardado de datos en BD", false);
                        for (int i = 0; i < 1; i++)
                        {
                            DB.InsertErrors(i, empObj);
                        }
                        cLog.Add("", NameScreen, "Término de guardado de datos en BD", false);
                    }
                }
            }
            catch (JsonSerializationException exx)
            {
                ErrorSerialization(Employ, exx.ToString(), NameScreen);
                cLog.Add("Line 1106", NameScreen, exx.Message, true);
                MessageBox.Show("No se puede obtener los datos. Consulte con un técnico si el error persiste.");
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                ErrorJsonReader(Employ, e.ToString(), NameScreen);
                cLog.Add("Line 1112", NameScreen, e.Message, true);
                MessageBox.Show("Error al recibir respuesta del servidor. Consulte con un técnico si el error persiste.");
            }
            catch (System.NullReferenceException withOutInternet)
            {
                ErrorWithOutInternet(Employ, withOutInternet.ToString(), NameScreen);
                cLog.Add("Line 1118", NameScreen, withOutInternet.Message, true);
                MessageBox.Show("No se puede enviar los datos. Error de conexión.");
            }
        }

        public void UpdatePT(EmployeeModel Employ, string Place, bool Valide)
        {
            cLog.Add("", NameScreen, "Toma de datos de Producto Terminado", false);
            List<string> ListUbicationJaulas = new List<string>();
            ResletsOfNS Locations = new ResletsOfNS();
            string DatasLocations = Locations.LocationPTUnique;
            string DatasLocationsCompletes = Locations.LocationPT;
            //DataBase SaveData = new DataBase();

            if (Valide)
            {
                var Q = TakeJaulasPT(Employ, Place);

                bool ThereAreData = Q.Item1;

                if (ThereAreData)
                {
                    PlannedJaulasPT Jaulas = Q.Item2;
                    int Leng = Jaulas.Jaulas.Length;
                    for (int i = 0; i < Leng; i++)
                    {
                        if (ListUbicationJaulas.Count > 0)
                        {
                            bool FindIt = false;
                            foreach (var dto in new List<string>(ListUbicationJaulas))
                            {
                                if (dto.Equals(Jaulas.Jaulas[i].UbicationJaula))
                                {
                                    FindIt = true;
                                    break;
                                }
                            }
                            if (!FindIt)
                            {
                                ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                            }
                        }
                        else
                        {
                            ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                        }
                        QueryUbications(Jaulas.Jaulas[i].BinID.ToString(), Employ, 8, Place);
                    }
                }
            }
            cLog.Add("", NameScreen, "Primera consulta de Producto Terminado", false);
            var Datas1 = QueryUbications(DatasLocations, Employ, 1, Place);
            UbicationsPT PT = Datas1.Item1;
            if (Valide)
            {
                cLog.Add("", NameScreen, "Primer guardato de datos de PT", false);
                DB.DelateAllInTable("pt_logs");
                DB.SavePT(PT);
            }
            cLog.Add("", NameScreen, "Primer guardato de datos de BIN", false);
            DB.SaveBIN(PT);
            cLog.Add("", NameScreen, "Primer guardato de datos de TJ", false);
            DB.SaveTJ(PT);
           
            if (Valide)
            {
                cLog.Add("", NameScreen, "Segunda consulta de Producto Terminado", false);
                var Datas2 = QueryUbications(DatasLocationsCompletes, Employ, 1, Place);
                UbicationsPT AnotersPT = Datas2.Item1;
                cLog.Add("", NameScreen, "Segundo guardato de datos de PT", false);
                DB.SavePT(AnotersPT);
            }

            if (!DB.FindInfoPT())
            {
                cLog.Add("", NameScreen, "No hay datos en en BD", false);
            }
            else
            {
                cLog.Add("", NameScreen, "Si hay datos en en BD", false);
            }

        }

        public void UpdateVJ(EmployeeModel Employ, string Place)
        {
            cLog.Add("", NameScreen, "Toma de datos de Validación de Jaulas", false);
            List<string> ListUbicationJaulas = new List<string>();
            //DataBase SaveData = new DataBase();
            var Q = TakeJaulas(true, Employ, "");
            bool ThereAreData = Q.Item1;
            if (ThereAreData)
            {
                cLog.Add("", NameScreen, "Petición de datos de Validación de Jaulas", false);
                PlannedJaulas Jaulas = Q.Item2;
                int Leng = Jaulas.Jaulas.Length;
                for (int i = 0; i < Leng; i++)
                {
                    if (ListUbicationJaulas.Count > 0)
                    {
                        bool FindIt = false;
                        foreach (var dto in new List<string>(ListUbicationJaulas))
                        {
                            if (dto.Equals(Jaulas.Jaulas[i].UbicationJaula))
                            {
                                FindIt = true;
                                break;
                            }
                        }
                        if (!FindIt)
                        {
                            ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                        }
                    }
                    else
                    {
                        ListUbicationJaulas.Add(Jaulas.Jaulas[i].UbicationJaula);
                    }
                }
                cLog.Add("", NameScreen, "Término de petición de datos de Validación de Jaulas", false);
                cLog.Add("", NameScreen, "Petición de seriales de Validación de Jaulas", false);
                string Ubications = "[", DataList = "";
                foreach (var dto in ListUbicationJaulas)
                {
                    DataList += "\"" + dto + "\",";
                }
                Ubications = Ubications + DataList;
                int posInicial = Ubications.LastIndexOf(",");
                int longitud = posInicial - Ubications.IndexOf("[");
                Ubications = Ubications.Substring(0, longitud);
                var Datas = QueryUbications(Ubications + "]", Employ, 6, Place);
                UbicationsPT PT = Datas.Item1;
                DB.SaveVJ(PT);
                cLog.Add("", NameScreen, "Término de petición de seriales de Validación de Jaulas", false);
            }
        }

        public void UpdateMP(EmployeeModel Employ, string Place)
        {
            ResletsOfNS Locations = new ResletsOfNS();
            cLog.Add("", NameScreen, "Petición de datos de MP", false);
            string DataLocations = Locations.LocationMP;
            var Data = QueryUbications(DataLocations, Employ, 2, Place);
            DataMP MP = Data.Item2;
            DB.SaveMP(MP);
            cLog.Add("", NameScreen, "Término de petición de datos de MP", false);
        }
    }
}
