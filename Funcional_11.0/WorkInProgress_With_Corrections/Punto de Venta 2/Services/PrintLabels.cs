﻿//using iText.Kernel.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public class PrintLabels
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "SeriesPrint";
        DataBase DB = new DataBase(false);
        public PrintLabels()
        {

        }
        public bool Print(string FileName, string EmpId, string Place)
        {
            bool printed = false;
            try
            {

                string Filepath = @"C:\Temp\" + FileName;
                using (Process proc = new Process())
                {
                    proc.StartInfo.Verb = "print";
                    proc.StartInfo.FileName = @"C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";
                    string namePath = string.Format(@"/p /h {0}", Filepath);
                    proc.StartInfo.Arguments = string.Format(@"/p /h {0}", Filepath);
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.RedirectStandardOutput = true;
                    cLog.Add("", NameScreen, "Proceso de impresión iniciado.", false);
                    proc.Start();
                    proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    proc.EnableRaisingEvents = false;
                    if (proc.HasExited == false)
                    {
                        cLog.Add("", NameScreen, "Inicio de espera para cerrar la app Adobe", false);
                        proc.WaitForExit(12000);
                    }
                    proc.StartInfo.UseShellExecute = true;
                    proc.StartInfo.CreateNoWindow = false;
                    proc.StartInfo.RedirectStandardOutput = false;
                    proc.EnableRaisingEvents = true;

                    proc.Close();

                }
                printed = true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                cLog.Add("Line 52", NameScreen, e.Message, true);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                DB.SaveError(EmpId, Date[0], Date[1], e.ToString(), Place);
                printed = false;
            }
            catch (Exception e)
            {
                cLog.Add("Line 60", NameScreen, e.Message, true);
                string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                DB.SaveError(EmpId, Date[0], Date[1], e.ToString(), Place);
                printed = false;
            }
            return printed;
        }

        public bool KillAdobe(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses().Where(clsProcess => clsProcess.ProcessName.StartsWith(name)))
            {
                clsProcess.Kill();
                return true;
            }
            return false;
        }

    }
}
