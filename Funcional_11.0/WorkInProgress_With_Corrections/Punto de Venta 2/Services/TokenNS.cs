﻿using System;
using System.IO;
using System.Xml;

namespace NetSuiteApps
{
    class TokenNS
    {
        string XMLFile = "Authorization.xml";
        XmlDocument XMLDocument;
        public TokenNS()
        {
            if (!File.Exists(System.IO.Path.GetFullPath(XMLFile)))
            {
                XmlDocument doc = new XmlDocument();
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlElement root = doc.DocumentElement;
                doc.InsertBefore(xmlDeclaration, root);
                XmlElement element1 = doc.CreateElement(string.Empty, "Authorization", string.Empty);
                doc.AppendChild(element1);

                XmlElement element2 = doc.CreateElement(string.Empty, "nlauth_account", string.Empty);
                XmlText text1 = doc.CreateTextNode("6387011_SB1");
                element2.AppendChild(text1);
                element1.AppendChild(element2);

                XmlElement element3 = doc.CreateElement(string.Empty, "nlauth_email", string.Empty);
                XmlText text2 = doc.CreateTextNode("desarrollo4@disruptt.mx");
                element3.AppendChild(text2);
                element1.AppendChild(element3);

                XmlElement element4 = doc.CreateElement(string.Empty, "nlauth_signature", string.Empty);
                XmlText text3 = doc.CreateTextNode("Sol140218357!");
                element4.AppendChild(text3);
                element1.AppendChild(element4);

                XmlElement element5 = doc.CreateElement(string.Empty, "nlauth_role", string.Empty);
                XmlText text4 = doc.CreateTextNode("1060");
                element5.AppendChild(text4);
                element1.AppendChild(element5);

                XmlElement element6 = doc.CreateElement(string.Empty, "nlauth_application_id", string.Empty);
                XmlText text5 = doc.CreateTextNode("59530D64-0F4B-488F-ABF4-3A56261AD721");
                element6.AppendChild(text5);
                element1.AppendChild(element6);

                doc.Save(XMLFile);
                XMLDocument = new XmlDocument();
                XMLDocument.Load(XMLFile);
            }
            else
            {
                XMLDocument = new XmlDocument();
                XMLDocument.Load(XMLFile);
            }
            
        }

        public string TokenAuthorization()
        {
            string SAuthorization = "";
            foreach (XmlNode XMLNode in XMLDocument.DocumentElement.ChildNodes)
            {
                SAuthorization += XMLNode.InnerText.ToString() + ",";
            }
            String[] result = SAuthorization.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            SAuthorization = "NLAuth nlauth_account =" + result[0] + ", nlauth_email=" + result[1] + " , nlauth_signature=" + result[2] + ", nlauth_role=" + result[3] + ", nlauth_application_id=" + result[4];
            return SAuthorization;
        }

    }
}
