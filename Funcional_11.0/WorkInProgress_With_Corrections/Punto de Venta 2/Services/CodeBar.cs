﻿using iText.Barcodes;
using iText.IO.Font.Constants;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using iText.Layout.Properties;
using System.Windows.Forms;

namespace NetSuiteApps
{
    class CodeBar
    {
        public PdfDocument pdfDoc;
        public iText.Layout.Document doc;
        public Table table;
        public float MarginBottomPdf = 0;
        public float MarginTopPdf = 0;
        public float MarginLeftPdf = 0;
        public float MarginRightPdf = 0;
        public CodeBar(string NameDocument, bool WithOutMargins)
        {
            string dirF = @"C:\Temp\" + NameDocument;
            pdfDoc = new PdfDocument(new PdfWriter(dirF));
            PageSize e = new PageSize(283f, 644f);
            pdfDoc.SetDefaultPageSize(e);//PageSize.GetCommonRectangle(pageRect));
            //pdfDoc.SetDefaultPageSize();
            doc = new iText.Layout.Document(pdfDoc);
            doc.SetMargins(10f, 0, 0, 0);
            MarginBottomPdf = doc.GetBottomMargin();
            MarginTopPdf = doc.GetTopMargin();
            MarginLeftPdf = doc.GetLeftMargin();
            MarginRightPdf = doc.GetRightMargin();
            if (WithOutMargins)
            {
                doc.SetMargins(0, 0, 0, 0);
            }
        }

        public void SizePageWithOutMargin(bool SizeA7)
        {
            if (SizeA7)
            {
                PageSize e = new PageSize(240f, 992f);
                pdfDoc.SetDefaultPageSize(e);
                //pdfDoc.SetDefaultPageSize(PageSize.B4);
                //Rectangle pageRect = new Rectangle(8, 29);
                //Rectangle[] n = new Rectangle(9,30);
                //pdfDoc.SetDefaultPageSize(PageSize.TABLOID);//PageSize.GetCommonRectangle(pageRect));
                doc.SetMargins(0, 0, 0, 0);
            }
            else
            {
                PageSize e = new PageSize(240f, 655f);
                pdfDoc.SetDefaultPageSize(e);
                //pdfDoc.SetDefaultPageSize(PageSize.B4);
                //Rectangle pageRect = new Rectangle(8, 29);
                //pdfDoc.SetDefaultPageSize(PageSize.TABLOID);
                doc.SetMargins(0, 0, 0, 0);
                //doc.SetMargins(MarginTopPdf, MarginRightPdf, MarginBottomPdf, MarginLeftPdf);
            }
        }

        public Image CodeBarStick(string StringCode, float SetBaselineBC, float SetSizeBC, float AngleBC, float CodeW, float CodeH)
        {
            Barcode39 Serie = new Barcode39(pdfDoc);
            Serie.SetBaseline(SetBaselineBC);
            //Serie.SetSize(SetSizeBC);
            Serie.SetCode(StringCode);
            Serie.SetTextAlignment(Barcode1D.ALIGN_CENTER);
            Serie.SetStartStopText(true);
            PdfFont font = PdfFontFactory.CreateFont(StandardFonts.COURIER);
            Serie.SetFont(font);
            Image ImageSerie = new Image(Serie.CreateFormXObject(pdfDoc));
            ImageSerie.SetRotationAngle(AngleBC);
            ImageSerie.SetHeight(CodeH);
            ImageSerie.SetWidth(CodeW);
            ImageSerie.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            return ImageSerie;
        }

        public Image CodeBarStick2(string StringCode, float AngleBC, float CodeW, float CodeH)
        {

            Barcode39 Serie = new Barcode39(pdfDoc);
            Serie.SetCode(StringCode);
            Serie.SetTextAlignment(Barcode1D.ALIGN_CENTER);
            Serie.SetStartStopText(true);
            Serie.SetFont(null);
            Image ImageSerie = new Image(Serie.CreateFormXObject(pdfDoc));
            ImageSerie.SetRotationAngle(AngleBC);
            ImageSerie.SetHeight(CodeH);
            ImageSerie.SetWidth(CodeW);
            ImageSerie.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            return ImageSerie;
        }

        public Paragraph CreateParagraph(string StringData, float AngleBC, float FontSizeP, float CodeW, PdfFont Font)
        {
            Text Descripcion = new Text(StringData).SetFontColor(ColorConstants.BLACK).SetFont(Font); 
            Descripcion.SetFontSize(FontSizeP);
            Paragraph PDescripcion = new Paragraph(Descripcion);//.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            PDescripcion.SetRotationAngle(AngleBC);
            //PDescripcion.SetTextAlignment(TextAlignment.JUSTIFIED_ALL);
            PDescripcion.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            PDescripcion.SetWidth(CodeW);
            return PDescripcion;
        }

        public Table CreateOneTableInsert(int QuantTable, float SetWidthT)
        { 
            table = new Table(UnitValue.CreatePercentArray(QuantTable)).SetFixedLayout().SetWidth(SetWidthT);
            table.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            //table.SetHeight(SetHeightT);
            return table;
        }

        public Table CreateOneTableInsert2(int QuantTable)
        {
            table = new Table(QuantTable);
            table.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.LEFT);
            return table;
        }

        public void CloseDocument(bool NewPage)
        {
            if (NewPage)
            {
                doc.Add(table);
            }
            doc.Close();
        }

        public void AddNewPageAndTable(bool NewPage, bool margins)
        {
            doc.Add(table);
            if (NewPage)
            {
                if (margins)
                {
                    PageSize e = new PageSize(240f, 992f);
                    pdfDoc.SetDefaultPageSize(e);
                    //pdfDoc.SetDefaultPageSize(PageSize.B4);
                    doc.SetMargins(0, 0, 0, 0);
                }
                else
                {
                    PageSize e = new PageSize(240f, 655f);
                    pdfDoc.SetDefaultPageSize(e);
                    //pdfDoc.SetDefaultPageSize(PageSize.B4);
                    doc.SetMargins(0, 0, 0, 0);
                    //doc.SetMargins(MarginTopPdf, MarginRightPdf, MarginBottomPdf, MarginLeftPdf);
                }
                doc.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
            }
        }

    }
}
