﻿
namespace NetSuiteApps
{
    partial class TransferPTFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferPTFrm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.CBJaula = new System.Windows.Forms.ComboBox();
            this.PWait = new System.Windows.Forms.PictureBox();
            this.Counter = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSerieScanned = new System.Windows.Forms.Label();
            this.SendData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.CBTransferPT = new System.Windows.Forms.ComboBox();
            this.txtBDes = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CodeBar = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BGWork = new System.ComponentModel.BackgroundWorker();
            this.TUpdateL = new System.Windows.Forms.Timer(this.components);
            this.BGSend = new System.ComponentModel.BackgroundWorker();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.CBJaula);
            this.panel2.Controls.Add(this.PWait);
            this.panel2.Controls.Add(this.Counter);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.lblSerieScanned);
            this.panel2.Controls.Add(this.SendData);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.CBTransferPT);
            this.panel2.Controls.Add(this.txtBDes);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.CodeBar);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 320);
            this.panel2.TabIndex = 19;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(12, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 15);
            this.label4.TabIndex = 36;
            this.label4.Text = "Jaula";
            // 
            // CBJaula
            // 
            this.CBJaula.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CBJaula.FormattingEnabled = true;
            this.CBJaula.Location = new System.Drawing.Point(12, 100);
            this.CBJaula.Name = "CBJaula";
            this.CBJaula.Size = new System.Drawing.Size(215, 23);
            this.CBJaula.TabIndex = 35;
            this.CBJaula.SelectedIndexChanged += new System.EventHandler(this.CBJaula_SelectedIndexChanged);
            this.CBJaula.Click += new System.EventHandler(this.CBJaula_Click);
            // 
            // PWait
            // 
            this.PWait.Image = global::NetSuiteApps.Properties.Resources.cargando;
            this.PWait.Location = new System.Drawing.Point(83, 245);
            this.PWait.Name = "PWait";
            this.PWait.Size = new System.Drawing.Size(68, 59);
            this.PWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PWait.TabIndex = 34;
            this.PWait.TabStop = false;
            this.PWait.Visible = false;
            // 
            // Counter
            // 
            this.Counter.AutoSize = true;
            this.Counter.BackColor = System.Drawing.Color.Transparent;
            this.Counter.ForeColor = System.Drawing.Color.White;
            this.Counter.Location = new System.Drawing.Point(90, 185);
            this.Counter.Name = "Counter";
            this.Counter.Size = new System.Drawing.Size(13, 15);
            this.Counter.TabIndex = 32;
            this.Counter.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 31;
            this.label2.Text = "Escaneadas:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(12, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 15);
            this.label6.TabIndex = 30;
            this.label6.Text = "Etiqueta escaneada:";
            // 
            // lblSerieScanned
            // 
            this.lblSerieScanned.AutoSize = true;
            this.lblSerieScanned.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSerieScanned.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblSerieScanned.Location = new System.Drawing.Point(130, 166);
            this.lblSerieScanned.Name = "lblSerieScanned";
            this.lblSerieScanned.Size = new System.Drawing.Size(12, 15);
            this.lblSerieScanned.TabIndex = 29;
            this.lblSerieScanned.Text = "-";
            // 
            // SendData
            // 
            this.SendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.SendData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SendData.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SendData.Location = new System.Drawing.Point(83, 204);
            this.SendData.Name = "SendData";
            this.SendData.Size = new System.Drawing.Size(75, 30);
            this.SendData.TabIndex = 27;
            this.SendData.Text = "Enviar";
            this.SendData.UseVisualStyleBackColor = false;
            this.SendData.Click += new System.EventHandler(this.SendData_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(13, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 15);
            this.label5.TabIndex = 26;
            this.label5.Text = "Tipo de transferencia";
            // 
            // CBTransferPT
            // 
            this.CBTransferPT.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CBTransferPT.FormattingEnabled = true;
            this.CBTransferPT.Location = new System.Drawing.Point(13, 56);
            this.CBTransferPT.Name = "CBTransferPT";
            this.CBTransferPT.Size = new System.Drawing.Size(215, 23);
            this.CBTransferPT.TabIndex = 23;
            this.CBTransferPT.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.CBTransferPT.Click += new System.EventHandler(this.CBTransferPT_Click);
            // 
            // txtBDes
            // 
            this.txtBDes.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtBDes.Enabled = false;
            this.txtBDes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBDes.ForeColor = System.Drawing.SystemColors.Window;
            this.txtBDes.Location = new System.Drawing.Point(12, 239);
            this.txtBDes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBDes.Multiline = true;
            this.txtBDes.Name = "txtBDes";
            this.txtBDes.Size = new System.Drawing.Size(215, 70);
            this.txtBDes.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel4.Location = new System.Drawing.Point(13, 163);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(215, 1);
            this.panel4.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(13, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ingresar código:";
            // 
            // CodeBar
            // 
            this.CodeBar.BackColor = System.Drawing.Color.White;
            this.CodeBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeBar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CodeBar.ForeColor = System.Drawing.Color.Black;
            this.CodeBar.Location = new System.Drawing.Point(14, 142);
            this.CodeBar.Name = "CodeBar";
            this.CodeBar.Size = new System.Drawing.Size(215, 16);
            this.CodeBar.TabIndex = 18;
            this.CodeBar.Text = "123";
            this.CodeBar.Click += new System.EventHandler(this.CodeBar_Click);
            this.CodeBar.TextChanged += new System.EventHandler(this.CodeBar_TextChanged);
            this.CodeBar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CodeBar_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 35);
            this.panel3.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(68, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Transferencia PT";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 35);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BGWork
            // 
            this.BGWork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGWork_DoWork);
            this.BGWork.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGWork_RunWorkerCompleted);
            // 
            // TUpdateL
            // 
            this.TUpdateL.Tick += new System.EventHandler(this.TUpdateL_Tick);
            // 
            // BGSend
            // 
            this.BGSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGSend_DoWork);
            this.BGSend.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGSend_RunWorkerCompleted);
            // 
            // TransferPTFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TransferPTFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TransferPTFrm";
            this.Load += new System.EventHandler(this.TransferPTFrm_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CBTransferPT;
        private System.Windows.Forms.TextBox txtBDes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CodeBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button SendData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSerieScanned;
        private System.Windows.Forms.Label Counter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox PWait;
        private System.ComponentModel.BackgroundWorker BGWork;
        private System.Windows.Forms.Timer TUpdateL;
        private System.ComponentModel.BackgroundWorker BGSend;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CBJaula;
    }
}