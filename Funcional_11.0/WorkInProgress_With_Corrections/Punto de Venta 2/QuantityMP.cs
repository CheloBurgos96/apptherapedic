﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class QuantityMP : Form
    {
        public double CurrentQuantity { get; set; }
        public QuantityMP()
        {
            InitializeComponent();
        }

        private void SendQuantity_Click(object sender, EventArgs e)
        {
            //TransferMPFrm TransferQuantity = new TransferMPFrm();
            //CurrentQuantity = 
            this.CurrentQuantity = Convert.ToDouble(Quantity.Text);
            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void Quantity_Click(object sender, EventArgs e)
        {
            Quantity.Clear();
        }

        private void Quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            CultureInfo cc = System.Threading.Thread.CurrentThread.CurrentCulture;
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
            e.Handled = !(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || (e.KeyChar.ToString() == cc.NumberFormat.NumberDecimalSeparator))
                || ((e.KeyChar.ToString() == cc.NumberFormat.NumberDecimalSeparator)
                    && txt.Text.Contains('.'));
        }

        private void Quantity_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
