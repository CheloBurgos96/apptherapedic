﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetSuiteApps
{
    public partial class TransferBin : Form
    {
        delegate void PanelColor();
        delegate void Labels();
        delegate void FocusTxt();
        delegate void INDC();
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Transfer BIN";
        public EmployeeModel Employ;
        delegate void InsertData();
        public List<string> ListLabels = new List<string>();
        public List<int> idBins = new List<int>();
        public List<string> authorizationTransfer = new List<string>();

        public List<string> idBinsInserts = new List<string>();

        List<string> Message = new List<string>();

        public string SeriesNotSend = "\"series\":[";
        public int CounterLabels = 0;
        
        public int ValueIndex = 0;
        public bool ItsSame = false;
        public bool Once = false;
        public bool WriteCb = false;
        public int INDCB = 0;
        public bool UpdateAll = true;

        DataBase DB = new DataBase(false);
        ResletsOfNS Location = new ResletsOfNS();
        QueryNS GetQueryFromNS = new QueryNS();
        UbicationsPT FoundS = new UbicationsPT();
        string Serial = "";

        public void Empl(EmployeeModel n)
        {
            try
            {
                Employ = n;
                cLog.Add("", NameScreen, "Toma de datos de bines de las ubicaciones", false);
                var Q = GetQueryFromNS.TakeBin(Location.LBIN, Employ, "Transfer Bin, Line 23");
                if (Q.Item1)
                {
                    GetBinsService BinsTrans = Q.Item2;

                    cbBinDest.Items.Clear();
                    int Leng = BinsTrans.data.Length;
                    if (Leng > 0)
                    {
                        for (int i = 0; i < Leng; i++)
                        {

                            cbBinDest.Items.Add(BinsTrans.data[i].name);
                            idBins.Add(BinsTrans.data[i].id);
                            authorizationTransfer.Add(BinsTrans.data[i].name);
                        }
                        cbBinDest.Text = authorizationTransfer[0];
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "No hay datos registros de bines", false);
                        MessageBox.Show(new Form() { TopMost = true },"No hay registros de bins.");
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "No hay datos registros de bines", false);
                    cbBinDest.Enabled = false;
                    CodeBar.Enabled = false;
                    SendData.Enabled = false;
                    MessageBox.Show(new Form() { TopMost = true },"No hay registros de bins.");
                }
            }
            catch (Exception ex)
            {
                cbBinDest.Enabled = false;
                CodeBar.Enabled = false;
                SendData.Enabled = false;
                cLog.Add("Line 87", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
            
        }
        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                InsertData n = new InsertData(ControlPanel);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                cLog.Add("Line 101" + "", NameScreen, ex.Message, true);
            }
        }

        private void ControlPanel()
        {
            panel1.BackColor = Color.Black;
        }

        private void WQ()
        {
            try
            {
                var Serials = GetQueryFromNS.QueryUbications(Serial, Employ, 10, NameScreen);
                FoundS = Serials.Item1;
            }
            catch (Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 123", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void ActiveWait(bool Active)
        {
            SendData.Enabled = Active;
            cbBinDest.Enabled = Active;
            CodeBar.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 11)
                {
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    string ID = "";
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);
                    if (ItsSame)
                    {
                        string ShowDes = "";
                        bool SeriaSameBin = false;
                        for (int i = 0; i < FoundS.Quantity; i++)
                        {
                            //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },idBins[cbBinDest.SelectedIndex].ToString() + " - " + FoundS.Series[i].binID);
                            if (idBins[cbBinDest.SelectedIndex].ToString().Equals(FoundS.Series[i].binID))
                            {
                                SeriaSameBin = true;
                                break;
                            }
                            else
                            {
                                ShowDes = FoundS.Series[i].item;
                                ID = FoundS.Series[i].binID;
                            }
                        }
                        if (FoundS.IsSuccess)
                        {
                            cLog.Add("", NameScreen, "El serial está disponible", false);
                            if (!SeriaSameBin)
                            {
                                cLog.Add("", NameScreen, "El serial no está en el mismo bin seleccionado", false);
                                txtBDes.Text = ShowDes;
                                if (ListLabels.Count > 0)
                                {
                                    bool findIt = false;
                                    foreach (var dto in ListLabels)
                                    {
                                        if (dto.Equals(Serial))
                                        {
                                            findIt = true;
                                            break;
                                        }
                                    }

                                    if (!findIt)
                                    {
                                        ThreadStart delegado = new ThreadStart(ChangeColor);
                                        var t = new Thread(delegado);
                                        t.Start();
                                        panel1.BackColor = Color.Green;
                                        ListLabels.Add(Serial);
                                        idBinsInserts.Add(ID);
                                        lblSerieScanned.Text = Serial;
                                        CodeBar.Clear();

                                    }
                                    else
                                    {
                                        cLog.Add("", NameScreen, "Etiqueta ya ingresada", false);
                                        panel1.BackColor = Color.Red;
                                        CodeBar.Clear();
                                        MessageBox.Show(new Form() { TopMost = true },"Ya ingresó esa etiqueta.");
                                        panel1.BackColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    ThreadStart delegado = new ThreadStart(ChangeColor);
                                    var t = new Thread(delegado);
                                    t.Start();
                                    panel1.BackColor = Color.Green;
                                    ListLabels.Add(Serial);
                                    idBinsInserts.Add(ID);
                                    lblSerieScanned.Text = Serial;
                                    CodeBar.Clear();

                                }
                                CounterLabels = ListLabels.Count;
                                Counter.Text = CounterLabels.ToString();
                            }
                            else
                            {
                                cLog.Add("", NameScreen, "Serial pertenece a este bin", false);
                                panel1.BackColor = Color.Red;
                                CodeBar.Clear();
                                MessageBox.Show(new Form() { TopMost = true },"Esta etiqueta está en ese bin.");
                                panel1.BackColor = Color.Black;
                            }
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "Etiqueta no encontrada", false);
                            panel1.BackColor = Color.Red;
                            CodeBar.Clear();
                            MessageBox.Show(new Form() { TopMost = true },"No se encuentra esa etiqueta");
                            panel1.BackColor = Color.Black;
                        }
                        CodeBar.Focus();
                        ItsSame = true;
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Bin no encontrado", false);
                        panel1.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true },"Bin no encontrado.");
                        panel1.BackColor = Color.Black;
                    }
                }
            }
            catch (Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 261", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            SendData.Enabled = false;
            cbBinDest.Enabled = false;
            CodeBar.Enabled = false;
            pictureBox2.Enabled = false;
            PWait.Visible = true;
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        public TransferBin()
        {
            InitializeComponent();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ListLabels.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún {0} no son enviados {0}¿Desea enviarlos?", Environment.NewLine);
                //Alert.Empl(Employ, strMensaje, false);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    UpdateAll = false;
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                UpdateAll = false;
                //DataBase Query = new DataBase();
                if (DB.ThereAreLabelsNotSend())
                {
                    //this.Close();
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                    //Alert.Empl(Employ, strMensaje, false);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvpios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if (result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
            }


        }

        private void cbBinDest_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValueIndex = cbBinDest.SelectedIndex;
            ItsSame = true;
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void cbBinDest_TextUpdate(object sender, EventArgs e)
        {

        }

        private void cbBinDest_Click(object sender, EventArgs e)
        {
            Once = false;
        }

        private void TransferBin_Load(object sender, EventArgs e)
        {
            if (!(idBins.Count > 0))
            {
                cLog.Add("", NameScreen, "Error al obtener datos de bines, ubicaciones y seriales", false);
                MessageBox.Show(new Form() { TopMost = true },"Error al cargar los datos.");
            }
        }

        private void cbBinDest_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //if (ListLabels.Count == 0)
                //{
                //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },ListLabels);
                int CountElements = 0;
                ItsSame = false;
                    foreach (string i in authorizationTransfer)
                    {
                        CountElements++;
                        if (i.Equals(cbBinDest.Text))
                        {
                            ItsSame = true;
                            cLog.Add("", NameScreen, "Usuario cambio el bin destino", false);
                            cbBinDest.SelectedIndex = CountElements - 1;
                            ValueIndex = CountElements - 1;
                            CodeBar.Text = "";
                            CodeBar.Focus();
                            //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },idBins[ValueIndex].ToString());
                            break;
                        }
                    }
                    if(!ItsSame){
                        cbBinDest.Text = "";
                        cbBinDest.Focus();
                    }
                //}
            }
        }

        private void ColorBlack()
        {
            panel1.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListLabels.Count;
            Counter.Text = CounterLabels.ToString();
            CodeBar.Text = "";
        }

        private void CBINDEX()
        {
            INDCB = idBins[ValueIndex];
        }

        private void Tick()
        {
            TUpdateL.Interval = 60000;
            TUpdateL.Enabled = true;
            TUpdateL.Start();
        }

        private void BGWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (UpdateAll)
                {
                    TUpdateL.Stop();
                    cLog.Add("", NameScreen, "--Actualizar BD--", false);
                    QueryNS GetLbls = new QueryNS();
                    GetLbls.UpdatePT(Employ, NameScreen, false);
                    cLog.Add("", NameScreen, "--BD Actualizada--", false);
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 463", NameScreen, ex.Message, true);
            }
            
        }

        private void BGWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (UpdateAll)
            {
                cLog.Add("", NameScreen, "Inicio Timer", false);
                Tick();
            }
            else
            {
                cLog.Add("", NameScreen, "--Trabajo cancelado--", false);
                TUpdateL.Stop();
            }
            
        }

        private void TUpdateL_Tick(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Comienzo de actualización", false);
            if (BGWork.IsBusy != true)
            {
                BGWork.RunWorkerAsync();
            }
        }

        private void BGSend_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (ListLabels.Count > 0)
                {
                    bool MessageUser = false; int counter = 0;
                    cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                    string DataList = "";
                    SeriesNotSend = "\"series\":[";
                    List<string> ListSupport = new List<string>();
                    List<string> ListSupport2 = new List<string>();
                    for (int i = 0; i < ListLabels.Count; i++)
                    {
                        if (!idBins[ValueIndex].ToString().Equals(idBinsInserts[i]))
                        {
                            DataList += "\"" + ListLabels[i] + "\",";
                            counter++;
                        }
                        else
                        {
                            ListSupport.Add(ListLabels[i]);
                            ListSupport2.Add(idBinsInserts[i]);
                            MessageUser = true;
                        }
                    }
                    if (MessageUser)
                    {
                        string strMensaje = string.Format("Hay seriales que pertenecen a esta {0}ubicación, no se enviarán esos datos.", Environment.NewLine);
                        MessageBox.Show(new Form() { TopMost = true },strMensaje);
                        foreach (string data in ListSupport)
                        {
                            MessageBox.Show(new Form() { TopMost = true },data);
                        }
                    }
                    if (counter > 0)
                    {
                        SeriesNotSend += DataList;
                        int posInicial = SeriesNotSend.LastIndexOf(",");
                        int longitud = posInicial - SeriesNotSend.IndexOf("\"s");
                        SeriesNotSend = SeriesNotSend.Substring(0, longitud);
                        PanelColor BlackColor = new PanelColor(ColorBlack);
                        this.Invoke(BlackColor);
                        /*INDC IND = new INDC(CBINDEX);
                        this.Invoke(IND);*/
                        QueryNS GetQueryFromNS = new QueryNS();
                        //MessageBox.Show(new Form() { TopMost = true },new Form() { TopMost = true },idBins[ValueIndex].ToString());
                        var Query = GetQueryFromNS.SendDataTransfer("", SeriesNotSend, 0, idBins[ValueIndex], Employ, 3, "Transfer Bin, Line 515");
                        if (Query.Item1)
                        {
                            cLog.Add("", NameScreen, "Información enviada a NS para el primer tipo de envio", false);

                            if (ListSupport.Count > 0)
                            {
                                ListLabels.Clear();
                                idBinsInserts.Clear();
                                ListLabels = ListSupport;
                                idBinsInserts = ListSupport2;
                            }
                        }
                        Message.Clear();
                        Message = Query.Item2;
                        
                        if (!MessageUser)
                        {
                            ListLabels.Clear();
                            idBinsInserts.Clear();
                        }
                        Labels PanelTxtC = new Labels(LPanel);
                        this.Invoke(PanelTxtC);
                    }
                    else
                    {
                        MessageBox.Show(new Form() { TopMost = true },"No se pueden enviar los seriales.");
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                    MessageBox.Show(new Form() { TopMost = true },"No hay operaciones a enviar.");
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 560", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }
        }

        private void BGSend_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            SendData.Enabled = true;
            cbBinDest.Enabled = true;
            CodeBar.Enabled = true;
            pictureBox2.Enabled = true;
            PWait.Visible = false;
            CodeBar.Focus();
        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
           // e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }
    }
}