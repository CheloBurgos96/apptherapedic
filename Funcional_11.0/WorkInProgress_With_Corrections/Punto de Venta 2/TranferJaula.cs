﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class TranferJaula : Form
    {
        delegate void PanelColor();
        delegate void Labels();
        delegate void FocusTxt();
        delegate void INDC();
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Transfer Jaules";
        public EmployeeModel Employ;
        delegate void InsertData();
        public List<string> ListLabels = new List<string>();
        public List<string> ListNames = new List<string>();
        public List<int> ListID = new List<int>();
        public List<Int64> ListOfSKU = new List<Int64>();
        public string SeriesNotSend = "\"serials\":[";
        public bool Once = false;
        public int IndexJaula = 0;
        public int CounterLabels = 0;
        public bool ThereAreNotLabels = false;
        public int INDCB = 0;
        public bool UpdateAll = true;
        DataBase DB = new DataBase(false);
        ResletsOfNS Location = new ResletsOfNS();
        QueryNS GetQueryFromNS = new QueryNS();
        UbicationsPT FoundS = new UbicationsPT();
        List<string> Message = new List<string>();
        string Serial = "";

        List<string> skuInsert = new List<string>();

        public void Empl(EmployeeModel n)
        {
            try
            {
                txtBDes.Visible = false;
                Employ = n;

                cLog.Add("", NameScreen, "Toma de datos de Jaulas", false);
                var Q = GetQueryFromNS.TakeJaulas(false, Employ, NameScreen);

                bool ThereAreData = Q.Item1;

                cbPlannedJaulas.Items.Clear();
                if (ThereAreData)
                {
                    cLog.Add("", NameScreen, "Borrado de datos contenidos en la BD de las planeaciones y sus seriales", false);
                    PlannedJaulas Jaulas = Q.Item2;
                    DB.DelateAllInTable("jaulasTJ_logs");
                    int Leng = Jaulas.Jaulas.Length;
                    for (int i = 0; i < Leng; i++)
                    {
                        cbPlannedJaulas.Items.Add(Jaulas.Jaulas[i].BinName);
                        ListNames.Add(Jaulas.Jaulas[i].BinName);
                        ListID.Add(Convert.ToInt32(Jaulas.Jaulas[i].BinID));
                        GetQueryFromNS.QueryUbications(Jaulas.Jaulas[i].BinID.ToString(), Employ, 3, "");
                    }

                    cbPlannedJaulas.Text = ListNames[IndexJaula];
                }
                else
                {
                    cLog.Add("", NameScreen, "No hay planeaciones", false);
                    ThereAreNotLabels = true;
                    cbPlannedJaulas.Enabled = false;
                    SendData.Enabled = false;
                    CodeBar.Enabled = false;
                    MessageBox.Show(new Form() { TopMost = true },"No hay planeaciones.");
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 75" + "", NameScreen, ex.Message, true);
                //ThereAreNotLabels = true;
                cbPlannedJaulas.Enabled = false;
                SendData.Enabled = false;
                CodeBar.Enabled = false;
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
            }

        }

        public TranferJaula()
        {
            InitializeComponent();
        }

        private void ChangeColor()
        {
            try
            {
                Thread.Sleep(2000);
                InsertData n = new InsertData(ControlPanel);
                this.Invoke(n);
            }
            catch (Exception ex)
            {
                cLog.Add("Line 100" + "", NameScreen, ex.Message, true);
            }
        }

        private void ControlPanel()
        {
            panel1.BackColor = Color.Black;
        }

        public bool CounterSerialsScanned(string SKU)
        {
            bool SerialsFinished = false;
            int Validation = DB.UpdateDBJaulaTransfer(CodeBar.Text, SKU,  ListID[cbPlannedJaulas.SelectedIndex].ToString());
            switch (Validation)
            {
                case 0:
                    MessageBox.Show(new Form() { TopMost = true },"No se encuentró el valor.");
                    break;
                case 1:
                    break;
                case 2:
                    MessageBox.Show(new Form() { TopMost = true },"Cantidad a escanear y cantidad actual no fueron ingresadas en el servidor.");
                    break;
                case 3:
                    MessageBox.Show(new Form() { TopMost = true },"Cantidad a escaneada no fue ingresada en el servidor.");
                    break;
                case 4:
                    MessageBox.Show(new Form() { TopMost = true },"Cantidad actual no fue ingresada en el servidor.");
                    break;
                case 5:
                    SerialsFinished = true;
                    break;
            }
            return SerialsFinished;
        }

        private void WQ()
        {
            try
            {
                var Serials = GetQueryFromNS.QueryUbications(Serial, Employ, 10, NameScreen);//"nuevo query" cambiarlo
                FoundS = Serials.Item1;//DB.FindDescriptionMP(CodeBar.Text);
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 149" + "", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void ActiveWait(bool Active)
        {
            SendData.Enabled = Active;
            cbPlannedJaulas.Enabled = Active;
            CodeBar.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 11)
                {
                    cLog.Add("", NameScreen, "Se ingresó un serial", false);
                    Serial = CodeBar.Text;
                    string SKUID = "";
                    CodeBar.Clear();
                    bool SerialFound = false, SerialsFinished = false;
                    bool IsSuccess = false;
                    
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);
                    IsSuccess = FoundS.IsSuccess;
                    foreach (int sku in ListOfSKU)
                    {
                        for (int i = 0; i < FoundS.Quantity; i++)
                        {
                            //MessageBox.Show(new Form() { TopMost = true },"Sku: " + sku + " Serie Qu: " + FoundS.Series[i].itemID + " Location: " + FoundS.Series[i].Location);
                            if (sku.ToString().Equals(FoundS.Series[i].itemID) && FoundS.Series[i].Location.Equals(Location.LBIN))
                            {
                                SerialFound = true;
                                SKUID = FoundS.Series[i].itemID;
                                break;
                            }
                        }
                        if (SerialFound)
                        {
                            break;
                        }

                    }

                    if (IsSuccess && SerialFound)
                    {
                        cLog.Add("", NameScreen, "El serial está en la misma ubicación seleccionada", false);
                        if (ListLabels.Count > 0)
                        {

                            bool findIt = false;
                            foreach (var dto in ListLabels)
                            {
                                if (dto.Equals(Serial))
                                {
                                    findIt = true;
                                    break;
                                }
                            }

                            if (!findIt)
                            {
                                SerialsFinished = CounterSerialsScanned(SKUID);
                                if (!SerialsFinished)
                                {
                                    skuInsert.Add(SKUID);
                                    ThreadStart delegado = new ThreadStart(ChangeColor);
                                    var t = new Thread(delegado);
                                    t.Start();
                                    panel1.BackColor = Color.Green;
                                    ListLabels.Add(Serial);
                                    lblSerieScanned.Text = Serial;
                                    CodeBar.Clear();
                                }
                                else
                                {
                                    cLog.Add("", NameScreen, "Usuario ingresó la cantidad necesaria para la Jaula", false);
                                    panel1.BackColor = Color.Red;
                                    CodeBar.Clear();
                                    MessageBox.Show(new Form() { TopMost = true },"Ya ingresó la cantidad necesaria");
                                    panel1.BackColor = Color.Black;
                                }
                            }
                            else
                            {
                                cLog.Add("", NameScreen, "Etiqueta ya ingresada", false);
                                panel1.BackColor = Color.Red;
                                CodeBar.Clear();
                                MessageBox.Show(new Form() { TopMost = true },"Ya ingresó esa etiqueta.");
                                panel1.BackColor = Color.Black;
                            }
                        }
                        else
                        {
                            SerialsFinished = CounterSerialsScanned(SKUID);
                            if (!SerialsFinished)
                            {
                                skuInsert.Add(SKUID);
                                ThreadStart delegado = new ThreadStart(ChangeColor);
                                var t = new Thread(delegado);
                                t.Start();
                                panel1.BackColor = Color.Green;
                                ListLabels.Add(Serial);
                                lblSerieScanned.Text = Serial;
                                CodeBar.Clear();
                            }
                            else
                            {
                                cLog.Add("", NameScreen, "Usuario ingresó la cantidad necesaria para la Jaula", false);
                                panel1.BackColor = Color.Red;
                                CodeBar.Clear();
                                MessageBox.Show(new Form() { TopMost = true },"Ya ingresó la cantidad necesaria");
                                panel1.BackColor = Color.Black;
                            }
                        }
                        CounterLabels = ListLabels.Count;
                        Counter.Text = CounterLabels.ToString();
                    }
                    else
                    {
                        cLog.Add("", NameScreen, "Serial no pertenece a esta planeación", false);
                        panel1.BackColor = Color.Red;
                        CodeBar.Clear();
                        MessageBox.Show(new Form() { TopMost = true },"No se encuentra esa etiqueta en planeación");
                        panel1.BackColor = Color.Black;

                    }
                    CodeBar.Focus();
                }
            }
            catch (Exception ex)
            {
                panel2.BackColor = Color.Red;
                cLog.Add("Line 291", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true },"Un error ha ocurrido.");
                panel2.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }

        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ListLabels.Count > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("Hay seriales que aún{0}no son enviados{0}¿Desea enviarlos?", Environment.NewLine);
                //Alert.Empl(Employ, strMensaje, false);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    UpdateAll = false;
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                UpdateAll = false;
                //DataBase Query = new DataBase();
                if (DB.ThereAreLabelsNotSend())
                {
                    //this.Close();
                    MessageBoxSupport Alert = new MessageBoxSupport();
                    string strMensaje = string.Format("Existen elementos que {0}aún no son enviados {0}¿Desea reenviarlos?", Environment.NewLine);
                    //Alert.Empl(Employ, strMensaje, false);
                    Alert.Empl(strMensaje);
                    var result = Alert.ShowDialog();
                    if (result == DialogResult.Yes)
                    {
                        cLog.Add("", NameScreen, "Click para ir a la pantalla reenvpios de la aplicación", false);
                        this.Close();
                        RetrySendingFrm n = new RetrySendingFrm();
                        n.Empl(Employ, true);
                        n.Show();
                    }
                    if(result == DialogResult.No)
                    {
                        cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                        this.Close();
                        MainFrm AccF = new MainFrm();
                        AccF.SetEmployeeInfo(Employ);
                        AccF.Show();
                    }
                }
                else
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
            }
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            SendData.Enabled = false;
            cbPlannedJaulas.Enabled = false;
            CodeBar.Enabled = false;
            pictureBox2.Enabled = false;
            PWait.Visible = true;
            if (BGSend.IsBusy != true)
            {
                BGSend.RunWorkerAsync();
            }
        }

        private void cbPlannedJaulas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Once)
            {
                Once = true;
                cLog.Add("", NameScreen, "Cambio de jaula", false);
                if (ListLabels.Count > 0)
                {
                    cbPlannedJaulas.Text = ListNames[IndexJaula];
                    string strMensaje = string.Format("Hay seriales que aún no a enviado{0}en " + cbPlannedJaulas.Text + ",{0}cambie cuando haya realizado el envio.", Environment.NewLine);
                    MessageBox.Show(new Form() { TopMost = true },strMensaje);
                }
                else
                {
                    IndexJaula = cbPlannedJaulas.SelectedIndex;
                    ListOfSKU.Clear();
                    ListOfSKU = DB.TakeIDSKUFromJaulas(ListID[cbPlannedJaulas.SelectedIndex].ToString());
                }
            }
        }

        private void cbPlannedJaulas_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void TranferJaula_Load(object sender, EventArgs e)
        {
            if (!(ListNames.Count > 0))
            {
                cLog.Add("", NameScreen, "Error al obtener datos de bines, ubicaciones y seriales", false);
                if (!ThereAreNotLabels) {
                    MessageBox.Show(new Form() { TopMost = true },"Error al cargar los datos.");
                }
            }
        }

        private void ColorBlack()
        {
            panel1.BackColor = Color.Black;
        }

        private void LPanel()
        {
            CounterLabels = ListLabels.Count;
            Counter.Text = CounterLabels.ToString();
        }

        private void CBINDEX()
        {
            INDCB = ListID[cbPlannedJaulas.SelectedIndex];
        }

        private void Tick()
        {
            TUpdateL.Interval = 60000;
            TUpdateL.Enabled = true;
            TUpdateL.Start();
        }

        private void BGWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (UpdateAll)
                {
                    TUpdateL.Stop();
                    cLog.Add("", NameScreen, "--Actualizar BD--", false);
                    QueryNS LblsTJ = new QueryNS();
                    LblsTJ.UpdatePT(Employ, NameScreen, false);
                    cLog.Add("", NameScreen, "--BD Actualizada--", false);
                }
            }
            catch (Exception ex)
            {
                cLog.Add("Line 426", NameScreen, ex.Message, true);
            }
        }

        private void BGWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (UpdateAll)
            {
                cLog.Add("", NameScreen, "Inicio Timer", false);
                Tick();
            }
            else
            {
                cLog.Add("", NameScreen, "--Trabajo cancelado--", false);
                TUpdateL.Stop();
            }
            
        }

        private void TUpdateL_Tick(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Comienzo de actualización", false);
            if (BGWork.IsBusy != true)
            {
                BGWork.RunWorkerAsync();
            }
        }

        private void BGSend_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                var Q = GetQueryFromNS.TakeJaulas(false, Employ, NameScreen);
                bool ThereAreData = Q.Item1;
                if (ThereAreData)
                {
                    INDC CBIN = new INDC(CBINDEX);
                    this.Invoke(CBIN);
                    PlannedJaulas Jaulas = Q.Item2;
                    var pi = GetQueryFromNS.QueryUbications(INDCB.ToString(), Employ, 12, NameScreen);
                    PlannedInventory Plan = pi.Item4;
                    //MessageBox.Show(Plan.IsSuccess.ToString());
                    List<int> qtySKU = new List<int>();
                    List<int> qtyInventory = new List<int>();
                    List<string> skuNoInsert = new List<string>();
                    List<string> lblNotSend = new List<string>();

                    
                    /*foreach (var qty in qtySKU)
                    {
                        MessageBox.Show("Qty: " + qty.ToString());
                    }*/

                    List<string> id = new List<string>();
                    List<int> counter1 = new List<int>();
                    List<int> counter2 = new List<int>();

                    for (int i = 0; i < Plan.DataJaula.ItemsJaula.Length; i++)
                    {
                        bool foundIt = false;
                        int co = 0;
                        if (id.Count > 0)
                        {
                            foreach (var item in id)
                            {
                                if (item.Equals(Plan.DataJaula.ItemsJaula[i].SKUID))
                                {
                                    foundIt = true;
                                    counter1[co] += Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].Quantity);
                                    counter2[co] += Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].firstScannedQty);
                                    break;
                                }
                                co++;
                            }
                            if (!foundIt)
                            {
                                id.Add(Plan.DataJaula.ItemsJaula[i].SKUID);
                                counter1.Add(Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].Quantity));
                                counter2.Add(Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].firstScannedQty));
                            }
                        }
                        else
                        {
                            id.Add(Plan.DataJaula.ItemsJaula[i].SKUID);
                            counter1.Add(Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].Quantity));
                            counter2.Add(Convert.ToInt32(Plan.DataJaula.ItemsJaula[i].firstScannedQty));
                        }
                    }

                    //MessageBox.Show("Cantidad unificada: " + id.Count.ToString());

                    foreach (var items in id)
                    {
                        //MessageBox.Show(items.ToString());
                        int count = 0;
                        for (int i = 0; i < skuInsert.Count; i++)
                        {
                            //MessageBox.Show("SKU Insert: " + skuInsert[i] + " SKU comparativo: " + items.ToString());
                            if (skuInsert[i].Equals(items.ToString()))
                            {
                                //MessageBox.Show(items.ToString());
                                count++;
                            }
                        }
                        qtySKU.Add(count);
                    }

                    //MessageBox.Show("Cantidad dentro de qtySKU: " + qtySKU.Count.ToString());

                    /*for (int i = 0; i < id.Count; i++)
                    {
                        MessageBox.Show("SKU: " + id[i] + " Qty total: " + counter1[i] + " Qty fs: " + counter2[i]);
                    }//*/

                    for (int i = 0; i < id.Count; i++)
                    {
                        int qtyRest = counter1[i] - counter2[i];
                        //MessageBox.Show("SKU: " + id[i] + " Cantidad restante: " + qtyRest);
                        qtyInventory.Add(qtyRest);
                    }


                    for (int i = 0; i < id.Count; i++)
                    {
                        //MessageBox.Show("Cantidad scanned: " + qtySKU[i].ToString() + " Cantidad inventory: " + qtyInventory[i].ToString());
                        if (qtySKU[i] > qtyInventory[i])
                        {
                            skuNoInsert.Add(id[i].ToString());
                        }
                    }

                    if (skuNoInsert.Count == 0)
                    {
                        cLog.Add("", NameScreen, "Click para enviar los datos de la aplicación", false);
                        if (ListLabels.Count > 0)
                        {
                            string DataList = "";
                            SeriesNotSend = "\"serials\":[";
                            foreach (var dto in ListLabels)
                            {
                                DataList += "\"" + dto + "\",";
                            }
                            SeriesNotSend += DataList;
                            int posInicial = SeriesNotSend.LastIndexOf(",");
                            int longitud = posInicial - SeriesNotSend.IndexOf("\"s");
                            SeriesNotSend = SeriesNotSend.Substring(0, longitud);

                            PanelColor BlackColor = new PanelColor(ColorBlack);
                            this.Invoke(BlackColor);


                            QueryNS GetQueryFromNS = new QueryNS();
                            var Query = GetQueryFromNS.SendDataTransfer("", SeriesNotSend, INDCB, 0, Employ, 4, "Transfer Jaula, Line 158");
                            if (Query.Item1)
                            {
                                cLog.Add("", NameScreen, "Información enviada a NS TJ", false);
                            }//*/
                            Message.Clear();
                            Message = Query.Item2;
                            ListLabels.Clear();
                            skuInsert.Clear();
                            Labels PanelTxtC = new Labels(LPanel);
                            this.Invoke(PanelTxtC);
                        }
                        else
                        {
                            cLog.Add("", NameScreen, "El usuario intentó enviar valores vacios", false);
                            MessageBox.Show(new Form() { TopMost = true }, "No hay operaciones a enviar.");
                        }
                    }
                    else
                    {
                        string skus = "SKU: ";
                        foreach (var item in skuNoInsert)
                        {
                            skus += item + ",";
                        }
                        int posInicial = skus.LastIndexOf(",");
                        int longitud = posInicial - skus.IndexOf("SKU:");
                        skus = skus.Substring(0, longitud);
                        string strMensaje = string.Format("Hay seriales que no pueden ser enviados debido a que ya{0}se cumplio la cantidad o porque la cantidad ingresada es mayor.", Environment.NewLine);
                        MessageBox.Show(new Form() { TopMost = true }, strMensaje);
                        MessageBox.Show(new Form() { TopMost = true }, skus);
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                cLog.Add("Line 636", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }

        }

        private void BGSend_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            SendData.Enabled = true;
            cbPlannedJaulas.Enabled = true;
            CodeBar.Enabled = true;
            pictureBox2.Enabled = true;
            PWait.Visible = false;
            CodeBar.Focus();
        }

        private void CodeBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }

        private void cbPlannedJaulas_Click(object sender, EventArgs e)
        {
            Once = false;
        }
    }
}
