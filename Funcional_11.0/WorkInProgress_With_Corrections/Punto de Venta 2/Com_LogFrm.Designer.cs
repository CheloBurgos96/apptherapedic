﻿
namespace NetSuiteApps
{
    partial class Com_LogFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Com_LogFrm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.DGDB = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkTypeError = new System.Windows.Forms.CheckBox();
            this.ChkUser = new System.Windows.Forms.CheckBox();
            this.ChkBDate = new System.Windows.Forms.CheckBox();
            this.FindError = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.WorkDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.UserID = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGDB)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1300, 38);
            this.panel2.TabIndex = 15;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(232, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 22;
            this.label7.Text = "-/-/-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(185, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 21;
            this.label6.Text = "Fecha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(24, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 20;
            this.label3.Text = "Registro de errores";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1262, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // DGDB
            // 
            this.DGDB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGDB.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DGDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGDB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGDB.Location = new System.Drawing.Point(0, 0);
            this.DGDB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DGDB.Name = "DGDB";
            this.DGDB.RowHeadersWidth = 51;
            this.DGDB.RowTemplate.Height = 29;
            this.DGDB.Size = new System.Drawing.Size(1127, 590);
            this.DGDB.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 628);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1300, 22);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.chkTypeError);
            this.panel1.Controls.Add(this.ChkUser);
            this.panel1.Controls.Add(this.ChkBDate);
            this.panel1.Controls.Add(this.FindError);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.WorkDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.UserID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(173, 590);
            this.panel1.TabIndex = 18;
            // 
            // chkTypeError
            // 
            this.chkTypeError.AutoSize = true;
            this.chkTypeError.BackColor = System.Drawing.Color.Transparent;
            this.chkTypeError.ForeColor = System.Drawing.Color.White;
            this.chkTypeError.Location = new System.Drawing.Point(16, 37);
            this.chkTypeError.Name = "chkTypeError";
            this.chkTypeError.Size = new System.Drawing.Size(96, 19);
            this.chkTypeError.TabIndex = 26;
            this.chkTypeError.Text = "Errores de NS";
            this.chkTypeError.UseVisualStyleBackColor = false;
            this.chkTypeError.CheckedChanged += new System.EventHandler(this.chkTypeError_CheckedChanged);
            // 
            // ChkUser
            // 
            this.ChkUser.AutoSize = true;
            this.ChkUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ChkUser.Location = new System.Drawing.Point(16, 84);
            this.ChkUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ChkUser.Name = "ChkUser";
            this.ChkUser.Size = new System.Drawing.Size(139, 19);
            this.ChkUser.TabIndex = 25;
            this.ChkUser.Text = "Buscar por ID Usuario";
            this.ChkUser.UseVisualStyleBackColor = true;
            // 
            // ChkBDate
            // 
            this.ChkBDate.AutoSize = true;
            this.ChkBDate.Checked = true;
            this.ChkBDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBDate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ChkBDate.Location = new System.Drawing.Point(16, 61);
            this.ChkBDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ChkBDate.Name = "ChkBDate";
            this.ChkBDate.Size = new System.Drawing.Size(116, 19);
            this.ChkBDate.TabIndex = 24;
            this.ChkBDate.Text = "Buscar por Fecha";
            this.ChkBDate.UseVisualStyleBackColor = true;
            // 
            // FindError
            // 
            this.FindError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.FindError.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FindError.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FindError.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.FindError.Location = new System.Drawing.Point(16, 247);
            this.FindError.Name = "FindError";
            this.FindError.Size = new System.Drawing.Size(135, 26);
            this.FindError.TabIndex = 23;
            this.FindError.Text = "Buscar";
            this.FindError.UseVisualStyleBackColor = false;
            this.FindError.Click += new System.EventHandler(this.FindError_Click_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label8.Location = new System.Drawing.Point(39, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 22;
            this.label8.Text = "Fecha de error";
            // 
            // WorkDate
            // 
            this.WorkDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.WorkDate.Location = new System.Drawing.Point(24, 140);
            this.WorkDate.Name = "WorkDate";
            this.WorkDate.Size = new System.Drawing.Size(117, 23);
            this.WorkDate.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(39, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 20;
            this.label1.Text = "ID Usuario:";
            // 
            // UserID
            // 
            this.UserID.Location = new System.Drawing.Point(24, 199);
            this.UserID.Name = "UserID";
            this.UserID.Size = new System.Drawing.Size(117, 23);
            this.UserID.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DGDB);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(173, 38);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1127, 590);
            this.panel3.TabIndex = 19;
            // 
            // Com_LogFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Com_LogFrm";
            this.Text = "Consulta de Errores";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGDB)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DataGridView DGDB;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkTypeError;
        private System.Windows.Forms.CheckBox ChkUser;
        private System.Windows.Forms.CheckBox ChkBDate;
        private System.Windows.Forms.Button FindError;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker WorkDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserID;
        private System.Windows.Forms.Panel panel3;
    }
}