﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace NetSuiteApps
{
    
    public partial class OperationsRegFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Operations Logs";
        public EmployeeModel Employ = new EmployeeModel();
        List<string> ListLabels = new List<string>();
        public string Operaciones = "\"series\":[";
        public int Counter = 0;

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        public void Empl(EmployeeModel n)
        {
            Employ = n;
            CodeBar.Text = Employ.RoleId;
        }


        public OperationsRegFrm()
        {
            InitializeComponent();
            this.CenterToScreen();
            cLog.Add("", NameScreen, "Insersión de fecha en la pantalla", false);
            String[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            label7.Text = result[0];
            stsStatus.Text = "No enviado";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox1.Text.Length >= 13)
            {
                cLog.Add("", NameScreen, "Ingreso de serial", false);
                string LBL = textBox1.Text;
                ListLabels.Add(LBL);
                LBLabels.DataSource = null;
                LBLabels.DataSource = ListLabels;
                stsCounterlbl.Text = ListLabels.Count.ToString();
                textBox1.Text = "";
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Regreso al menú principal de la aplicación", false);
            this.Close();
            MainFrm AccF = new MainFrm();
            AccF.SetEmployeeInfo(Employ);
            AccF.Show();
        }

        private void CleanScreenBTN_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Limpieza de variables y pantalla", false);
            Clean();
        }

        private void Clean()
        {
            Operaciones = "\"series\":[";
            stsStatus.Text = "No enviado";

            ListLabels.Clear();
            /*for (int i = 0; i < ListLabels.Count; i++)
            {
            }*/
            stsCounterlbl.Text = ListLabels.Count.ToString();
            LBLabels.DataSource = null;
            LBLabels.DataSource = ListLabels;
        }

        private void SendReportBTN_Click(object sender, EventArgs e)
        {

            if (ListLabels.Count > 0)
            {
                cLog.Add("", NameScreen, "Preparacíón de string de envio de seriales a NS", false);
                string DataList = "";
                foreach (var dto in ListLabels)
                {
                    DataList += "\"" + dto + "\",";
                }
                Operaciones = Operaciones + DataList;
                int posInicial = Operaciones.LastIndexOf(",");
                int longitud = posInicial - Operaciones.IndexOf("\"s");
                Operaciones = Operaciones.Substring(0, longitud);
                SendDataNS(Operaciones);
                Operaciones = "\"series\":[";
                stsCounterlbl.Text = ListLabels.Count.ToString();
            }
            else
            {
                cLog.Add("", NameScreen, "Envío de seriales a NS no se puede realizar porque está vacío", false);
                MessageBox.Show(new Form() { TopMost = true },"No hay operaciones a enviar.");
            }
            textBox1.Focus();
        }

        private void LBLabels_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (LBLabels.SelectedIndex >= 0)
                {
                    cLog.Add("", NameScreen, "Borrado de serial", false);
                    DialogResult oDlgRes;
                    oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea eliminar la operación seleccionada?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (oDlgRes == DialogResult.Yes)
                    {
                        ListLabels.Remove(LBLabels.Text);
                        LBLabels.DataSource = null;
                        LBLabels.DataSource = ListLabels;
                        stsCounterlbl.Text = ListLabels.Count.ToString();
                    }
                }
            }
        }

        

        public void SendDataNS(string n)
        {
            if (CodeBar.Text != "")
            {
                string[] result = WorkDate.Value.Date.ToShortDateString().Split(' ');
                QueryNS SendData = new QueryNS();
                cLog.Add("", NameScreen, "Envío de seriales a NS", false);
                if (SendData.RegisterOperations(n, CodeBar.Text, result[0], Employ, "Operations Register - Line 159"))
                {
                    cLog.Add("", NameScreen, "Recepción de respuesta de NS, recibida de manera correcta", false);
                    textBox1.Text = "";
                    Counter = 0;
                    stsCounterlbl.Text = Counter.ToString();
                    ListLabels.Clear();

                    LBLabels.DataSource = null;
                    LBLabels.DataSource = ListLabels;
                    stsStatus.Text = "Enviado";
                }
            }
            else
            {
                cLog.Add("", NameScreen, "Recepción de respuesta de NS, no fue recibida de manera correcta", false);
                MessageBox.Show(new Form() { TopMost = true },"Ingrese No. de Usuario");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back);
        }
    }
}
