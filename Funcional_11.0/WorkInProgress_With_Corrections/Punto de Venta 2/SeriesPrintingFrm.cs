﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using iText.Layout.Element;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using Newtonsoft.Json;
using iText.Layout.Borders;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

namespace NetSuiteApps
{
    public partial class SeriesPrintingFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "SeriesPrint";
        public EmployeeModel Employ;
        public static bool IsDbRecentlyCreated = false;
        public int contador = 0;
        public int counterCheckBox = 0;
        public CheckBox HeaderCheckBox = null;
        public bool IsHeaderCheckBoxClicked = false;
        public bool One = false;
        public int HeaderIndex = 0;
        bool printEt = false;
        public string Operaciones = "\"series\":[";
        bool FoundLbl = false;
        public CheckBox headerCheckBox = new CheckBox();
        delegate void InsertData(DataTable dtt);
        delegate void ShowB();
        delegate void ShowPrint();
        public bool ExecuteOne = false;
        List<string> ListSeries = new List<string>();

        DataBase DB = new DataBase(false);

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012,0);
        }

        public void Empl(EmployeeModel n)
        {
            Employ = n;
        }

        public SeriesPrintingFrm()
        {
            InitializeComponent();
            this.CenterToScreen();
            string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            label7.Text = result[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para envio de etiquetas impresas de etiquetas", false);
            if (WorkPrint.IsBusy != true)
            {
                cLog.Add("", NameScreen, "Entrada a etiquetas impresas a NS", false);
                WorkPrint.RunWorkerAsync();
            }
        }

        public void SendDataNS()
        {
           string[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
           int posInicial = Operaciones.LastIndexOf(",");
           int longitud = posInicial - Operaciones.IndexOf("\"s");
           Operaciones = Operaciones.Substring(0, longitud);
           cLog.Add("", NameScreen, "Envio de seriales impresos a NS", false);
           QueryNS SendLabels = new QueryNS();
           SendLabels.SendDataNSPrintLabels(Operaciones, result[0], Employ, "Series Print - Line 80");
           Operaciones = "\"series\":[";
        }

        private void TabFecha1_ValueChanged(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Cambio de fechas de inicio", false);
        }

        private void TabFecha2_ValueChanged(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Cambio de fechas de final", false);
        }

        private void DatosBD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!ExecuteOne)
            {
                cLog.Add("", NameScreen, "Adaptación de columnas y filas de DGV de la BD", false);
                if (DatosBD.Columns[e.ColumnIndex].Name == "checkBoxColumn")
                {
                    //int countCheckTrue = 0;
                    counterCheckBox = 0;
                    //foreach (DataGridViewRow row in this.DatosBD.Rows)
                    for (int i = 0; i <= DatosBD.RowCount - 1; i++)
                    {
                        bool n = false;
                        if (ListSeries.Count > 0)
                        {
                            foreach (var dto in new List<string>(ListSeries))
                            {
                                if (dto.Equals(DatosBD.Rows[i].Cells[1].Value.ToString()))
                                {
                                    n = true;
                                    break;
                                }
                            }
                        }
                        if (Convert.ToBoolean(DatosBD.Rows[i].Cells["checkBoxColumn"].Value) == true )
                        {
                            cLog.Add("", NameScreen, "Búsqueda para etiqueta ya impresa", false);
                            string ExistDB = "SELECT unique_serie FROM mattress_logs WHERE series_count = 1 AND unique_serie = '";
                            string ComExistQuery = ExistDB + DatosBD.Rows[i].Cells[1].Value.ToString() + "'";
                            if (DB.ExistInDB(ComExistQuery) && !n)
                            {
                                DialogResult oDlgRes;
                                oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea imprimir esta etiqueta ya impresa?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                                if (oDlgRes == DialogResult.No)
                                {
                                    cLog.Add("", NameScreen, "Usuario no aceptó imprimir etiqueta reimpresa", false);
                                    DataGridViewCheckBoxCell checkBox = (DatosBD.Rows[i].Cells["checkBoxColumn"] as DataGridViewCheckBoxCell);
                                    checkBox.Value = CheckState.Unchecked;

                                }
                                else
                                {
                                    cLog.Add("", NameScreen, "Usuario aceptó imprimir etiqueta reimpresa", false);
                                    ListSeries.Add(DatosBD.Rows[i].Cells[1].Value.ToString());
                                    counterCheckBox++;
                                }
                            }
                            else
                            {
                                counterCheckBox++;
                            }
                        }
                        else
                        {
                            if (ListSeries.Count > 0)
                            {
                                foreach (var dto in new List<string>(ListSeries))
                                {
                                    if (dto.Equals(DatosBD.Rows[i].Cells[1].Value.ToString()))
                                    {
                                        ListSeries.Remove(DatosBD.Rows[i].Cells[1].Value.ToString());
                                    }
                                }
                            }
                        }
                    }
                    Counterlblsts.Text = counterCheckBox.ToString();
                }
                ExecuteOne = true;
                cLog.Add("", NameScreen, "DGV refrescar edición hecha", false);
                this.DatosBD.RefreshEdit();
                cLog.Add("", NameScreen, "DGV refrescar para elimnar hecha", false);
                this.DatosBD.Refresh();
            }
            else
            {
                ExecuteOne = false;
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                //Loop to verify whether all row CheckBoxes are checked or not.
                bool isChecked = true;
                foreach (DataGridViewRow row in this.DatosBD.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["checkBoxColumn"].EditedFormattedValue) == false)
                    {
                        isChecked = false;
                        break;
                    }
                }
                headerCheckBox.Checked = isChecked;
            }
        }

        private void ConsultaDatos_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para consulta de datos series en NS", false);
            if (BGWork.IsBusy != true)
            {
                cLog.Add("", NameScreen, "Consulta de datos series en NS", false);
                BGWork.RunWorkerAsync();
            }
        }

        private void Showlabels()
        {
            stsBProgressBar.Visible = true;
            stsOperation.Visible = true;
            toolStripStatusLabel2.Visible = true;
            stsOperation.Text = "Consulta";
        }

        private void ShowButtom()
        {
            ImprimirDatos.Visible = true;
        }

        private void PrintLabelsWait()
        {
            stsOperation.Text = "Impresión";
        }

        private void SetDG(DataTable dt)
        {
            cLog.Add("", NameScreen, "Construcción de tabla DGV", false);
            this.DatosBD.DataSource = dt;
            this.DatosBD.AllowUserToAddRows = false;
            this.DatosBD.ReadOnly = false;
            this.DatosBD.Columns[HeaderIndex].HeaderText = "Serie";
            this.DatosBD.Columns[HeaderIndex].Width = 110;
            this.DatosBD.Columns[HeaderIndex + 1].HeaderText = "Descripción";
            this.DatosBD.Columns[HeaderIndex + 2].HeaderText = "SKU";
            this.DatosBD.Columns[HeaderIndex + 2].Width = 110;
            this.DatosBD.Columns[HeaderIndex + 3].HeaderText = "Orden de trabajo";
            this.DatosBD.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Find the Location of Header Cell.
            //this.DatosBD.EndEdit();
            Point headerCellLocation = this.DatosBD.GetCellDisplayRectangle(0, -1, true).Location;

            //Place the Header CheckBox in the Location of the Header Cell.
            headerCheckBox.Location = new Point(headerCellLocation.X + 8, headerCellLocation.Y + 2);
            headerCheckBox.BackColor = Color.White;
            headerCheckBox.Size = new Size(18, 18);

            //Assign Click event to the Header CheckBox.
            headerCheckBox.Click += new EventHandler(this.HeaderCheckBox_Clicked);
            this.DatosBD.Controls.Add(headerCheckBox);


            if (!One)
            {
                //Add a CheckBox Column to the DataGridView at the first position.
                //if (DatosBD.Rows.Count == 0){
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "",
                    Width = 30,
                    Name = "checkBoxColumn",
                    ReadOnly = false
                };
                this.DatosBD.Columns.Insert(0, checkBoxColumn);
                this.DatosBD.Columns[0].Width = 30;
                HeaderIndex = 1;
                One = true;
            }
            //}

            //Assign Click event to the DataGridView Cell.
            this.DatosBD.CellContentClick += new DataGridViewCellEventHandler(this.DatosBD_CellContentClick);
            this.DatosBD.RowPostPaint += new DataGridViewRowPostPaintEventHandler(this.dgvUserDetails_RowPostPaint);
            Counterlblsts.Text = "0";
           // TxtBoxOrder.Text = "";
        }

        private void GridZero(DataTable dt)
        {
            //this.DatosBD.Columns.Clear();
            dt.Rows.Clear();
            this.DatosBD.DataSource = dt;
            Counterlblsts.Text = "0";
            ImprimirDatos.Visible = false;
        }
        
        private void BindGrid()
        {
            cLog.Add("", NameScreen, "Inserción de datos a DGV", false);
            /*Descomentar si se quiere cambiar motor de base de datos del programa
                 y cambiarlo por el motor que desee, SQLite o SQL Server*/
            //DataBase InsertInDGV = new DataBase();
            //SQLServerDataBase InsertInDGV = new SQLServerDataBase();
            DataTable dt = new DataTable();
            //if (!chkBoxOrder.Checked)
            //{
                string[] result = TabFecha1.Value.Date.ToShortDateString().Split(new char[] { ' ', '/' }, StringSplitOptions.RemoveEmptyEntries);
                string[] result2 = TabFecha2.Value.Date.ToShortDateString().Split(new char[] { ' ', '/' }, StringSplitOptions.RemoveEmptyEntries);
                string DateOne = result[2] + result[1] + result[0];
                string DateTwo = result2[2] + result2[1] + result2[0];

                int TypeQuery = 1;
                if (chkPrinted.Checked) {
                    if (chkSKU.Checked && chkBoxOrder.Checked)
                    {
                        TypeQuery = 11;
                    }
                    else if (chkSKU.Checked)
                    {
                        TypeQuery = 10;
                    }
                    else if (chkBoxOrder.Checked)
                    {
                        TypeQuery = 2;
                    }
                    else
                    {
                        TypeQuery = 0;
                    }
                    //query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND date_serie BETWEEN '" + DateOne + "' AND '" + DateTwo + "';";
                }
                else
                {
                    if (chkSKU.Checked && chkBoxOrder.Checked)
                    {
                        TypeQuery = 14;
                    }
                    else if (chkSKU.Checked)
                    {
                        TypeQuery = 13;
                    }
                    else if (chkBoxOrder.Checked)
                    {
                        TypeQuery = 12;
                    }
                    else
                    {
                        TypeQuery = 1;
                    }
                    //query = "SELECT unique_serie, description, sku, sell_order FROM mattress_logs WHERE series_reprinted = false AND date_serie BETWEEN '" + DateOne + "' AND '" + DateTwo + "' AND series_count < 1;";
                }
                bool VideTxt = false, VideOrder = false, VideSku = false, VideAll = false;
            switch (TypeQuery)
            {
                case 0:
                    dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    break;
                case 1:
                    dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    break;
                case 2:
                    if (TxtBoxOrder.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideOrder = true;
                        VideTxt = true;
                    }
                    dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    break;
                case 10:
                    if (TxtBoxSKU.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideSku = true;
                        VideTxt = true;
                    }
                    dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    break;
                case 11:
                    if (TxtBoxOrder.Text.Length > 0 && TxtBoxSKU.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideAll = true;
                        VideTxt = true;
                    }
                    dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    break;
                case 12:
                    if (TxtBoxOrder.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideOrder = true;
                        VideTxt = true;
                    }
                    break;
                case 13:
                    if (TxtBoxSKU.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideSku = true;
                        VideTxt = true;
                    }
                    break;
                case 14:
                    if (TxtBoxOrder.Text.Length > 0 && TxtBoxSKU.Text.Length > 0)
                    {
                        dt = DB.DataTableBD(TypeQuery, DateOne, DateTwo, TxtBoxOrder.Text, "", TxtBoxSKU.Text);
                    }
                    else
                    {
                        VideAll = true;
                        VideTxt = true;
                    }
                    break;
                }

            if (!VideTxt)
            {
                if (dt.Rows.Count <= 0)
                {
                    cLog.Add("", NameScreen, "No hay datos por mostrar en DGV", false);
                    FoundLbl = false;
                    InsertData DataDGV = new InsertData(GridZero);
                    this.Invoke(DataDGV, dt);
                    //Counterlblsts.Text = "0";
                    MessageBox.Show(new Form() { TopMost = true },"No se encontraron etiquetas.");
                }
                else
                {
                    cLog.Add("", NameScreen, "Hay datos por mostrar en DGV", false);
                    InsertData DataDGV = new InsertData(SetDG);
                    this.Invoke(DataDGV, dt);
                }
            }
            else
            {
                FoundLbl = false;
                InsertData DataDGV = new InsertData(GridZero);
                this.Invoke(DataDGV, dt);
                if (VideAll)
                {
                    cLog.Add("", NameScreen, "Criterios de busqueda por SKU y Orden de trabajao vacíos", false);
                    MessageBox.Show(new Form() { TopMost = true },"Campos SKU y Orden de trabajo vacios.");
                }else if (VideSku)
                {
                    cLog.Add("", NameScreen, "Criterios de busqueda por SKU vacíos", false);
                    MessageBox.Show(new Form() { TopMost = true },"Campo SKU vacio.");
                }else if (VideOrder)
                {
                    cLog.Add("", NameScreen, "Criterios de busqueda por Orden de trabajao vacíos", false);
                    MessageBox.Show(new Form() { TopMost = true },"Campo Orden de trabajo vacio.");
                }
            }
                
        }

        private void dgvUserDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(DatosBD.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
            }
        }

        private void HeaderCheckBox_Clicked(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Selección de impresión de todas las etiquetas", false);
            //Necessary to end the edit mode of the Cell.
            this.DatosBD.EndEdit();

            //Loop and check and uncheck all row CheckBoxes based on Header Cell CheckBox.
            foreach (DataGridViewRow row in this.DatosBD.Rows)
            {
                DataGridViewCheckBoxCell checkBox = (row.Cells["checkBoxColumn"] as DataGridViewCheckBoxCell);
                checkBox.Value = headerCheckBox.Checked;
            }

            counterCheckBox = 0;

            //DataBase QueryPrint = new DataBase();
            for (int i = 0; i <= DatosBD.RowCount - 1; i++)
            {
                bool n = false;
                if (ListSeries.Count > 0)
                {
                    foreach (var dto in new List<string>(ListSeries))
                    {
                        if (dto.Equals(DatosBD.Rows[i].Cells[1].Value.ToString()))
                        {
                            n = true;
                            break;
                        }
                    }
                    
                }
                if (Convert.ToBoolean(DatosBD.Rows[i].Cells["checkBoxColumn"].Value) == true)
                {
                    cLog.Add("", NameScreen, "Etiquetas ya impresas", false);
                    string ExistDB = "SELECT unique_serie FROM mattress_logs WHERE series_count = 1 AND unique_serie = '";
                    string ComExistQuery = ExistDB + DatosBD.Rows[i].Cells[1].Value.ToString() + "'";
                    if (DB.ExistInDB(ComExistQuery) && !n)
                    {
                        DialogResult oDlgRes;
                        oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea imprimir esta etiqueta [" + DatosBD.Rows[i].Cells[1].Value.ToString() + "] ya impresa?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                        if (oDlgRes == DialogResult.No)
                        {
                            DataGridViewCheckBoxCell checkBox = (DatosBD.Rows[i].Cells["checkBoxColumn"] as DataGridViewCheckBoxCell);
                            checkBox.Value = CheckState.Unchecked;
                            //headerCheckBox.Checked = false;
                        }
                        else
                        {
                            ListSeries.Add(DatosBD.Rows[i].Cells[1].Value.ToString());
                            counterCheckBox++;
                        }
                    }
                }
                else
                {
                    if (ListSeries.Count > 0)
                    {
                        foreach (var dto in new List<string>(ListSeries))
                        {
                            if (dto.Equals(DatosBD.Rows[i].Cells[1].Value.ToString()))
                            {
                                ListSeries.Remove(DatosBD.Rows[i].Cells[1].Value.ToString());
                            }
                        }
                    }
                }
            }

            //foreach (DataGridViewRow row in this.DatosBD.Rows)
            for (int i = 0; i <= DatosBD.RowCount - 1; i++)
            {
                if (Convert.ToBoolean(DatosBD.Rows[i].Cells["checkBoxColumn"].Value) == true)
                {
                    counterCheckBox++;
                }

            }
            Counterlblsts.Text = counterCheckBox.ToString();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void BGWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (TabFecha1.Value <= TabFecha2.Value)
            {
                ShowPrint Changelbl = new ShowPrint(Showlabels);
                this.Invoke(Changelbl);
                BGWork.ReportProgress(5);
                string[] result = TabFecha1.Value.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                string[] result2 = TabFecha2.Value.Date.ToShortDateString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                        
                BGWork.ReportProgress(10);
                QueryNS QueryLabels = new QueryNS();
                cLog.Add("", NameScreen, "Envío de datos de seriales", false);
                var Q = QueryLabels.LabelsToPrint(result[0], result2[0], true, Employ, "Series print - Line 608");
                cLog.Add("", NameScreen, "Recepción de datos de seriales", false);
                bool ThereAreLabels = Q.Item1;
                if (ThereAreLabels)
                {
                    cLog.Add("", NameScreen, "Hay etiquetas por realizar", false);
                    FoundLbl = true;
                    BGWork.ReportProgress(15);
                    int Tamano = Q.Item2.Datos.Length;
                    BGWork.ReportProgress(20);
                    /*Descomentar si se quiere cambiar motor de base de datos del programa
                    y cambiarlo por el motor que desee, SQLite o SQL Server*/
                    //DataBase InsertData = new DataBase();

                    //SQLServerDataBase InsertData = new SQLServerDataBase();
                    //DB.DelateAllInTable("mattress_logs");
                    //for (int i = 0; i < 1; i++)
                    //{
                    cLog.Add("", NameScreen, "Guardado de etiquetas a BD", false);
                    DB.InsertAllInDB2(Q.Item2);
                        //float percent = (80 * (i + 1) / Tamano);
                        //BGWork.ReportProgress(20 + Convert.ToInt32(percent));
                    //}
                    cLog.Add("", NameScreen, "Término de guardado de etiquetas a BD", false);
                    this.BindGrid();

                    ShowB ButtomV = new ShowB(ShowButtom);
                    this.Invoke(ButtomV);
                    BGWork.ReportProgress(100);
                }
                else
                {
                    cLog.Add("", NameScreen, "No hay etiquetas por realizar", false);
                    BGWork.ReportProgress(100);
                    MessageBox.Show(new Form() { TopMost = true },"No se encontraron etiquetas.");
                }
            }
            else
            {
                cLog.Add("", NameScreen, "Usuario ingresó fecha inicial mayor a la final", false);
                MessageBox.Show(new Form() { TopMost = true },"Fecha inicio no puede ser mayor a Fecha final");
            }
        }

        private void BGWork_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            stsBProgressBar.Value = e.ProgressPercentage;
        }

        private void BGWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (FoundLbl)
            {
                cLog.Add("", NameScreen, "Etiquetas listas para ser impresas", false);
                //MessageBox.Show(new Form() { TopMost = true },"Etiquetas listas para imprimir.");
                FoundLbl = false;
            }
        }

        private void WorkPrint_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

            WorkPrint.ReportProgress(0);
            bool PrintFiles = true;
            int ContCheck = 0;
            string UniqueSerie = "";
            cLog.Add("", NameScreen, "Selección de lineas de seriales a imprimir", false);
            for (int i = 0; i <= DatosBD.RowCount - 1; i++)
            {
                if (Convert.ToBoolean(DatosBD.Rows[i].Cells["checkBoxColumn"].Value) == true)
                {
                    string dat = DatosBD.Rows[i].Cells[1].Value.ToString();
                    ContCheck++;
                    UniqueSerie += dat + ",";
                }

            }
            if (ContCheck == DatosBD.RowCount)
            {
                DialogResult oDlgRes;
                oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea imprimir todas las etiquetas seleccionadas?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (oDlgRes == DialogResult.No)
                {
                    PrintFiles = false;
                    WorkPrint.ReportProgress(100);
                }
            }
            else if (ContCheck >= DatosBD.RowCount)
            {
                MessageBox.Show(new Form() { TopMost = true },"Ocurrió un error, casillas de más seleccionadas");
                PrintFiles = false;
                WorkPrint.ReportProgress(100);
            }
            else if (ContCheck >= 2 && ContCheck <= DatosBD.RowCount)
            {
                DialogResult oDlgRes;
                oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea imprimir las " + ContCheck.ToString() + " etiquetas seleccionadas?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (oDlgRes == DialogResult.No)
                {
                    PrintFiles = false;
                }
            }
            else if (ContCheck == 1)
            {
                DialogResult oDlgRes;
                oDlgRes = MessageBox.Show(new Form() { TopMost = true },"¿Está seguro que desea imprimir la etiqueta seleccionada?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (oDlgRes == DialogResult.No)
                {
                    PrintFiles = false;
                }
            }
            cLog.Add("", NameScreen, "Selección de seriales a imprimir", false);

            //var consulta = "SELECT * FROM mattress_logs WHERE unique_serie = '";
            //var consulta = "SELECT * FROM mattress_logs WHERE unique_serie = '";
            WorkInProgressModel datos = new WorkInProgressModel();

            string[] result = UniqueSerie.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int NuCant = result.Length;
            cLog.Add("", NameScreen, "Selección de seriales a imprimir", false);
            cLog.Add("", NameScreen, "Asignación de nombre al archivo de impresión", false);
            string nameF = "PROC_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
            if (NuCant > 0)
            {
                cLog.Add("", NameScreen, "Hay seriales por imprimir", false);
                if (PrintFiles)
                {
                    cLog.Add("", NameScreen, "Proceso para formar la etiqueta de PP", false);
                    ShowPrint Changelbl = new ShowPrint(PrintLabelsWait);
                    this.Invoke(Changelbl);
                    cLog.Add("", NameScreen, "Apertura de documento que contiene las series", false);
                    CodeBar Serie = new CodeBar(nameF, false);
                    bool PageSizeChange = false;
                    bool Once = true;
                    //DataBase ChangeDocument = new DataBase();
                    cLog.Add("", NameScreen, "Se consulta la cantidad de procesos que tiene ese serial", false);
                    if (DB.ExistInDB("SELECT id_operation5 FROM mattress_logs WHERE unique_serie = '" + result[0] + "' AND id_operation5 != ''"))
                    {
                        PageSizeChange = true;
                    }
                    else
                    {
                        Serie.SizePageWithOutMargin(false);
                    }
                    cLog.Add("", NameScreen, "Comienzo de lectura de la información de la series", false);
                    for (int w = 0; w < NuCant; w++)
                    {

                        if (PageSizeChange && Once)
                        {
                            Serie.SizePageWithOutMargin(true);
                            Once = false;
                        }
                        else
                        {
                            Once = false;
                        }

                        float Progress = (w * 100 / NuCant) ;
                        WorkPrint.ReportProgress(Convert.ToInt32( Progress));
                        printEt = false;
                        Table table = Serie.CreateOneTableInsert(1, 230f);
                        datos.Serie = result[w];
                        Operaciones += "\"" + result[w] + "\",";
                        //string NR = consulta + datos.Serie + "'";

                        float HeighCBLP = 57f;// 59.58f;//50.77f = 1.278cm // 79.45f = 2.32 cm //68.49f = 1.982 //50.77f = 
                        float HeighCB = 45f;//45.1f;//50.77f = 1.482cm  // 45.1f =
                        float WidthCB = 198f;//270.77f = 9.546 cm //227f = 7.997cm // 176f = 6.2cm
                        float HeighCellP1 = 98f;//83.28f;  //95f = 3.5cm // 73.28f = 2.7cm
                        float HeighCellP2 = 115f;  //115f = 4cm // 135.125f = 4.926cm // 130.3f = 4.7cm // 
                        float HeighCellP3 = 88f;//88f;  //95F = 3.5cm // 88.612F = 2.7cm // 73.48f = 
                        //float HeighCellP = 307.82f;  //372.31f = 13.2 cm //338.46f = 12 cm // 310.255f = 11.087 //307.82f = 11cm
                        float HeighCell = 79f;//81.82f; //101.54f = 3.723 cm //81.82f = 3 cm
                        float FontSizeL = 2f;// 4.9f;//4f = 0.181cm // 11.05f = 0.45cm // 4.9f = 0.3cm // 3.7f = 0.2cm
                        float FontSizeP = 6f;// 7.876f;//5f = 0.127cm // 19.69f = 0.5cm // 7.876f = 0.2cm
                        float FontSizePX = 15f; //16f = 0.418cm // 38.27f = 0.981cm //19.5f = 0.508cm // 15.354f = 
                        float FontSizeX = 50.77f;
                        float SetBaseLN = 6f;
                        //float LetterSize = ;

                        /*Descomentar si se quiere cambiar motor de base de datos del programa
                         y cambiarlo por el motor que desee, SQLite o SQL Server*/
                        //DataBase Consultas = new DataBase();
                        //SQLServerDataBase Consultas = new SQLServerDataBase();
                        datos = DB.TablesDatas(datos.Serie);

                        try
                        {
                            iText.Layout.Element.Image SerieSM = Serie.CodeBarStick(datos.Serie, SetBaseLN, FontSizeL, 0, WidthCB, HeighCBLP);
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, WidthCB, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell Serie1 = new Cell().Add(SerieSM).Add(DescriptionS);
                            Serie1.SetHeight(HeighCellP1).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                            table.AddCell(Serie1);

                            iText.Layout.Element.Image SKU = Serie.CodeBarStick(datos.SKU, SetBaseLN, FontSizeL, 0, WidthCB, HeighCBLP);
                            Paragraph DescriptionXL = Serie.CreateParagraph(datos.Descripcion, 0, FontSizePX, WidthCB, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell LabelSKU = new Cell().Add(SKU).Add(DescriptionXL);
                            LabelSKU.SetHeight(HeighCellP2).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                            table.AddCell(LabelSKU);

                            iText.Layout.Element.Image SerieL = Serie.CodeBarStick2(datos.Serie, 0, WidthCB, 45f);//41.8f);
                            Paragraph SerieXL = Serie.CreateParagraph("*"+datos.Serie+"*", 0, 18f, WidthCB, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell Serie2 = new Cell().Add(SerieL).Add(SerieXL);
                            Serie2.SetHeight(HeighCellP3).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                            table.AddCell(Serie2);

                            /*Cell cell1 = new Cell().Add(SerieSM).Add(DescriptionS).Add(SKU).Add(DescriptionXL).Add(SerieL);
                            cell1.SetHeight(HeighCellP).SetBorder(Border.NO_BORDER);
                            table.AddCell(cell1);*/

                            printEt = true;
                        }
                        catch (Exception ee)
                        {
                            string[] Date = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                            //DataBase Error = new DataBase();
                            DB.SaveError(Employ.RoleId, Date[0], Date[1], ee.ToString(), "Create codebar");
                            MessageBox.Show(new Form() { TopMost = true },"Valores vacios para crear etiqueta, no se imprimirá. Consulte con un técnico si el error persiste.");
                            break;
                        }

                        if (datos.StepSerial1 != "" && datos.StepName1 != "" && datos.Descripcion != "" && datos.StepSerial1 != null && datos.StepName1 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess1 = Serie.CodeBarStick(datos.StepSerial1, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess1 = Serie.CreateParagraph(datos.StepName1, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess1).Add(PNameProcess1).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(PNameProcess5);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                            table.AddCell(cell2);

                        }

                        if (datos.StepSerial2 != "" && datos.StepName2 != "" && datos.Descripcion != "" && datos.StepSerial2 != null && datos.StepName2 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess2 = Serie.CodeBarStick(datos.StepSerial2, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess2 = Serie.CreateParagraph(datos.StepName2, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess2).Add(PNameProcess2).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(PNameProcess5);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.TOP);
                            table.AddCell(cell2);
                        }

                        if (datos.StepSerial3 != "" && datos.StepName3 != "" && datos.Descripcion != "" && datos.StepSerial3 != null && datos.StepName3 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess3 = Serie.CodeBarStick(datos.StepSerial3, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess3 = Serie.CreateParagraph(datos.StepName3, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess3).Add(PNameProcess3).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(PNameProcess5);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.MIDDLE);
                            table.AddCell(cell2);
                        }

                        if (datos.StepSerial4 != "" && datos.StepName4 != "" && datos.Descripcion != "" && datos.StepSerial4 != null && datos.StepName4 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess4 = Serie.CodeBarStick(datos.StepSerial4, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess4 = Serie.CreateParagraph(datos.StepName4, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess4).Add(PNameProcess4).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(PNameProcess5);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }

                        if (datos.StepSerial5 != "" && datos.StepName5 != "" && datos.Descripcion != "" && datos.StepSerial5 != null && datos.StepName5 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess5 = Serie.CodeBarStick(datos.StepSerial5, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess5 = Serie.CreateParagraph(datos.StepName5, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess5).Add(PNameProcess5).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            if (PageSizeChange)
                            {
                                Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                                Cell cell2 = new Cell().Add(PNameProcess5);
                                cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                                table.AddCell(cell2);
                            }
                        }

                        if (datos.StepSerial6 != "" && datos.StepName6 != "" && datos.Descripcion != "" && datos.StepSerial6 != null && datos.StepName6 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess6 = Serie.CodeBarStick(datos.StepSerial6, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess6 = Serie.CreateParagraph(datos.StepName6, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess6).Add(PNameProcess6).Add(DescriptionS);
                            cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            if (PageSizeChange)
                            {
                                Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                                Cell cell2 = new Cell().Add(PNameProcess5);
                                cell2.SetHeight(HeighCell).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                                table.AddCell(cell2);
                            }
                        }

                        if (datos.StepSerial7 != "" && datos.StepName7 != "" && datos.Descripcion != "" && datos.StepSerial7 != null && datos.StepName7 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess7 = Serie.CodeBarStick(datos.StepSerial7, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess7 = Serie.CreateParagraph(datos.StepName7, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess7).Add(PNameProcess7).Add(DescriptionS);
                            cell2.SetHeight((HeighCell + 6f)).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            if (PageSizeChange)
                            {
                                Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                                Cell cell2 = new Cell().Add(PNameProcess5);
                                cell2.SetHeight(HeighCell + 6f).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                                table.AddCell(cell2);
                            }
                        }

                        if (datos.StepSerial8 != "" && datos.StepName8 != "" && datos.Descripcion != "" && datos.StepSerial8 != null && datos.StepName8 != null && datos.Descripcion != null)
                        {
                            iText.Layout.Element.Image SerialProcess8 = Serie.CodeBarStick(datos.StepSerial8, SetBaseLN, FontSizeL, 0, WidthCB, HeighCB);
                            Paragraph PNameProcess8 = Serie.CreateParagraph(datos.StepName8, 0, FontSizeP, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Paragraph DescriptionS = Serie.CreateParagraph(datos.Descripcion, 0, FontSizeP, 190f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                            Cell cell2 = new Cell().Add(SerialProcess8).Add(PNameProcess8).Add(DescriptionS);
                            cell2.SetHeight(HeighCell + 6f).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                            table.AddCell(cell2);
                        }
                        else
                        {
                            if (PageSizeChange)
                            {
                                Paragraph PNameProcess5 = Serie.CreateParagraph("X", 0, FontSizeX, 100f, PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD));
                                Cell cell2 = new Cell().Add(PNameProcess5);
                                cell2.SetHeight(HeighCell + 6f).SetBorder(Border.NO_BORDER).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetVerticalAlignment(iText.Layout.Properties.VerticalAlignment.BOTTOM);
                                table.AddCell(cell2);
                            }
                        }

                        if (datos.CounterPrint == 0 && datos.Reprinted == false)
                        {
                            DB.UpDateDB("UPDATE mattress_logs SET series_count = 1, series_reprinted = 0 WHERE unique_serie = '" + datos.Serie + "';");
                        }
                        else if (datos.CounterPrint == 1 && datos.Reprinted == false)
                        {
                            DB.UpDateDB("UPDATE mattress_logs SET series_count = 1, series_reprinted = 1 WHERE unique_serie = '" + datos.Serie + "';");
                        }

                        if (w < NuCant - 1)
                        {
                            if (DB.ExistInDB("SELECT id_operation5 FROM mattress_logs WHERE unique_serie = '" + result[w + 1] + "' AND id_operation5 != ''"))
                            {
                                PageSizeChange = true;
                            }
                            else
                            {
                                PageSizeChange = false;
                            }
                            Serie.AddNewPageAndTable(true, PageSizeChange);
                        }
                        else
                        {
                            Serie.AddNewPageAndTable(false, PageSizeChange);
                        }
                    }
                    cLog.Add("", NameScreen, "Término de la formación de los códigos de los seriales", false);
                    if (printEt)
                    {
                        cLog.Add("", NameScreen, "Se imprimirán el archivo que contiene los seriales", false);
                        /*string functionReturnValue = null;
                        System.Drawing.Printing.PrinterSettings oPS = new System.Drawing.Printing.PrinterSettings();
                        try
                        {
                            functionReturnValue = oPS.PrinterName;
                        }
                        catch (Exception ex)
                        {
                            functionReturnValue = "";
                        }
                        finally
                        {
                            oPS = null;
                        }*/
                        cLog.Add("", NameScreen, "Cierre del documento que contiene los seriales", false);
                        Serie.CloseDocument(false);
                        string dirF = @"C:\Temp\" + nameF;
                        PrintLabels Printlbl = new PrintLabels();
                        cLog.Add("", NameScreen, "Inicio de impresión de las series", false);
                        while (true)
                        {
                            if (Printlbl.Print(nameF, Employ.RoleId, "Bagged"))
                            {
                                cLog.Add("", NameScreen, "Etiqueta impresa", false);
                                DialogResult dialog;
                                dialog = MessageBox.Show("¿Se imprimió la(s) etiqueta(s)?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                                if (dialog == DialogResult.Yes)
                                {
                                    //proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                    cLog.Add("", NameScreen, "Proceso de impresión finalizado", false);
                                    Printlbl.KillAdobe("AcroRd32");
                                    cLog.Add("", NameScreen, "Proceso de cerrar Adobe finalizado", false);
                                    SendDataNS();
                                    break;
                                }
                                //MessageBox.Show(new Form() { TopMost = true },"Etiqueta impresa");
                            }
                            else
                            {
                                cLog.Add("", NameScreen, "Error al tratar de imprimir", false);
                                MessageBox.Show("Error al imprimir");
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                cLog.Add("", NameScreen, "El usuario no seleccionó los campos a imprimir", false);
                MessageBox.Show(new Form() { TopMost = true },"Seleccione los campos a imprimir");
            }
            BindGrid();
        }

        private void WorkPrint_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            stsBProgressBar.Value = e.ProgressPercentage;
        }

        private void WorkPrint_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show(new Form() { TopMost = true },"Etiqueta/as impresa/s.");
        }

        private void chkBoxOrder_CheckedChanged(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en el check de Orden de venta", false);
            if (chkBoxOrder.Checked)
            {
                TxtBoxOrder.Enabled = true;
            }
            else
            {
                TxtBoxOrder.Enabled = false;
                TxtBoxOrder.Text = "";
            }
        }

        private void DatosBD_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (DatosBD.IsCurrentCellDirty)
            {
                DatosBD.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Regreso al menú principal de la aplicación", false);
            this.Close();
            MainFrm AccF = new MainFrm();
            AccF.SetEmployeeInfo(Employ);
            AccF.Show();

        }

        private void TxtBoxOrder_TextChanged(object sender, EventArgs e)
        {

        }

        private void chkSKU_CheckedChanged(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en el check de SKU", false);
            if (chkSKU.Checked)
            {
                TxtBoxSKU.Enabled = true;
            }
            else
            {
                TxtBoxSKU.Enabled = false;
                TxtBoxSKU.Text = "";
            }
        }

        private void chkPrinted_CheckedChanged(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click en el check de Impresas", false);
        }

        private void SeriesPrintingFrm_Load(object sender, EventArgs e)
        {

        }
    }
}
