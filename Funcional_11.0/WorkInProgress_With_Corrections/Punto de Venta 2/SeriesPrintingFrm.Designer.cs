﻿
namespace NetSuiteApps
{
    partial class SeriesPrintingFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeriesPrintingFrm));
            this.ImprimirDatos = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TabFecha2 = new System.Windows.Forms.DateTimePicker();
            this.TabFecha1 = new System.Windows.Forms.DateTimePicker();
            this.ConsultaDatos = new System.Windows.Forms.Button();
            this.DatosBD = new System.Windows.Forms.DataGridView();
            this.chkPrinted = new System.Windows.Forms.CheckBox();
            this.TxtBoxOrder = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkBoxOrder = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.Counterlblsts = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stsOperation = new System.Windows.Forms.ToolStripStatusLabel();
            this.stsBProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.BGWork = new System.ComponentModel.BackgroundWorker();
            this.WorkPrint = new System.ComponentModel.BackgroundWorker();
            this.Message = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.TxtBoxSKU = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkSKU = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.DatosBD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImprimirDatos
            // 
            this.ImprimirDatos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.ImprimirDatos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ImprimirDatos.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ImprimirDatos.Location = new System.Drawing.Point(130, 411);
            this.ImprimirDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ImprimirDatos.Name = "ImprimirDatos";
            this.ImprimirDatos.Size = new System.Drawing.Size(86, 40);
            this.ImprimirDatos.TabIndex = 11;
            this.ImprimirDatos.Text = "Imprimir";
            this.ImprimirDatos.UseVisualStyleBackColor = false;
            this.ImprimirDatos.Visible = false;
            this.ImprimirDatos.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(18, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fecha Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(18, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Fecha Inicio";
            // 
            // TabFecha2
            // 
            this.TabFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TabFecha2.Location = new System.Drawing.Point(112, 185);
            this.TabFecha2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabFecha2.Name = "TabFecha2";
            this.TabFecha2.Size = new System.Drawing.Size(123, 27);
            this.TabFecha2.TabIndex = 8;
            this.TabFecha2.ValueChanged += new System.EventHandler(this.TabFecha2_ValueChanged);
            // 
            // TabFecha1
            // 
            this.TabFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TabFecha1.Location = new System.Drawing.Point(112, 133);
            this.TabFecha1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabFecha1.Name = "TabFecha1";
            this.TabFecha1.Size = new System.Drawing.Size(123, 27);
            this.TabFecha1.TabIndex = 15;
            this.TabFecha1.ValueChanged += new System.EventHandler(this.TabFecha1_ValueChanged);
            // 
            // ConsultaDatos
            // 
            this.ConsultaDatos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.ConsultaDatos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ConsultaDatos.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ConsultaDatos.Location = new System.Drawing.Point(31, 411);
            this.ConsultaDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ConsultaDatos.Name = "ConsultaDatos";
            this.ConsultaDatos.Size = new System.Drawing.Size(86, 40);
            this.ConsultaDatos.TabIndex = 16;
            this.ConsultaDatos.Text = "Consulta";
            this.ConsultaDatos.UseVisualStyleBackColor = false;
            this.ConsultaDatos.Click += new System.EventHandler(this.ConsultaDatos_Click);
            // 
            // DatosBD
            // 
            this.DatosBD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DatosBD.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.DatosBD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatosBD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatosBD.Location = new System.Drawing.Point(0, 0);
            this.DatosBD.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DatosBD.Name = "DatosBD";
            this.DatosBD.ReadOnly = true;
            this.DatosBD.RowHeadersWidth = 51;
            this.DatosBD.RowTemplate.Height = 25;
            this.DatosBD.Size = new System.Drawing.Size(1241, 784);
            this.DatosBD.TabIndex = 13;
            this.DatosBD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatosBD_CellContentClick);
            this.DatosBD.CurrentCellDirtyStateChanged += new System.EventHandler(this.DatosBD_CurrentCellDirtyStateChanged);
            // 
            // chkPrinted
            // 
            this.chkPrinted.AutoSize = true;
            this.chkPrinted.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.chkPrinted.Location = new System.Drawing.Point(25, 88);
            this.chkPrinted.Name = "chkPrinted";
            this.chkPrinted.Size = new System.Drawing.Size(199, 24);
            this.chkPrinted.TabIndex = 21;
            this.chkPrinted.Text = "Incluir etiquetas impresas";
            this.chkPrinted.UseVisualStyleBackColor = true;
            this.chkPrinted.CheckedChanged += new System.EventHandler(this.chkPrinted_CheckedChanged);
            // 
            // TxtBoxOrder
            // 
            this.TxtBoxOrder.Enabled = false;
            this.TxtBoxOrder.Location = new System.Drawing.Point(50, 268);
            this.TxtBoxOrder.Name = "TxtBoxOrder";
            this.TxtBoxOrder.Size = new System.Drawing.Size(157, 27);
            this.TxtBoxOrder.TabIndex = 20;
            this.TxtBoxOrder.TextChanged += new System.EventHandler(this.TxtBoxOrder_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(73, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "Orden de trabajo";
            // 
            // chkBoxOrder
            // 
            this.chkBoxOrder.AutoSize = true;
            this.chkBoxOrder.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.chkBoxOrder.Location = new System.Drawing.Point(25, 31);
            this.chkBoxOrder.Name = "chkBoxOrder";
            this.chkBoxOrder.Size = new System.Drawing.Size(210, 24);
            this.chkBoxOrder.TabIndex = 18;
            this.chkBoxOrder.Text = "Filtro por Orden de trabajo";
            this.chkBoxOrder.UseVisualStyleBackColor = true;
            this.chkBoxOrder.CheckedChanged += new System.EventHandler(this.chkBoxOrder_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 697);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(193, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1486, 51);
            this.panel2.TabIndex = 18;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(302, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "-/-/-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(248, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 20);
            this.label6.TabIndex = 23;
            this.label6.Text = "Fecha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(16, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "Impresión de Etiquetas";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1443, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(43, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.Counterlblsts,
            this.toolStripStatusLabel2,
            this.stsOperation,
            this.stsBProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 835);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1486, 32);
            this.statusStrip1.TabIndex = 20;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(156, 26);
            this.toolStripStatusLabel1.Text = "Líneas seleccionadas:";
            // 
            // Counterlblsts
            // 
            this.Counterlblsts.AutoSize = false;
            this.Counterlblsts.Name = "Counterlblsts";
            this.Counterlblsts.Size = new System.Drawing.Size(40, 26);
            this.Counterlblsts.Text = "0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(10, 4, 0, 2);
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(197, 26);
            this.toolStripStatusLabel2.Text = "Estado de lectura de datos:";
            this.toolStripStatusLabel2.Visible = false;
            // 
            // stsOperation
            // 
            this.stsOperation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.stsOperation.Name = "stsOperation";
            this.stsOperation.Size = new System.Drawing.Size(66, 26);
            this.stsOperation.Text = "Consulta";
            this.stsOperation.Visible = false;
            // 
            // stsBProgressBar
            // 
            this.stsBProgressBar.AutoSize = false;
            this.stsBProgressBar.Name = "stsBProgressBar";
            this.stsBProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.stsBProgressBar.Size = new System.Drawing.Size(101, 24);
            this.stsBProgressBar.Visible = false;
            // 
            // BGWork
            // 
            this.BGWork.WorkerReportsProgress = true;
            this.BGWork.WorkerSupportsCancellation = true;
            this.BGWork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGWork_DoWork);
            this.BGWork.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BGWork_ProgressChanged);
            this.BGWork.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGWork_RunWorkerCompleted);
            // 
            // WorkPrint
            // 
            this.WorkPrint.WorkerReportsProgress = true;
            this.WorkPrint.WorkerSupportsCancellation = true;
            this.WorkPrint.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkPrint_DoWork);
            this.WorkPrint.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.WorkPrint_ProgressChanged);
            this.WorkPrint.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkPrint_RunWorkerCompleted);
            // 
            // Message
            // 
            this.Message.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.Message.Text = "notifyIcon1";
            this.Message.Visible = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.TxtBoxSKU);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.chkSKU);
            this.panel1.Controls.Add(this.ImprimirDatos);
            this.panel1.Controls.Add(this.chkPrinted);
            this.panel1.Controls.Add(this.ConsultaDatos);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TxtBoxOrder);
            this.panel1.Controls.Add(this.TabFecha1);
            this.panel1.Controls.Add(this.TabFecha2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.chkBoxOrder);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 784);
            this.panel1.TabIndex = 22;
            // 
            // TxtBoxSKU
            // 
            this.TxtBoxSKU.Enabled = false;
            this.TxtBoxSKU.Location = new System.Drawing.Point(50, 345);
            this.TxtBoxSKU.Name = "TxtBoxSKU";
            this.TxtBoxSKU.Size = new System.Drawing.Size(157, 27);
            this.TxtBoxSKU.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(111, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 20);
            this.label5.TabIndex = 23;
            this.label5.Text = "SKU";
            // 
            // chkSKU
            // 
            this.chkSKU.AutoSize = true;
            this.chkSKU.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.chkSKU.Location = new System.Drawing.Point(25, 57);
            this.chkSKU.Name = "chkSKU";
            this.chkSKU.Size = new System.Drawing.Size(123, 24);
            this.chkSKU.TabIndex = 22;
            this.chkSKU.Text = "Filtro por SKU";
            this.chkSKU.UseVisualStyleBackColor = true;
            this.chkSKU.CheckedChanged += new System.EventHandler(this.chkSKU_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DatosBD);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(245, 51);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1241, 784);
            this.panel3.TabIndex = 23;
            // 
            // SeriesPrintingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1486, 867);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SeriesPrintingFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Imprimir Etiquetas";
            this.Load += new System.EventHandler(this.SeriesPrintingFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DatosBD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ImprimirDatos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker TabFecha2;
        private System.Windows.Forms.DateTimePicker TabFecha1;
        private System.Windows.Forms.Button ConsultaDatos;
        private System.Windows.Forms.DataGridView DatosBD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel Counterlblsts;
        private System.Windows.Forms.ToolStripProgressBar stsBProgressBar;
        private System.ComponentModel.BackgroundWorker BGWork;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.ComponentModel.BackgroundWorker WorkPrint;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel stsOperation;
        private System.Windows.Forms.TextBox TxtBoxOrder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkBoxOrder;
        private System.Windows.Forms.NotifyIcon Message;
        private System.Windows.Forms.CheckBox chkPrinted;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox TxtBoxSKU;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkSKU;
    }
}