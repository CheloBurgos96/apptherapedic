﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace NetSuiteApps
{
    public partial class Com_LogFrm : Form
    {
        SystemLogs cLog = new SystemLogs();
        private string NameScreen = "Com Logs";
        public EmployeeModel Employ;
        DataBase DB = new DataBase(false);

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        public void Empl(EmployeeModel n)
        {
            Employ = n;
            QueryNS TakeErrors = new QueryNS();
            cLog.Add("", NameScreen, "Toma de datos de de los errores en NS", false);
            TakeErrors.TakeErrorsNS(Employ);
        }


        public Com_LogFrm()
        {
            InitializeComponent();
            this.CenterToScreen();
            String[] result = DateTime.Now.Date.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            label7.Text = result[0];
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Click para salir al menú principal de la aplicación", false);
            this.Close();
            MainFrm AccF = new MainFrm();
            AccF.SetEmployeeInfo(Employ);
            AccF.Show();
        }

        private void ChkBDate_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        

        private void chkTypeError_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkTypeError.Checked)
            {
                ChkUser.Enabled = true;
                ChkUser.ForeColor = Color.White;
                ChkBDate.Enabled = true;
                ChkBDate.ForeColor = Color.White;
            }
            else
            {
                ChkUser.Enabled = false;
                ChkUser.ForeColor = Color.White;
                ChkBDate.Enabled = false;
                ChkBDate.ForeColor = Color.White;
            }
        }

        private void FindError_Click_1(object sender, EventArgs e)
        {
            cLog.Add("", NameScreen, "Actualizar el DGV con los errores", false);
            string[] result = WorkDate.Value.Date.ToShortDateString().Split(' ');
            int TypeQuery = 3;
            //string query = "";
            if (chkTypeError.Checked != true)
            {
                if (ChkBDate.Checked == true && ChkUser.Checked == false)
                {
                    TypeQuery = 3;
                    //query = "SELECT * FROM com_logs WHERE date_reg = '" + result[0] + "';";
                }
                else if (ChkBDate.Checked == false && ChkUser.Checked == true)
                {
                    TypeQuery = 4;
                    //query = "SELECT * FROM com_logs WHERE id_user = " + UserID.Text + ";";
                }
                else if (ChkBDate.Checked == true && ChkUser.Checked == true)
                {
                    TypeQuery = 5;
                    //query = "SELECT * FROM com_logs WHERE id_user = " + UserID.Text + " AND date_reg = '" + result[0] + "';";
                }
                else if (ChkBDate.Checked == false && ChkUser.Checked == false)
                {
                    TypeQuery = 6;
                    //query = "SELECT * FROM com_logs";
                }
            }
            else
            {
                TypeQuery = 7;
                //query = "SELECT recordType, name_user, custrecord_trp_numero_serie, custrecord_trp_tipo_movimiento FROM com_errorsNS_logs";
            }
            //bool Find = true;


            /*Descomentar si se quiere cambiar motor de base de datos del programa
             y cambiarlo por el motor que desee, SQLite o SQL Server*/
            //DataBase InsertInDGV = new DataBase();
            //SQLServerDataBase InsertInDGV = new SQLServerDataBase();
            DataTable dt = DB.DataTableBD(TypeQuery, result[0], "", "", UserID.Text, "");
            if (dt.Rows.Count > 0)
            {
                this.DGDB.DataSource = dt;
                this.DGDB.RowHeadersVisible = false;
                this.DGDB.AllowUserToAddRows = false;
                if (chkTypeError.Checked != true)
                {
                    DGDB.Columns[0].HeaderText = "Usuario";
                    DGDB.Columns[1].HeaderText = "Fecha";
                    DGDB.Columns[2].HeaderText = "Error";
                    DGDB.Columns[3].HeaderText = "Lugar";
                }
                else
                {
                    DGDB.Columns[0].HeaderText = "Tipo de error";
                    DGDB.Columns[1].HeaderText = "Usuario";
                    DGDB.Columns[2].HeaderText = "Numero de serie";
                    DGDB.Columns[3].HeaderText = "Movimiento";
                }

                DGDB.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                //this.DGDB.RowPostPaint += new DataGridViewRowPostPaintEventHandler(this.dgvUserDetails_RowPostPaint);
            }
            else
            {
                cLog.Add("", NameScreen, "No se encontró registro de errores de NS", false);
                MessageBox.Show(new Form() { TopMost = true },"No se encontraron registros");
            }
        }
    }
}
