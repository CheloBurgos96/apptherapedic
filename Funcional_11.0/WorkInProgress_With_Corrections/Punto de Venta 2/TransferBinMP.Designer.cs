﻿
namespace NetSuiteApps
{
    partial class TransferBinMP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferBinMP));
            this.panel1 = new System.Windows.Forms.Panel();
            this.PWait = new System.Windows.Forms.PictureBox();
            this.Counter = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SendData = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbBinDest = new System.Windows.Forms.ComboBox();
            this.txtBDes = new System.Windows.Forms.TextBox();
            this.lblSerieScanned = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CodeBar = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.PictureBox();
            this.BGWork = new System.ComponentModel.BackgroundWorker();
            this.TUpdateL = new System.Windows.Forms.Timer(this.components);
            this.BGSend = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exit)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel1.Controls.Add(this.PWait);
            this.panel1.Controls.Add(this.Counter);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.SendData);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbBinDest);
            this.panel1.Controls.Add(this.txtBDes);
            this.panel1.Controls.Add(this.lblSerieScanned);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CodeBar);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 320);
            this.panel1.TabIndex = 17;
            // 
            // PWait
            // 
            this.PWait.Image = global::NetSuiteApps.Properties.Resources.cargando;
            this.PWait.Location = new System.Drawing.Point(82, 233);
            this.PWait.Name = "PWait";
            this.PWait.Size = new System.Drawing.Size(68, 71);
            this.PWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PWait.TabIndex = 33;
            this.PWait.TabStop = false;
            this.PWait.Visible = false;
            // 
            // Counter
            // 
            this.Counter.AutoSize = true;
            this.Counter.BackColor = System.Drawing.Color.Transparent;
            this.Counter.ForeColor = System.Drawing.Color.White;
            this.Counter.Location = new System.Drawing.Point(86, 156);
            this.Counter.Name = "Counter";
            this.Counter.Size = new System.Drawing.Size(13, 15);
            this.Counter.TabIndex = 32;
            this.Counter.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(10, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 15);
            this.label7.TabIndex = 31;
            this.label7.Text = "Escaneadas:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(11, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 15);
            this.label6.TabIndex = 28;
            this.label6.Text = "Etiqueta escaneada:";
            // 
            // SendData
            // 
            this.SendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.SendData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SendData.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SendData.Location = new System.Drawing.Point(79, 194);
            this.SendData.Name = "SendData";
            this.SendData.Size = new System.Drawing.Size(75, 30);
            this.SendData.TabIndex = 27;
            this.SendData.Text = "Enviar";
            this.SendData.UseVisualStyleBackColor = false;
            this.SendData.Click += new System.EventHandler(this.SendData_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(10, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 15);
            this.label4.TabIndex = 25;
            this.label4.Text = "Bin destino";
            // 
            // cbBinDest
            // 
            this.cbBinDest.DropDownHeight = 110;
            this.cbBinDest.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbBinDest.FormattingEnabled = true;
            this.cbBinDest.IntegralHeight = false;
            this.cbBinDest.Items.AddRange(new object[] {
            "TH-PP",
            "GENERAL-PP-BASES",
            "GENERAL-PP-BCOS",
            "GENERAL-MPBCOS",
            "GENERAL-MP-BASES",
            "TH-MQ-GENERAL",
            "GENERAL-PP-BCOS",
            "BCOS-MQ-GENERAL",
            "GENERAL-MPTH",
            "GENERAL-PT-BCOS",
            "GENERAL-PP-BASES"});
            this.cbBinDest.Location = new System.Drawing.Point(77, 57);
            this.cbBinDest.Name = "cbBinDest";
            this.cbBinDest.Size = new System.Drawing.Size(150, 23);
            this.cbBinDest.TabIndex = 34;
            this.cbBinDest.SelectedIndexChanged += new System.EventHandler(this.cbBinDest_SelectedIndexChanged);
            this.cbBinDest.Click += new System.EventHandler(this.cbBinDest_Click);
            this.cbBinDest.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbBinDest_KeyUp);
            // 
            // txtBDes
            // 
            this.txtBDes.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtBDes.Enabled = false;
            this.txtBDes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBDes.ForeColor = System.Drawing.SystemColors.Window;
            this.txtBDes.Location = new System.Drawing.Point(12, 229);
            this.txtBDes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBDes.Multiline = true;
            this.txtBDes.Name = "txtBDes";
            this.txtBDes.Size = new System.Drawing.Size(215, 80);
            this.txtBDes.TabIndex = 21;
            // 
            // lblSerieScanned
            // 
            this.lblSerieScanned.AutoSize = true;
            this.lblSerieScanned.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSerieScanned.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblSerieScanned.Location = new System.Drawing.Point(128, 136);
            this.lblSerieScanned.Name = "lblSerieScanned";
            this.lblSerieScanned.Size = new System.Drawing.Size(12, 15);
            this.lblSerieScanned.TabIndex = 20;
            this.lblSerieScanned.Text = "-";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel4.Location = new System.Drawing.Point(9, 130);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(215, 1);
            this.panel4.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(9, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ingresar código:";
            // 
            // CodeBar
            // 
            this.CodeBar.BackColor = System.Drawing.Color.White;
            this.CodeBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeBar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CodeBar.ForeColor = System.Drawing.Color.Black;
            this.CodeBar.Location = new System.Drawing.Point(10, 109);
            this.CodeBar.Name = "CodeBar";
            this.CodeBar.Size = new System.Drawing.Size(215, 16);
            this.CodeBar.TabIndex = 24;
            this.CodeBar.Text = "123";
            this.CodeBar.Click += new System.EventHandler(this.CodeBar_Click);
            this.CodeBar.TextChanged += new System.EventHandler(this.CodeBar_TextChanged);
            this.CodeBar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CodeBar_KeyPress);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.Exit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 35);
            this.panel2.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(46, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Transferencia entre Bines MP";
            // 
            // Exit
            // 
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.Dock = System.Windows.Forms.DockStyle.Left;
            this.Exit.Image = ((System.Drawing.Image)(resources.GetObject("Exit.Image")));
            this.Exit.Location = new System.Drawing.Point(0, 0);
            this.Exit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(35, 35);
            this.Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Exit.TabIndex = 17;
            this.Exit.TabStop = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // BGSend
            // 
            this.BGSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGSend_DoWork);
            this.BGSend.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGSend_RunWorkerCompleted);
            // 
            // TransferBinMP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TransferBinMP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TransferBinMP";
            this.Load += new System.EventHandler(this.TransferBinMP_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PWait)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox PWait;
        private System.Windows.Forms.Label Counter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button SendData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbBinDest;
        private System.Windows.Forms.TextBox txtBDes;
        private System.Windows.Forms.Label lblSerieScanned;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CodeBar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.PictureBox Exit;
        private System.ComponentModel.BackgroundWorker BGWork;
        private System.Windows.Forms.Timer TUpdateL;
        private System.ComponentModel.BackgroundWorker BGSend;
    }
}