﻿using System;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class MessageBoxSupport : Form
    {
        public void Empl(string msgShow)
        {
            msgImp.Text = msgShow;
        }
        public MessageBoxSupport()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void msgImp_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
