﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetSuiteApps
{
    public partial class ReturnMPFrm : Form
    {
        delegate void InsertData();
        QueryNS GetQueryFromNS = new QueryNS();
        SerialsMP VS = new SerialsMP();
        SystemLogs cLog = new SystemLogs();
        EmployeeModel Employ;
        List<string> Message = new List<string>();
        string NameScreen = "Return MP";
        double quantity = 0;
        string Serial = "";

        public void Empl(EmployeeModel n)
        {
            Employ = n;
            CodeBar.Focus();
        }

        public ReturnMPFrm()
        {
            InitializeComponent();
        }

        

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (Serial.Length > 0)
            {
                MessageBoxSupport Alert = new MessageBoxSupport();
                string strMensaje = string.Format("No ha enviado el serial{0}aún ¿Desea enviarlo?", Environment.NewLine);
                Alert.Empl(strMensaje);
                var result = Alert.ShowDialog();
                if (result == DialogResult.No)
                {
                    cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                    this.Close();
                    MainFrm AccF = new MainFrm();
                    AccF.SetEmployeeInfo(Employ);
                    AccF.Show();
                }
                cLog.Add("", NameScreen, "Usuario permaneció en la pantalla", false);
            }
            else
            {
                cLog.Add("", NameScreen, "Click para regresar al menú principal de la aplicación", false);
                this.Close();
                MainFrm AccF = new MainFrm();
                AccF.SetEmployeeInfo(Employ);
                AccF.Show();
            }
        }

        private void ChangeColor()
        {
            Thread.Sleep(2000);
            InsertData n = new InsertData(ControlPanel);
            this.Invoke(n);
        }

        private void ControlPanel()
        {
            panel1.BackColor = Color.Black;
        }

        private void ActiveWait(bool Active)
        {
            SendData.Enabled = Active;
            CodeBar.Enabled = Active;
            pictureBox2.Enabled = Active;
            PWait.Visible = !Active;
        }

        private void WQ()
        {
            try
            {
                string Body = Serial + ", \"devolution\": true";
                var ValidateSerial = GetQueryFromNS.QueryUbications(Body, Employ, 9, "Transfer MP, Line 471");
                VS = ValidateSerial.Item3;
            }
            catch (Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 152", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
        }

        private void SendData_Click(object sender, EventArgs e)
        {
            ActiveWait(false);
            if (Serial.Length != 0)
            {
                if (BGSend.IsBusy != true)
                {
                    BGSend.RunWorkerAsync();
                }
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "No hay serial a enviar.");
            }
        }

        private async void CodeBar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (CodeBar.Text != "" && CodeBar.Text.Length >= 10)
                {
                    Serial = CodeBar.Text;
                    CodeBar.Clear();
                    ActiveWait(false);
                    Task oTask = new Task(WQ);
                    oTask.Start();
                    await oTask;
                    ActiveWait(true);
                    if (VS.IsSuccess)
                    {
                        cLog.Add("", NameScreen, "Serie encontrada", false);
                        ThreadStart delegado = new ThreadStart(ChangeColor);
                        var t = new Thread(delegado);
                        t.Start();
                        panel1.BackColor = Color.Green;
                        //MessageBox.Show("Serie encontrada");
                        using (QuantityMP ShowQ = new QuantityMP())
                        {
                            var result = ShowQ.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                quantity = ShowQ.CurrentQuantity;
                            }
                        }
                        lblSerieScanned.Text = Serial;
                        qty.Text = quantity.ToString();
                    }
                    else
                    {
                        panel1.BackColor = Color.Red;
                        MessageBox.Show("Serie no encontrada");
                        panel1.BackColor = Color.Black;
                    }
                }
            }
            catch (Exception ex)
            {
                panel1.BackColor = Color.Red;
                cLog.Add("Line 165", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
                panel1.BackColor = Color.Black;
                CodeBar.Clear();
                CodeBar.Focus();
            }
            
        }

        private void BGSend_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (Serial.Length > 0)
                {
                    if (quantity == 0)
                    {
                        using (QuantityMP ShowQ = new QuantityMP())
                        {
                            var result = ShowQ.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                quantity = ShowQ.CurrentQuantity;
                            }
                        }
                    }
                    string SeriesNotSend = "\"qty\": " + quantity.ToString() + ", \"serial\":" + Serial;
                    var Query = GetQueryFromNS.SendDataTransfer("1", SeriesNotSend, 0, 0, Employ, 9, "Transfer MP, Line 471");
                    if (Query.Item1)
                    {
                        cLog.Add("", NameScreen, "Envio de serie con cantidad cero, realizado", false);
                        Serial = "";
                    }
                    Message.Clear();
                    Message = Query.Item2;
                    quantity = 0;
                }
            }catch (Exception ex)
            {
                cLog.Add("Line 201", NameScreen, ex.Message, true);
                MessageBox.Show(new Form() { TopMost = true }, "Un error ha ocurrido.");
            }
            
        }

        private void BGSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            for (int i = 0; i < Message.Count; i++)
            {
                if (Message[i].Length > 0)
                {
                    MessageBox.Show(new Form() { TopMost = true }, Message[i]);
                }
            }
            Message.Clear();
            ActiveWait(true);
            CodeBar.Focus();
            if (Serial.Length == 0)
            {
                lblSerieScanned.Text = "-";
            }
            qty.Text = "-";
        }

        private void CodeBar_Click(object sender, EventArgs e)
        {
            CodeBar.Clear();
        }
    }
}
