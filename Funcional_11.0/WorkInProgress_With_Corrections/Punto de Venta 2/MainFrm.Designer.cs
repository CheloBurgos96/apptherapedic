﻿
using System.Drawing;

namespace NetSuiteApps
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.P13 = new System.Windows.Forms.Panel();
            this.P12 = new System.Windows.Forms.Panel();
            this.ReturnMP = new System.Windows.Forms.Button();
            this.P14 = new System.Windows.Forms.Panel();
            this.P11 = new System.Windows.Forms.Panel();
            this.btnPIMP = new System.Windows.Forms.Button();
            this.btnPIPT = new System.Windows.Forms.Button();
            this.TBINMP = new System.Windows.Forms.Button();
            this.P8 = new System.Windows.Forms.Panel();
            this.BTNValidateJaula = new System.Windows.Forms.Button();
            this.P9 = new System.Windows.Forms.Panel();
            this.P10 = new System.Windows.Forms.Panel();
            this.BTN_Log = new System.Windows.Forms.Button();
            this.P1 = new System.Windows.Forms.Panel();
            this.P2 = new System.Windows.Forms.Panel();
            this.P3 = new System.Windows.Forms.Panel();
            this.P4 = new System.Windows.Forms.Panel();
            this.P7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ImprCod = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblRol = new System.Windows.Forms.Label();
            this.BTNJaula = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMP = new System.Windows.Forms.Button();
            this.lblEmpl = new System.Windows.Forms.Label();
            this.EscaCod = new System.Windows.Forms.Button();
            this.lblIDEmp = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.P6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.EscEmb = new System.Windows.Forms.Button();
            this.FinalBase = new System.Windows.Forms.Panel();
            this.btnBin = new System.Windows.Forms.Button();
            this.P5 = new System.Windows.Forms.Panel();
            this.btnPT = new System.Windows.Forms.Button();
            this.BTNCom_log = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.BGWait = new System.ComponentModel.BackgroundWorker();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1300, 40);
            this.panel2.TabIndex = 14;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(293, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 24;
            this.label7.Text = "-/-/-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(240, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "Fecha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Menú Principal";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1260, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 40);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.P13);
            this.panel1.Controls.Add(this.P12);
            this.panel1.Controls.Add(this.ReturnMP);
            this.panel1.Controls.Add(this.P14);
            this.panel1.Controls.Add(this.P11);
            this.panel1.Controls.Add(this.btnPIMP);
            this.panel1.Controls.Add(this.btnPIPT);
            this.panel1.Controls.Add(this.TBINMP);
            this.panel1.Controls.Add(this.P8);
            this.panel1.Controls.Add(this.BTNValidateJaula);
            this.panel1.Controls.Add(this.P9);
            this.panel1.Controls.Add(this.P10);
            this.panel1.Controls.Add(this.BTN_Log);
            this.panel1.Controls.Add(this.P1);
            this.panel1.Controls.Add(this.P2);
            this.panel1.Controls.Add(this.P3);
            this.panel1.Controls.Add(this.P4);
            this.panel1.Controls.Add(this.P7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.ImprCod);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.lblRol);
            this.panel1.Controls.Add(this.BTNJaula);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnMP);
            this.panel1.Controls.Add(this.lblEmpl);
            this.panel1.Controls.Add(this.EscaCod);
            this.panel1.Controls.Add(this.lblIDEmp);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.P6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.EscEmb);
            this.panel1.Controls.Add(this.FinalBase);
            this.panel1.Controls.Add(this.btnBin);
            this.panel1.Controls.Add(this.P5);
            this.panel1.Controls.Add(this.btnPT);
            this.panel1.Controls.Add(this.BTNCom_log);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 610);
            this.panel1.TabIndex = 21;
            // 
            // P13
            // 
            this.P13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P13.Location = new System.Drawing.Point(22, 447);
            this.P13.Name = "P13";
            this.P13.Size = new System.Drawing.Size(10, 30);
            this.P13.TabIndex = 55;
            // 
            // P12
            // 
            this.P12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P12.Location = new System.Drawing.Point(22, 412);
            this.P12.Name = "P12";
            this.P12.Size = new System.Drawing.Size(10, 30);
            this.P12.TabIndex = 51;
            // 
            // ReturnMP
            // 
            this.ReturnMP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReturnMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReturnMP.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ReturnMP.Location = new System.Drawing.Point(30, 412);
            this.ReturnMP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ReturnMP.Name = "ReturnMP";
            this.ReturnMP.Size = new System.Drawing.Size(181, 30);
            this.ReturnMP.TabIndex = 50;
            this.ReturnMP.Text = "Devolución MP";
            this.ReturnMP.UseVisualStyleBackColor = true;
            this.ReturnMP.Click += new System.EventHandler(this.ReturnMP_Click);
            // 
            // P14
            // 
            this.P14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P14.Location = new System.Drawing.Point(22, 482);
            this.P14.Name = "P14";
            this.P14.Size = new System.Drawing.Size(10, 30);
            this.P14.TabIndex = 53;
            // 
            // P11
            // 
            this.P11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P11.Location = new System.Drawing.Point(22, 377);
            this.P11.Name = "P11";
            this.P11.Size = new System.Drawing.Size(10, 30);
            this.P11.TabIndex = 49;
            // 
            // btnPIMP
            // 
            this.btnPIMP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPIMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPIMP.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPIMP.Location = new System.Drawing.Point(30, 447);
            this.btnPIMP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPIMP.Name = "btnPIMP";
            this.btnPIMP.Size = new System.Drawing.Size(181, 30);
            this.btnPIMP.TabIndex = 54;
            this.btnPIMP.Text = "Inventario físico MP";
            this.btnPIMP.UseVisualStyleBackColor = true;
            this.btnPIMP.Click += new System.EventHandler(this.btnPIMP_Click);
            // 
            // btnPIPT
            // 
            this.btnPIPT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPIPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPIPT.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPIPT.Location = new System.Drawing.Point(30, 482);
            this.btnPIPT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPIPT.Name = "btnPIPT";
            this.btnPIPT.Size = new System.Drawing.Size(181, 30);
            this.btnPIPT.TabIndex = 52;
            this.btnPIPT.Text = "Inventario físico PT";
            this.btnPIPT.UseVisualStyleBackColor = true;
            this.btnPIPT.Click += new System.EventHandler(this.btnPIPT_Click);
            // 
            // TBINMP
            // 
            this.TBINMP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TBINMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TBINMP.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.TBINMP.Location = new System.Drawing.Point(30, 377);
            this.TBINMP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TBINMP.Name = "TBINMP";
            this.TBINMP.Size = new System.Drawing.Size(181, 30);
            this.TBINMP.TabIndex = 48;
            this.TBINMP.Text = "Transferencia entre Bines MP";
            this.TBINMP.UseVisualStyleBackColor = true;
            this.TBINMP.Click += new System.EventHandler(this.TBINMP_Click);
            // 
            // P8
            // 
            this.P8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P8.Location = new System.Drawing.Point(22, 342);
            this.P8.Name = "P8";
            this.P8.Size = new System.Drawing.Size(10, 30);
            this.P8.TabIndex = 34;
            // 
            // BTNValidateJaula
            // 
            this.BTNValidateJaula.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNValidateJaula.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNValidateJaula.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.BTNValidateJaula.Location = new System.Drawing.Point(30, 342);
            this.BTNValidateJaula.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BTNValidateJaula.Name = "BTNValidateJaula";
            this.BTNValidateJaula.Size = new System.Drawing.Size(181, 30);
            this.BTNValidateJaula.TabIndex = 47;
            this.BTNValidateJaula.Text = "Validación de Jaula";
            this.BTNValidateJaula.UseVisualStyleBackColor = true;
            this.BTNValidateJaula.Click += new System.EventHandler(this.BTNValidateJaula_Click_1);
            // 
            // P9
            // 
            this.P9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P9.Location = new System.Drawing.Point(22, 517);
            this.P9.Name = "P9";
            this.P9.Size = new System.Drawing.Size(10, 30);
            this.P9.TabIndex = 46;
            // 
            // P10
            // 
            this.P10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P10.Location = new System.Drawing.Point(22, 552);
            this.P10.Name = "P10";
            this.P10.Size = new System.Drawing.Size(10, 30);
            this.P10.TabIndex = 44;
            // 
            // BTN_Log
            // 
            this.BTN_Log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTN_Log.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Log.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.BTN_Log.Location = new System.Drawing.Point(30, 517);
            this.BTN_Log.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BTN_Log.Name = "BTN_Log";
            this.BTN_Log.Size = new System.Drawing.Size(181, 30);
            this.BTN_Log.TabIndex = 45;
            this.BTN_Log.Text = "Errores de envío";
            this.BTN_Log.UseVisualStyleBackColor = true;
            this.BTN_Log.Click += new System.EventHandler(this.BTN_Log_Click);
            // 
            // P1
            // 
            this.P1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P1.Location = new System.Drawing.Point(22, 97);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(10, 30);
            this.P1.TabIndex = 33;
            // 
            // P2
            // 
            this.P2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P2.Location = new System.Drawing.Point(22, 132);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(10, 30);
            this.P2.TabIndex = 34;
            // 
            // P3
            // 
            this.P3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P3.Location = new System.Drawing.Point(22, 167);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(10, 30);
            this.P3.TabIndex = 34;
            // 
            // P4
            // 
            this.P4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P4.Location = new System.Drawing.Point(22, 202);
            this.P4.Name = "P4";
            this.P4.Size = new System.Drawing.Size(10, 30);
            this.P4.TabIndex = 36;
            // 
            // P7
            // 
            this.P7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P7.Location = new System.Drawing.Point(22, 307);
            this.P7.Name = "P7";
            this.P7.Size = new System.Drawing.Size(10, 30);
            this.P7.TabIndex = 42;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel6.Location = new System.Drawing.Point(17, 36);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(194, 1);
            this.panel6.TabIndex = 32;
            // 
            // ImprCod
            // 
            this.ImprCod.AutoEllipsis = true;
            this.ImprCod.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ImprCod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ImprCod.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ImprCod.Location = new System.Drawing.Point(30, 97);
            this.ImprCod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ImprCod.Name = "ImprCod";
            this.ImprCod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ImprCod.Size = new System.Drawing.Size(181, 30);
            this.ImprCod.TabIndex = 22;
            this.ImprCod.Text = "Imprimir Etiquetas";
            this.ImprCod.UseVisualStyleBackColor = true;
            this.ImprCod.Click += new System.EventHandler(this.ImprCod_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel5.Location = new System.Drawing.Point(17, 63);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(194, 1);
            this.panel5.TabIndex = 24;
            // 
            // lblRol
            // 
            this.lblRol.AutoSize = true;
            this.lblRol.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblRol.Location = new System.Drawing.Point(57, 67);
            this.lblRol.Name = "lblRol";
            this.lblRol.Size = new System.Drawing.Size(12, 15);
            this.lblRol.TabIndex = 30;
            this.lblRol.Text = "-";
            // 
            // BTNJaula
            // 
            this.BTNJaula.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNJaula.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNJaula.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.BTNJaula.Location = new System.Drawing.Point(30, 307);
            this.BTNJaula.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BTNJaula.Name = "BTNJaula";
            this.BTNJaula.Size = new System.Drawing.Size(181, 30);
            this.BTNJaula.TabIndex = 41;
            this.BTNJaula.Text = "Transferencia de Jaula";
            this.BTNJaula.UseVisualStyleBackColor = true;
            this.BTNJaula.Click += new System.EventHandler(this.BTNJaula_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label4.Location = new System.Drawing.Point(19, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 15);
            this.label4.TabIndex = 29;
            this.label4.Text = "Rol:";
            // 
            // btnMP
            // 
            this.btnMP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMP.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnMP.Location = new System.Drawing.Point(30, 202);
            this.btnMP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMP.Name = "btnMP";
            this.btnMP.Size = new System.Drawing.Size(181, 30);
            this.btnMP.TabIndex = 35;
            this.btnMP.Text = "Transferencia MP";
            this.btnMP.UseVisualStyleBackColor = true;
            this.btnMP.Click += new System.EventHandler(this.btnMP_Click);
            // 
            // lblEmpl
            // 
            this.lblEmpl.AutoSize = true;
            this.lblEmpl.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblEmpl.Location = new System.Drawing.Point(87, 40);
            this.lblEmpl.Name = "lblEmpl";
            this.lblEmpl.Size = new System.Drawing.Size(12, 15);
            this.lblEmpl.TabIndex = 28;
            this.lblEmpl.Text = "-";
            // 
            // EscaCod
            // 
            this.EscaCod.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EscaCod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EscaCod.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.EscaCod.Location = new System.Drawing.Point(30, 167);
            this.EscaCod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EscaCod.Name = "EscaCod";
            this.EscaCod.Size = new System.Drawing.Size(181, 30);
            this.EscaCod.TabIndex = 21;
            this.EscaCod.Text = "Registro Operaciones";
            this.EscaCod.UseVisualStyleBackColor = true;
            this.EscaCod.Click += new System.EventHandler(this.EscaCod_Click_1);
            // 
            // lblIDEmp
            // 
            this.lblIDEmp.AutoSize = true;
            this.lblIDEmp.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblIDEmp.Location = new System.Drawing.Point(115, 13);
            this.lblIDEmp.Name = "lblIDEmp";
            this.lblIDEmp.Size = new System.Drawing.Size(13, 15);
            this.lblIDEmp.TabIndex = 27;
            this.lblIDEmp.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(17, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = "Nombre:";
            // 
            // P6
            // 
            this.P6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P6.Location = new System.Drawing.Point(22, 272);
            this.P6.Name = "P6";
            this.P6.Size = new System.Drawing.Size(10, 30);
            this.P6.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(19, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 25;
            this.label1.Text = "Id Empleado:";
            // 
            // EscEmb
            // 
            this.EscEmb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EscEmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EscEmb.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.EscEmb.Location = new System.Drawing.Point(30, 132);
            this.EscEmb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EscEmb.Name = "EscEmb";
            this.EscEmb.Size = new System.Drawing.Size(181, 30);
            this.EscEmb.TabIndex = 20;
            this.EscEmb.Text = "Embolsado";
            this.EscEmb.UseVisualStyleBackColor = true;
            this.EscEmb.Click += new System.EventHandler(this.EscEmb_Click_1);
            // 
            // FinalBase
            // 
            this.FinalBase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.FinalBase.Location = new System.Drawing.Point(17, 89);
            this.FinalBase.Name = "FinalBase";
            this.FinalBase.Size = new System.Drawing.Size(194, 1);
            this.FinalBase.TabIndex = 23;
            // 
            // btnBin
            // 
            this.btnBin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBin.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBin.Location = new System.Drawing.Point(30, 272);
            this.btnBin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBin.Name = "btnBin";
            this.btnBin.Size = new System.Drawing.Size(181, 30);
            this.btnBin.TabIndex = 39;
            this.btnBin.Text = "Transferencia entre Bines PT";
            this.btnBin.UseVisualStyleBackColor = true;
            this.btnBin.Click += new System.EventHandler(this.btnBin_Click);
            // 
            // P5
            // 
            this.P5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.P5.Location = new System.Drawing.Point(22, 237);
            this.P5.Name = "P5";
            this.P5.Size = new System.Drawing.Size(10, 30);
            this.P5.TabIndex = 38;
            // 
            // btnPT
            // 
            this.btnPT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPT.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPT.Location = new System.Drawing.Point(30, 237);
            this.btnPT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPT.Name = "btnPT";
            this.btnPT.Size = new System.Drawing.Size(181, 30);
            this.btnPT.TabIndex = 37;
            this.btnPT.Text = "Transferencia PT";
            this.btnPT.UseVisualStyleBackColor = true;
            this.btnPT.Click += new System.EventHandler(this.btnPT_Click);
            // 
            // BTNCom_log
            // 
            this.BTNCom_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTNCom_log.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNCom_log.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.BTNCom_log.Location = new System.Drawing.Point(30, 552);
            this.BTNCom_log.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BTNCom_log.Name = "BTNCom_log";
            this.BTNCom_log.Size = new System.Drawing.Size(181, 30);
            this.BTNCom_log.TabIndex = 43;
            this.BTNCom_log.Text = "Registro Errores";
            this.BTNCom_log.UseVisualStyleBackColor = true;
            this.BTNCom_log.Click += new System.EventHandler(this.BTNCom_log_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(240, 628);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1060, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(240, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1060, 588);
            this.panel4.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 517);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1060, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(110)))), ((int)(((byte)(200)))));
            this.panel15.Location = new System.Drawing.Point(465, 280);
            this.panel15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(9, 30);
            this.panel15.TabIndex = 35;
            // 
            // button1
            // 
            this.button1.AutoEllipsis = true;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(472, 280);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(158, 30);
            this.button1.TabIndex = 34;
            this.button1.Text = "Imprimir Etiquetas";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // BGWait
            // 
            this.BGWait.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGWait_DoWork);
            this.BGWait.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGWait_RunWorkerCompleted);
            // 
            // MainFrm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainFrm";
            this.Text = "Menú Principal";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEmpl;
        private System.Windows.Forms.Label lblIDEmp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel FinalBase;
        private System.Windows.Forms.Button ImprCod;
        private System.Windows.Forms.Button EscEmb;
        private System.Windows.Forms.Button EscaCod;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel P8;
        private System.Windows.Forms.Panel P3;
        private System.Windows.Forms.Panel P2;
        private System.Windows.Forms.Panel P1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel P7;
        private System.Windows.Forms.Button BTNJaula;
        private System.Windows.Forms.Panel P6;
        private System.Windows.Forms.Button btnBin;
        private System.Windows.Forms.Panel P5;
        private System.Windows.Forms.Button btnPT;
        private System.Windows.Forms.Panel P4;
        private System.Windows.Forms.Button btnMP;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker BGWait;
        private System.Windows.Forms.Panel P9;
        private System.Windows.Forms.Panel P10;
        private System.Windows.Forms.Button BTN_Log;
        private System.Windows.Forms.Button BTNCom_log;
        private System.Windows.Forms.Button BTNValidateJaula;
        private System.Windows.Forms.Panel P11;
        private System.Windows.Forms.Button TBINMP;
        private System.Windows.Forms.Panel P12;
        private System.Windows.Forms.Button ReturnMP;
        private System.Windows.Forms.Panel P13;
        private System.Windows.Forms.Panel P14;
        private System.Windows.Forms.Button btnPIMP;
        private System.Windows.Forms.Button btnPIPT;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}